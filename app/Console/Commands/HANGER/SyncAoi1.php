<?php

namespace App\Console\Commands\HANGER;

use Illuminate\Console\Command;

use Carbon\Carbon;
use DB;

use App\Http\Controllers\Hanger\SyncHangerOutput as HI;
class SyncAoi1 extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'HANGER:SyncAoi1';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'HANGER:SyncAoi1';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
            // $getHanger = Scheduler::whereIn('job',['HANGER:SyncAoi1'])->where('status','ongoing')->exists();

            // if(!$getHanger)
            // {
                // $new_scheduler = Scheduler::create([
                //     'job'       => 'HANGER:SyncAoi1',
                //     'status'    => 'ongoing'
                // ]);
                // $this->setStartJob($new_scheduler);
                $this->info('Sync HANGER:SyncAoi1 JOB AT '.carbon::now());
                
                $from = carbon::now()->subdays(1)->format('Y-m-d 00:00:00');
                $to = carbon::now()->addDays(1)->format('Y-m-d 00:00:00');

                $getLine = DB::connection('line')
                            ->table('route_hanger_lines')
                            ->where('factory_id',1)
                            ->where('ihs_bridging','IHS_SHARED')
                            ->select('ip_address_server','ihs_server','ihs_bridging','line_id','line_hanger_id','ip_address_local','ihs_local')->inRandomOrder()->get();
                

                foreach ($getLine as $gL) {
                        
                        HI::getDataIna($gL,$from,$to);
                        HI::syncOutput($gL->line_id,$from,$to);
                }

                $this->info('Done HANGER:SyncAoi1 JOB AT '.carbon::now());
                // $this->setEndJob($new_scheduler); 
            // }

        } catch (Exception $e) {
            echo "error";
            $message = $e->getMessage();
        } finally {
            // if (isset($new_scheduler)) {
            //     $this->setEndJob($new_scheduler);
            // };
        }
    }
}
