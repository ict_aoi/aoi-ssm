<?php

namespace App\Http\Controllers\Marker;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use DB;
use Carbon\Carbon;
use DataTables;

use App\Models\GarmentId;
use App\Models\GarmentMove;
use App\Models\Locator;
use App\Models\StyleMark;

class MarkerController extends Controller
{
    public function index(){
    	return view('marker.index');
    }

    public function ajaxScan(Request $request){
    	$barcode = $request->bargar;
    	$status = $request->status;
        $rack = $request->rack;
    	$ip = $request->ip();
        
    	$check = $this->cek_barcode($barcode);

    	if (!$check) {
    		return response()->json("Barcode garment not found",422);
    	}

    	if ($status=="storage") {
            if ($check->current_location=='marker' && $check->borrow==true && $check->storage==false) {
                $locid = Locator::where('id',$rack)->whereNull('deleted_at')->first();
                $data = array(
                    'barcode_id'=>$check->barcode_id,
                    'docno'=>$check->documentno,
                    'season'=>$check->season,
                    'style'=>$check->style,
                    'article'=>$check->article,
                    'size'=>$check->size,
                    'status'=>$check->status,
                    'type'=>"storage",
                    'rack'=>$locid->name
                );

                $query = $this->barcodeGarment($barcode,"marker_storage",$ip,$rack);

                if (!$query) {
                    return response()->json("Update movement garment error ! ! !",422);
                }
            }else if ($check->current_location=='marker_storage' && $check->borrow==false && $check->storage==true) {
                $locid = Locator::where('id',$rack)->whereNull('deleted_at')->first();
                $data = array(
                    'barcode_id'=>$check->barcode_id,
                    'docno'=>$check->documentno,
                    'season'=>$check->season,
                    'style'=>$check->style,
                    'article'=>$check->article,
                    'size'=>$check->size,
                    'status'=>$check->status,
                    'type'=>"storage",
                    'rack'=>$locid->name
                );

                $query = $this->barcodeGarment($barcode,"move_storage",$ip,$rack);

                if (!$query) {
                    return response()->json("Update movement garment error ! ! !",422);
                }
            }else{
                return response()->json("Barcode last location ".$check->current_location."  ! ! !",422);
            }
        }else if ($status=="reverse") {
            if ($check->current_location=='marker_storage' && $check->borrow==false && $check->storage==true) {

                $data = array(
                    'barcode_id'=>$check->barcode_id,
                    'docno'=>$check->documentno,
                    'season'=>$check->season,
                    'style'=>$check->style,
                    'article'=>$check->article,
                    'size'=>$check->size,
                    'status'=>$check->status,
                    'type'=>"marker"
                );

                $query = $this->barcodeGarment($barcode,"marker",$ip,null);

                if (!$query) {
                    return response()->json("Update movement garment error ! ! !",422);
                }
            }else{
                return response()->json("Barcode last location ".$check->current_location."  ! ! !",422);
            }
        }

        return view('marker.ajax_list')->with('data',$data);

    }



    private function cek_barcode($barcode){
        $dt_bar = DB::table('barcode_garment')
                    ->join('master_mo','barcode_garment.mo_id','=','master_mo.id')
                    ->where('barcode_garment.barcode_id',$barcode)
                    ->where('barcode_garment.factory_id',Auth::user()->factory_id)
                    ->whereNull('barcode_garment.deleted_at')
                    ->whereNull('master_mo.deleted_at')
                    ->select('barcode_garment.barcode_id',
                                'barcode_garment.mo_id',
                                'barcode_garment.current_location',
                                'master_mo.documentno',
                                'master_mo.season',
                                'master_mo.style','master_mo.article',
                                'master_mo.size',
                                'master_mo.status',
                                'barcode_garment.checkin_md',
                                'barcode_garment.storage',
                                'barcode_garment.borrow')->first();

        return $dt_bar;
    }

    

    private function barcodeGarment($barcode_id,$location,$ipaddress,$idloc= null){
        #query update & movement
        $last = GarmentMove::where('barcode_id',$barcode_id)->whereNull('deleted_at')->where('is_canceled',false)->orderBy('created_at','asc')->first();

       
        

        try {
            DB::begintransaction();

            switch ($location) {
                case 'marker_storage':
                    
                    $update = array(
                        'current_location'=>$location,
                        'borrow'=>false,
                        'storage'=>true,
                        'updated_at'=>Carbon::now()
                    );

                    $move = array(
                        'barcode_id'=>$barcode_id,
                        'locator_from'=>$last->locator_to,
                        'locator_to'=>$location,
                        'locator_id'=>$idloc,
                        'description'=>"Checkin on marker storage",
                        'created_at'=>Carbon::now(),
                        'user_id'=>Auth::user()->id,
                        'ip_address'=>$ipaddress
                    );
                    break;

                case 'marker':
                    $borw = GarmentMove::where('barcode_id',$barcode_id)->where('locator_from','storage_md')->where('locator_to','marker')->orderBy('created_at','desc')->first();

                    $update = array(
                        'current_location'=>$location,
                        'borrow'=>true,
                        'storage'=>false,
                        'updated_at'=>Carbon::now()
                    );

                    $move = array(
                        'barcode_id'=>$barcode_id,
                        'locator_from'=>$last->locator_to,
                        'locator_to'=>$location,
                        'locator_id'=>$idloc,
                        'description'=>"Reverse to marker",
                        'created_at'=>Carbon::now(),
                        'user_id'=>Auth::user()->id,
                        'borrower'=>$borw->borrower,
                        'ip_address'=>$ipaddress
                    );
                    break;

                case 'move_storage':
                    $desc = "move rack storage";
                    $update = array(
                        'current_location'=>'marker_storage',
                        'borrow'=>false,
                        'storage'=>true,
                        'updated_at'=>Carbon::now()
                    );

                    $move = array(
                        'barcode_id'=>$barcode_id,
                        'locator_from'=>"marker",
                        'locator_to'=>"marker_storage",
                        'locator_id'=>$idloc,
                        'description'=>"move rack storage",
                        'created_at'=>Carbon::now(),
                        'user_id'=>Auth::user()->id,
                        'ip_address'=>$ipaddress
                    );

                    GarmentMove::where('id',$last->id)->update(['is_canceled'=>true]);
                    break;
                
     
            }
                

                

                GarmentId::where('barcode_id',$barcode_id)->update($update);
                GarmentMove::where('id',$last->id)->update(['deleted_at'=>Carbon::now()]);
                GarmentMove::firstorcreate($move);
            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            $message = $ex->getMessage();
            ErrorHandler::db($message);
        }

        return true;
    }

    public function getRack(Request $request){
        $barcode = $request->barcode;
        $loc = Locator::where('id',$barcode)->where('factory_id',Auth::user()->factory_id)->whereNull('deleted_at')->first();

        if ($loc!=null) {
            return response()->json($loc,200);
        }else{
             return response()->json("Location not Found ! ! !",422);
        }
        
    }

    //marker info style

    public function markerStyle(){
        return view('marker_style.index');
    }

    public function ajaxGetData(){
        $data = StyleMark::whereNull('deleted_at')->orderBy('created_at','desc')->orderBy('updated_at','desc');

        return DataTables::of($data)
                            ->editColumn('release',function($data){
                                return Carbon::parse($data->release)->format('d-m-Y');
                            })
                            ->addColumn('podd',function($data){
                                return Carbon::parse($data->podd)->format('d-m-Y');
                            })
                            ->addColumn('expired',function($data){
                                return Carbon::parse($data->podd)->addDays(91)->format('d-m-Y');
                            })
                            ->addColumn('action',function($data){
                                return view('_action',[
                                                'editmarker'=>array(
                                                        'id'=>$data->id,
                                                        'season'=>$data->season,
                                                        'style'=>$data->style,
                                                        'release'=>$data->release,
                                                        'podd'=>$data->podd,
                                                        'pic'=>$data->pic),
                                            ]);
                            })
                            ->setRowAttr([
                                'style' => function($data)
                                {
                                    $now = Carbon::now()->format('Y-m-d');
                                    $expired = Carbon::parse($data->podd)->addDays(91)->format('Y-m-d');
                                    

                                    if ($now>$expired) {
                                       return  'background-color: #ff3300;';
                                    }
                                    
                                },
                            ])
                            ->rawColumns(['release','podd','action'])->make(true);
    }

    public function ajaxUploadStyle(Request $request){
       $ip = $request->ip();
        
        $results = array();
        if($request->hasFile('file_upload')){
          $extension = \File::extension($request->file_upload->getClientOriginalName());

          if ($extension == "xlsx" || $extension == "xls") {
              $path = $request->file_upload->getRealPath();

              $datax = \Excel::selectSheetsByIndex(0)->load($path)->get();
                
          }
        }

        try {
            DB::begintransaction();
                foreach ($datax as $dt) {
                    
                    if (trim($dt->season)!="" && trim($dt->style)!="" && trim($dt->pattern_release)!=""  && trim($dt->pic)!="" && trim($dt->podd)!="") {

                        $season = trim($dt->season);
                        $style = trim($dt->style);
                        $marker_release =date_format(date_create(trim($dt->pattern_release)),'Y-m-d');
                        $podd =date_format(date_create(trim($dt->podd)),'Y-m-d');
                        $pic = trim($dt->pic);

                        
                        $cek = StyleMark::where('season',$season)->where('style',$style)->whereNull('deleted_at')->exists();
                       
                        if (!$cek) {
                            $in = array(
                                'season'=>strtoupper($season),
                               'style'=>strtoupper($style),
                               'release'=>$marker_release,
                               'podd'=>$podd,
                               'pic'=>strtoupper($pic),
                               'created_at'=>carbon::now(),
                               'user_id'=>auth::user()->id,
                               'ip_address'=>$ip,
                               'remark'=>"Create by id= ".auth::user()->id." nik= ".auth::user()->nik
                            );

                        //    dd($in);
                           StyleMark::firstorcreate($in);
                        }
                        
                    }
                   
                }

                
            
            DB::commit();
            $data_response = [
                            'status' => 200,
                            'output' => 'Upload Success . . .'
                          ];
        } catch (Exception $ex) {
            DB::rollback();
            $message = $ex->getMessage();
            ErrorHandler::db($message);
            $data_response = [
                            'status' => 422,
                            'output' => 'Upload Field . . .'.$message
                          ];
        }

        return response()->json(['data'=>$data_response]);
    }

    public function getTemplate(){
        try {
            $path = storage_path('template/template_patern.xlsx');
            return response()->download($path);
        } catch (Exception $e) {
            $e->getMessage();
        }
    }

    public function editMarkerStyle(Request $request){


        $id = $request->id;
        $season = strtoupper(trim($request->season));
        $style = strtoupper(trim($request->style));
        // $release = $request->release;
        $release =date_format(date_create(trim($request->release)),'Y-m-d');
        $podd =date_format(date_create(trim($request->podd)),'Y-m-d');
        $pic = strtoupper(trim($request->pic));

        try {
            db::begintransaction();

            $up = array(
                        'season'=>$season,
                        'style'=>$style,
                        'release'=>$release,
                        'podd'=>$podd,
                        'pic'=>$pic,
                        'updated_at'=>Carbon::now(),
                        'remark'=>"Last update by id= ".auth::user()->id." nik= ".auth::user()->nik
                    );
            StyleMark::where('id',$id)->update($up);
            $data_response = [
                            'status' => 200,
                            'output' => 'Edit Success . . .'
                          ];
            db::commit();
        } catch (Exception $ex) {
            DB::rollback();
            $message = $ex->getMessage();
            ErrorHandler::db($message);
            $data_response = [
                            'status' => 422,
                            'output' => 'Edit Field . . .'.$message
                          ];
        }
        return response()->json(['data'=>$data_response]);
    }


}
