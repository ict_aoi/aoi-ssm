<?php

namespace App\Http\Controllers\Shipping;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use DB;
use Carbon\Carbon;
use DataTables;

use App\Models\GarmentId;
use App\Models\GarmentMove;

class ScanshipController extends Controller
{
    private function cek_barcode($barcode){
        $dt_bar = DB::table('barcode_garment')
                    ->join('master_mo','barcode_garment.mo_id','=','master_mo.id')
                    ->where('barcode_garment.barcode_id',$barcode)
                    ->where('barcode_garment.factory_id',Auth::user()->factory_id)
                    ->whereNull('barcode_garment.deleted_at')
                    ->whereNull('master_mo.deleted_at')
                    ->select('barcode_garment.barcode_id',
                                'barcode_garment.mo_id',
                                'barcode_garment.current_location',
                                'master_mo.documentno',
                                'master_mo.season',
                                'master_mo.style','master_mo.article',
                                'master_mo.size',
                                'master_mo.status',
                                'barcode_garment.checkin_md',
                                'barcode_garment.storage',
                                'barcode_garment.borrow',
                                'barcode_garment.factory_id')->first();

        return $dt_bar;
    }

    private function _barcodeGarment($barcode,$pic,$loct,$ipaddress){
        $last = GarmentMove::where('barcode_id',$barcode)->whereNull('deleted_at')->where('is_canceled',false)->orderBy('created_at','asc')->first();

        try {
            DB::begintransaction();
                $update = array(
                    'current_location'=>$loct,
                    'updated_at'=>Carbon::now(),
                    'borrow'=>false,
                    'storage'=>false
                );

                $move = array(
                    'barcode_id'=>$barcode,
                    'locator_from'=>$last->locator_to,
                    'locator_to'=>$loct,
                    'description'=>"Direct shipping from md",
                    'created_at'=>Carbon::now(),
                    'user_id'=>Auth::user()->id,
                    'ip_address'=>$ipaddress,
                    'borrower'=>$pic
                );

                GarmentId::where('barcode_id',$barcode)->update($update);
                GarmentMove::where('id',$last->id)->update(['deleted_at'=>Carbon::now()]);
                GarmentMove::firstorcreate($move);
            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            $message = $ex->getMessage();
            ErrorHandler::db($message);
        }

        return true;
    }

    public function scanShip(){
        return view('shipping.index');
    }

    public function ajaxScan(Request $request){
        $barcode = trim($request->barcode);
        $nik = trim($request->nik);
        $ip = $request->ip();

        $cek = $this->cek_barcode($barcode);
        if (!$cek) {
            return response()->json("Barcode garment not found",422);
        }

        if (($cek->current_location=='storage_md' && $cek->storage==true && $cek->borrow==false) || ($cek->current_location=='qc_storage' && $cek->storage==true && $cek->borrow==false)) {
            $emp = DB::table('employee')->where('nik',$nik)->first();
            $data = array(
                        'barcode'=>$cek->barcode_id,
                        'docno'=>$cek->documentno,
                        'season'=>$cek->season,
                        'style'=>$cek->style,
                        'article'=>$cek->article,
                        'size'=>$cek->size,
                        'status'=>$cek->status,
                        'remark'=>"Shipping PIC ".$emp->name." (".$emp->nik.")"
                    );
            $query = $this->_barcodeGarment($barcode,$nik,"shipping",$ip);

            if (!$query) {
                return response()->json("Update movement garment error ! ! !",422);
            }

        }else{
           return response()->json("Barcode garment current location on ".$cek->current_location." ! ! !",422);
        }

        return view('shipping.ajax_list')->with('data',$data);
    }

    public function ajaxGetNik(Request $request){
        $nik = $request->barcode;

        $data = DB::table('employee')->where('nik',$nik)->whereNull('deleted_at')->first();
        if ($data!=null) {
            return response()->json($data,200);
        }else{
            return response()->json("Pic not found ! ! !",422);
        }
    }
}
