<?php

namespace App\Http\Controllers\Hanger;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Config;
use StdClass;
use Carbon\Carbon;
use Uuid;
use App\Models\Scheduler;

class SyncHangerOutput extends Controller
{
    static function getDataIna($route,$from,$to){
        echo " ".$route->line_hanger_id."
        ";
        DB::purge('sqlsrv');
        Config::set('database.connections.sqlsrv.host', $route->ip_address_server);
        Config::set('database.connections.sqlsrv.username', 'ina_open');
        Config::set('database.connections.sqlsrv.password', 'ina');
        Config::set('database.connections.sqlsrv.database', $route->ihs_server);
        Config::set('database.connections.sqlsrv.schema', 'dbo');
        Config::set('database.connections.sqlsrv.port', '1433');

        try {
            
            $dout = DB::connection('sqlsrv')
                ->select("select 
                            convert(nvarchar(36), HRDH_Key) as HRDH_Key,
                            HMH_Loaded_On_Line,
                            HRDH_Line,
                            HMH_SRM_Key,
                            HMH_ST_Key,
                            HMH_STPO_Key, 
                            HMH_SM_Key,
                            convert(nvarchar(36), HRDH_HMH_Key) as HRDH_HMH_Key,
                            HRDH_OC_Key,
                            HRDH_Workstation,
                            HRDH_ODP_Credited as HRD_ODP_Credited,
                            Style_Master.ST_ID as style,
                            SPO.STPO_ID as poreference,
                            Size_Master.SM_Short_Description as size,
                            CM.CM_Description as color,
                            HRDH_Completed_Time,
                            HRDH_Create_Time,
                            HRDH_Comments,
                            qc_rework.QCSC_Number,
                            qc_rework.QCSC_Description,
                            qc_rework.QCR_Repair_Quantity,
                            qc_rework.QCR_Repair_DateTime
                        from Hanger_Routing_Details_History hrdh
                        LEFT JOIN Hanger_Master_History hmh on hmh.HMH_Key=hrdh.HRDH_HMH_Key
                        LEFT JOIN $route->ihs_bridging.dbo.Style_Master Style_Master on Style_Master.st_key=hmh.HMH_ST_Key
                        LEFT JOIN $route->ihs_bridging.dbo.Style_Planned_Orders AS SPO on STPO_Key=HMH_STPO_Key
                        LEFT JOIN $route->ihs_bridging.dbo.Size_Master Size_Master on Size_Master.SM_Key=hmh.HMH_SM_Key
                        left join $route->ihs_bridging.dbo.Colour_Master as CM on CM_Key=HMH_CM_KEY
                        left join(
                                SELECT  
                                                        qcr.QCR_HM_Key,
                                                        qcr.QCR_Repair_Quantity,
                                                        qcr.QCR_Repair_DateTime,
                                                        qcsc.QCSC_Number,
                                                        qcsc.QCSC_Description 
                                                    FROM $route->ihs_bridging.dbo.QC_Rework as qcr join $route->ihs_bridging.dbo.QC_Sub_Codes as qcsc on qcr.QCR_QCSC_Key=qcsc.QCSC_Key WHERE qcsc.QCSC_Is_Rework=1
                            ) qc_rework on qc_rework.QCR_HM_Key=hrdh.HRDH_HMH_Key
                        where HRDH_Create_Time>='$from' and HRDH_Create_Time<'$to' and HRDH_OC_Key='99990002' and HMH_Loaded_On_Line=$route->line_hanger_id  and (HRDH_Ict_Flag is null OR HRDH_Ict_Flag=0)   ORDER BY HRDH_Create_Time asc"
                );

            foreach ($dout as $key => $vl) {
                $cekIna = self::cekIna($vl->HRDH_HMH_Key);
                $line_id = $route->line_id;

                if ($cekIna==null && $vl->HRD_ODP_Credited==1) {
                    
                    if ($vl->QCSC_Number==null) {
                        $inIna = [
                            'id'=>Uuid::generate(1)->string,
                            'ina_id'=>trim($vl->HRDH_HMH_Key),
                            'style'=>trim(explode("_", $vl->style)[0]),
                            'poreference'=>trim($vl->poreference),
                            'article'=>trim($vl->color),
                            'size'=>trim($vl->size),
                            'qty'=>1,
                            'create_date'=>carbon::now(),
                            'is_integrate'=>false,
                            'line_id'=>$line_id,
                            'color'=>trim($vl->color),
                            'status'=>1,
                            'qc_operator'=>'33333333',
                            'activity_date'=>carbon::now()
                        ];

                        self::flagIct($vl->HRDH_Key);
                    }else{
                        $indefcode = (substr($vl->QCSC_Number, 0,1).".".substr($vl->QCSC_Number, 1));

                        $cekMdefect = self::getDefect($indefcode);

                        $inIna = [
                            'id'=>Uuid::generate(1)->string,
                            'ina_id'=>trim($vl->HRDH_HMH_Key),
                            'style'=>trim(explode("_", $vl->style)[0]),
                            'poreference'=>trim($vl->poreference),
                            'article'=>trim($vl->color),
                            'size'=>trim($vl->size),
                            'qty'=>1,
                            'create_date'=>carbon::now(),
                            'line_id'=>$line_id,
                            'color'=>trim($vl->color),
                            'ina_defect_code'=>$cekMdefect!=null ? $indefcode : null,
                            'status'=>$cekMdefect!=null ? 0 : 1,
                            'qc_operator'=>'33333333',
                            'activity_date'=>carbon::now()
                        ];

                        self::flagIct($vl->HRDH_Key);
                    }

                    DB::connection('line')->table('inna_system')->insert($inIna);
                }else if ($cekIna!=null && $cekIna->is_integrate==false && $cekIna->status==0 && $vl->HRDH_Comments=="Repair recheck complete.") {
                   $upIna = [
                                'status'=>1,
                                'activity_date'=>carbon::now()
                            ];
                   DB::connection('line')->table('inna_system')->where('id',$cekIna->id)->update($upIna);
                   self::flagIct($vl->HRDH_Key);
                }

                continue;
            }
        } catch (Exception $e) {
            echo " ".$e->getMessage()."
            ";
        }

        return true;
    }

    static function cekIna($HRDH_Key){
        $cek = DB::connection('line')->table('inna_system')->where('ina_id',$HRDH_Key)->first();

        return $cek;
    }

    static function syncOutput($lineid,$from,$to){


        $getIna = DB::connection('line')->table('inna_system')->whereBetween('activity_date',[$from,$to])->where('line_id',$lineid)->where('is_integrate',false)->orderby('activity_date','asc')->get();

        try {
            DB::connection('line')->beginTransaction();
                foreach ($getIna as $gi) {
                    $cekHmh = self::cekHmKey($gi->ina_id);
                    $cekoutput = self::cekOutputQc($gi->poreference,$gi->style,$gi->article,$gi->size,$gi->line_id);

                    if ($gi->status==1) {


                        if ($cekHmh!=null && $cekHmh->status_defect==0) {
                            DB::connection('line')->table('qc_endline_defect')->where('endline_defect_id',$cekHmh->endline_defect_id)->update(['status_defect'=>1,'update_at'=>carbon::now()]);
                            DB::connection('line')->table('qc_endline')->where('qc_endline_id',$cekHmh->qc_endline_id)->update(['repairing'=>$cekHmh->repairing-1]);
                            DB::connection('line')->table('inna_system')->where('id',$gi->id)->update(['is_integrate'=>true,'integrate_date'=>carbon::now(),'inline_header_id'=>$cekoutput->inline_header_id]);
                        }else if($cekHmh==null && $cekoutput!=null && $cekoutput->balance<0){
                                if ($cekoutput->qc_endline_id==null) {
                                    $qcendlineid = Uuid::generate(1)->string;
                                    DB::connection('line')
                                                ->table('qc_endline')
                                                ->insert([
                                                    'qc_endline_id'=>$qcendlineid,
                                                    'inline_header_id'=>$cekoutput->inline_header_id,
                                                    'counter_qc'=>1,
                                                    'factory_id'=>$cekoutput->factory_id,
                                                    'header_size_id'=>$cekoutput->header_size_id,
                                                    'create_by'=>"33333333",
                                                    'create_date'=>carbon::now(),
                                                    'style'=>$cekoutput->style
                                                ]);
                                }else{
                                    $qcendlineid = $cekoutput->qc_endline_id;
                                }

                                DB::connection('line')
                                                ->table('qc_endline_trans')
                                                ->insert([
                                                    'qc_endline_trans_id'=>Uuid::generate(1)->string,
                                                    'qc_endline_id'=>$qcendlineid,
                                                    'inline_header_id'=>$cekoutput->inline_header_id,
                                                    'counter_pcs'=>$cekoutput->qc_output+1,
                                                    'create_date'=>carbon::now(),
                                                    'style'=>$cekoutput->style,
                                                    'factory_id'=>$cekoutput->factory_id,
                                                    'create_by'=>"33333333",
                                                    'sys'=>"syncron hanger",
                                                    'ip'=>\Request::ip(),
                                                    'hmh_key'=>$gi->ina_id
                                                ]);
                                DB::connection('line')->select(db::raw("SELECT * from update_counter_qc('".$qcendlineid."')"));
                                DB::connection('line')->select(db::raw("SELECT * from update_counter('".$qcendlineid."')"));
                                DB::connection('line')->table('inna_system')->where('id',$gi->id)->update(['is_integrate'=>true,'integrate_date'=>carbon::now(),'inline_header_id'=>$cekoutput->inline_header_id]);
                        }


                    }else if ($gi->status==0) {
                        if ($cekHmh==null && $cekoutput!=null && $cekoutput->balance<0) {

                            if ($cekoutput->qc_endline_id==null) {
                                $qcendlineid = Uuid::generate(1)->string;
                                DB::connection('line')
                                            ->table('qc_endline')
                                            ->insert([
                                                'qc_endline_id'=>$qcendlineid,
                                                'inline_header_id'=>$cekoutput->inline_header_id,
                                                'counter_qc'=>1,
                                                'factory_id'=>$cekoutput->factory_id,
                                                'header_size_id'=>$cekoutput->header_size_id,
                                                'create_by'=>"33333333",
                                                'create_date'=>carbon::now(),
                                                'repairing'=>$cekoutput->repairing+1,
                                                'style'=>$cekoutput->style
                                            ]);
                            }else{
                                $qcendlineid = $cekoutput->qc_endline_id;
                            }
                            $qcendtransid = Uuid::generate(1)->string;
                            DB::connection('line')
                                            ->table('qc_endline_trans')
                                            ->insert([
                                                'qc_endline_trans_id'=>$qcendtransid,
                                                'qc_endline_id'=>$qcendlineid,
                                                'inline_header_id'=>$cekoutput->inline_header_id,
                                                'counter_pcs'=>$cekoutput->qc_output+1,
                                                'create_date'=>carbon::now(),
                                                'style'=>$cekoutput->style,
                                                'factory_id'=>$cekoutput->factory_id,
                                                'create_by'=>"33333333",
                                                'sys'=>"syncron hanger",
                                                'ip'=>\Request::ip(),
                                                'hmh_key'=>$gi->ina_id
                                            ]);

                            DB::connection('line')
                                            ->table('qc_endline_defect')
                                            ->insert([
                                                'endline_defect_id'=>Uuid::generate(1)->string,
                                                'qc_endline_trans_id'=>$qcendtransid,
                                                'qc_endline_id'=>$qcendlineid,
                                                'defect_id'=>self::getDefect($gi->ina_defect_code),
                                                'inline_header_id'=>$cekoutput->inline_header_id,
                                                'status_defect'=>0,
                                                'create_date'=>carbon::now(),
                                                'style'=>$cekoutput->style,
                                                'factory_id'=>$cekoutput->factory_id
                                            ]);
                            DB::connection('line')->select(db::raw("SELECT * from update_counter_qc('".$qcendlineid."')"));
                            DB::connection('line')->select(db::raw("SELECT * from update_counter('".$qcendlineid."')"));
                        }
                    }

                    continue;
                }
            DB::connection('line')->commit();
        } catch (Exception $e) {
            DB::connection('line')->rollBack();
            $message = $e->getMessage();
        }
    }

    public function SyncAoi1Local($lineid){
     
        $from = carbon::now()->format('Y-m-d 00:00:00');
        $to = carbon::now()->addDays(1)->format('Y-m-d 00:00:00');

        $getLine = DB::connection('line')
                        ->table('route_hanger_lines')
                        ->where('factory_id',1)
                        ->where('line_id',$lineid)
                        ->select('ip_address_server','ihs_server','ihs_bridging','line_id','line_hanger_id','ip_address_local','ihs_local')->inRandomOrder()->get();
            
                       
            foreach ($getLine as $gL) {
                    
                    self::getDataLocal($gL,$from,$to);
                    self::syncOutput($gL->line_id,$from,$to);

            }
    }
    static function getDataLocal($route,$from,$to){
        echo " ".$route->line_hanger_id."
        ";
        DB::purge('sqlsrv');
        Config::set('database.connections.sqlsrv.host', $route->ip_address_local);
        Config::set('database.connections.sqlsrv.username', 'ina_open');
        Config::set('database.connections.sqlsrv.password', 'ina');
        Config::set('database.connections.sqlsrv.database', 'IHS');
        Config::set('database.connections.sqlsrv.schema', 'dbo');
        Config::set('database.connections.sqlsrv.port', '1433');

        $dout = DB::connection('sqlsrv')
            ->select("SELECT 
                    convert(nvarchar(36), HRDH_Key) as HRDH_Key,
                    convert(nvarchar(36), HRDH_HM_Key) as HRDH_HM_Key,
                    HRD_ODP_Credited,
                    style,
                    poreference,
                    size,
                    color
                FROM V_Transaction_Hangers WHERE HRDH_Create_Time>='".$from."' and HRDH_Create_Time<'".$to."' and HM_Loaded_On_Line=".$route->line_hanger_id." and  HRD_Ict_Flag=0 ORDER BY HRDH_Create_Time DESC"
            );


        foreach ($dout as $key => $vl) {
            $cekIna = self::cekIna($vl->HRDH_HMH_Key);
            $line_id = $route->line_id;
            $qcsc = self::cekReworkLocal($vl->HRDH_HMH_Key);

            if ($cekIna==null && $vl->HRD_ODP_Credited==1) {
                
                if ($qcsc==null) {
                    $inIna = [
                        'id'=>Uuid::generate(1)->string,
                        'ina_id'=>trim($vl->HRDH_HMH_Key),
                        'style'=>trim(explode("_", $vl->style)[0]),
                        'poreference'=>trim($vl->poreference),
                        'article'=>trim($vl->color),
                        'size'=>trim($vl->size),
                        'qty'=>1,
                        'create_date'=>carbon::now(),
                        'is_integrate'=>false,
                        'line_id'=>$line_id,
                        'color'=>trim($vl->color),
                        'status'=>1,
                        'qc_operator'=>'33333333',
                        'activity_date'=>carbon::now()
                    ];

                    // self::flagIct($vl->HRDH_Key);
                }else{
                    $indefcode = (substr($qcsc, 0,1).".".substr($qcsc, 1));

                    $cekMdefect = self::getDefect($indefcode);

                    $inIna = [
                        'id'=>Uuid::generate(1)->string,
                        'ina_id'=>trim($vl->HRDH_HMH_Key),
                        'style'=>trim(explode("_", $vl->style)[0]),
                        'poreference'=>trim($vl->poreference),
                        'article'=>trim($vl->color),
                        'size'=>trim($vl->size),
                        'qty'=>1,
                        'create_date'=>carbon::now(),
                        'line_id'=>$line_id,
                        'color'=>trim($vl->color),
                        'ina_defect_code'=>$cekMdefect!=null ? $indefcode : null,
                        'status'=>1,
                        'qc_operator'=>'33333333',
                        'activity_date'=>carbon::now()
                    ];

                    // self::flagIct($vl->HRDH_Key);
                }

                DB::connection('line')->table('inna_system')->insert($inIna);
            }else if ($cekIna!=null && $cekIna->is_integrate==false && $cekIna->status==0 && $vl->HRDH_Comments=="Repair recheck complete.") {
               $upIna = [
                            'status'=>1,
                            'activity_date'=>carbon::now()
                        ];
               DB::connection('line')->table('inna_system')->where('id',$cekIna->id)->update($upIna);
               // self::flagIct($vl->HRDH_Key);
            }

            continue;
        }


        return true;
    }

    static function cekReworkLocal($HRDH_Key){
        $data = DB::connection('sqlsrv')
            ->select("SELECT TOP 1 QCSC_Number FROM V_Qc_Rework WHERE QCR_HM_Key='".$HRDH_Key."' ORDER BY QCR_Defect_DateTime ASC "
            );
        return $data!=null ? $data->QCSC_Number : null;
    }

    static function cekOutputQc($poref,$style,$article,$size,$lineid){
        $data = DB::connection('line')->table('ns_qcoutput')
                                            ->where('poreference',$poref)
                                            ->where('style',$style)
                                            ->where('article',$article)
                                            ->where('size',$size)
                                            ->where('line_id',$lineid)->first();
        return $data;
    }

    static function cekHmKey($hmhkey){
        $cek = DB::connection('line')->select("SELECT * FROM ns_cek_hmh_key('".$hmhkey."')");

        return !empty($cek) ? $cek[0] : null;
    }

    static function getDefect($code){
        $dataDef = DB::connection('line')->table('master_defect')->where('defect_code',$code)->where('is_active',true)->first();

        return isset($dataDef) ? $dataDef->defect_id : null;
    }


    static function flagIct($HRDH_Key){
        DB::purge('sqlsrv');
        DB::connection('sqlsrv')->table('Hanger_Routing_Details_History')->where('HRDH_Key',$HRDH_Key)->update(['HRDH_Ict_Flag'=>1]);
        return true;
    }

    static function cekLine($hanger_id){
        $line = DB::connection('line')->table('route_hanger_lines')->where('line_hanger_id',$hanger_id)->select('line_hanger_id','line_id')->first();

        return isset($line) ?  $line->line_id : null;
    }
}
