<?php

namespace App\Http\Controllers\Qc;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use DB;
use Carbon\Carbon;
use DataTables;

use App\Models\GarmentId;
use App\Models\GarmentMove;
use App\Models\Locator;


class QcController extends Controller
{
    public function index(){
    	return view('qc.storage.index');
    }

    public function ajaxScan(Request $request){
    	$barcode = $request->bargar;
    	$status = $request->status;
        $rack = $request->rack;
        $ip = $request->ip();
    	
    	$check = $this->cek_barcode($barcode);

    	if (!$check) {
    		return response()->json("Barcode garment not found",422);
    	}

    	if ($status=="storage") {
            if (($check->current_location=='qc' && $check->borrow==true && $check->storage==false) || ($check->current_location=='handover') || ($check->current_location=='checkin_md')) {
                $locid = Locator::where('id',$rack)->whereNull('deleted_at')->first();
                $data = array(
                    'barcode_id'=>$check->barcode_id,
                    'docno'=>$check->documentno,
                    'season'=>$check->season,
                    'style'=>$check->style,
                    'article'=>$check->article,
                    'size'=>$check->size,
                    'status'=>$check->status,
                    'type'=>"storage",
                    'rack'=>$locid->name
                );

                $query = $this->barcodeGarment($barcode,"qc_storage",$ip,$rack);

                if (!$query) {
                    return response()->json("Update movement garment error ! ! !",422);
                }
            }else{
                return response()->json("Barcode last location ".$check->current_location."  ! ! !",422);
            }
        }else if($status=="reverse"){
            if ($check->current_location=='qc_storage' && $check->borrow==false && $check->storage==true) {
                $data = array(
                    'barcode_id'=>$check->barcode_id,
                    'docno'=>$check->documentno,
                    'season'=>$check->season,
                    'style'=>$check->style,
                    'article'=>$check->article,
                    'size'=>$check->size,
                    'status'=>$check->status,
                    'type'=>"reverse"
                );

                $query = $this->barcodeGarment($barcode,"qc",$ip,null);

                if (!$query) {
                    return response()->json("Update movement garment error ! ! !",422);
                }
            }else{
                return response()->json("Barcode last location ".$check->current_location."  ! ! !",422);
            }
        }

    	return view('qc.storage.ajax_list')->with('data',$data);
    }


    private function cek_barcode($barcode){
        $dt_bar = DB::table('barcode_garment')
                    ->join('master_mo','barcode_garment.mo_id','=','master_mo.id')
                    ->where('barcode_garment.barcode_id',$barcode)
                    ->where('barcode_garment.factory_id',Auth::user()->factory_id)
                    ->whereNull('barcode_garment.deleted_at')
                    ->whereNull('master_mo.deleted_at')
                    ->select('barcode_garment.barcode_id',
                                'barcode_garment.mo_id',
                                'barcode_garment.current_location',
                                'master_mo.documentno',
                                'master_mo.season',
                                'master_mo.style','master_mo.article',
                                'master_mo.size',
                                'master_mo.status',
                                'barcode_garment.checkin_md',
                                'barcode_garment.storage',
                                'barcode_garment.borrow')->first();

        return $dt_bar;
    }
     

    private function barcodeGarment($barcode_id,$location,$ipaddress,$idloc= null){
    	#query update & movement
    	$last = GarmentMove::where('barcode_id',$barcode_id)->whereNull('deleted_at')->where('is_canceled',false)->orderBy('created_at','asc')->first();

    	

    	try {
            DB::begintransaction();

            switch ($location) {
                case 'qc_storage':
                    $update = array(
                        'current_location'=>$location,
                        'borrow'=>false,
                        'storage'=>true,
                        'updated_at'=>Carbon::now()
                    );

                    $move = array(
                        'barcode_id'=>$barcode_id,
                        'locator_from'=>$last->locator_to,
                        'locator_to'=>$location,
                        'locator_id'=>$idloc,
                        'description'=>"Checkin on qc storage",
                        'created_at'=>Carbon::now(),
                        'user_id'=>Auth::user()->id,
                        'ip_address'=>$ipaddress
                    );
                    break;

                case 'qc':
                    $bor= GarmentMove::where('barcode_id',$barcode_id)->where('locator_to','qc')->orderBy('created_at','desc')->first();
                   
                    $update = array(
                        'current_location'=>$location,
                        'borrow'=>true,
                        'storage'=>false,
                        'updated_at'=>Carbon::now()
                    );

                    $move = array(
                        'barcode_id'=>$barcode_id,
                        'locator_from'=>$last->locator_to,
                        'locator_to'=>$location,
                        'description'=>"Reverse on qc",
                        'created_at'=>Carbon::now(),
                        'user_id'=>Auth::user()->id,
                        'ip_address'=>$ipaddress,
                        'borrower'=>isset($bor->borrower) ? $bor->borrower  : null
                    );
                    break;
            }
            
            	

	           

                GarmentId::where('barcode_id',$barcode_id)->update($update);
                GarmentMove::where('id',$last->id)->update(['deleted_at'=>Carbon::now()]);
                GarmentMove::firstorcreate($move);
            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            $message = $ex->getMessage();
            ErrorHandler::db($message);
        }

        return true;
    }

    public function getRack(Request $request){
        $barcode = $request->barcode;
        $loc = Locator::where('id',$barcode)->where('factory_id',Auth::user()->factory_id)->whereNull('deleted_at')->first();

        if ($loc!=null) {
            return response()->json($loc,200);
        }else{
             return response()->json("Location not Found ! ! !",422);
        }
        
    }


    public function borrow(){
        $dept = DB::table('department')->whereNull('deleted_at')->get();
        return view('qc.borrow.borrow')->with('dept',$dept);
    }

    public function ajaxBorrow(Request $request){
        $barcode = $request->bargar;
        $status = $request->status;
        $dept = $request->dept;
        $ip = $request->ip();
        $nik = $request->nik;
        $line = $request->line;
        $retdate = isset($request->retdate) ? Carbon::parse($request->retdate)->format('Y-m-d') : null;

        $check = $this->cek_barcode($barcode);

        if (!$check) {
            return response()->json("Barcode garment not found",422);
        }

        if ($status=="borrow") {
            if ($check->current_location=="qc_storage" && $check->storage==true && $check->borrow==false) {

                $data = array(
                    'barcode_id'=>$check->barcode_id,
                    'docno'=>$check->documentno,
                    'season'=>$check->season,
                    'style'=>$check->style,
                    'article'=>$check->article,
                    'size'=>$check->size,
                    'status'=>$check->status,
                    'type'=>"out",
                    'loct'=>strtoupper($dept),
                    'returndate'=>Carbon::parse($request->retdate)->format('d-M-Y')
                );
                $query = $this->barcodeBorrow($barcode,"out",$dept,$ip,$nik,$line,$retdate);

                if (!$query) {
                     return response()->json("pdate movement garment error ! ! !",422);
                }
            }else{
                return response()->json("Barcode last location ".$check->current_location." ! ! !",422);
            }
        }else if ($status=="return") {
            if (($check->current_location!="qc_storage" || $check->current_location!="qc") && $check->storage==false && $check->borrow==true) {

                $data = array(
                    'barcode_id'=>$check->barcode_id,
                    'docno'=>$check->documentno,
                    'season'=>$check->season,
                    'style'=>$check->style,
                    'article'=>$check->article,
                    'size'=>$check->size,
                    'status'=>$check->status,
                    'type'=>"in"
                );
                $query = $this->barcodeBorrow($barcode,"in","qc",$ip);

                if (!$query) {
                     return response()->json("pdate movement garment error ! ! !",422);
                }
            }else{
                return response()->json("Barcode last location ".$check->current_location." ! ! !",422);
            }
        }

        return view('qc.borrow.ajax_list')->with('data',$data);
    }

    private function barcodeBorrow($barcode,$type,$location,$ipaddress,$nik=null,$line=null,$retdate=null){
        $last = GarmentMove::where('barcode_id',$barcode)->whereNull('deleted_at')->where('is_canceled',false)->orderBy('created_at','asc')->first();

        try {
            DB::begintransaction();
                switch ($type) {
                    case 'out':
                        $desc = "Checkout to ".$location;
                        $update = array(
                            'current_location'=>$location,
                            'borrow'=>true,
                            'storage'=>false,
                            'updated_at'=>Carbon::now()
                        );

                        $move = array(
                            'barcode_id'=>$barcode,
                            'locator_from'=>$last->locator_to,
                            'locator_to'=>$location,
                            'description'=>$desc,
                            'created_at'=>Carbon::now(),
                            'user_id'=>Auth::user()->id,
                            'ip_address'=>$ipaddress,
                            'borrower'=>$nik,
                            'line'=>$line,
                            'promise_return'=>$retdate
                        );
                        break;

                    case 'in':
                        $desc = "Return to ".$location;
                        $bor= GarmentMove::where('barcode_id',$barcode)->where('locator_from','qc_storage')->where('is_canceled',false)->whereNull('deleted_at')->orderBy('created_at','desc')->first();

                       
                        // $borrower = $bor->borrower;
                        
                        $update = array(
                            'current_location'=>$location,
                            'borrow'=>true,
                            'storage'=>false,
                            'updated_at'=>Carbon::now()
                        );

                        $move = array(
                            'barcode_id'=>$barcode,
                            'locator_from'=>$last->locator_to,
                            'locator_to'=>$location,
                            'description'=>$desc,
                            'created_at'=>Carbon::now(),
                            'user_id'=>Auth::user()->id,
                            'ip_address'=>$ipaddress,
                            'borrower'=>isset($bor->borrower) ? $bor->borrower : null
                        );
                        break;
                }

                

                GarmentId::where('barcode_id',$barcode)->update($update);
                GarmentMove::where('id',$last->id)->update(['deleted_at'=>Carbon::now()]);
                GarmentMove::firstorcreate($move);
            DB::commit();
        } catch (Exception $er) {
            DB::rollback();
            $message = $er->getMessage();
            ErrorHandler::db($message);
        }

        return true;
    }

    public function getNik(Request $request){
        $nik = $request->nik;

        $data = DB::table('employee')->where('nik',$nik)->whereNull('deleted_at')->first();

        if ($data!=null) {
            return response()->json($data,200);
        }else{
            return response()->json("Borrower not found ! ! !",422);
        }
    }

    public function ajaxGetLine(){
       $line =  DB::connection('line')->table('master_line')
                                    ->where('factory_id',Auth::user()->factory_id)
                                    ->where('active',true)
                                    ->whereNull('delete_at')
                                    ->orderBy('line_name','asc')
                                    ->get();

        return response()->json(['line'=>$line],200);
    }
    public function ajaxGetDept(){
        $dept = DB::table('department')->whereNull('deleted_at')->get();

        return response()->json(['dept'=>$dept],200);
    }
}
