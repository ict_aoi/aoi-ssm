<?php

namespace App\Http\Controllers\Sample;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use DB;
use Carbon\Carbon;
use DataTables;
use Excel;
use Illuminate\Support\Facades\Storage;

use Rap2hpoutre\FastExcel\FastExcel;

use App\Models\SampleReq;
use App\Models\SrDetail;
use App\Models\SrCsi;
use App\Models\SrInfo;
use App\Models\ns_report;

class SampleReqController extends Controller
{
    protected $erp;

    public function __construct(){
        ini_set('max_execution_time', 1800);

        $this->erp = DB::connection('erp');
    }

    public function sreq(){
        return view('sample.sample_req.sreq');
    }

    public function csi(){
        return view('sample.sample_req.csi');
    }

    public function ajaxGetData(Request $request){
        $radio = trim($request->radio_search);


        $data = SampleReq::whereNull('deleted_at')->orderBy('created_at','desc')->orderBy('updated_at','desc');

        switch ($radio) {
            case 'documentno':
                $this->validate($request,[
                    'txdoc'=>'required|min:4',
                ]);

                $data = $data->where('documentno','LIKE','%'.trim($request->txdoc))
                                    ->orWhere('documentno','LIKE','%'.strtoupper(trim($request->txdoc)));
            break;


            case 'style':
                $this->validate($request,[
                    'txstyle'=>'required|min:4',
                ]);
                $data = $data->where('style','LIKE','%'.trim($request->txstyle))
                                    ->orWhere('style','LIKE','%'.strtoupper(trim($request->txstyle)));
            break;


            case 'shipment_date':
                $sdate = explode('-', preg_replace('/\s+/', '',$request->txship));

                $sfrom = Carbon::parse($sdate[0])->format('Y-m-d');
                $sto = Carbon::parse($sdate[1])->format('Y-m-d');


                $data = $data->whereBetween('shipment_date',[$sfrom,$sto]);
            break;


            case 'mo_created':
                $mdate = explode('-', preg_replace('/\s+/', '',$request->txmo));

                $mfrom = Carbon::parse($mdate[0])->format('Y-m-d 00:00:00');
                $mto = Carbon::parse($mdate[1])->format('Y-m-d 23:59:59');


                $data = $data->whereBetween('mo_created',[$mfrom,$mto])
                                ->orderBy('updated_at','desc')
                                ->orderBy('created_at','desc');
            break;

            case 'sinkron_date':
                $mdate = explode('-', preg_replace('/\s+/', '',$request->txsink));

                $mfrom = Carbon::parse($mdate[0])->format('Y-m-d 00:00:00');
                $mto = Carbon::parse($mdate[1])->format('Y-m-d 23:59:59');


                // dd($range);
                // $data = $data->whereBetween('created_at',[$mfrom,$mto])
                //                             ->orWhereBetween('updated_at',[$mfrom,$mto])
                //                             ->orderBy('updated_at','desc')
                //                             ->orderBy('created_at','desc');

                 $data= $data->where(function($query) use ($mfrom,$mto){
                                        $query->whereBetween('created_at',[$mfrom,$mto])
                                        ->orwhere(function($query) use ($mfrom,$mto){
                                            $query->whereBetween('updated_at',[$mfrom,$mto])
                                                    ->where('is_updated',true);
                                        });
                                    })
                                    ->orderBy('updated_at','desc')
                                    ->orderBy('created_at','desc');
            break;

            case 'completed_date':
                $mdate = explode('-', preg_replace('/\s+/', '',$request->txcompl));

                $mfrom = Carbon::parse($mdate[0])->format('Y-m-d 00:00:00');
                $mto = Carbon::parse($mdate[1])->format('Y-m-d 23:59:59');


                $data = $data->whereBetween('completed_date',[$mfrom,$mto])
                                            ->orderBy('updated_at','desc')
                                            ->orderBy('created_at','desc');
            break;


        }

        // if ($key==null) {
        //    $from = Carbon::now()->subDays(120)->format('Y-m-d 00:00:00');
        //    $to = Carbon::now()->format('Y-m-d 23:59:59');

        //    $data = $data->whereBetween('created_at',[$from,$to])
        //                 ->orwhereBetween('updated_at',[$from,$to]);
        // }else if ($key!=null && $radio=="no_mo") {
        //    $data = $data->where('documentno','LIKE','%'.$key);
        // }else if ($key!=null && $radio=="style") {
        //    $data = $data->where('style','LIKE','%'.$key);
        // }


        return DataTables::of($data)
                        ->addColumn('checkbox',function($data){
                            return '<input type="checkbox" class="srid" name="selector[]" id="Inputselector" value="'.$data->id.'">';
                        })
                        ->editColumn('shipment_date',function($data){
                            // return date_format(date_create($data->shipment_date),'d-M-Y');
                            return Carbon::parse($data->shipment_date)->format('d-M-Y');
                        })
                        ->editColumn('documentno',function($data){
                            if ($data->is_updated==true) {
                                $remup ='<span class="badge badge-warning"><i class="icon-quill4"></i></span>';
                                $last = '<br> Last Update : '.Carbon::parse($data->mo_updated)->format('d-M-Y H:i:s');
                            }else{
                                $remup = '';
                                $last = '';
                            }

                            if (isset($data->completed_date) && $data->docstatus=='CO') {
                                $compl = '<br> Complate at '.Carbon::parse($data->completed_date)->format('d-M-Y H:i:s');
                            }else{
                                $compl = '';
                            }

                            return $data->documentno." ".$remup.$last.$compl;
                        })
                        ->setRowAttr([
                            'style'=>function($data){
                                if ($data->docstatus=='VO') {
                                    return 'background-color:#ffff99';
                                }else{
                                    return 'background-color:#ffffff';
                                }
                            }
                        ])
                        ->rawColumns(['checkbox','shipment_date','documentno'])->make(true);
    }

    public function downloadSr(Request $request){
        $id =explode(",",trim($request->id));
        $date = Carbon::now()->format('YmdHis');
        $sr = SampleReq::whereNull('deleted_at')->whereIn('id',$id)->get();
        $samreq = [];
        foreach ($sr as $sr) {

            $data['head'] = $sr;
            $data['img'] = $sr->image_id!=null ? $this->getImgERP($sr->image_id) : null;
            $data['main'] = SrDetail::where('sr_id',$sr->id)->where('type','Fabric')->whereNull('deleted_at')->get();
            $data['trim'] = SrDetail::where('sr_id',$sr->id)->where('type','Trim')->whereNull('deleted_at')->get();
            $data['labl'] = SrDetail::where('sr_id',$sr->id)->where('type','Label')->whereNull('deleted_at')->get();

            $samreq[]=$data;
        }


        // dd($samreq);
        if ($sr!=null) {
            $filename = "Sample_request_".$date."_".count($id);

            // dd($filename);
            $size = array(0, 0, 612.00, 1008.00);
            $pdf = \PDF::loadview('sample.sample_req.sr_temp',['samreq'=>$samreq])->setPaper('A4','potrait');
            return $pdf->stream($filename);

            // return view('sample.sample_req.sr_temp',['samreq'=>$samreq]);
        }else{
              return response()->json('no data', 422);
        }
    }

    private function getImgERP($id){
        return $this->erp->table('ad_image')->where('ad_image_id',$id)->select(DB::raw("encode(binarydata,'base64') as binarydata"))->first();
    }

    public function downloadCsi(Request $request){
        $id = explode(",",trim($request->id));
        $date = Carbon::now()->format('YmdHis');
        $dtsr = SampleReq::whereIn('id',$id)->whereNull('deleted_at')->count();
        $data = DB::table('sample_request')
                        ->join('sr_csi_detail','sample_request.id','=','sr_csi_detail.sr_id')
                        ->whereIn('sample_request.id',$id)
                        // ->where('sample_request.documentno',$dtsr->documentno)
                        ->whereNull('sample_request.deleted_at')
                        ->whereNull('sr_csi_detail.deleted_at')
                        ->select('sample_request.documentno',
                            'sample_request.buyer',
                            'sample_request.season',
                            'sample_request.style',
                            'sample_request.article',
                            'sample_request.size',
                            'sample_request.status',
                            'sample_request.qty',
                            'sample_request.shipment_date',
                            'sample_request.description',
                            'sample_request.mo_pic',
                            'sample_request.product_category',
                            'sample_request.mo_created',
                            'sample_request.mo_updated',
                            'sample_request.ori_date',
                            'sr_csi_detail.sr_id',
                            'sr_csi_detail.part_no',
                            'sr_csi_detail.material_code',
                            'sr_csi_detail.material',
                            'sr_csi_detail.uom',
                            'sr_csi_detail.fbc',
                            'sr_csi_detail.note',
                            'sr_csi_detail.usage');



        $i =1;
        $filename = "Cutting_sewing_instruction_".$date."_".$dtsr;

        $data_result = $data->get();
        foreach ($data_result as $key => $data_results) {
            $data_results->no = $i++;
        }


        if ($data_result->count()>0) {
            // SampleReq::where('id',$id)->update(['is_csi'=>true,'updated_at'=>Carbon::now()]);

            return (new FastExcel($data_result))->download($filename.'.xlsx',function($row) use ($i){



                return
                [
                    'Document No.'=>$row->documentno,
                    'Season'=>$row->season,
                    'Style'=>$row->style,
                    'Article Number'=>$row->article,
                    'Size'=>$row->size,
                    'Ordered Qty'=>$row->qty,
                    'User'=>$row->mo_pic,
                    'Product Category'=>$row->product_category,
                    'Part No'=>$row->part_no,
                    'Code Raw Material'=>$row->material_code,
                    'Material'=>$row->material,
                    'UOM Product'=>$row->uom,
                    'FBC'=>floatval($row->fbc),
                    'Usage'=>$row->usage,
                    'Note'=>$row->note,
                    'Upload date'=> $row->mo_created,
                    'Update Date'=> $row->mo_created!=$row->mo_updated ? $row->mo_updated : "",
                    'Ori Date'=>isset($row->ori_date) ? $row->ori_date : ""
                ];

            });
        }else{
            return response()->json('no data', 422);
        }

    }


    public function reportRequest(){
        return view('sample.sample_req.report_request');
    }

    public function getDataReq(Request $request){
        $radio = trim($request->radio_search);


        $data = ns_report::orderBy('created_at','asc');

        switch ($radio) {
            case 'documentno':
                $this->validate($request,[
                    'txdoc'=>'required|min:4',
                ]);

                $data = $data->where('documentno','LIKE','%'.trim($request->txdoc))
                                    ->orWhere('documentno','LIKE','%'.strtoupper(trim($request->txdoc)));
                                   
            break;


            case 'style':
                $this->validate($request,[
                    'txstyle'=>'required|min:4',
                ]);
                $data = $data->where('style','LIKE','%'.trim($request->txstyle))
                                    ->orWhere('style','LIKE','%'.strtoupper(trim($request->txstyle)));
                                  
            break;


            case 'shipment_date':
                $sdate = explode('-', preg_replace('/\s+/', '',$request->txship));

                $sfrom = Carbon::parse($sdate[0])->format('Y-m-d');
                $sto = Carbon::parse($sdate[1])->format('Y-m-d');


                $data = $data->whereBetween('shipment_date',[$sfrom,$sto])->orderBy('shipment_date','asc');
            break;


            case 'mo_created':
                $mdate = explode('-', preg_replace('/\s+/', '',$request->txmo));

                $mfrom = Carbon::parse($mdate[0])->format('Y-m-d 00:00:00');
                $mto = Carbon::parse($mdate[1])->format('Y-m-d 23:59:59');


                $data = $data->whereBetween('mo_created',[$mfrom,$mto])->orderBy('mo_created','asc');
            break;


            case 'sinkron_date':
                $mdate = explode('-', preg_replace('/\s+/', '',$request->txsink));

                $mfrom = Carbon::parse($mdate[0])->format('Y-m-d 00:00:00');
                $mto = Carbon::parse($mdate[1])->format('Y-m-d 23:59:59');


                // $data = $data->whereBetween('created_at',[$mfrom,$mto])
                //                             ->orWhereBetween('updated_at',[$mfrom,$mto])
                //                             ->orderBy('updated_at','desc')
                //                             ->orderBy('created_at','desc');


                $data= $data->where(function($query) use ($mfrom,$mto){
                                        $query->whereBetween('created_at',[$mfrom,$mto])
                                        ->orwhere(function($query) use ($mfrom,$mto){
                                            $query->whereBetween('updated_at',[$mfrom,$mto])
                                                    ->where('is_updated',true);
                                        });
                                    });
            break;


            case 'completed_date':
                $mdate = explode('-', preg_replace('/\s+/', '',$request->txcompl));

                $mfrom = Carbon::parse($mdate[0])->format('Y-m-d 00:00:00');
                $mto = Carbon::parse($mdate[1])->format('Y-m-d 23:59:59');


                $data = $data->whereBetween('completed_date',[$mfrom,$mto])->orderBy('completed_date','asc');
            break;


        }

        $data = $data->orderBy('created_at','desc')->orderBy('updated_at','desc');

        return DataTables::of($data)
                            ->editColumn('created_at',function($data){
                                $crt = "<b>Created at : </b><br>".Carbon::parse($data->mo_created)->format('d M Y H:i:s');
                                $upd= (($data->created_at!=$data->updated_at) && $data->is_updated==true) ? "<br><b>Last Updated : </b><br>".Carbon::parse($data->mo_updated)->format('d M Y H:i:s') : "";
                                $compl = isset($data->completed_date) ? "<br><b>Completed at : </b><br>".Carbon::parse($data->completed_date)->format('d M Y H:i:s') : '';

                                $remup = $data->is_updated==true ? '<span class="badge badge-warning"><i class="icon-quill4"></i></span><br>' : '';

                                return $remup.$crt." ".$upd." ".$compl;
                            })
                            ->editColumn('shipment_date',function($data){
                                return isset($data->shipment_date) ? Carbon::parse($data->shipment_date)->format('d M Y')  : '';
                            })
                            ->editColumn('pattern_date',function($data){
                                // return Carbon::parse($data->pattern_date)->format('d M Y');
                                 return isset($data->pattern_date) ? Carbon::parse($data->pattern_date)->format('d M Y')  : '';
                            })
                            ->editColumn('date_plot',function($data){
                                // return Carbon::parse($data->date_plot)->format('d M Y');
                                return isset($data->date_plot) ? Carbon::parse($data->date_plot)->format('d M Y')  : '';
                            })
                            ->editColumn('marker_release',function($data){
                                // return Carbon::parse($data->marker_release)->format('d M Y');
                                return isset($data->marker_release) ? Carbon::parse($data->marker_release)->format('d M Y')  : '';
                            })
                            ->editColumn('sample_recieve_marker',function($data){
                                // return Carbon::parse($data->marker_release)->format('d M Y');
                                return isset($data->sample_recieve_marker) ? Carbon::parse($data->sample_recieve_marker)->format('d M Y')  : '';
                            })
                            // ->editColumn('marker_need',function($data){
                            //     switch ($data->marker_need) {
                            //         case true:
                            //             return "YES";
                            //             break;
                                    
                            //         case false:
                            //             return "NO";
                            //             break;
                            //         default:
                            //             return "NO";
                            //             break;
                            //     }
                            // }) 
                            ->editColumn('marker_length',function($data){
                                return $data->marker_length.' (inc)';
                            })
                            ->rawColumns(['created_at','shipment_date','pattern_date','date_plot','marker_release','sample_recieve_marker','marker_length'])
                            ->make(true);
    }

    public function exportReq(Request $request){
        $radio = trim($request->radio_search);
        $filterby = trim($request->filterby);

        $data = ns_report::orderBy('created_at','asc');

        switch ($radio) {
            case 'documentno':
                $this->validate($request,[
                    'txdoc'=>'required|min:4',
                ]);

                $data = $data->where('documentno','LIKE','%'.trim($request->txdoc))
                                    ->orWhere('documentno','LIKE','%'.strtoupper(trim($request->txdoc)));
                                   
            break;


            case 'style':
                $this->validate($request,[
                    'txstyle'=>'required|min:4',
                ]);
                $data = $data->where('style','LIKE','%'.trim($request->txstyle))
                                    ->orWhere('style','LIKE','%'.strtoupper(trim($request->txstyle)));
                                  
            break;


            case 'shipment_date':
                $sdate = explode('-', preg_replace('/\s+/', '',$request->txship));

                $sfrom = Carbon::parse($sdate[0])->format('Y-m-d');
                $sto = Carbon::parse($sdate[1])->format('Y-m-d');


                $data = $data->whereBetween('shipment_date',[$sfrom,$sto])->orderBy('shipment_date','asc');
            break;


            case 'mo_created':
                $mdate = explode('-', preg_replace('/\s+/', '',$request->txmo));

                $mfrom = Carbon::parse($mdate[0])->format('Y-m-d 00:00:00');
                $mto = Carbon::parse($mdate[1])->format('Y-m-d 23:59:59');


                $data = $data->whereBetween('mo_created',[$mfrom,$mto])->orderBy('mo_created','asc');
            break;


            case 'sinkron_date':
                $mdate = explode('-', preg_replace('/\s+/', '',$request->txsink));

                $mfrom = Carbon::parse($mdate[0])->format('Y-m-d 00:00:00');
                $mto = Carbon::parse($mdate[1])->format('Y-m-d 23:59:59');


                // $data = $data->whereBetween('created_at',[$mfrom,$mto])
                //                             ->orWhereBetween('updated_at',[$mfrom,$mto])
                //                             ->orderBy('updated_at','desc')
                //                             ->orderBy('created_at','desc');


                $data= $data->where(function($query) use ($mfrom,$mto){
                                        $query->whereBetween('created_at',[$mfrom,$mto])
                                        ->orwhere(function($query) use ($mfrom,$mto){
                                            $query->whereBetween('updated_at',[$mfrom,$mto])
                                                    ->where('is_updated',true);
                                        });
                                    });
            break;


            case 'completed_date':
                $mdate = explode('-', preg_replace('/\s+/', '',$request->txcompl));

                $mfrom = Carbon::parse($mdate[0])->format('Y-m-d 00:00:00');
                $mto = Carbon::parse($mdate[1])->format('Y-m-d 23:59:59');


                $data = $data->whereBetween('completed_date',[$mfrom,$mto])->orderBy('completed_date','asc');
            break;


        }

        


        if (isset($filterby)) {
            $data = $data->where(function($query) use($filterby){
                                    $query->where('season','like','%'.$filterby)
                                        ->orWhere('documentno','like','%'.$filterby)
                                        ->orWhere('season','like','%'.$filterby)
                                        ->orWhere('style','like','%'.$filterby)
                                        ->orWhere('article','like','%'.$filterby)
                                        ->orWhere('size','like','%'.$filterby)
                                        ->orWhere('qty','like','%'.$filterby)
                                        ->orWhere('mo_pic','like','%'.$filterby)
                                        ->orWhere('color','like','%'.$filterby)
                                        ->orWhere('allocation','like','%'.$filterby);  
                                });
        }

   
         $i =1;
        $dates = Carbon::now()->format('Ymdhis');
        $filename = "Report_request_".$dates;

        $data_result = $data->orderBy('created_at','desc')->orderBy('updated_at','desc')->get();


        foreach ($data_result as $key => $data_results) {
            $data_results->no = $i++;
        }


        if ($data_result->count()>0) {
            // SampleReq::where('id',$id)->update(['is_csi'=>true,'updated_at'=>Carbon::now()]);

            return (new FastExcel($data_result))->download($filename.'.xlsx',function($row) use ($i){



                return
                [
                    '#'=>$row->no,
                    'Created'=>Carbon::parse($row->mo_created)->format('d-M-Y H:i'),
                    'Last Updated'=>(($row->created_at!=$row->updated_at) && $row->is_updated==true) ? Carbon::parse($row->mo_updated)->format('d-M-Y H:i') : "",
                    'Completed'=>Carbon::parse($row->completed_date)->format('d-M-Y H:i'),
                    'PIC MD'=>$row->mo_pic,
                    'Doc. No.'=>$row->documentno,
                    'Season'=>$row->season,
                    'Buyer'=>$row->buyer,
                    'Style'=>$row->style,
                    'Article'=>$row->article,
                    'Status'=>$row->status,
                    'Description'=>$row->product_category,
                    'Size'=>$row->size,
                    'Qty'=>$row->qty,
                    'Shipment'=>isset($row->shipment_date) ? Carbon::parse($row->shipment_date)->format('d-M-Y') :'',
                    'Note'=> str_replace(array("\n", "\r"),' ',$row->description),
                    'Allocation'=>$row->allocation,
                    'Marker Needed'=>$row->marker_need==true ? "YES" : "NO",
                    'Pattern FU'=>$row->pattern_fu,
                    'Pattern Remark'=>$row->pattern_remark,
                    'Pattern Status'=>$row->pattern_status,
                    'Pattern Date'=>isset($row->pattern_date) ? Carbon::parse($row->pattern_date)->format('d-M-Y') :'',
                    'Pattern Stage'=>$row->pattern_stage,
                    'Marker Length (inc)'=>$row->marker_length,
                    'Marker Plot Date'=>isset($row->date_plot) ? Carbon::parse($row->date_plot)->format('d-M-Y') :'',
                    'Marker Release'=>isset($row->marker_release) ? Carbon::parse($row->marker_release)->format('d-M-Y') :'',
                    'PIC Marker'=>$row->pic_marker,
                    'Pattern Maker'=>$row->pattern_marker,
                    'Marker Status'=>isset($row->marker_status) ? $row->marker_status : 'OPEN',
                    'Recever'=>$row->recever,
                    'Marker Recieved Date'=>isset($row->sample_recieve_marker) ? Carbon::parse($row->sample_recieve_marker)->format('d-M-Y') :''

                ];

            });
        }else{
            return response()->json('no data', 422);
        }
    }

    public function getSamReq(Request $req){
        $id = trim($req->id);

        $data = ns_report::where('id',$id)->first();
    
        return response()->json(['data'=>$data],200);
    }

    public function updateSampleReq(Request $req){
        $id = trim($req->id);
  
        try {
            DB::begintransaction();
                $cekinfo = DB::table('sr_info')->where('id',$id)->whereNull('deleted_at')->exists();
               
                if ($cekinfo) {
                    $upt = array(
                            'pattern_fu'=>trim($req->pattern_fu),
                            'pattern_remark'=>trim($req->pattern_remark),
                            'pattern_status'=>trim($req->pattern_status),
                            'pattern_date'=> isset($req->pattern_date) ? date_format(date_create($req->pattern_date),'Y-m-d') : null,
                            'marker_length'=>trim($req->marker_length),
                            'date_plot'=>isset($req->date_plot) ? date_format(date_create($req->date_plot),'Y-m-d') : null,
                            'marker_release'=>isset($req->marker_release) ? date_format(date_create($req->marker_release),'Y-m-d') : null,
                            'recever'=>trim($req->recever),
                            'pic_marker'=>trim($req->pic_marker),
                            'pattern_marker'=>trim($req->pattern_marker),
                            'pattern_stage'=>trim($req->pattern_stage),
                            'pattern_stage'=>trim($req->pattern_stage),
                            'marker_status'=>trim($req->marker_status),
                            'sample_recieve_marker'=>isset($req->sample_recieve_marker) ? date_format(date_create($req->sample_recieve_marker),'Y-m-d') : null,
                            'updated_at'=>Carbon::now()
                        );


                    DB::table('sr_info')->where('id',$id)->update($upt);


                }else{
                    $in = array(
                            'id'=>$id,
                            'pattern_fu'=>trim($req->pattern_fu),
                            'pattern_remark'=>trim($req->pattern_remark),
                            'pattern_status'=>trim($req->pattern_status),
                            'pattern_date'=> isset($req->pattern_date) ? date_format(date_create($req->pattern_date),'Y-m-d') : null,
                            'marker_length'=>trim($req->marker_length),
                            'date_plot'=>isset($req->date_plot) ? date_format(date_create($req->date_plot),'Y-m-d') : null,
                            'marker_release'=>isset($req->marker_release) ? date_format(date_create($req->marker_release),'Y-m-d') : null,
                            'recever'=>trim($req->recever),
                            'pic_marker'=>trim($req->pic_marker),
                            'pattern_marker'=>trim($req->pattern_marker),
                            'pattern_stage'=>trim($req->pattern_stage),
                            'pattern_stage'=>trim($req->pattern_stage),
                            'marker_status'=>trim($req->marker_status),
                            'sample_recieve_marker'=>isset($req->sample_recieve_marker) ? date_format(date_create($req->sample_recieve_marker),'Y-m-d') : null,
                            'updated_at'=>Carbon::now(),
                            'created_at'=>carbon::now()
                        );
                    // SrInfo::firstOrCreate($in);
                    DB::table('sr_info')->insert($in);
                }
            DB::commit();
            $data_response = [
                            'status' => 200,
                            'output' => 'Update Report Sample Request Success ! ! !'
                          ];
        } catch (Exception $ex) {
            DB::rollback();
            $message = $ex->getMessage();
            ErrorHandler::db($message);
            $data_response = [
                            'status' => 422,
                            'output' => 'Update Report Sample Request Failed ! ! !'.$message
                          ];
        }

        return response()->json(['data'=>$data_response]);
    }

    public function getNotif(){
        try {
            $path = storage_path('exports/Notifikasi_Sample_Request.xlsx');
            return response()->download($path);
        } catch (Exception $e) {
            $e->getMessage();
        }
    }
}
