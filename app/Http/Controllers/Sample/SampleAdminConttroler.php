<?php

namespace App\Http\Controllers\Sample;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use DB;
use Carbon\Carbon;
use DataTables;
use Excel;
use Illuminate\Support\Facades\Storage;

use App\Models\MasterMO;
use App\Models\GarmentId;
use App\Models\GarmentMove;

class SampleAdminConttroler extends Controller
{
    public function __construct(){
        ini_set('max_execution_time', 1800);
    }
    
    public function index(){
        return view('sample.index');
    }

    public function getMO(Request $request){
        $docno = $request->docno;
        $style = $request->s_style;
        $radio = $request->radio_search;
        
        if ($docno!=null || $style!=null) {
           switch ($radio) {
                case 'no_mo':
                        $data = MasterMO::where('documentno','LIKE','%'.$docno)
                                        ->whereNull('deleted_at');
                        break;

                    case 'style':
                        $data = MasterMO::where('style','LIKE','%'.$style)
                                        ->whereNull('deleted_at');
                        break;
                
                default:
                    $data = array();
                    break;
            }

            $data = $data->orderBy('documentno','desc');
        }else{
            $data = array();
        }

        
        return Datatables::of($data)
                        ->editColumn('docstatus',function($data){
                            if ($data->docstatus=="CO") {
                                $ret = '<span class="badge badge-success">COMPLETED</span>';
                            }else if ($data->docstatus=="VO") {
                                $ret = '<span class="badge badge-danger">VOID</span>';
                            }else if ($data->docstatus=="IP") {
                                $ret = '<span class="badge badge-success">IN PROGRESS</span>';
                            }else{
                                 $ret = '<span class="badge badge-success">IN PROGRESS</span>';
                            }

                            return $ret;
                        })
                        ->editColumn('qty',function($data){
                            if (!isset($data->topbot)) {
                                $sets = "";
                            }else if (isset($data->topbot)){
                                switch ($data->topbot) {
                                    case true:
                                        $sets = "SET";
                                        break;
                                    case false:
                                        $sets = "PCS";
                                        break;
                                }
                            }

                        

                            if ($data->is_reciept==true) {
                                $lab =  '<label class="label label-success label-rounded">'.$data->qty.'</label>';
                            }else{

                                $lab =  '<label class="label label-default label-rounded">'.$data->qty.'</label>';
                            }

                            return $lab." ".$sets;
                        })
                        ->addcolumn('action',function($data){
                          
                                switch ($data->topbot) {
                                    case true:
                                        $sets = "true";
                                        break;

                                    case false:
                                        $sets = "false";
                                        break;

                                    case null:
                                        $sets = "null";
                                        break;
                                    
                                }
                            

                            if ($data->is_reciept==false) {
                                return view('_action',[
                                            'model'=>$data,
                                            'reciept'=>$data->id.'$'.$data->sets
                                        ]);
                            }else{
                                return view('_action',[
                                            'model'=>$data,
                                            'reciept'=>$data->id.'$'.$data->sets,
                                            'detailMo'=>route('reciept.detailMO',[
                                                                    'moid'=>$data->id
                                                                ]),
                                        ]);
                            }
                            
                        })
                        ->rawColumns(['action','qty','docstatus'])
                        ->make(true);

    }

    public function getDetMo(Request $request){
        $id = $request->id;

        $data = MasterMO::where('id',$id)->first();

        return response()->json(['data'=>$data]);
    }

    public function reciept(Request $request){
        $id = $request->id;
        $qty = $request->qty;
        $ip = $request->ip();
        
        $merch = $request->merch;
        $tech = $request->tech;

        $fabric1 = $request->fabric1;
        $fabric2 = $request->fabric2;

        // $recieptdate = date_format(date_create($request->recieptdate),'Y-m-d');
        // $recieptdate = Carbon::createFromFormat('d F, Y', $request->recieptdate)->format('Y-m-d');
        // $factory_id = isset($request->factory_id) ? $request->factory_id : Auth::user()->factory_id;
        $remark = $request->remark;
        $sets = $request->sets;

        try {
            DB::begintransaction();
                $upMo = array(
                    // 'date_reciept'=>$recieptdate,
                    'topbot'=>isset($sets) ? true : false,
                    'remark'=>$remark,
                    'fabric1'=>$fabric1,
                    'fabric2'=>$fabric2,
                    'merch'=>strtoupper($merch),
                    'technician'=>strtoupper($tech),
                    'is_reciept'=>true
 
                );
 
                $moupdate = MasterMO::where('id',$id)->update($upMo);

                if ($moupdate) {
                    $this->_barcodeGarment($id,$sets,$qty,$ip);
                }

                $data_response = [
                            'status' => 200,
                            'output' => 'Reciept Garment Success . . .'
                          ];
            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            $message = $ex->getMessage();
            ErrorHandler::db($message);
            $data_response = [
                            'status' => 422,
                            'output' => 'Reciept Garment Field . . .'.$message
                          ];
        }
        
        return response()->json(['data'=>$data_response]);

        
        
    }

    static function random_code(){

        $max = DB::table('barcode_garment')->whereDate('created_at',Carbon::now()->format('Y-m-d'))->max('barcode_id');

        if ($max==null) {
            $last = 0;
        }else{
            $last = (int) substr($max, 8,5);
        }

      
        $date = Carbon::now()->format('Ymd');

        $barcode_id = $date.sprintf("%05s", $last+1);

        return $barcode_id;
    }

    





    private function garmentMovement($barcode_id,$proses,$ipaddress){
        if ($proses=="print") {
            $last = GarmentMove::where('barcode_id',$barcode_id)
                                    ->where('is_canceled',false)
                                    ->whereNull('deleted_at')
                                    ->first();
            $uplast = array('deleted_at'=>Carbon::now());

            $move = array(
                'barcode_id'=> $barcode_id,
                'locator_from'=>$last->locator_to,
                'locator_to'=>"reservation",
                'locator_id'=>null,
                'description'=>"barcode garment print",
                'is_canceled'=>false,
                'created_at'=>Carbon::now(),
                'user_id'=>Auth::user()->id,
                'ip_address'=>$ipaddress
            );
        }

        try {
            DB::begintransaction();
                GarmentMove::where('id',$last->id)->update($uplast);
                GarmentMove::firstorcreate($move);
            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            $message = $ex->getMessage();
            ErrorHandler::db($message);
        }

        return true;
    }

    public function templateUpload(){
        try {
            $path = storage_path('template/template_new.xlsx');
            return response()->download($path);
        } catch (Exception $e) {
            $e->getMessage();
        }
        
    }

    


    public function ajaxUpload(Request $request){
        $ip = $request->ip();
        
        $results = array();
        if($request->hasFile('file')){
          $extension = \File::extension($request->file->getClientOriginalName());

          if ($extension == "xlsx" || $extension == "xls") {
              $path = $request->file->getRealPath();

              $datax = \Excel::selectSheetsByIndex(0)->load($path)->get();
                
          }
        }
        

    
        try {
            $moid = array();
      
            foreach ($datax as $dt) {
                if (trim($dt->documentno)!=="" && trim($dt->date_reciept)!=="" && trim($dt->topbot)!=="" && trim($dt->fabric1) && trim($dt->t1_merch) && trim($dt->t1_techinician)) {

                    
                        $docmo = strtoupper(trim($dt->documentno));
                        $date = date_format(date_create(trim($dt->date_reciept)),'Y-m-d');
                        $topbot = trim($dt->topbot)=='YA' ? true : false;
                        $remark = trim($dt->remark);
                        $merch = trim($dt->t1_merch);
                        $tech = trim($dt->t1_techinician);
                        $fabric1 = trim($dt->fabric1);
                        $fabric2 = trim($dt->fabric2);

                        $cekmo = MasterMO::whereNull('deleted_at')->where('is_reciept',false)->where('documentno',$docmo)->first();

                        if ($cekmo) {
                            if ($topbot==true && $fabric1!="" && $fabric2!=="") {
                               $upMo = array(
                                        // 'date_reciept'=>$date,
                                        'topbot'=>$topbot,
                                        'remark'=>$remark,
                                        'fabric1'=>$fabric1,
                                        'fabric2'=>$fabric2,
                                        'merch'=>strtoupper($merch),
                                        'technician'=>strtoupper($tech),
                                        'is_reciept'=>true
                                    );
                            }else if($topbot==false && $fabric1!==""){
                                $upMo = array(
                                        // 'date_reciept'=>$date,
                                        'topbot'=>$topbot,
                                        'remark'=>$remark,
                                        'fabric1'=>$fabric1,
                                        'merch'=>strtoupper($merch),
                                        'technician'=>strtoupper($tech),
                                        'is_reciept'=>true
                                    );
                            }else{
                                throw new \Exception('Fabric Not Completed ! ! !');
                            }
                           
                            $moupdate = MasterMO::where('id',$cekmo->id)->update($upMo);
                            if ($moupdate) {
                                $moid[]=$cekmo->id;
                                $this->_barcodeGarment($cekmo->id,$topbot,$cekmo->qty,$ip,$date);
                               
                            }


                        }                       
                }
            }
               
                return response()->json(['data'=>$moid],200);
            db::commit();
        } catch (Exception $ex) {
             DB::rollback();
            $message = $ex->getMessage();
            ErrorHandler::db($message);

            return response()->json("Upload Reservation Garment Field ".$message,422);
        }        

    }

    private function _barcodeGarment($moid,$topbot,$qty,$ip,$reciept_date= null){
        
       
        try {
            db::begintransaction();
            // if ($topbot==true) {
            //     for ($i=1; $i <=$qty ; $i++) { 
            //         // TOP
            //         $barcode_idT = $this->random_code();
            //         $barcodeT = array(
            //             'barcode_id'=>$barcode_idT,
            //             'mo_id'=>$moid,
            //             'current_location'=>"preparation",
            //             'created_at'=>Carbon::now(),
            //             'sets'=>"TOP",
            //             'factory_id'=>Auth::user()->factory_id,
            //             'reciept_date'=>$reciept_date
            //         );
            //         GarmentId::firstorcreate($barcodeT);

            //         $moveT = array(
            //             'barcode_id'=> $barcode_idT,
            //             'locator_from'=>null,
            //             'locator_to'=>"preparation" ,
            //             'locator_id'=>null,
            //             'description'=>"generate barcode garment",
            //             'is_canceled'=>false,
            //             'created_at'=>Carbon::now(),
            //             'user_id'=>Auth::user()->id,
            //             'ip_address'=>$ip
            //         );
            //         GarmentMove::firstorcreate($moveT);

            //         // BOT
            //         $barcode_idB = $this->random_code();
            //         $barcodeB = array(
            //             'barcode_id'=>$barcode_idB,
            //             'mo_id'=>$moid,
            //             'current_location'=>"preparation",
            //             'created_at'=>Carbon::now(),
            //             'sets'=>"BOT",
            //             'factory_id'=>Auth::user()->factory_id,
            //             'reciept_date'=>$reciept_date
            //         );
            //         GarmentId::firstorcreate($barcodeB);

            //         $moveB = array(
            //             'barcode_id'=> $barcode_idB,
            //             'locator_from'=>null,
            //             'locator_to'=>"preparation" ,
            //             'locator_id'=>null,
            //             'description'=>"generate barcode garment",
            //             'is_canceled'=>false,
            //             'created_at'=>Carbon::now(),
            //             'user_id'=>Auth::user()->id,
            //             'ip_address'=>$ip
            //         );
            //         GarmentMove::firstorcreate($moveB);
            //     }
                
            // }else{
            //     for ($i=1; $i <=$qty ; $i++) { 
            //         $barcode_id = $this->random_code();
            //         $barcode = array(
            //             'barcode_id'=>$barcode_id,
            //             'mo_id'=>$moid,
            //             'current_location'=>"preparation",
            //             'created_at'=>Carbon::now(),
            //             'factory_id'=>Auth::user()->factory_id,
            //             'reciept_date'=>$reciept_date
            //         );
            //         GarmentId::firstorcreate($barcode);

            //         $move = array(
            //             'barcode_id'=> $barcode_id,
            //             'locator_from'=>null,
            //             'locator_to'=>"preparation" ,
            //             'locator_id'=>null,
            //             'description'=>"generate barcode garment",
            //             'is_canceled'=>false,
            //             'created_at'=>Carbon::now(),
            //             'user_id'=>Auth::user()->id,
            //             'ip_address'=>$ip
            //         );
            //         GarmentMove::firstorcreate($move);
            //     }
            // }

                if ($topbot==true) {
                    for ($i=1; $i <=$qty ; $i++) { 

                        //TOP
                        $cekT = GarmentId::where('mo_id',$moid)->where('sets','TOP')->whereNull('deleted_at')->count();

                        if ($cekT<=$qty) {
                            $barcode_idT = $this->random_code();
                            $barcodeT = array(
                                'barcode_id'=>$barcode_idT,
                                'mo_id'=>$moid,
                                'current_location'=>"preparation",
                                'created_at'=>Carbon::now(),
                                'sets'=>"TOP",
                                'factory_id'=>Auth::user()->factory_id,
                                'reciept_date'=>$reciept_date
                            );
                            GarmentId::firstorcreate($barcodeT);

                            $moveT = array(
                                'barcode_id'=> $barcode_idT,
                                'locator_from'=>null,
                                'locator_to'=>"preparation" ,
                                'locator_id'=>null,
                                'description'=>"generate barcode garment",
                                'is_canceled'=>false,
                                'created_at'=>Carbon::now(),
                                'user_id'=>Auth::user()->id,
                                'ip_address'=>$ip
                            );
                            GarmentMove::firstorcreate($moveT);
                        }
                   
                        

                        // BOT
                        $cekB = GarmentId::where('mo_id',$moid)->where('sets','BOT')->whereNull('deleted_at')->count();

                        if ($cekB<=$qty) {
                            $barcode_idB = $this->random_code();
                            $barcodeB = array(
                                'barcode_id'=>$barcode_idB,
                                'mo_id'=>$moid,
                                'current_location'=>"preparation",
                                'created_at'=>Carbon::now(),
                                'sets'=>"BOT",
                                'factory_id'=>Auth::user()->factory_id,
                                'reciept_date'=>$reciept_date
                            );
                            GarmentId::firstorcreate($barcodeB);

                            $moveB = array(
                                'barcode_id'=> $barcode_idB,
                                'locator_from'=>null,
                                'locator_to'=>"preparation" ,
                                'locator_id'=>null,
                                'description'=>"generate barcode garment",
                                'is_canceled'=>false,
                                'created_at'=>Carbon::now(),
                                'user_id'=>Auth::user()->id,
                                'ip_address'=>$ip
                            );
                            GarmentMove::firstorcreate($moveB);
                        }
                        
                    }
                    
                }else{
                    for ($i=1; $i <=$qty ; $i++) { 

                        $cekQ = GarmentId::where('mo_id',$moid)->whereNull('sets')->whereNull('deleted_at')->count();
                        if ($cekQ<=$qty) {
                            $barcode_id = $this->random_code();
                            $barcode = array(
                                'barcode_id'=>$barcode_id,
                                'mo_id'=>$moid,
                                'current_location'=>"preparation",
                                'created_at'=>Carbon::now(),
                                'factory_id'=>Auth::user()->factory_id,
                                'reciept_date'=>$reciept_date
                            );
                            GarmentId::firstorcreate($barcode);

                            $move = array(
                                'barcode_id'=> $barcode_id,
                                'locator_from'=>null,
                                'locator_to'=>"preparation" ,
                                'locator_id'=>null,
                                'description'=>"generate barcode garment",
                                'is_canceled'=>false,
                                'created_at'=>Carbon::now(),
                                'user_id'=>Auth::user()->id,
                                'ip_address'=>$ip
                            );
                            GarmentMove::firstorcreate($move);
                        }
                        
                    }
                }
            db::commit();
        } catch (Exception $ex) {
            DB::rollback();
            $message = $ex->getMessage();
        }
        
        return true;
    }


    public function getDataUpload(Request $request){
        $moid = $request->moid;

        if ($moid!=null) {
            $data = MasterMO::whereIn('id',$moid);
        }else{
            $data = array();
        }
        


        return Datatables::of($data)
                        ->addcolumn('checkbox',function($data){
                            return '<input type="checkbox" class="moid" name="selector[]" id="Inputselector" value="'.$data->id.'">';
                        })
                        ->editColumn('docstatus',function($data){
                            if ($data->docstatus=="CO") {
                                $ret = '<span class="badge badge-success">COMPLETED</span>';
                            }else if ($data->docstatus=="VO") {
                                $ret = '<span class="badge badge-danger">VOID</span>';
                            }else if ($data->docstatus=="IP") {
                                $ret = '<span class="badge badge-success">IN PROGRESS</span>';
                            }

                            return $ret;
                        })
                        ->editColumn('qty',function($data){
                            if (!isset($data->topbot)) {
                                $sets = "";
                            }else if (isset($data->topbot)){
                                switch ($data->topbot) {
                                    case true:
                                        $sets = "SET";
                                        break;
                                    case false:
                                        $sets = "PCS";
                                        break;
                                }
                            }

                        

                            if ($data->is_reciept==true) {
                                $lab =  '<label class="label label-success label-rounded">'.$data->qty.'</label>';
                            }else{

                                $lab =  '<label class="label label-default label-rounded">'.$data->qty.'</label>';
                            }

                            return $lab." ".$sets;
                        })
                        ->rawColumns(['qty','docstatus','checkbox'])
                        ->make(true);
    }

    public function printUploadBarcode(Request $request){
        $ip = $request->ip();
        $moid = explode(";",$request->data);
        $tempt = $request->temp;

        
        $data = DB::table('barcode_garment')
                        ->join('master_mo','barcode_garment.mo_id','=','master_mo.id')
                        ->whereIn('master_mo.id',$moid)
                        ->whereNull('barcode_garment.deleted_at')
                        ->whereNull('master_mo.deleted_at')
                        ->whereNotNull('barcode_garment.reciept_date')
                        ->select('barcode_garment.barcode_id','barcode_garment.reciept_date','master_mo.documentno','master_mo.season','master_mo.style','master_mo.article','master_mo.size','master_mo.status','master_mo.brand','master_mo.product_name','master_mo.color','barcode_garment.is_print','barcode_garment.sets','master_mo.fabric1','master_mo.fabric2','master_mo.merch','master_mo.technician')->get();
            foreach ($data as $dt) {
                
               
                if ($dt->is_print==false) {
                    $upgar=array(
                        'is_print'=>true,
                        'current_location'=>"reservation",
                        'updated_at'=>Carbon::now()
                    );
                    GarmentId::where('barcode_id',$dt->barcode_id)->update($upgar);
                    $this->garmentMovement($dt->barcode_id,"print",$ip);
                }
            }

        switch ($tempt) {
            case 'sealing':
                $size = array(0, 0, 612.00, 1008.00);
                $orientation='potrait';
                $template='sealing';
                break;

            case 'aoi_tempt':
                $size = 'A4';
                $orientation='potrait';
                $template='barcode_garment';
                break;

            case 'porto':
                $size = array(0, 0, 612.00, 1008.00);
                $orientation='potrait';
                $template='porto';
                break;

            case 'wash_test':
                $size = array(0, 0, 612.00, 1008.00);
                $orientation='potrait';
                $template='wash_test';
                break;
            
            case 'counter_sample':
                $size = array(0, 0, 612.00, 1008.00);
                $orientation='potrait';
                $template='counter_sample';
                break;

        }
        

        $filename = "Garment_card";
        $pdf = \PDF::loadView('sample.template.'.$template,['data'=>$data])->setPaper($size,$orientation);
        return $pdf->stream($filename);
    }



    public function printBarcode(Request $request){
        $barcode_id = explode(";", trim($request->data));
        $ip = $request->ip();
        $tempt = $request->tempt;

        $data = DB::table('barcode_garment')
                        ->join('master_mo','barcode_garment.mo_id','=','master_mo.id')
                        ->whereIn('barcode_garment.barcode_id',$barcode_id)
                        ->whereNull('barcode_garment.deleted_at')
                        ->whereNull('master_mo.deleted_at')
                        ->whereNotNull('barcode_garment.reciept_date')
                        ->select('barcode_garment.barcode_id','barcode_garment.reciept_date','master_mo.documentno','master_mo.season','master_mo.style','master_mo.article','master_mo.size','master_mo.status','master_mo.brand','master_mo.product_name','master_mo.color','barcode_garment.is_print','barcode_garment.sets','master_mo.fabric1','master_mo.fabric2','master_mo.merch','master_mo.technician')->get();
            foreach ($data as $dt) {
                
               
                if ($dt->is_print==false) {
                    $upgar=array(
                        'is_print'=>true,
                        'current_location'=>"reservation",
                        'updated_at'=>Carbon::now()
                    );
                    GarmentId::where('barcode_id',$dt->barcode_id)->update($upgar);
                    $this->garmentMovement($dt->barcode_id,"print",$ip);
                }
            }

        switch ($tempt) {
            case 'sealing':
                $size = array(0, 0, 612.00, 1008.00);
                $orientation='potrait';
                $template='sealing';
                break;

            case 'aoi_tempt':
                $size = 'A4';
                $orientation='potrait';
                $template='barcode_garment';
                break;

            case 'porto':
                $size = array(0, 0, 612.00, 1008.00);
                $orientation='potrait';
                $template='porto';
                break;

            case 'wash_test':
                $size = array(0, 0, 612.00, 1008.00);
                $orientation='potrait';
                $template='wash_test';
                break;

            case 'counter_sample':
                $size = array(0, 0, 612.00, 1008.00);
                $orientation='potrait';
                $template='counter_sample';
                break;
            

        }
        
        $filename = "Garment_card_".$request->data;
        $pdf = \PDF::loadView('sample.template.'.$template,['data'=>$data])->setPaper($size,$orientation);
        return $pdf->stream($filename);
            // return view('sample.template.sealing')->with('data',$data);

       

    }

    public function detailMO(Request $request){
        $moid = $request->moid; 

        $data = MasterMO::where('id',$moid)->whereNull('deleted_at')->first();

        return view('sample.detail')->with('moid',$data->id)->with('docno',$data->documentno);
    }

    public function ajaxGetDetail(Request $request){
        $moid = $request->moid;

        $data = DB::table('barcode_garment')
                        ->join('master_mo','barcode_garment.mo_id','=','master_mo.id')
                        ->whereNull('barcode_garment.deleted_at')
                        ->whereNull('master_mo.deleted_at')
                        // ->where('barcode_garment.factory_id',Auth::user()->factory_id)
                        ->where('master_mo.id',$moid);

        return Datatables::of($data)
                         ->addcolumn('checkbox',function($data){
                            // return '<input type="checkbox" class="barcode" name="selector[]" id="Inputselector" value="'.$data->barcode_id.'">';
                            return '<input type="checkbox"  class="barcode" name="selector[]" id="Inputselector_'.$data->barcode_id.'" value="'.$data->barcode_id.'" data-barcode="'.$data->barcode_id.'" data-reciept="'.$data->reciept_date.'" onclick="check(this);">';
                        })
                        ->addcolumn('item',function($data){
                            // return $data->season."-".$data->style."-".$data->article."-".$data->size." <b> (".$data->sets.") </b>";
                            return $data->season."-".$data->style."-".$data->article."-".$data->size;
                        })
                        ->addcolumn('location',function($data){
                            $loct = strtoupper($data->current_location);
                            if ($data->storage==true) {
                                $dr = DB::table('garment_movements')
                                                ->join('locator','garment_movements.locator_id','=','locator.id')
                                                ->where('garment_movements.barcode_id',$data->barcode_id)
                                                ->where('garment_movements.locator_to',$data->current_location)
                                                ->where('garment_movements.is_canceled',false)
                                                ->whereNull('garment_movements.deleted_at')
                                                ->select('locator.name')->first();
                                $rack = $loct.' <span class="badge badge-success">'.$dr->name.'</span>';
                            }else{
                                $rack = $loct;
                            }

                            return $rack;

                           
                        })
                        ->editColumn('barcode_id',function($data){
                            $fact = DB::table('factory')->where('id',$data->factory_id)->whereNull('deleted_at')->first();
                        
                            return $data->barcode_id." <b>".strtoupper($data->sets)."</b> ".'<span class="badge badge-'.$fact->fac_col.'">'.$fact->factory_name.'</span>';
                        })
                        // ->editColumn('factory_id',function($data){
                        //     $fact = DB::table('factory')->where('id',$data->factory_id)->whereNull('deleted_at')->first();
                        //     return '<span class="badge badge-info">'.$fact->factory_name.'</span>';
                        // })
                        ->editColumn('reciept_date',function($data){
                            $rd = isset($data->reciept_date) ? date_format(date_create($data->reciept_date),'d-m-Y') : null;
                            return '<input type="text" class="form-control input_date" placeholder="'.Carbon::now()->format('d-m-Y').'" data-barcode="'.$data->barcode_id.'" data-dater ="'.$rd.'" value="'.$rd.'">';

                            // return $rd;
                        })
                        ->rawColumns(['checkbox','item','location','barcode_id','reciept_date'])->make(true);
    }

    public function ajaxRecieptDate(Request $request){
        $barcode_id = $request->barcode_id;
        $reciept_date = trim($request->reciept_date);

        try {
            DB::begintransaction();
                $upDate = array(
                    'reciept_date'=>Carbon::parse($reciept_date)->format('Y-m-d'),
                    'updated_at'=>Carbon::now()
 
                );
 
                GarmentId::where('barcode_id',$barcode_id)->update($upDate);

                $data_response = [
                            'status' => 200,
                            'output' => 'Update Date Reciept Success . . .'
                          ];
            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            $message = $ex->getMessage();
            ErrorHandler::db($message);
            $data_response = [
                            'status' => 500,
                            'output' => 'Update Date Reciept Field . . .'.$message
                          ];
        }
        
        return response()->json(['data'=>$data_response]);
    }

    public function deleteReciept(){
        return view('sample.delete_reciept');
    }

    public function ajaxGetDelete(Request $request){
        $par = $request->all();

        if ($request->request!=null) {
            $this->validate($request,[
                'docno'=>'required|min:4',
            ]);
        $data = DB::table('ns_data_bal_reciept')->where('documentno','LIKE','%'.$par['docno']);
        }

        return Datatables::of($data)
                        ->addcolumn('item',function($data){
                            return $data->season.'-'.$data->style.'-'.$data->article.'-'.$data->size;
                        })
                        ->addcolumn('qty',function($data){
                            $count = GarmentId::whereNull('deleted_at')->where('mo_id',$data->id)->count();

                            return $count;
                        })
                        ->addcolumn('action',function($data){
                            return '<button class="btn btn-default" id="btn-delete" data-id="'.$data->id.'" onclick="deletMo(this);">DELETE</button>';
                        })
                        ->rawColumns(['item','qty','action'])->make(true);
    }

    public function ajaxDeleteMo(Request $request){
        $id = $request->id;

        
            $mo = MasterMO::where('id',$id)->whereNull('deleted_at')->first();
        try {

            DB::begintransaction();
                $insert = array(
                        'documentno'=>$mo->documentno,
                        'season'=>$mo->season,
                        'style'=>$mo->style,
                        'article'=>$mo->article,
                        'size'=>$mo->size,
                        'brand'=>$mo->brand,
                        'qty'=>(int) $mo->qty,
                        'uom'=>$mo->uom,
                        'status'=>$mo->status,
                        'product_name'=>$mo->product_name,
                        'product_category'=>$mo->product_category,
                        'color'=>$mo->color,
                        'mo_created'=>$mo->mo_created,
                        'mo_updated'=>$mo->mo_updated,
                        'created_at'=>Carbon::now(),
                        'docstatus'=>$mo->docstatus,
                        'topbot'=>false
                    );


                $GarmentId = GarmentId::whereNull('deleted_at')->where('mo_id',$id)->get();
                foreach ($GarmentId as $gi) {
                    $upmv = GarmentMove::where('barcode_id',$gi->barcode_id)->whereNull('deleted_at')->update(['deleted_at'=>Carbon::now()]);
                    if ($upmv) {
                       GarmentId::whereNull('deleted_at')->where('barcode_id',$gi->barcode_id)->update(['deleted_at'=>Carbon::now()]);
                    }
                }

                MasterMO::where('id',$id)->update(['deleted_at'=>Carbon::now()]);

                MasterMO::firstOrCreate($insert);

                $data_response = [
                            'status' => 200,
                            'output' => 'Delete MO success . . .'
                          ];
            DB::commit();
        } catch (Exception $e) {
            DB::rollback();
            $message = $ex->getMessage();
            ErrorHandler::db($message);
            $data_response = [
                            'status' => 422,
                            'output' => 'Delete MO Field . . .'.$message
                          ];
        }
        
        return response()->json(['data'=>$data_response]);
    }

    public function syncDoc(){
        return view('sample.sync');
    }

    public function ajaxGetDocSync(Request $request){
        if ($request->docno!=null) {
            $data = DB::connection('erp')->table('bw_dss_mo_development')
                                                ->where('documentno','LIKE','%'.$request->docno.'%')
                                                ->orderBy('created','asc')
                                                ->orderBy('updated','asc');
        }else{
            $data = array();
        }
        
        
        return Datatables::of($data)
                            ->addcolumn('action',function($data){
                                return '<button class="btn btn-primary" data-doc="'.$data->documentno.'" onclick="syncdoc(this);"><span class="icon-sync"></span></button>';
                            })
                            ->rawColumns(['action'])
                            ->make(true);
    }

    public function syncDocument(Request $request){
        $erp = DB::connection('erp')->table('bw_dss_mo_development')
                                                ->where('documentno',$request->docno)
                                                ->whereIN('docstatus',['CO','VO'])
                                                ->orderBy('created','asc')
                                                ->orderBy('updated','asc')->get();
        try {
            DB::begintransaction();
                foreach ($erp as $erp) {
                    $mastermo = MasterMO::where('documentno',$erp->documentno)->whereNull('deleted_at')->first();
                            
                    if ($mastermo==null) {
                        $insert = array(
                            'documentno'=>$erp->documentno,
                            'season'=>$erp->season,
                            'style'=>$erp->style,
                            'article'=>$erp->article,
                            'size'=>$erp->size,
                            'brand'=>$erp->brand,
                            'qty'=>(int) $erp->qty,
                            'uom'=>$erp->uom,
                            'status'=>$erp->status,
                            'product_name'=>$erp->product_name,
                            'product_category'=>$erp->product_category,
                            'color'=>trim(explode('(',$erp->colorway)[0]),
                            'mo_created'=>$erp->created,
                            'mo_updated'=>$erp->updated,
                            'created_at'=>Carbon::now(),
                            'docstatus'=>$erp->docstatus
                    );

                        MasterMO::firstOrCreate($insert);

                    }else{
                        $update = array(
                            'documentno'=>$erp->documentno,
                            'season'=>$erp->season,
                            'style'=>$erp->style,
                            'article'=>$erp->article,
                            'size'=>$erp->size,
                            'brand'=>$erp->brand,
                            'qty'=>(int) $erp->qty,
                            'uom'=>$erp->uom,
                            'status'=>$erp->status,
                            'product_name'=>$erp->product_name,
                            'product_category'=>$erp->product_category,
                            'color'=>trim(explode('(',$erp->colorway)[0]),
                            'mo_created'=>$erp->created,
                            'mo_updated'=>$erp->updated,
                            'updated_at'=>Carbon::now(),
                            'docstatus'=>$erp->docstatus
                        );

                        MasterMO::where('id',$mastermo->id)->update($update);
                    }
                }
                
            DB::commit();
            $data_response = [
                            'status' => 200,
                            'output' => 'Sync Success ! ! ! '
                          ];
        } catch (Exception $ex) {
            DB::rollback();
                  $message = $ex->getMessage();
                  ErrorHandler::db($message);
            $data_response = [
                            'status' => 422,
                            'output' => 'Sync Field . . .'.$message
                          ];
        }

        return response()->json(['data'=>$data_response]);
    }
}
