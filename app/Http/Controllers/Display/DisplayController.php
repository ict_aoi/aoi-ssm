<?php

namespace App\Http\Controllers\Display;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Carbon\Carbon;
use DataTables;


class DisplayController extends Controller
{
    public function indexMd($factory_id){
        $data = $this->_getDatamd($factory_id);
        
        return view('display.dashboard_md')->with('factory_id',$factory_id)
                                        ->with('data',$data);
    }

    private function _getDatamd($factory_id){
        $list = [];
        DB::statement('REFRESH MATERIALIZED VIEW ns_md_borrowing');
        

    
        $data = DB::table('ns_md_borrowing')->where('factory_id',$factory_id)->orderBy('dateborrow','asc')->get();

        foreach ($data as $dt) {
            $expd = Carbon::parse($dt->dateborrow)->addDays($dt->expired)->format('Y-m-d');
            $now =  Carbon::now()->format('Y-m-d');
            
            if ($now <$expd) {
                $status = false;
            }else{
                $status = true;
            }
            $ld = array(
                'style'=>$dt->style,
                'dept_name'=>strtoupper($dt->dept_name),
                'date_bor'=>Carbon::parse($dt->dateborrow)->format('d-M-Y'),
                'qty'=>$dt->qty,
                'borrower'=>strtoupper($dt->borrower),
                'expired'=>Carbon::parse($dt->dateborrow)->addDays($dt->expired)->format('d-M-Y'),
                'status'=>$status
            );

            $list[]=$ld;
        }


        return $list;
    }

    public function indexqc($factory_id){
        $data = $this->_getDataqc($factory_id);
        
        return view('display.dashboard_qc')->with('factory_id',$factory_id)
                                        ->with('data',$data);
    }

    private function _getDataqc($factory_id){
        $list = [];
        DB::statement('REFRESH MATERIALIZED VIEW ns_qc_borrowing');
        

    
        $data = DB::table('ns_qc_borrowing')->where('factory_id',$factory_id)->orderBy('borrowdate','asc')->get();

        foreach ($data as $dt) {
            $bordate = Carbon::parse($dt->borrowdate)->format('Y-m-d');
            $prodate =   Carbon::parse($dt->promise_return)->format('Y-m-d');
            
            if ($bordate <$prodate) {
                $status = false;
            }else{
                $status = true;
            }
            $ld = array(
                'style'=>$dt->style,
                'dept_name'=>$dt->line!="-" ? $dt->dept_name." (".strtoupper($dt->line).")" : $dt->dept_name,
                'date_bor'=>Carbon::parse($dt->borrowdate)->format('d-M-Y'),
                'qty'=>$dt->qty,
                'borrower'=>strtoupper($dt->borrower),
                'promise'=>isset($dt->promise_return) ? Carbon::parse($dt->promise_return)->format('d-M-Y') : "",
                'status'=>$status
            );

            $list[]=$ld;
        }


        return $list;
    }
}
