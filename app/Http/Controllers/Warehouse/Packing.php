<?php

namespace App\Http\Controllers\Warehouse;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use DB;
use Carbon\Carbon;
use DataTables;
use Excel;

use App\Models\Package;
use App\Models\PackDetail;
use App\Models\GarmentId;
use App\Models\GarmentMove;

class Packing extends Controller
{
    //scanpack

    public function scanPack(){
        return view('warehouse.scanpack.scanpack');
    }

    public function ajaxGetCarton(Request $request){
        $barcode = trim($request->barcode);

        $ctn = Package::where('carton_id',$barcode)->whereNull('deleted_at')->first();
        $grm = $this->cekGrmCtn($barcode);

        return response()->json(['ctn_id'=>$ctn->carton_id,'grm'=>$grm],200);
    }

    public function ajaxScan(Request $request){
        $barcode = trim($request->bargar);
        $ctn_id = trim($request->ctn);
        $ip = $request->ip();

        $chek = $this->cek_barcode($barcode);
        if (!$chek) {
            return response()->json("Barcode garment not found",422);
        }else if($chek->current_location!='storage_md' ){
            return response()->json("Barcode location on ".strtoupper($chek->current_location),422);
        }else{

            $cekPL = $this->cekPL($chek->season,$chek->style,$chek->article,$chek->size,$ctn_id);

            if ($cekPL['status']==422) {
               return response()->json($cekPL['output'],422);
            }else{

                $data = array(
                            'barcode_id'=>$cek_barcode->barcode_id,
                            'docno'=>$cek_barcode->decumentno ,
                            'season'=>$cek_barcode->season ,
                            'style'=>$cek_barcode->style ,
                            'article'=>$cek_barcode->article ,
                            'size'=>$cek_barcode->size ,
                            'status'=>$cek_barcode->status ,
                            'carton_id'=>$ctn_id
                        );

                try {
                    DB::begintransaction();
                        $up = array(
                            'carton_id'=>$ctn_id,
                            'updated_at'=>Carbon::now()
                        );

                        GarmentId::where('barcode_id',$barcode)->update($up);
                    DB::commit();
                } catch (Exception $ex) {
                    DB::rollback();
                    $message = $ex->getMessage();
                    ErrorHandler::db($message);
                }

                 return view('warehouse.scanpack.ajax_list')->with('data',$data);
            }
        }

    }

    private function cekGrmCtn($carton_id){
        $grm = DB::table('barcode_garment')->where('carton_id',$carton_id)->whereNull('deleted_at')->count();

        return $grm;
    }

    private function cek_barcode($barcode){
        $dt_bar = DB::table('barcode_garment')
                    ->join('master_mo','barcode_garment.mo_id','=','master_mo.id')
                    ->leftjoin('garment_movements','barcode_garment.barcode_id','=','garment_movements.barcode_id')
                    ->where('barcode_garment.barcode_id',$barcode)
                    ->where('barcode_garment.factory_id',Auth::user()->factory_id)
                    ->whereNull('barcode_garment.deleted_at')
                    ->whereNull('master_mo.deleted_at')
                    ->whereNull('garment_movements.deleted_at')
                    ->where('garment_movements.is_canceled',false)
                    ->select('barcode_garment.barcode_id',
                                'barcode_garment.mo_id',
                                'barcode_garment.current_location',
                                'master_mo.documentno',
                                'master_mo.season',
                                'master_mo.style','master_mo.article',
                                'master_mo.size',
                                'master_mo.status',
                                'master_mo.id',
                                'barcode_garment.checkin_md',
                                'barcode_garment.storage',
                                'barcode_garment.borrow',
                                'garment_movements.locator_id')->first();

        return $dt_bar;
    }

    private function cekPL($season,$style,$article,$size,$ctn){
        $packdet = PackDetail::where('carton_id',$ctn)
                                ->where('season',$season)
                                ->where('style',$style)
                                ->where('article',$article)
                                ->where('size',$size)
                                ->first();

        $inner = GarmentId::where('carton_id',$ctn)->whereNull('deleted_at')->count();

        if (isset($packdet->qty)>$inner) {
            $ret = ['status'=>200,'output'=>"OK"];
        }else if (isset($packdet->qty)<=$inner) {
            $ret = ['status'=>422,'output'=>"Item Completed ! ! !"];
        }else{
            $ret = ['status'=>422,'output'=>"Item not for this carton ! ! !"];
        }
        
        return $ret;
    }


    //stuffing
    public function stuffing(){
        return view('warehouse.stuffing.stuffing');
    }

    public function ajaxScanStuffing(Request $request){
        $carton = trim($request->ctn);
        $ip = $request->ip();
        $cek = DB::table('package')
                            ->join('barcode_garment','package.carton_id','=','barcode_garment.carton_id')
                            ->join('invoices','package.invoice_id','=','invoices.id')
                            ->whereNull('package.deleted_at')
                            ->whereNull('barcode_garment.deleted_at')
                            ->where('package.carton_id',$carton)
                            ->groupBy('package.carton_id')
                            ->groupBy('package.location')
                            ->groupBy('package.status')
                            ->groupBy('invoices.invoice')
                            ->groupBy('invoices.country_to')
                            ->groupBy('invoices.customer_number')
                            ->groupBy('invoices.document_type')
                            ->select('package.carton_id','package.location','package.status','invoices.invoice','invoices.country_to','invoices.customer_number','invoices.document_type')
                            ->first();
        if ($cek==null) {
            return response()->json("Carton not found / not fill ! ! !",422);
        }

        $data = array(
                    'carton_id'=>$cek->carton_id,
                    'invoice'=>$cek->invoice,
                    'custno'=>$cek->customer_number,
                    'country'=>$cek->country_to,
                    'doctype'=>$cek->document_type,
                    'location'=>"Stuffing",
                    'status'=>"Completed"
                );
        try {
            DB::begintransaction();
                $uppack = array(
                                'location'=>"shipment",
                                'status'=>'completed',
                                'updated_at'=>Carbon::now()
                            );
                $update = Package::where('carton_id',$carton)->update($uppack);

                if ($upadate) {
                    $bargar = GarmentId::where('carton_id',$carton)->whereNull('deleted_at')->get();

                    foreach ($bargar as $bg) {
                        $this->updateGarment($bg->barcode_id,$carton,$ip);
                    }
                }
            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            $message = $ex->getMessage();
            ErrorHandler::db($message);
        }

        return view('warehouse.stuffing.ajax_list')->with('data',$data);
    }

    private function updateGarment($barcode,$ctn,$ip){
        $last = GarmentMove::where('barcode_id',$barcode)->whereNull('deleted_at')->where('is_canceled',false)->first();

        try {
            DB::begintransaction();
                $move = array(
                            'barcode_id'=>$barcode,
                            'locator_from'=>$last->locator_to,
                            'description'=>"stuffing with carton ".$ctn,
                            'created_at'=>carbon::now(),
                            'user_id'=>auth::user()->id,
                            'ip_address'=>$ip
                        );

                GarmentId::where('barcode_id',$barcode)->update(['current_location'=>"shipment",'updated_at'=>Carbon::now()]);
                GarmentMove::where('barcode_id',$barcode)->update(['deleted_at'=>Carbon::now()]);
                GarmentMove::firstorcreate($move);
            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            $message = $ex->getMessage();
            ErrorHandler::db($message);
        }

        return true;
    }
}
