<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use DB;
use Carbon\Carbon;
use DataTables;

use App\User;
use App\Models\MasterMO;
use App\Models\GarmentId;

class DashboardController extends Controller
{
    public function index(){
        $factory = DB::table('factory')->whereNull('deleted_at')->get();
        return view('dashboard.dashboard')->with('factory',$factory);
    }

    public function getData(Request $request){
        $docno = trim($request->docno);
        $style = trim($request->style);
        $radio = $request->radio_search;
        $factory_id = $request->factory;


        if ($docno!=null || $style!=null) {
           switch ($radio) {
                case 'no_mo':
                        $data = DB::table('ns_dashboard')
                            ->where('documentno','LIKE','%'.$docno)
                            ->where('factory_id',$factory_id);
                        break;

                    case 'style':
                        $data = DB::table('ns_dashboard')
                            ->where('style','LIKE','%'.$style)
                            ->where('factory_id',$factory_id);
                        break;
                
                default:
                    $data = array();
                    break;
            }

            $data = $data->orderBy('documentno','desc');
        }else{
            $data = array();
        }


        return Datatables::of($data)
                        ->editColumn('qty',function($data){
                                $qty = GarmentId::where('mo_id',$data->id)->where('factory_id',$data->factory_id)->whereNull('deleted_at')->count();
                                return $qty;
                        })
                        ->addColumn('item_code',function($data){
                            $item_code = $data->season." - ".$data->style." - ".$data->article." - ".$data->size;

                            return $item_code;
                        })
                        ->addColumn('reserv',function($data){
                            $pre = $this->countBarcode($data->id,"preparation",$data->factory_id);
                            $res = $this->countBarcode($data->id,"reservation",$data->factory_id);

                            if ($pre==0 && $res==0) {
                                return '<span class="badge badge-default">COMPLETED</span>';
                            }else{
                                return '<span class="badge badge-primary">'.$pre.'</span> <span class="badge badge-success">'.$res.'</span>';
                            }
                        })
                        ->addColumn('md',function($data){
                            
                            $check = $this->countBarcode($data->id,"checkin_md",$data->factory_id);
                            $store = $this->countBarcode($data->id,"storage_md",$data->factory_id);
                            
                            if ($check==0 && $store==0) {
                                return '<span class="badge badge-default">0</span>';
                            }else{
                                return '<span class="badge badge-primary">'.$check.'</span> <span class="badge badge-success">'.$store.'</span>';
                            }
                        })
                        ->addColumn('qc',function($data){
                            
                            $qc = $this->countBarcode($data->id,"qc",$data->factory_id);
                            $store = $this->countBarcode($data->id,"qc_storage",$data->factory_id);
                            
                            if ($qc==0 && $store==0) {
                                return '<span class="badge badge-default">0</span>';
                            }else{
                                return '<span class="badge badge-primary">'.$qc.'</span> <span class="badge badge-success">'.$store.'</span>';
                            }
                        })
                        ->addColumn('lab',function($data){
                            
                            $lab = $this->countBarcode($data->id,"lab",$data->factory_id);
                            $store = $this->countBarcode($data->id,"lab_storage",$data->factory_id);
                            
                            if ($lab==0 && $store==0) {
                                return '<span class="badge badge-default">0</span>';
                            }else{
                                return '<span class="badge badge-primary">'.$lab.'</span> <span class="badge badge-success">'.$store.'</span>';
                            }
                        })
                        ->addColumn('marker',function($data){
                            
                            $marker = $this->countBarcode($data->id,"marker",$data->factory_id);
                            $store = $this->countBarcode($data->id,"marker_storage",$data->factory_id);
                            
                            
                            if ($marker==0 && $store==0) {
                                return '<span class="badge badge-default">0</span>';
                            }else{
                                return '<span class="badge badge-primary">'.$marker.'</span> <span class="badge badge-success">'.$store.'</span>';
                            }
                        })
                        ->addColumn('cutting',function($data){
                            
                            $cutting = $this->countBarcode($data->id,"cutting",$data->factory_id);
                            
                            if ($cutting==0) {
                                return '<span class="badge badge-default">0</span>';
                            }else{
                                return '<span class="badge badge-primary">'.$cutting.'</span>';
                            }
                        })
                        ->addColumn('pack_sample',function($data){
                            
                            $packsample = $this->countBarcode($data->id,"packsample",$data->factory_id);
                            
                            if ($packsample==0) {
                                return '<span class="badge badge-default">0</span>';
                            }else{
                                return '<span class="badge badge-primary">'.$packsample.'</span>';
                            }
                        })
                        ->addColumn('ie',function($data){
                            
                            $ie = $this->countBarcode($data->id,"ie",$data->factory_id);
                            
                            if ($ie==0) {
                                return '<span class="badge badge-default">0</span>';
                            }else{
                                return '<span class="badge badge-primary">'.$ie.'</span>';
                            }
                        })
                        ->addColumn('ppc',function($data){
                            
                            $ppc = $this->countBarcode($data->id,"ppc",$data->factory_id);
                            
                            if ($ppc==0) {
                                return '<span class="badge badge-default">0</span>';
                            }else{
                                return '<span class="badge badge-primary">'.$ppc.'</span>';
                            }
                        })
                        ->addColumn('ppa',function($data){
                            
                            $ppa = $this->countBarcode($data->id,"ppa",$data->factory_id);
                            
                            if ($ppa==0) {
                                return '<span class="badge badge-default">0</span>';
                            }else{
                                return '<span class="badge badge-primary">'.$ppa.'</span>';
                            }
                        })
                        ->addColumn('sewing',function($data){
                            
                            $sewing = $this->countBarcode($data->id,"sewing",$data->factory_id);
                            
                            if ($sewing==0) {
                                return '<span class="badge badge-default">0</span>';
                            }else{
                                return '<span class="badge badge-primary">'.$sewing.'</span>';
                            }
                        })
                        ->addColumn('sample_room',function($data){
                            
                            $sample_room = $this->countBarcode($data->id,"sample_room",$data->factory_id);
                            
                            if ($sample_room==0) {
                                return '<span class="badge badge-default">0</span>';
                            }else{
                                return '<span class="badge badge-primary">'.$sample_room.'</span>';
                            }
                        })
                        ->addColumn('om',function($data){
                            
                            $om = $this->countBarcode($data->id,"om",$data->factory_id);
                            
                            if ($om==0) {
                                return '<span class="badge badge-default">0</span>';
                            }else{
                                return '<span class="badge badge-primary">'.$om.'</span>';
                            }
                        })
                        ->addColumn('ship',function($data){
                            
                            $loading = $this->countBarcode($data->id,"loading",$data->factory_id);
                            $shipping = $this->countBarcode($data->id,"shipping",$data->factory_id);
                            
                            if ($loading==0 && $shipping==0) {
                                return '<span class="badge badge-default">0</span>';
                            }else{
                                return '<span class="badge badge-primary">'.$loading.'</span> <span class="badge badge-success">'.$shipping.'</span>';
                            }
                        })
                        ->addColumn('bapb',function($data){
                            
                            $destory = $this->countBarcode($data->id,"bapb",$data->factory_id);
                            
                            if ($destory==0) {
                                return '<span class="badge badge-default">0</span>';
                            }else{
                                return '<span class="badge badge-danger">'.$destory.'</span>';
                            }
                        })
                        ->addColumn('handover',function($data){
                            
                            $hand = $this->countBarcode($data->id,"handover",$data->factory_id);
                            
                            if ($hand==0) {
                                return '<span class="badge badge-default">0</span>';
                            }else{
                                return '<span class="badge badge-danger">'.$hand.'</span>';
                            }
                        })
                        ->addColumn('action',function($data){
                            return '<button class="btn btn-default btn-detail" data-id="'.$data->id.'" data-fact="'.$data->factory_id.'" data-mo="'.$data->documentno.'"><span class="icon-stack3"></span></button>';
                            
                        })
                        ->rawColumns(['qty','reserv','md','qc','lab','marker','borrow','ship','bapb','action','cutting','pack_sample','ie','ppc','ppa','sewing','sample_room','om','handover'])
                        ->make(true);
    }

    private function countBarcode($mo,$loc,$factory_id){
        if ($loc=='borrow') {
           $qty = GarmentId::where('factory_id',$factory_id)
                            ->where('mo_id',$mo)
                            ->whereIn('current_location',['sewing','ie','cutting','packsample','ppc','ppa','sample_room'])
                            ->whereNull('deleted_at')
                            ->count();
        }else{
            $qty = GarmentId::where('factory_id',$factory_id)
                            ->where('mo_id',$mo)
                            ->where('current_location',$loc)
                            ->whereNull('deleted_at')
                            ->count();
        }
        
      
        return $qty;
    }

    public function detailDash(Request $request){
        $moid = $request->moid;
        $factory_id = $request->factory;

        $mo = MasterMO::where('id',$moid)->whereNull('deleted_at')->first();

        $data = array(
            'docno'=>$mo->documentno,
            'moid'=>$moid,
            'factory'=>$factory_id
        );
        return view('dashboard.dashboard_detail',$data);
    }

    public function ajaxGetDetail(Request $request){
        $moid = $request->moid;
        $factory = $request->factory;
        $filterby = $request->filterby;

        $data = DB::table('master_mo')
                            ->join('barcode_garment','barcode_garment.mo_id','=','master_mo.id')
                            ->where('master_mo.id',$moid)
                            ->where('barcode_garment.factory_id',$factory)
                            ->whereNull('master_mo.deleted_at')
                            ->whereNull('barcode_garment.deleted_at');

        

        return Datatables::of($data)
                        ->addColumn('item',function($data){
                            return $data->season."-".$data->style."-".$data->article."-".$data->size;
                        })
                        ->addColumn('location',function($data){
                            if ($data->storage==true) {
                                return '<span class="badge badge-primary"> '.strtoupper($data->current_location).' </span>';
                            }else if ($data->borrow==true) {
                                return '<span class="badge badge-success"> '.strtoupper($data->current_location).' </span>';
                            }else{
                                return '<span class="badge badge-warning"> '.strtoupper($data->current_location).' </span>';
                            }
                        })
                        ->addColumn('rack',function($data){
                            if ($data->storage==true) {
                                $loc = DB::table('garment_movements')
                                            ->join('locator','garment_movements.locator_id','=','locator.id')
                                            ->where('garment_movements.barcode_id',$data->barcode_id)
                                            ->whereNull('garment_movements.deleted_at')
                                            ->select('locator.name','garment_movements.barcode_id')->first();

                                $rack = '<span class="badge badge-success"> '.strtoupper($loc->name).' </span>';
                            }else{
                                $rack = "";
                            }

                            return $rack;
                        })
                        ->rawColumns(['item','location','rack'])
                        ->make(true);
    }
}
