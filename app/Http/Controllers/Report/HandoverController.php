<?php

namespace App\Http\Controllers\Report;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use DB;
use Carbon\Carbon;
use DataTables;
use Excel;

use App\Models\GarmentId;
use App\Models\GarmentMove;
use App\Models\Locator;
use App\User;

use Rap2hpoutre\FastExcel\FastExcel;


class HandoverController extends Controller
{
    public function handover(){
       $factory = DB::table('factory')->whereNull('deleted_at')->get();
      return view('report.handover')->with('factory',$factory);
    }

    public function ajaxGetData(Request $request){
        $date_range = explode('-', preg_replace('/\s+/', '', $request->date_range));
        $from = date_format(date_create($date_range[0]), 'Y-m-d 00:00:00');
        $to = date_format(date_create($date_range[1]), 'Y-m-d 23:59:59');
        $factory_id = $request->factory_id;
        $handover = $request->handover;

        switch ($handover) {
            case 'in':
                $data = DB::table('ns_report_handover_in')->whereBetween('date_scan',[$from,$to])->whereNotIn('factory_from',[$factory_id])->orderBy('date_scan','asc');
            break;

            case 'out':
                $data = DB::table('ns_report_handover_out')->whereBetween('date_scan',[$from,$to])->whereNotIn('factory_to',[$factory_id])->orderBy('date_scan','asc');
            break;
        }


        return DataTables::of($data)
                            ->editColumn('date_scan',function($data){
                                $us = DB::Table('users')->where('id',$data->user_id)->first();

                                return Carbon::parse($data->date_scan)->format('d-M-Y')." (".$us->name.")";
                            })
                            ->rawColumns(['date_scan'])->make(true);

    }

    public function exportData(Request $request){
        $date_range = explode('-', preg_replace('/\s+/', '', $request->date_range));
        $from = date_format(date_create($date_range[0]), 'Y-m-d 00:00:00');
        $to = date_format(date_create($date_range[1]), 'Y-m-d 23:59:59');
        $factory_id = $request->factory_id;
        $handover = $request->handover;

        switch ($handover) {
            case 'in':
                $data = DB::table('ns_report_handover_in')->whereBetween('date_scan',[$from,$to])->whereNotIn('factory_from',[$factory_id])->orderBy('date_scan','asc');
                $names = "Report_history_handover_to_";
            break;

            case 'out':
                $data = DB::table('ns_report_handover_out')->whereBetween('date_scan',[$from,$to])->whereNotIn('factory_to',[$factory_id])->orderBy('date_scan','asc');
                $names = "Report_history_handover_from_";
            break;
        }

        $fact = DB::table('factory')->where('id',$factory_id)->whereNull('deleted_at')->first();
       if (isset($filterby)) {
            $data = $data->where('documentno','LIKE','%'.$filterby.'%')
                            ->orwhere('item','LIKE','%'.$filterby.'%')
                            ->orwhere('barcode_id','LIKE','%'.$filterby.'%')
                            ->orwhere('date_scan','LIKE','%'.$filterby.'%')
                            ->orwhere('product_category','LIKE','%'.$filterby.'%')
                            ->orwhere('brand','LIKE','%'.$filterby.'%')
                            ->orwhere('date_reciept','LIKE','%'.$filterby.'%');
        }

        $i = 1;

        $filename = $names.$fact->factory_name."_".$from.'_until_'.$to;
       $data_result = $data->get();
       foreach ($data_result as $data_results) {
          $data_results->no=$i++;
       }

       if ($data_result->count()>0) {
          return (new FastExcel($data_result))->download($filename.'.xlsx',function($row) use($i,$handover){

                if ($handover=="in") {
                    $head = 'Factory From';
                   $pabr = $row->factory_from;
                }else{
                    $head = 'Factory To';
                   $pabr = $row->factory_to;
                }
                $factname = DB::table('factory')->where('id',$pabr)->first();
                $us = DB::Table('users')->where('id',$row->user_id)->first();

            return
              [
                '#'=>$row->no,
                'Barcode_id'=>$row->barcode_id,
                'Item'=>$row->item,
                'Doc. No.'=>$row->documentno,
                'Brand'=>$row->brand,
                'Date Reciept'=>$row->date_reciept,
                'Status'=>$row->status,
                'Color'=>$row->color,
                'Prod. Name'=>$row->product_name,
                'Prod. Ctg.'=>$row->product_category,
                $head=>$factname->factory_name,
                'date_scan'=>$row->date_scan." (".$us->name.")"

              ];
          });
       }else{
          return response()->json('no data', 422);
       }

    }
}
