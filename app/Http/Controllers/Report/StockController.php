<?php

namespace App\Http\Controllers\Report;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use DB;
use Carbon\Carbon;
use DataTables;
use Excel;

use App\Models\GarmentId;
use App\Models\GarmentMove;
use App\Models\Locator;
use App\Models\StyleMark;
use App\User;
use App\Models\TRF\Employee;

use Rap2hpoutre\FastExcel\FastExcel;

class StockController extends Controller
{
 
  // stock MD
    public function stockMD(){
      $factory = DB::table('factory')->whereNull('deleted_at')->get();
      return view('report.stock.stock_md')->with('factory',$factory);
    }

    public function ajaxGetStockMD(Request $request){
      $factory_id = $request->factory_id;

      $data =  DB::table('ns_report_stockmd')
                  ->where('factory_id',$factory_id)
                  ->orderBy('current_location','desc')
                  ->orderBy('created_at','desc');

        return DataTables::of($data)
                  ->addColumn('scan_date',function($data){
                    $user = User::where('id',$data->user_id)->first();
                    $exp = explode(" ", $user->name);
                    $name = isset($exp[1]) ? $exp[0]." ".$exp[1] : $exp[0];
                    return $data->created_at."<br> By ".$name;

                  })
                  ->editColumn('current_location',function($data){
                    return strtoupper($data->current_location);
                  })
                  ->rawColumns(['scan_date','current_location'])->make(true);
    }

    public function exportStockMD(Request $request){
      $factory_id = $request->factory_id;
      $filterby = $request->filterby;


      $data =  DB::table('ns_report_stockmd')
                  ->where('factory_id',$factory_id)
                  ->orderBy('current_location','desc')
                  ->orderBy('created_at','desc');

      if (isset($filterby)) {
        $data = $data->where('barcode_id','LIKE','%'.$filterby.'%')
                    ->orwhere('item','LIKE','%'.$filterby.'%')
                    ->orwhere('documentno','LIKE','%'.$filterby.'%')
                    ->orwhere('current_location','LIKE','%'.$filterby.'%')
                    ->orwhere('rack','LIKE','%'.$filterby.'%');
      }

      $i = 1;

 
        
      $filename = "AOI_".$factory_id."_report_stock_MD";
       
      

      

       $data_result = $data->get();
       foreach ($data_result as $data_results) {
          $data_results->no=$i++;
       }

       if ($data_result->count()>0) {
          return (new FastExcel($data_result))->download($filename.'.xlsx',function($row) use($i){

            

            $sets = isset($row->sets) ? $row->sets : "";
            $rack = isset($row->name) ? $row->name : "";
            $user = User::where('id',$row->user_id)->first();
            
            return
              [
                '#'=>$row->no,
                'Barcode_id'=>$row->barcode_id." ".$sets,
                'Item'=>$row->season."-".$row->style."-".$row->article."-".$row->size,
                'Doc. No.'=>$row->documentno,
                'Brand'=>$row->brand,
                'Date Reciept'=>$row->date_reciept,
                'Status'=>$row->status,
                'Color'=>$row->color,
                'Prod. Name'=>$row->product_name,
                'Prod. Ctg.'=>$row->product_category,
                'Location'=>strtoupper($row->current_location),
                'Rack'=>$row->rack,
                'Remark'=>$row->remark,
                'Scan Date'=>$row->created_at." (".$user->name.")"

              ];
          });
       }else{
          return response()->json('no data', 422);
       }

    }

    

    // stock MD

    // history borrowing
    public function borrowingHistory(){
      $factory = DB::table('factory')->whereNull('deleted_at')->get();
      return view('report.history_borrowing')->with('factory',$factory);
    }

    public function ajaxGetHistoryBorrowing(Request $request){
      $factory_id = $request->factory_id;
      $date_range = explode('-', preg_replace('/\s+/', '', $request->date_range));
      $from = date_format(date_create($date_range[0]), 'Y-m-d');
      $to = date_format(date_create($date_range[1]), 'Y-m-d');

      $data =  DB::table('ns_report_historyborrowing')
                    ->where('factory_id',$factory_id)
                    ->whereBetween('created_at',[$from,$to]);
                  

        return DataTables::of($data)
                  ->editColumn('current_location',function($data){
                    return strtoupper($data->current_location);
                  })
                  ->rawColumns(['current_location','borrower'])->make(true);
    }

    public function exportHistoryBorrowing(Request $request){
      $factory_id = $request->factory_id;
      $filterby = $request->filterby;
      $date_range = explode('-', preg_replace('/\s+/', '', $request->date_range));
      $from = date_format(date_create($date_range[0]), 'Y-m-d');
      $to = date_format(date_create($date_range[1]), 'Y-m-d');

      $data =  DB::table('ns_report_historyborrowing')
                    ->where('factory_id',$factory_id)
                    ->whereBetween('created_at',[$from,$to]);

        if (isset($filterby)) {
            $data = $data->where('barcode_id','LIKE','%'.$filterby.'%')
                          ->orWhere('item','LIKE','%'.$filterby.'%')
                          ->orWhere('borrower','LIKE','%'.$filterby.'%')
                          ->orWhere('current_location','LIKE','%'.$filterby.'%')
                          ->orWhere('documentno','LIKE','%'.$filterby.'%');
        }

        $i = 1;

       $filename = "AOI_".$factory_id."_report_history_borrowing_from".$from."_until_".$to;
       $data_result = $data->get();
       foreach ($data_result as $data_results) {
          $data_results->no=$i++;
       }

       if ($data_result->count()>0) {
          return (new FastExcel($data_result))->download($filename.'.xlsx',function($row) use($i){

            

            
            return
              [
                '#'=>$row->no,
                'Barcode_id'=>$row->barcode_id,
                'Item'=>$row->item,
                'Doc. No.'=>$row->documentno,
                'Brand'=>$row->brand,
                'Date Reciept'=>$row->date_reciept,
                'Status'=>$row->status,
                'Color'=>$row->color,
                'Prod. Name'=>$row->product_name,
                'Prod. Ctg.'=>$row->product_category,
                'Location'=>strtoupper($row->current_location),
                'Remark'=>$row->remark,
                'Borrow Date'=>$row->created_at,
                'Borrower'=>$row->borrower

              ];
          });
       }else{
          return response()->json('no data', 422);
       }
    }
    // history borrowing


    // borrowing
    public function borrowing(){
      $factory = DB::table('factory')->whereNull('deleted_at')->get();
      return view('report.borrowing')->with('factory',$factory);
    }

    public function ajaxGetBorrowing(Request $request){
      $factory_id = $request->factory_id;
      $radio_type = $request->radio_type;
      $filter  = trim($request->search['value']);

    

      $data = DB::table('master_mo')
                        ->Join('barcode_garment','master_mo.id','barcode_garment.mo_id')
                        ->LeftJoin('garment_movements','barcode_garment.barcode_id','garment_movements.barcode_id')
                        ->LeftJoin('employee','garment_movements.borrower','employee.nik')
                        ->where('barcode_garment.factory_id',$factory_id);
      
      if ($radio_type=="onborrow") {
        $data = $data->where([
          'garment_movements.is_canceled'=>false,
          'master_mo.is_reciept'=>true,
          'barcode_garment.borrow'=>true
        ])
        ->whereNull('master_mo.deleted_at')
        ->whereNull('barcode_garment.deleted_at')
        ->whereNull('garment_movements.deleted_at');
      }else{
        $date_range = explode('-', preg_replace('/\s+/', '', $request->date_range));
        $from = date_format(date_create($date_range[0]), 'Y-m-d');
        $to = date_format(date_create($date_range[1]), 'Y-m-d');

        $data = $data->whereBetween('garment_movements.created_at',[$from,$to])
                        ->where('garment_movements.locator_from','storage_md')
                        ->where(function($query){
                            $query->where('garment_movements.locator_to','<>','shipping')
                                ->orWhere('garment_movements.locator_to','<>','handover');
                        })
                        ->whereNull('master_mo.deleted_at')
                        ->whereNull('barcode_garment.deleted_at')
                        ->where('garment_movements.is_canceled',false);
                        
      }

      if($filter!=null || $filter!=""){
        $data = $data->where(function($query) use ($filter){
            $query->where('master_mo.documentno','LIKE','%'.$filter.'%')
                        ->orWhere('master_mo.season','LIKE','%'.$filter.'%')
                        ->orWhere('master_mo.style','LIKE','%'.$filter.'%')
                        ->orWhere('master_mo.article','LIKE','%'.$filter.'%')
                        ->orWhere('master_mo.size','LIKE','%'.$filter.'%')
                        ->orWhere('master_mo.color','LIKE','%'.$filter.'%')
                        ->orWhere('master_mo.status','LIKE','%'.$filter.'%')
                        ->orWhere('master_mo.product_category','LIKE','%'.$filter.'%')
                        ->orWhere('master_mo.product_name','LIKE','%'.$filter.'%')
                        ->orWhere('master_mo.brand','LIKE','%'.$filter.'%')
                        ->orWhere('barcode_garment.sets','LIKE','%'.$filter.'%')
                        ->orWhere('barcode_garment.barcode_id','LIKE','%'.$filter.'%')
                        ->orWhere('barcode_garment.current_location','LIKE','%'.$filter.'%')
                        ->orWhere('employee.nik','LIKE','%'.$filter.'%')
                        ->orWhere('employee.name','LIKE','%'.$filter.'%')
                        ->orWhere('barcode_garment.factory_id','LIKE','%'.$filter.'%');
            

        });
      }

      $data = $data->select(
        'master_mo.id',
        'master_mo.documentno',
        'master_mo.season',
        'master_mo.style',
        'master_mo.article',
        'master_mo.size',
        'master_mo.brand',
        'master_mo.status',
        'master_mo.product_name',
        'master_mo.product_category',
        'master_mo.color',
        'master_mo.docstatus',
        'master_mo.remark',
        'barcode_garment.current_location',
        'barcode_garment.factory_id',
        'barcode_garment.storage',
        'barcode_garment.borrow',
        'garment_movements.created_at',
        'garment_movements.user_id',
        'garment_movements.locator_to',
        'barcode_garment.barcode_id',
        'employee.nik',
        'employee.name'
      );
        return DataTables::of($data)
                  ->addColumn('borrower',function($data){
                      return $data->name." (".$data->nik.")";
                  })
                  ->editColumn('locator_to',function($data){
                    return strtoupper($data->locator_to);
                  })
                  ->editColumn('season',function($data){
                    return $data->season."-".$data->style."-".$data->article."-".$data->size;
                  })
                  ->editColumn('created_at',function($data){
                      return date_format(date_create($data->created_at),'d-m-Y H:i:s');
                  })
                  ->rawColumns(['locator_to','borrower','created_at','season'])->make(true);
    }

    public function exportBorrowing(Request $request){
      $factory_id = $request->factory_id;
      $radio_type = $request->radio_type;
      $filterby = trim($request->filterby);


      $data = DB::table('master_mo')
                        ->Join('barcode_garment','master_mo.id','barcode_garment.mo_id')
                        ->LeftJoin('garment_movements','barcode_garment.barcode_id','garment_movements.barcode_id')
                        ->LeftJoin('employee','garment_movements.borrower','employee.nik')
                        ->where('barcode_garment.factory_id',$factory_id);
      
      if ($radio_type=="onborrow") {
        $data = $data->where([
          'garment_movements.is_canceled'=>false,
          'master_mo.is_reciept'=>true,
          'barcode_garment.borrow'=>true
        ])
        ->whereNull('master_mo.deleted_at')
        ->whereNull('barcode_garment.deleted_at')
        ->whereNull('garment_movements.deleted_at');

        $name = "borrowing";
      }else{
        $date_range = explode('-', preg_replace('/\s+/', '', $request->date_range));
        $from = date_format(date_create($date_range[0]), 'Y-m-d 00:00:00');
        $to = date_format(date_create($date_range[1]), 'Y-m-d 23:59:59');

        $data = $data->whereBetween('garment_movements.created_at',[$from,$to])
                        ->where('garment_movements.locator_from','storage_md')
                        ->where(function($query){
                            $query->where('garment_movements.locator_to','<>','shipping')
                                ->orWhere('garment_movements.locator_to','<>','handover');
                        })
                        ->whereNull('master_mo.deleted_at')
                        ->whereNull('barcode_garment.deleted_at')
                        ->where('garment_movements.is_canceled',false);
        
        $name = "scan_out_borrowing_".$from."-".$to;
      }

        // if ($filterby!=null) {
        //     $query->where('master_mo.documentno','LIKE','%'.$filter.'%')
        //                 ->orWhere('master_mo.season','LIKE','%'.$filter.'%')
        //                 ->orWhere('master_mo.style','LIKE','%'.$filter.'%')
        //                 ->orWhere('master_mo.article','LIKE','%'.$filter.'%')
        //                 ->orWhere('master_mo.size','LIKE','%'.$filter.'%')
        //                 ->orWhere('master_mo.color','LIKE','%'.$filter.'%')
        //                 ->orWhere('master_mo.status','LIKE','%'.$filter.'%')
        //                 ->orWhere('master_mo.product_category','LIKE','%'.$filter.'%')
        //                 ->orWhere('master_mo.product_name','LIKE','%'.$filter.'%')
        //                 ->orWhere('master_mo.brand','LIKE','%'.$filter.'%')
        //                 ->orWhere('barcode_garment.sets','LIKE','%'.$filter.'%')
        //                 ->orWhere('barcode_garment.barcode_id','LIKE','%'.$filter.'%')
        //                 ->orWhere('barcode_garment.current_location','LIKE','%'.$filter.'%')
        //                 ->orWhere('employee.nik','LIKE','%'.$filter.'%')
        //                 ->orWhere('employee.name','LIKE','%'.$filter.'%')
        //                 ->orWhere('barcode_garment.factory_id','LIKE','%'.$filter.'%');
        // }

        if($filterby!=null || $filterby!=""){
          $data = $data->where(function($query) use ($filterby){
              $query->where('master_mo.documentno','LIKE','%'.$filterby.'%')
                          ->orWhere('master_mo.season','LIKE','%'.$filterby.'%')
                          ->orWhere('master_mo.style','LIKE','%'.$filterby.'%')
                          ->orWhere('master_mo.article','LIKE','%'.$filterby.'%')
                          ->orWhere('master_mo.size','LIKE','%'.$filterby.'%')
                          ->orWhere('master_mo.color','LIKE','%'.$filterby.'%')
                          ->orWhere('master_mo.status','LIKE','%'.$filterby.'%')
                          ->orWhere('master_mo.product_category','LIKE','%'.$filterby.'%')
                          ->orWhere('master_mo.product_name','LIKE','%'.$filterby.'%')
                          ->orWhere('master_mo.brand','LIKE','%'.$filterby.'%')
                          ->orWhere('barcode_garment.sets','LIKE','%'.$filterby.'%')
                          ->orWhere('barcode_garment.barcode_id','LIKE','%'.$filterby.'%')
                          ->orWhere('barcode_garment.current_location','LIKE','%'.$filterby.'%')
                          ->orWhere('employee.nik','LIKE','%'.$filterby.'%')
                          ->orWhere('employee.name','LIKE','%'.$filterby.'%')
                          ->orWhere('barcode_garment.factory_id','LIKE','%'.$filterby.'%');
              
  
          });
        }


        $data = $data->select(
          'master_mo.id',
          'master_mo.documentno',
          'master_mo.season',
          'master_mo.style',
          'master_mo.article',
          'master_mo.size',
          'master_mo.brand',
          'master_mo.status',
          'master_mo.product_name',
          'master_mo.product_category',
          'master_mo.color',
          'master_mo.docstatus',
          'master_mo.remark',
          'barcode_garment.current_location',
          'barcode_garment.factory_id',
          'barcode_garment.storage',
          'barcode_garment.borrow',
          'garment_movements.created_at',
          'garment_movements.user_id',
          'garment_movements.locator_to',
          DB::raw("barcode_garment.barcode_id AS barcode"),
          'employee.nik',
          'employee.name',
          DB::raw("CASE
              WHEN barcode_garment.sets IS NULL THEN barcode_garment.barcode_id
              WHEN barcode_garment.sets IS NOT NULL THEN concat(barcode_garment.barcode_id, ' ', barcode_garment.sets)::character varying
              ELSE NULL::character varying
          END AS barcode_id"),
          DB::raw("concat(master_mo.season, '-', master_mo.style, '-', master_mo.article, '-', master_mo.size) AS item")
        );
        $i = 1;

       $filename = "AOI_".$factory_id."_report_".$name;

       $data_result = $data->get();

       foreach ($data_result as $data_results) {
          $data_results->no=$i++;
       }

      //  dd($data_result);
       if ($data_result->count()>0) {
          return (new FastExcel($data_result))->download($filename.'.xlsx',function($row) use($i){

            

            
            return
              [
                '#'=>$row->no,
                'Barcode_id'=>$row->barcode_id,
                'Item'=>$row->item,
                'Doc. No.'=>$row->documentno,
                'Brand'=>$row->brand,
                'Status'=>$row->status,
                'Color'=>$row->color,
                'Prod. Name'=>$row->product_name,
                'Prod. Ctg.'=>$row->product_category,
                'Location'=>strtoupper($row->locator_to),
                'Remark'=>$row->remark,
                'Borrow Date'=>date_format(date_create($row->created_at),'d-m-Y H:i:s'),
                'Borrower'=>$row->name." (".$row->nik.")"

              ];
          });
       }else{
          return response()->json('no data', 422);
       }
    }

    // public function ajaxGetBorrowing(Request $request){
    //   $factory_id = $request->factory_id;

    //   $data =  DB::table('ns_report_borrowing')
    //                 ->where('factory_id',$factory_id);
                  

    //     return DataTables::of($data)
    //               ->editColumn('current_location',function($data){
    //                 return strtoupper($data->current_location);
    //               })
    //               ->rawColumns(['current_location','borrower'])->make(true);
    // }

    // public function exportBorrowing(Request $request){
    //   $factory_id = $request->factory_id;
    //   $filterby = $request->filterby;

    //   $data =  DB::table('ns_report_borrowing')
    //                 ->where('factory_id',$factory_id);

    //     if (isset($filterby)) {
    //         $data = $data->where('barcode_id','LIKE','%'.$filterby.'%')
    //                       ->orWhere('item','LIKE','%'.$filterby.'%')
    //                       ->orWhere('borrower','LIKE','%'.$filterby.'%')
    //                       ->orWhere('current_location','LIKE','%'.$filterby.'%')
    //                       ->orWhere('documentno','LIKE','%'.$filterby.'%');
    //     }

    //     $i = 1;

    //    $filename = "AOI_".$factory_id."_report_borrowing";
    //    $data_result = $data->get();
    //    foreach ($data_result as $data_results) {
    //       $data_results->no=$i++;
    //    }

    //    if ($data_result->count()>0) {
    //       return (new FastExcel($data_result))->download($filename.'.xlsx',function($row) use($i){

            

            
    //         return
    //           [
    //             '#'=>$row->no,
    //             'Barcode_id'=>$row->barcode_id,
    //             'Item'=>$row->item,
    //             'Doc. No.'=>$row->documentno,
    //             'Brand'=>$row->brand,
    //             'Date Reciept'=>$row->date_reciept,
    //             'Status'=>$row->status,
    //             'Color'=>$row->color,
    //             'Prod. Name'=>$row->product_name,
    //             'Prod. Ctg.'=>$row->product_category,
    //             'Location'=>strtoupper($row->current_location),
    //             'Remark'=>$row->remark,
    //             'Borrow Date'=>$row->created_at,
    //             'Borrower'=>$row->borrower

    //           ];
    //       });
    //    }else{
    //       return response()->json('no data', 422);
    //    }
    // }
    // borrowing

    // pattern stock
    public function patternStock(){
      $factory = DB::table('factory')->whereNull('deleted_at')->get();
      return view('report.stock.stock_pattern')->with('factory',$factory);
    }

    public function ajaxGetStockPattern(Request $request){
        $factory_id = $request->factory_id;

        $data =  DB::table('ns_report_stockmarker')
                            ->where('factory_id',$factory_id)
                            ->orderBy('created_at','desc');

        return DataTables::of($data)
                            ->editColumn('rack',function($data){
                              if ($data->current_location=="MARKER_STORAGE") {
                                    $ret = $data->rack;
                              }else if ($data->current_location=="MARKER") {
                                    if (isset($data->borrower)) {
                                    
                                      $ret = $this->getEmp($data->borrower);
                                    }else{
                                      $ret = "-";
                                    }
                                    
                              }

                              return $ret;
                            })
                            ->addColumn('scan_date',function($data){
                             

                                if ($data->locator_from=="storage_md" and $data->locator_to="marker") {
                                  $act = carbon::parse($data->created_at)->format('d-M-Y H:i:s')." Borrow by ".$this->getUser($data->user_id);
                                }else if ($data->locator_from=="marker" and $data->locator_to="marker_storage"){
                                  $act = carbon::parse($data->created_at)->format('d-M-Y H:i:s')." Storage by ".$this->getUser($data->user_id);
                                }else if ($data->locator_from=="marker_storage" and $data->locator_to="marker"){
                                  $act = carbon::parse($data->created_at)->format('d-M-Y H:i:s')." Return by ".$this->getUser($data->user_id);
                                }

                                return $act;

                            })
                            ->addColumn('remark_marker',function($data){

                                if (isset($data->id_style)) {
                                  $expr = Carbon::parse($data->podd)->addDays(91)->format('Y-m-d');
                                    return 'Style PIC : '.$data->pic.'<br> Return By Release : '.$data->release.' Return By PODD '.$data->podd.'<br> Expired '.$expr;
                                }else{
                                    return '-';
                                }
                               
                            })
                            ->setRowAttr([
                                'style' => function($data)
                                {
                                    

                                    if (isset($data->id_style)) {
                                        $now = Carbon::now()->format('Y-m-d');
                                        $expired = Carbon::parse($data->podd)->addDays(91)->format('Y-m-d');
                                        

                                        if ($now>$expired) {
                                           return  'background-color: #ff3300;';
                                        }
                                    }
                                    
                                },
                            ])
                            ->rawColumns(['rack','scan_date','remark_marker'])->make(true);
    } 


    public function exportStockPattern(Request $request){
        $factory_id = $request->factory_id;
        $filterby = $request->filterby;

        $data =  DB::table('ns_report_stockmarker')
                            ->where('factory_id',$factory_id)
                            ->orderBy('created_at','desc');

        if (isset($filterby)) {
            $data = $data->where('barcode_id','LIKE','%'.$filterby.'%')
                          ->orWhere('item','LIKE','%'.$filterby.'%')
                          ->orWhere('current_location','LIKE','%'.$filterby.'%')
                          ->orWhere('documentno','LIKE','%'.$filterby.'%')
                          ->orWhere('rack','LIKE','%'.$filterby.'%');
        }

        $i = 1;

        
       $filename = $this->getFact($factory_id)."_report_stock_pattern_marker";
       
      

      

       $data_result = $data->get();
       foreach ($data_result as $data_results) {
          $data_results->no=$i++;
       }

       if ($data_result->count()>0) {
          return (new FastExcel($data_result))->download($filename.'.xlsx',function($row) use($i){
                if ($row->current_location=="MARKER") {
                    $pic = !empty($row->borrower) ? $this->getEmp($row->borrower) : "-";
                }else if ($row->current_location=="MARKER_STORAGE") {
                    $pic = $row->rack;
                }


                if ($row->locator_from=="storage_md" && $row->locator_to=="marker") {
                  $act = " Borrow by ";
                }else if ($row->locator_from=="marker" && $row->locator_to=="marker_storage"){
                   $act = " Storage by ";
                }else if ($row->locator_from=="marker_storage" && $row->locator_to=="marker"){
                  $act = " Return by ";
                }else{
                  $act = "-";
                }


                $used = isset($row->user_id) ? $this->getUser($row->user_id) : "-";
                

            return
              [
                '#'=>$row->no,
                'Barcode_id'=>$row->barcode_id,
                'Season'=>$row->season,
                'Style'=>$row->style,
                'Article'=>$row->article,
                'Size'=>$row->size,
                'Doc. No.'=>$row->documentno,
                'Brand'=>$row->brand,
                'Date Reciept'=>$row->date_reciept,
                'Status'=>$row->status,
                'Color'=>$row->color,
                'Prod. Name'=>$row->product_name,
                'Prod. Ctg.'=>$row->product_category,
                'Location'=>$row->current_location,
                'Remark'=>$row->remark,
                'Rack/PIC'=>$pic,
                'Scan Date'=>date_format(date_create($row->created_at),'d-M-Y H:i:s').$act.$used,
                'Item'=>$row->item,
                'Style PIC'=>isset($row->pic) ? $row->pic : "",
                'Return By Release'=>isset($row->release) ? $row->release : "",
                'Return By PODD'=>isset($row->podd) ? $row->podd : "",
                'Expired Date'=>isset($row->podd) ? Carbon::parse($row->podd)->addDays(91)->format('d-m-Y') : ""
                

              ];
          });
       }else{
          return response()->json('no data', 422);
       }

    }

    // public function ajaxGetStockPattern(Request $request){
    //     $factory_id = $request->factory_id;

    //     $data =  DB::table('ns_report_stockmarker')
    //                         ->where('factory_id',$factory_id)
    //                         ->orderBy('current_location','ASC')
    //                         ->orderBy('created_at','desc');

    //     return DataTables::of($data)
    //                         ->editColumn('rack',function($data){
    //                           if ($data->storage==true && $data->borrow==false) {
    //                                 $ret = $data->rack;
    //                           }else if ($data->storage==false && $data->borrow==true) {
    //                                 if (isset($data->borrower)) {
    //                                  $emp = DB::table('employee')->where('nik',$data->borrower)->first();

    //                                   $ret = strtoupper($emp->name)." (".$emp->nik.")";
    //                                 }else{
    //                                   $ret = "-";
    //                                 }
                                    
    //                           }

    //                           return $ret;
    //                         })
    //                         ->addColumn('scan_date',function($data){
    //                           $us = User::where('id',$data->user_id)->first();

    //                             if ($data->locator_from=="storage_md" and $data->locator_to="marker") {
    //                               $act = carbon::parse($data->created_at)->format('d-M-Y H:i:s')." (Scan Borrow by ".$us->name." )";
    //                             }else if ($data->locator_from=="marker" and $data->locator_to="marker_storage"){
    //                               $act = carbon::parse($data->created_at)->format('d-M-Y H:i:s')." (Scan Storage by ".$us->name." )";
    //                             }else if ($data->locator_from=="marker_storage" and $data->locator_to="marker"){
    //                               $act = carbon::parse($data->created_at)->format('d-M-Y H:i:s')." (Scan Return by ".$us->name." )";
    //                             }

    //                             return $act;

    //                         })
    //                         ->rawColumns(['rack','scan_date'])->make(true);
    // } 


    // public function exportStockPattern(Request $request){
    //     $factory_id = $request->factory_id;
    //     $filterby = $request->filterby;

    //     $data =  DB::table('ns_report_stockmarker')
    //                         ->where('factory_id',$factory_id);

    //     if (isset($filterby)) {
    //         $data = $data->where('barcode_id','LIKE','%'.$filterby.'%')
    //                       ->orWhere('item','LIKE','%'.$filterby.'%')
    //                       ->orWhere('current_location','LIKE','%'.$filterby.'%')
    //                       ->orWhere('documentno','LIKE','%'.$filterby.'%')
    //                       ->orWhere('rack','LIKE','%'.$filterby.'%');
    //     }

    //     $i = 1;

        
    //    $filename = "AOI_".$factory_id."_report_stock_pattern_marker";
       
      

      

    //    $data_result = $data->get();
    //    foreach ($data_result as $data_results) {
    //       $data_results->no=$i++;
    //    }

    //    if ($data_result->count()>0) {
    //       return (new FastExcel($data_result))->download($filename.'.xlsx',function($row) use($i){

            
    //         return
    //           [
    //             '#'=>$row->no,
    //             'Barcode_id'=>$row->barcode_id,
    //             'Season'=>$row->season,
    //             'Style'=>$row->style,
    //             'Article'=>$row->article,
    //             'Size'=>$row->size,
    //             'Doc. No.'=>$row->documentno,
    //             'Brand'=>$row->brand,
    //             'Date Reciept'=>$row->date_reciept,
    //             'Status'=>$row->status,
    //             'Color'=>$row->color,
    //             'Prod. Name'=>$row->product_name,
    //             'Prod. Ctg.'=>$row->product_category,
    //             'Location'=>strtoupper($row->current_location),
    //             'Rack'=>$row->rack,
    //             'Scan Date'=>$row->created_at,
    //             'Item'=>$row->item,
    //             'Remark'=>$row->remark

    //           ];
    //       });
    //    }else{
    //       return response()->json('no data', 422);
    //    }

    // }
    // pattern stock

    

    //global stock
    public function globalStock(){
      $factory = DB::table('factory')->whereNull('deleted_at')->get();
      return view('report.stock.garment_active')->with('factory',$factory);
    }

    public function ajaxGetGlobalStock(Request $request){
      $factory_id = $request->factory_id;

      $data = DB::table('ns_report_globalstock')
                    ->where('factory_id',$factory_id)
                    ->orderBy('created_at','desc');

      return DataTables::of($data)
                ->editColumn('current_location',function($data){
                    return strtoupper($data->current_location);
                })
                ->addColumn('borrower',function($data){
                    if ($data->borrow==true) {
                      return strtoupper($data->name)." (".$data->nik.")";
                    }else if($data->storage==true){
                      return '<span class="badge badge-success">'.$data->rack.'</span>';
                    }else{
                      return "";
                    }
                })
                ->rawColumns(['current_location','borrower'])
                ->make(true);
    }

    public function exportGlobalStock(Request $request){
      $factory_id = $request->factory_id;
        $filterby = $request->filterby;

        $data =  DB::table('ns_report_globalstock')
                            ->where('factory_id',$factory_id);

        if (isset($filterby)) {
            $data = $data->where('barcode_id','LIKE','%'.$filterby.'%')
                          ->orWhere('item','LIKE','%'.$filterby.'%')
                          ->orWhere('current_location','LIKE','%'.$filterby.'%')
                          ->orWhere('documentno','LIKE','%'.$filterby.'%')
                          ->orWhere('nik','LIKE','%'.$filterby.'%')
                          ->orWhere('rack','LIKE','%'.$filterby.'%')
                          ->orWhere('name','LIKE','%'.$filterby.'%');
        }

        $i = 1;

        
       $filename = "AOI_".$factory_id."_report_global_stock";
       
      

      

       $data_result = $data->get();
       foreach ($data_result as $data_results) {
          $data_results->no=$i++;
       }

       if ($data_result->count()>0) {
          return (new FastExcel($data_result))->download($filename.'.xlsx',function($row) use($i){
                  if ($row->borrow==true) {
                     $ket = strtoupper($row->name)." (".$row->nik.")";
                  }else if($row->storage==true){
                      $ket = $row->rack;
                  }else{
                      $ket = "";
                  }
            
            return
              [
                '#'=>$row->no,
                'Barcode_id'=>$row->barcode_id,
                'Item'=>$row->item,
                'Doc. No.'=>$row->documentno,
                'Brand'=>$row->brand,
                'Date Reciept'=>$row->date_reciept,
                'Status'=>$row->status,
                'Color'=>$row->color,
                'Prod. Name'=>$row->product_name,
                'Prod. Ctg.'=>$row->product_category,
                'Remark'=>$row->remark,
                'Location'=>strtoupper($row->current_location),
                '. '=>$ket
                

              ];
          });
       }else{
          return response()->json('no data', 422);
       }
    }
    //global stock


     //recieving
    public function receiving(){
      $factory = DB::table('factory')->whereNull('deleted_at')->get();
      return view('report.stock.receiving')->with('factory',$factory);
    }

    public function ajaxGetReceiving(Request $request){
      
      $factory_id = $request->factory_id;
      $radio = $request->radio_search;

      $data =  DB::table('ns_report_checkin')
                  ->where('factory_id',$factory_id);
      

      if ($radio=='sstyle') {
        $data = $data->where('style',trim($request->style));
      }else if($radio=='sdocno'){
        $data = $data->where('documentno',trim($request->docno));
      }else if ($radio=='sdate') {
        $date_range = explode('-', preg_replace('/\s+/', '', $request->date_range));
        $from = date_format(date_create($date_range[0]), 'Y-m-d 00:00:00');
        $to = date_format(date_create($date_range[1]), 'Y-m-d 23:59:59');

        $data = $data->whereIn('created_at',[$from,$to]);
      }


      

        return DataTables::of($data)
                  ->editColumn('current_location',function($data){
                    return strtoupper($data->current_location);
                  })
                  ->rawColumns(['current_location'])->make(true);
    }

    public function exportReceiving(Request $request){
        $factory_id = $request->factory_id;
        $radio = $request->radio_search;

        $data =  DB::table('ns_report_checkin')
                    ->where('factory_id',$factory_id);
        

        if ($radio=='sstyle') {
          $data = $data->where('style',trim($request->style));
          $name = "_style_".trim($request->style);
        }else if($radio=='sdocno'){
          $data = $data->where('documentno',trim($request->docno));
          $name = "_documentno_".trim($request->docno);
        }else if ($radio=='sdate') {
          $date_range = explode('-', preg_replace('/\s+/', '', $request->date_range));
          $from = date_format(date_create($date_range[0]), 'Y-m-d 00:00:00');
          $to = date_format(date_create($date_range[1]), 'Y-m-d 23:59:59');

          $data = $data->whereIn('created_at',[$from,$to]);
          $name = "_checkin_".$from."_".$to;
        }

        if (isset($filterby)) {
            $data = $data->where('documentno','LIKE','%'.$filterby.'%')
                            ->orwhere('item','LIKE','%'.$filterby.'%')
                            ->orwhere('barcode_id','LIKE','%'.$filterby.'%')
                            ->orwhere('rack','LIKE','%'.$filterby.'%')
                            ->orwhere('current_location','LIKE','%'.$filterby.'%')
                            ->orwhere('date_reciept','LIKE','%'.$filterby.'%');
        }

       $i = 1;

       $filename = "AOI_".$factory_id."_report_receiving".$name;
       $data_result = $data->get();
       foreach ($data_result as $data_results) {
          $data_results->no=$i++;
       }

       if ($data_result->count()>0) {
          return (new FastExcel($data_result))->download($filename.'.xlsx',function($row) use($i){

           
            return
              [
                '#'=>$row->no,
                'Barcode_id'=>$row->barcode_id,
                'Item'=>$row->item,
                'Doc. No.'=>$row->documentno,
                'Brand'=>$row->brand,
                'Date Reciept'=>$row->date_reciept,
                'Status'=>$row->status,
                'Color'=>$row->color,
                'Prod. Name'=>$row->product_name,
                'Prod. Ctg.'=>$row->product_category,
                'Location.'=>strtoupper($row->current_location),
                'Rack.'=>$row->rack,
                'Checkin'=>$row->created_at,
                'Remark'=>$row->remark

              ];
          });
       }else{
          return response()->json('no data', 422);
       }
    }
    //recieving


    private function getUser($iduser){
      $usr = User::where('id',$iduser)->first();

      return $usr->name." (".$usr->nik.")";
    }

    private function getEmp($nik){
      $emp = DB::table('employee')->where('nik',$nik)->first();

      return $emp->name." (".$emp->nik.")";
    }

    private function getFact($facid){
      $fact = DB::table('factory')->where('id',$facid)->first();

      return $fact->title;
    }

     //report mutation
     public function reportMutation(Request $request){
      $factory = DB::table('factory')->whereNull('deleted_at')->get();

      return view('report.mutation')->with('factory',$factory);
    }

    public function ajaxGetMutation(Request $request){
      $from = date_format(date_create(explode(" - ",$request->date_range)[0]),'Y-m-d 00:00:00');
      $to = date_format(date_create(explode(" - ",$request->date_range)[1]),'Y-m-d 23:59:59');
      $factory_id = $request->factory_id;
      $barcode_id = trim($request->barcode_id);
      $type = $request->radio;
      $filter = trim($request['search']['value']);

      $data = DB::table('garment_movements')
                          ->join('barcode_garment','garment_movements.barcode_id','barcode_garment.barcode_id')
                          ->join('master_mo','barcode_garment.mo_id','master_mo.id')
                          ->join('users','garment_movements.user_id','users.id')
                          ->leftJoin('locator','garment_movements.locator_id','locator.id')
                          // ->whereBetween('garment_movements.created_at',[$from,$to])
                          ->where('barcode_garment.factory_id',$factory_id)
                          ->where('garment_movements.is_canceled',false)
                          // ->where('barcode_garment.barcode_id','2024042600020')
                          ->select(
                            'barcode_garment.barcode_id',
                            'barcode_garment.current_location',
                            'barcode_garment.factory_id',
                            'barcode_garment.sets',
                            'barcode_garment.reciept_date',
                            'master_mo.documentno',
                            'master_mo.season',
                            'master_mo.style',
                            'master_mo.article',
                            'master_mo.size',
                            'master_mo.brand',
                            'master_mo.qty',
                            'master_mo.uom',
                            'master_mo.status',
                            'master_mo.product_name',
                            'master_mo.product_category',
                            'master_mo.color',
                            'master_mo.docstatus',
                            'master_mo.remark',
                            'garment_movements.locator_from',
                            'garment_movements.locator_to',
                            'garment_movements.locator_id',
                            'garment_movements.description',
                            'garment_movements.borrower',
                            'garment_movements.line',
                            'garment_movements.created_at',
                            'locator.name',
                            'locator.description',
                            'users.nik',
                            db::raw("users.name as username")
                          )->orderBy('garment_movements.created_at','asc');

                          // if ($type=="checkin") {
                          //     $data = $data->whereIn('garment_movements.locator_to',['checkin_md','marker_storage','qc_storage']);
                          // }else{
                          //     $data = $data->whereIn('garment_movements.locator_from',['storage_md','marker_storage','qc_storage']);
                          // }

                          if ($type=="checkin") {
                              $data = $data->whereBetween('garment_movements.created_at',[$from,$to])
                              ->whereIn('garment_movements.locator_to',['checkin_md','marker_storage','qc_storage']);
                          }else if ($type=="checkout"){
                              $data = $data->whereBetween('garment_movements.created_at',[$from,$to])
                              ->whereIn('garment_movements.locator_from',['storage_md','marker_storage','qc_storage']);
                          }else if ($type=="barcodeid"){
                              $data = $data->where('barcode_garment.barcode_id',$barcode_id)
                              ->whereIn('garment_movements.locator_to',['checkin_md','marker_storage','qc_storage']);
                          }


                          if ($filter!="") {
                            
                            $data = $data->where(function($query) use ($filter){
                                $query->where('barcode_garment.barcode_id','like','%'.$filter.'%')
                                  ->orWhere('barcode_garment.current_location','like','%'.$filter.'%')
                                  ->orWhere('master_mo.documentno','like','%'.$filter.'%')
                                  ->orWhere('master_mo.style','like','%'.$filter.'%')
                                  ->orWhere('master_mo.article','like','%'.$filter.'%')
                                  ->orWhere('master_mo.brand','like','%'.$filter.'%')
                                  ->orWhere('garment_movements.borrower','like','%'.$filter.'%')
                                  ->orWhere('users.name','like','%'.$filter.'%')
                                  ->orWhere('garment_movements.locator_from','like','%'.$filter.'%')
                                  ->orWhere('garment_movements.locator_to','like','%'.$filter.'%');
                            });
                            
                         }
        
        return DataTables::of($data)
                          ->editColumn('barcode_id',function($data){
                             return $data->barcode_id." ".(isset($data->sets) ? $data->sets : "");
                          })
                          ->addColumn('item',function($data){
                              return $data->season."-".$data->style."-".$data->article."-".$data->size;
                          })
                          ->addColumn('checkin_pic',function($data) use ($type){
                                if ($type=='checkin' || $type=="barcodeid") {
                                    return $data->username." (".$data->nik.")";
                                }else{
                                    $gm = DB::table('garment_movements')
                                              ->join('users','garment_movements.user_id','users.id')
                                              ->where('garment_movements.barcode_id',$data->barcode_id)
                                              ->where('garment_movements.created_at','<',$data->created_at)
                                              ->orderBy('garment_movements.created_at','desc')
                                              ->skip(1)
                                              ->select('users.nik','users.name')
                                              ->first();

                                    // return $gm->name." (".$gm->nik.")";

                                    return isset($gm) ? $gm->name : null;
                                  }
                          })
                          ->addColumn('checkin_date',function($data) use ($type){
                                if ($type=='checkin' || $type=="barcodeid") {
                                   return date_format(date_create($data->created_at),'d-M-Y H:i:s');
                                }else{
                                    $gm = DB::table('garment_movements')
                                                              ->where('barcode_id',$data->barcode_id)
                                                              ->where('created_at','<',$data->created_at)
                                                              ->orderBy('created_at','desc')
                                                              ->skip(1)
                                                              ->first();
                                  return date_format(date_create($gm->created_at),'d-M-Y H:i:s');
                                }
                          })
                          ->addColumn('checkout',function($data) use ($type){
                                if ($type=='checkin' || $type=="barcodeid") {
                                    $gm = DB::table('garment_movements')
                                                ->join('users','garment_movements.user_id','users.id')
                                                ->where('garment_movements.barcode_id',$data->barcode_id)
                                                ->where('garment_movements.created_at','>',$data->created_at)
                                                ->whereNotIn('garment_movements.locator_to',['checkin_md','handover','marker_storage','preparation','qc_storage','reservation','storage_md'])
                                                // ->where('description','NOT LIKE','%Reverse%')
                                                ->orderBy('garment_movements.created_at','asc')
                                                ->select('garment_movements.barcode_id','garment_movements.description','garment_movements.created_at','garment_movements.locator_to','garment_movements.borrower','users.nik','users.name')->first();
                                    // dd($gm);
                             
                                    if (isset($gm) && str_contains($gm->description,"Revers")==null) {
                                        $borw = Employee::where('nik',$gm->borrower)->first();

                                        return '<label style="font-weight: bold;">To Dept : </label>'.strtoupper($gm->locator_to).'<br> <label style="font-weight: bold;">PIC : </label>'.$borw->name.'('.$borw->nik.') <br> <label style="font-weight: bold;">Date : </label>'.date_format(date_create($gm->created_at),'d-m-Y H:i:s').'<br>';

                                        // return isset($borw) ? $borw->name : null;
                                    }else if (isset($gm) && str_contains($gm->description,"Revers")!=null){
                                        return '<label style="font-weight: bold;">Return By : </label>'.$gm->name.'('.$gm->nik.')<br>
                                        <label style="font-weight: bold;">Date : </label>'.date_format(date_create($gm->created_at),'d-m-Y H:i:s');

                                        // return $gm->created_at;
                                    
                                    }else{
                                        return "";
                                    }
                          
                                }else{
                                  $borw = Employee::where('nik',$data->borrower)->first();
                                  return '<label style="font-weight: bold;">To : </label>'.strtoupper($data->locator_to).'<br> <label style="font-weight: bold;">PIC : </label>'.$borw->name.'('.$borw->nik.') <br> <label style="font-weight: bold;">Date : </label>'.date_format(date_create($data->created_at),'d-m-Y H:i:s').'<br>';
                                  // return isset($borw) ? $borw->nik : null;
                                }
                          })
                          ->addColumn('store',function($data) use ($type){
                            
                            $str = DB::table('garment_movements')
                                  ->Leftjoin('locator','garment_movements.locator_id','locator.id')
                                  ->Leftjoin('users','garment_movements.user_id','users.id')
                                  ->where('garment_movements.barcode_id',$data->barcode_id)
                                  ->where('garment_movements.is_canceled',false)
                                  // ->where('garment_movements.created_at','>',$data->created_at)
                                  ->select('garment_movements.id','garment_movements.created_at','garment_movements.locator_from','garment_movements.locator_to','garment_movements.locator_id','locator.name','locator.description','users.nik',DB::raw("users.name as username"));
                                  // ->orderBy('garment_movements.created_at','asc')
                                  // ->first();

                                  if($type=="checkin" || $type=="barcodeid"){
                                    $str = $str->where('garment_movements.created_at','>',$data->created_at)->orderBy('garment_movements.created_at','asc')->first();
                                  }else{
                                    $str = $str->where('garment_movements.created_at','<',$data->created_at)->orderBy('garment_movements.created_at','desc')->first();
                                  }
                                  // dd($str);
                 
                              if ($data->locator_to=='checkin_md' && (isset($str) && $str->locator_to=="storage_md")) {

                                    return '<label style="font-weight: bold;">'.$str->name.' - '.$str->locator_id.'</label> <br>
                                    <label style="font-weight: bold;">Store By : </label>'.$str->username.'('.$str->nik.') <br>
                                    <label style="font-weight: bold;">Store At : </label>'.date_format(date_create($str->created_at),'d-m-Y H:i:s');

                                    // return isset($str) ? $str->id : null;
                              }else if(($data->locator_from=='marker' && $data->locator_to=='marker_storage')|| ($data->locator_from=='qc' && $data->locator_to=='qc_storage')){
                                  return '<label style="font-weight: bold;">'.$data->name.' - '.$data->locator_id.'</label> <br>
                                  <label style="font-weight: bold;">Store By : </label>'.$data->username.'('.$data->nik.') <br>
                                  <label style="font-weight: bold;">Store At : </label>'.date_format(date_create($data->created_at),'d-m-Y H:i:s');

                                  // return $data->created_at;
                              }elseif($data->locator_to=='checkin_md' && !isset($str)){
                                return null;
                              }else{
                                // return $data->locator_from." ".$data->locator_to." ".(isset($str) ? ($str->locator_from." ".$str->locator_to) : null);

                                return '<label style="font-weight: bold;">'.$str->name.' - '.$str->locator_id.'</label> <br>
                                <label style="font-weight: bold;">Store By : </label>'.$str->username.'('.$str->nik.') <br>
                                <label style="font-weight: bold;">Store At : </label>'.date_format(date_create($str->created_at),'d-m-Y H:i:s');
                                // return isset($str) ? $str->id : null;
                              }
                                
          

                          })
                          ->editColumn('current_location',function($data){
                              $rack = ['storage_md','marker_storage','qc_storage'];
                              if (!in_array($data->current_location,$rack) ) {
                                    return strtoupper($data->current_location);
                              }else{
                                  $curloc =DB::table('garment_movements')
                                                ->Leftjoin('locator','garment_movements.locator_id','locator.id')
                                                ->where('garment_movements.barcode_id',$data->barcode_id)
                                                ->whereNull('garment_movements.deleted_at')
                                                ->select('locator.name','locator.id')
                                                ->first();

                                  return $curloc->name.' - '.$curloc->id;
                              }

                          })
                          ->rawColumns(['barcode_id','item','checkin_pic','checkin_date','checkout','store','current_location'])
                          ->make(true);
    }

    public function exportMutation(Request $request){
      $from = date_format(date_create(explode(" - ",$request->date_range)[0]),'Y-m-d 00:00:00');
      $to = date_format(date_create(explode(" - ",$request->date_range)[1]),'Y-m-d 23:59:59');
      $factory_id = $request->factory_id;
      $barcode_id = trim($request->barcode_id);
      $type = $request->radio;
      $filter = $request->filter;

      $data = DB::table('garment_movements')
                          ->join('barcode_garment','garment_movements.barcode_id','barcode_garment.barcode_id')
                          ->join('master_mo','barcode_garment.mo_id','master_mo.id')
                          ->join('users','garment_movements.user_id','users.id')
                          ->leftJoin('locator','garment_movements.locator_id','locator.id')
                          // ->whereBetween('garment_movements.created_at',[$from,$to])
                          ->where('barcode_garment.factory_id',$factory_id)
                          ->where('garment_movements.is_canceled',false)
                          // ->where('barcode_garment.barcode_id','2022110200001')
                          ->select(
                            'barcode_garment.barcode_id',
                            'barcode_garment.current_location',
                            'barcode_garment.factory_id',
                            'barcode_garment.sets',
                            'barcode_garment.reciept_date',
                            'master_mo.documentno',
                            'master_mo.season',
                            'master_mo.style',
                            'master_mo.article',
                            'master_mo.size',
                            'master_mo.brand',
                            'master_mo.qty',
                            'master_mo.uom',
                            'master_mo.status',
                            'master_mo.product_name',
                            'master_mo.product_category',
                            'master_mo.color',
                            'master_mo.docstatus',
                            'master_mo.remark',
                            'garment_movements.locator_from',
                            'garment_movements.locator_to',
                            'garment_movements.locator_id',
                            'garment_movements.description',
                            'garment_movements.borrower',
                            'garment_movements.line',
                            'garment_movements.created_at',
                            'locator.name',
                            'locator.description',
                            'users.nik',
                            db::raw("users.name as username")
                          )->orderBy('garment_movements.created_at','asc');

                          // if ($type=="checkin") {
                          //     $data = $data->whereIn('garment_movements.locator_to',['checkin_md','marker_storage','qc_storage']);
                          // }else{
                          //     $data = $data->whereIn('garment_movements.locator_from',['storage_md','marker_storage','qc_storage']);
                          // }

                          if ($type=="checkin") {
                              $data = $data->whereBetween('garment_movements.created_at',[$from,$to])
                              ->whereIn('garment_movements.locator_to',['checkin_md','marker_storage','qc_storage']);
                          }else if ($type=="checkout"){
                              $data = $data->whereBetween('garment_movements.created_at',[$from,$to])
                              ->whereIn('garment_movements.locator_from',['storage_md','marker_storage','qc_storage']);
                          }else if ($type=="barcodeid"){
                              $data = $data->where('barcode_garment.barcode_id',$barcode_id)
                              ->whereIn('garment_movements.locator_to',['checkin_md','marker_storage','qc_storage']);
                          }

                          if ($filter!="") {
                            
                            $data = $data->where(function($query) use ($filter){
                                $query->where('barcode_garment.barcode_id','like','%'.$filter.'%')
                                  ->orWhere('barcode_garment.current_location','like','%'.$filter.'%')
                                  ->orWhere('master_mo.documentno','like','%'.$filter.'%')
                                  ->orWhere('master_mo.style','like','%'.$filter.'%')
                                  ->orWhere('master_mo.article','like','%'.$filter.'%')
                                  ->orWhere('master_mo.brand','like','%'.$filter.'%')
                                  ->orWhere('garment_movements.borrower','like','%'.$filter.'%')
                                  ->orWhere('users.name','like','%'.$filter.'%')
                                  ->orWhere('garment_movements.locator_from','like','%'.$filter.'%')
                                  ->orWhere('garment_movements.locator_to','like','%'.$filter.'%');
                            });
                            
                         }
        
        $i = 1;

        $filename = "Report_Mutation_".ucwords($type)."_AOI_".$factory_id."_".$from."-".$to.".xlsx";
        $data_result = $data->get();
        foreach ($data_result as $data_results) {
            $data_results->no = $i++;
        }

        // dd($data_result);
        if ($data_result->count()>0) {
            return (new FastExcel($data_result))->download($filename,function($row) use ($i,$type){
                $rack = ['storage_md','marker_storage','qc_storage'];
                if (!in_array($row->current_location,$rack) ) {
                      $currentloc= strtoupper($row->current_location);
                }else{
                    $curloc =DB::table('garment_movements')
                                  ->Leftjoin('locator','garment_movements.locator_id','locator.id')
                                  ->where('garment_movements.barcode_id',$row->barcode_id)
                                  ->whereNull('garment_movements.deleted_at')
                                  ->select('locator.name','locator.id')
                                  ->first();

                    $currentloc= $curloc->name.' - '.$curloc->id;
                }


                if ($type=="checkin" || $type=="barcodeid") {
                    $checkinpic = $row->username." (".$row->nik.")";
                    $checkindate = date_format(date_create($row->created_at),'d-m-Y H:i:s');



                    $gmc = DB::table('garment_movements')
                              ->join('users','garment_movements.user_id','users.id')
                              ->where('garment_movements.barcode_id',$row->barcode_id)
                              ->where('garment_movements.created_at','>',$row->created_at)
                              ->whereNotIn('garment_movements.locator_to',['checkin_md','handover','marker_storage','preparation','qc_storage','reservation','storage_md'])
                              // ->where('description','NOT LIKE','%Reverse%')
                              ->orderBy('garment_movements.created_at','asc')
                              ->select('garment_movements.barcode_id','garment_movements.description','garment_movements.created_at','garment_movements.locator_to','garment_movements.borrower','users.nik','users.name')->first();
       
                  if (isset($gmc) && str_contains($gmc->description,"Revers")==null) {
                      $borw = Employee::where('nik',$gmc->borrower)->first();

                      $checkoutdate = date_format(date_create($gmc->created_at),'d-m-Y H:i:s');
                      $checkoutpic = $borw->name.'('.$borw->nik.')';
                      $checkoutpto = strtoupper($gmc->locator_to);

                  }else if (isset($gmc) && str_contains($gmc->description,"Revers")!=null){

                      $checkoutdate = date_format(date_create($gmc->created_at),'d-m-Y H:i:s');
                      $checkoutpic = $gmc->name.'('.$gmc->nik.')';
                      $checkoutpto = strtoupper($gmc->locator_to);
                  
                  }else{
                      $checkoutdate = null;
                      $checkoutpic = null;
                      $checkoutpto = null;
                  }
                }else{
                  $gm = DB::table('garment_movements')
                        ->join('users','garment_movements.user_id','users.id')
                        ->where('garment_movements.barcode_id',$row->barcode_id)
                        ->where('garment_movements.created_at','<',$row->created_at)
                        ->orderBy('garment_movements.created_at','desc')
                        ->skip(1)
                        ->select('users.nik','users.name','garment_movements.created_at')
                        ->first();
        
                    $checkinpic = isset($gm) ? ($gm->name." (".$gm->nik.")") :null;
                    $checkindate =isset($gm) ? date_format(date_create($gm->created_at),'d-m-Y H:i:s') :null;


                    $borw = Employee::where('nik',$row->borrower)->first();

                    $checkoutdate = date_format(date_create($row->created_at),'d-m-Y H:i:s');
                    $checkoutpic = isset($borw) ? ($borw->name.'('.$borw->nik.')') : null;
                    $checkoutpto = strtoupper($row->locator_to);
                }

                $str = DB::table('garment_movements')
                    ->Leftjoin('locator','garment_movements.locator_id','locator.id')
                    ->Leftjoin('users','garment_movements.user_id','users.id')
                    ->where('garment_movements.barcode_id',$row->barcode_id)
                    ->where('garment_movements.is_canceled',false)
                    ->select('garment_movements.id','garment_movements.created_at','garment_movements.locator_from','garment_movements.locator_to','garment_movements.locator_id','locator.name','locator.description','users.nik',DB::raw("users.name as username"));
                    // ->orderBy('garment_movements.created_at','asc')
                    // ->first();

                    if($type=="checkin"){
                      $str = $str->where('garment_movements.created_at','>',$row->created_at)->orderBy('garment_movements.created_at','asc')->first();
                    }else{
                      $str = $str->where('garment_movements.created_at','<',$row->created_at)->orderBy('garment_movements.created_at','desc')->first();
                    }
                    // dd($str);


                if ($row->locator_to=='checkin_md' && (isset($str) && $str->locator_to=="storage_md")) {
                      $storedate = date_format(date_create($str->created_at),'d-m-Y H:i:s');
                      $storeto = $str->name.' - '.$str->locator_id;
                      $storepic = $str->username.'('.$str->nik.')' ;
                }else if(($row->locator_from=='marker' && $row->locator_to=='marker_storage')|| ($row->locator_from=='qc' && $row->locator_to=='qc_storage')){

                      $storedate = date_format(date_create($row->created_at),'d-m-Y H:i:s');
                      $storeto = $row->name.' - '.$row->locator_id;
                      $storepic = $row->username.'('.$row->nik.')' ;

                }elseif($row->locator_to=='checkin_md' && !isset($str)){

                  $storedate = null;
                  $storeto = null;
                  $storepic = null;
                }else{

                      $storedate = isset($str->created_at) ? date_format(date_create($str->created_at),'d-m-Y H:i:s') : null;
                      $storeto = $str->name.' - '.$str->locator_id;
                      $storepic = $str->username.'('.$str->nik.')' ;
                }
                

                return[
                    '#'=>$row->no,
                    'Barcode ID'=>$row->barcode_id,
                    'Item'=>$row->season."-".$row->style."-".$row->article."-".$row->size,
                    'Document No.'=>$row->documentno,
                    'Brand'=>$row->brand,
                    'Date Reciept'=>date_format(date_create($row->reciept_date),'d-m-Y'),
                    'Status'=>$row->status,
                    'Color'=>$row->color,
                    'Prod. Name'=>$row->product_name,
                    'Prod. Ctg.'=>$row->product_category,
                    'Checkin Date'=>$checkindate,
                    'Checkin Pic'=>$checkinpic,
                    'Store date'=>$storedate,
                    'Storage'=>$storeto,
                    'Pic Store'=>$storepic,
                    'Checkout Date'=>$checkoutdate,
                    'Checkout To'=>$checkoutpto,
                    'Borrower'=>$checkoutpic,
                    'Current Location'=>$currentloc
                ];
            });
        }else{
          return response()->json('no data', 422);
        }
    }
    //report mutation
}
