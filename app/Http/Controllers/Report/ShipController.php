<?php

namespace App\Http\Controllers\Report;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use DB;
use Carbon\Carbon;
use DataTables;

use App\User;

use Rap2hpoutre\FastExcel\FastExcel;

class ShipController extends Controller
{
    public function index(){
        $fact = DB::Table('factory')->whereNull('deleted_at')->get();

        return view('report.shipping')->with('fact',$fact);
    }

    public function getData(Request $request){
        $date_range = explode('-', preg_replace('/\s+/', '', $request->date_range));
        $from = date_format(date_create($date_range[0]), 'Y-m-d 00:00:00');
        $to = date_format(date_create($date_range[1]), 'Y-m-d 23:59:59');
        $factory_id = $request->factory_id;

        $data =  DB::table('ns_report_ship')
                  ->whereBetween('created_at',[$from,$to])
                  ->where('factory_id',$factory_id);

        return DataTables::of($data)
                  ->editColumn('product_category',function($data){
                    return $data->product_category." (".$data->product_name.")";
                  })
                  ->editColumn('created_at',function($data){
                        return Carbon::parse($data->created_at)->format('Y-m-d H:i:s')." By ".$data->name;
                  })
                  ->editColumn('borrower',function($data){
                        $get = DB::Table('employee')->where('nik',$data->borrower)->first();

                        return $get->name." (".$get->nik.")";
                  })
                  ->rawColumns(['product_category','created_at','borrower'])->make(true);
    }

    public function exportShip(Request $request){
        $date_range = explode('-', preg_replace('/\s+/', '', $request->date_range));
        $from = date_format(date_create($date_range[0]), 'Y-m-d 00:00:00');
        $to = date_format(date_create($date_range[1]), 'Y-m-d 23:59:59');
        $factory_id = $request->factory_id;
        $filterby = $request->filterby;

        $data =  DB::table('ns_report_ship')
                  ->whereBetween('created_at',[$from,$to])
                  ->where('factory_id',$factory_id);

        if (isset($filterby)) {
            $data = $data->where('documentno','LIKE','%'.$filterby.'%')
                            ->orwhere('item','LIKE','%'.$filterby.'%')
                            ->orwhere('barcode_id','LIKE','%'.$filterby.'%')
                            ->orwhere('product_category','LIKE','%'.$filterby.'%')
                            ->orwhere('product_name','LIKE','%'.$filterby.'%')
                            ->orwhere('created_at','LIKE','%'.$filterby.'%');
        }

       $i = 1;

       $filename = "AOI_".$factory_id."_report_shipping_from_".$from."_to_".$to;
       $data_result = $data->get();
       foreach ($data_result as $data_results) {
          $data_results->no=$i++;
       }

       if ($data_result->count()>0) {
          return (new FastExcel($data_result))->download($filename.'.xlsx',function($row) use($i){

           $pic = DB::Table('employee')->where('nik',$row->borrower)->first();

            return
              [
                '#'=>$row->no,
                'Barcode_id'=>$row->barcode_id,
                'Item'=>$row->item,
                'Doc. No.'=>$row->documentno,
                'Brand'=>$row->brand,
                'Date Reciept'=>$row->date_reciept,
                'Status'=>$row->status,
                'Color'=>$row->color,
                'Prod. Name'=>$row->product_name,
                'Prod. Ctg.'=>$row->product_category,
                'Scan Date'=>Carbon::parse($row->created_at)->format('Y-m-d H:i:s')." By ".$row->name,
                'PIC '=>$pic->name." (".$pic->nik.")"

              ];
          });
       }else{
          return response()->json('no data', 422);
       }

    }
}
