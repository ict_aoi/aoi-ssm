<?php

namespace App\Http\Controllers\Report;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use DB;
use Carbon\Carbon;
use DataTables;

use App\Models\GarmentId;
use App\Models\GarmentMove;
use App\Models\Locator;
use App\User;

use Rap2hpoutre\FastExcel\FastExcel;

class RecieptController extends Controller
{
    public function index(){
      $factory = DB::table('factory')->whereNull('deleted_at')->get();
      return view('report.reciept')->with('factory',$factory);
    } 

    // public function ajaxGetData(Request $request){
    //   $date_range = explode('-', preg_replace('/\s+/', '', $request->date_range));
    //     $from = date_format(date_create($date_range[0]), 'Y-m-d');
    //     $to = date_format(date_create($date_range[1]), 'Y-m-d');
    //     $factory_id = $request->factory_id;

    //     $data =  DB::table('ns_report_reciept')
    //               ->whereBetween('date_reciept',[$from,$to])
    //               ->where('factory_id',$factory_id);

    //     return DataTables::of($data)
    //               ->editColumn('current_location',function($data){
    //                 return strtoupper($data->current_location);
    //               })
    //               ->rawColumns(['current_location'])->make(true);

    // }

    public function ajaxGetData(Request $request){
      
      $date_range = explode('-', preg_replace('/\s+/', '', $request->date_range));

        $factory_id = $request->factory_id;
        $radio_type = $request->radio_type;
        $filter  = trim($request->search['value']);

        $data = DB::table('master_mo')
                        ->Join('barcode_garment','master_mo.id','barcode_garment.mo_id')
                        ->Join('garment_movements','barcode_garment.barcode_id','garment_movements.barcode_id')
                        ->where('barcode_garment.factory_id',$factory_id);
        
        if ($radio_type=="receipt") {
          $from = date_format(date_create($date_range[0]), 'Y-m-d');
          $to = date_format(date_create($date_range[1]), 'Y-m-d');

          $data = $data->whereBetween('barcode_garment.reciept_date',[$from,$to])
                                  ->whereNull('master_mo.deleted_at')
                                  ->whereNull('barcode_garment.deleted_at')
                                  ->whereNull('garment_movements.deleted_at')
                                  ->where([
                                    'garment_movements.is_canceled'=>false,
                                    'master_mo.is_reciept'=>true
                                  ])
                                  ->orderBy('barcode_garment.reciept_date','asc');
                                  
        }else{
          $from = date_format(date_create($date_range[0]), 'Y-m-d 00:00:00');
          $to = date_format(date_create($date_range[1]), 'Y-m-d 23:59:59');

          $data = $data->whereBetween('garment_movements.created_at',[$from,$to])
                            ->where('garment_movements.locator_to','checkin_md')
                            ->where('garment_movements.locator_from','<>','reservation')
                            ->where([
                              'garment_movements.is_canceled'=>false,
                              'master_mo.is_reciept'=>true
                            ])
                            ->whereNull('master_mo.deleted_at')
                            ->whereNull('barcode_garment.deleted_at')
                            // ->whereNull('garment_movements.deleted_at')
                            ->orderBy('garment_movements.created_at','asc');
        }


        if($filter!=null || $filter!=""){
            $data = $data->where(function($query) use ($filter){
                $query->where('master_mo.documentno','LIKE','%'.$filter.'%')
                            ->orWhere('master_mo.season','LIKE','%'.$filter.'%')
                            ->orWhere('master_mo.style','LIKE','%'.$filter.'%')
                            ->orWhere('master_mo.article','LIKE','%'.$filter.'%')
                            ->orWhere('master_mo.size','LIKE','%'.$filter.'%')
                            ->orWhere('master_mo.color','LIKE','%'.$filter.'%')
                            ->orWhere('master_mo.status','LIKE','%'.$filter.'%')
                            ->orWhere('master_mo.product_category','LIKE','%'.$filter.'%')
                            ->orWhere('master_mo.product_name','LIKE','%'.$filter.'%')
                            ->orWhere('master_mo.brand','LIKE','%'.$filter.'%')
                            ->orWhere('barcode_garment.sets','LIKE','%'.$filter.'%')
                            ->orWhere('barcode_garment.current_location','LIKE','%'.$filter.'%')
                            ->orWhere('barcode_garment.factory_id','LIKE','%'.$filter.'%');
                

            });
        }

        $data = $data->select(
          'master_mo.id',
          'master_mo.documentno',
          'master_mo.season',
          'master_mo.style',
          'master_mo.article',
          'master_mo.size',
          'master_mo.color',
          'master_mo.status',
          'master_mo.product_category',
          'master_mo.product_name',
          'master_mo.brand',
          'master_mo.remark',
          'barcode_garment.sets',
          'barcode_garment.current_location',
          'barcode_garment.factory_id',
          'barcode_garment.storage',
          'barcode_garment.borrow',
          'garment_movements.created_at',
          'barcode_garment.barcode_id',
          'barcode_garment.reciept_date'
        );

        return DataTables::of($data)
                  ->editColumn('current_location',function($data){

                      $last = DB::table('garment_movements')
                                          ->leftJoin('locator','garment_movements.locator_id','locator.id')
                                          ->where('garment_movements.barcode_id',$data->barcode_id)
                                          ->where('garment_movements.is_canceled',false)
                                          ->whereNull('garment_movements.deleted_at')
                                          ->select('garment_movements.locator_to','locator.name')->first();

                        if(isset($last->name)){
                          $rack = '<span class="badge badge-success">'.$last->name.'</span>';
                        }else{
                          $rack = '';
                        }

                        return strtoupper($last->locator_to)."<br>".$rack;
                  })
                  ->editColumn('reciept_date',function($data) use ($radio_type){
                    if ($radio_type=="receipt") {
                     return date_format(date_create($data->reciept_date),'d-m-Y');
                    }else{
                      return date_format(date_create($data->created_at),'d-m-Y H:i:s');
                    }
                  })
                  ->editColumn('season',function($data){
                      return $data->season."-".$data->style."-".$data->article."-".$data->size;
                  })
                  ->editColumn('barcode_id',function($data){
                      if($data->sets==null){
                        return $data->barcode_id;
                      }else{
                        return $data->barcode_id." ".$data->sets;
                      }
                })
                  ->rawColumns(['current_location','reciept_date','season','barcode_id'])->make(true);

    }

    // public function exportReciept(Request $request){
    //     $date_range = explode('-', preg_replace('/\s+/', '', $request->date_range));
    //     $from = date_format(date_create($date_range[0]), 'Y-m-d');
    //     $to = date_format(date_create($date_range[1]), 'Y-m-d');
    //     $factory_id = $request->factory_id;
    //     $filterby = $request->filterby;

    //     $data =  DB::table('ns_report_reciept')
    //               ->whereBetween('date_reciept',[$from,$to])
    //               ->where('factory_id',$factory_id);

    //     if (isset($filterby)) {
    //         $data = $data->where('documentno','LIKE','%'.$filterby.'%')
    //                         ->orwhere('item','LIKE','%'.$filterby.'%')
    //                         ->orwhere('barcode_id','LIKE','%'.$filterby.'%')
    //                         ->orwhere('rack','LIKE','%'.$filterby.'%')
    //                         ->orwhere('current_location','LIKE','%'.$filterby.'%')
    //                         ->orwhere('date_reciept','LIKE','%'.$filterby.'%');
    //     }

    //    $i = 1;

    //    $filename = "AOI_".$factory_id."_report_reciept_from_".$from."_to_".$to;
    //    $data_result = $data->get();
    //    foreach ($data_result as $data_results) {
    //       $data_results->no=$i++;
    //    }

    //    if ($data_result->count()>0) {
    //       return (new FastExcel($data_result))->download($filename.'.xlsx',function($row) use($i){

           
    //         return
    //           [
    //             '#'=>$row->no,
    //             'Barcode_id'=>$row->barcode_id,
    //             'Item'=>$row->item,
    //             'Doc. No.'=>$row->documentno,
    //             'Brand'=>$row->brand,
    //             'Date Reciept'=>$row->date_reciept,
    //             'Status'=>$row->status,
    //             'Color'=>$row->color,
    //             'Prod. Name'=>$row->product_name,
    //             'Prod. Ctg.'=>$row->product_category,
    //             'Location.'=>strtoupper($row->current_location),
    //             'Rack.'=>$row->rack,
    //             'Remark'=>$row->remark

    //           ];
    //       });
    //    }else{
    //       return response()->json('no data', 422);
    //    }

    // }

    public function exportReciept(Request $request){
        $date_range = explode('-', preg_replace('/\s+/', '', $request->date_range));
       
        $factory_id = $request->factory_id;
        $filterby = $request->filterby;
        $radio_type = $request->radio_type;


        
       $i = 1;
        
       $data = DB::table('master_mo')
                        ->Join('barcode_garment','master_mo.id','barcode_garment.mo_id')
                        ->Join('garment_movements','barcode_garment.barcode_id','garment_movements.barcode_id')
                        ->where('barcode_garment.factory_id',$factory_id);
        
        if ($radio_type=="receipt") {

          $from = date_format(date_create($date_range[0]), 'Y-m-d');
          $to = date_format(date_create($date_range[1]), 'Y-m-d');

          $data = $data->whereBetween('barcode_garment.reciept_date',[$from,$to])
                                  ->whereNull('master_mo.deleted_at')
                                  ->whereNull('barcode_garment.deleted_at')
                                  ->whereNull('garment_movements.deleted_at')
                                  ->where([
                                    'garment_movements.is_canceled'=>false,
                                    'master_mo.is_reciept'=>true
                                  ])
                                  ->orderBy('barcode_garment.reciept_date','asc');
          $name = "receipt";
        }else{
          $from = date_format(date_create($date_range[0]), 'Y-m-d 00:00:00');
          $to = date_format(date_create($date_range[1]), 'Y-m-d 23:59:59');

          $data = $data->whereBetween('garment_movements.created_at',[$from,$to])
                            ->where('garment_movements.locator_to','checkin_md')
                            ->where('garment_movements.locator_from','<>','reservation')
                            ->where([
                              'garment_movements.is_canceled'=>false,
                              'master_mo.is_reciept'=>true
                            ])
                            ->whereNull('master_mo.deleted_at')
                            ->whereNull('barcode_garment.deleted_at')
                            // ->whereNull('garment_movements.deleted_at')
                            ->orderBy('garment_movements.created_at','asc');
          
          $name = "return";
        }

        $data = $data->select(
          'master_mo.id',
          'master_mo.documentno',
          'master_mo.season',
          'master_mo.style',
          'master_mo.article',
          'master_mo.size',
          'master_mo.color',
          'master_mo.status',
          'master_mo.product_category',
          'master_mo.product_name',
          'master_mo.brand',
          'master_mo.remark',
          'barcode_garment.sets',
          'barcode_garment.current_location',
          'barcode_garment.factory_id',
          'barcode_garment.storage',
          'barcode_garment.borrow',
          'garment_movements.created_at',
          DB::raw("CASE
              WHEN barcode_garment.sets IS NULL THEN barcode_garment.barcode_id
              WHEN barcode_garment.sets IS NOT NULL THEN concat(barcode_garment.barcode_id, ' ', barcode_garment.sets)::character varying
              ELSE NULL::character varying
          END AS barcode_id"),
          DB::raw("concat(master_mo.season, '-', master_mo.style, '-', master_mo.article, '-', master_mo.size) AS item"),
          DB::raw("barcode_garment.reciept_date AS date_receipt"),
          DB::raw("barcode_garment.barcode_id as barcode")
        );

       $filename = "AOI_".$factory_id."_report_".$name."_from_".$from."_to_".$to;
       $data_result = $data->get();
       foreach ($data_result as $data_results) {
          $data_results->no=$i++;
       }

       if ($data_result->count()>0) {
          return (new FastExcel($data_result))->download($filename.'.xlsx',function($row) use($i,$radio_type){

            $last = DB::table('garment_movements')
                              ->leftJoin('locator','garment_movements.locator_id','locator.id')
                              ->where('garment_movements.barcode_id',$row->barcode)
                              ->where('garment_movements.is_canceled',false)
                              ->whereNull('garment_movements.deleted_at')
                              ->select('garment_movements.locator_to','locator.name')->first();

              if(isset($last->name)){
                $rack = $last->name;
              }else{
                $rack = '';
              }

              if ($radio_type=="receipt") {
                $daterect = date_format(date_create($row->date_receipt),'d-m-Y');
               }else{
                 $daterect = date_format(date_create($row->created_at),'d-m-Y H:i:s');
               }

            return
              [
                '#'=>$row->no,
                'Barcode_id'=>$row->barcode_id,
                'Item'=>$row->item,
                'Doc. No.'=>$row->documentno,
                'Brand'=>$row->brand,
                'Date Reciept'=>$daterect,
                'Status'=>$row->status,
                'Color'=>$row->color,
                'Prod. Name'=>$row->product_name,
                'Prod. Ctg.'=>$row->product_category,
                'Location.'=>strtoupper($row->current_location)." ".$rack,
                'Remark'=>$row->remark

              ];
          });
       }else{
          return response()->json('no data', 422);
       }

    }


}