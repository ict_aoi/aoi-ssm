<?php

namespace App\Http\Controllers\Report;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use DB;
use Carbon\Carbon;
use DataTables;
use Excel;

use App\Models\GarmentId;
use App\Models\GarmentMove;
use App\Models\Locator;
use App\User;

use Rap2hpoutre\FastExcel\FastExcel;

class QcController extends Controller
{
    //qc stock
    public function stockQc(){
      $factory = DB::table('factory')->whereNull('deleted_at')->get();
      return view('report.stock.stock_qc')->with('factory',$factory);
    }


    public function ajaxGetStockQc(Request $request){
        $factory_id = $request->factory_id;

        $data =  DB::table('ns_report_stockqc')
                            ->where('factory_id',$factory_id)
                            ->orderBy('current_location','ASC')
                            ->orderBy('created_at','desc');

        return DataTables::of($data)
                            ->editColumn('current_location',function($data){
                                return strtoupper($data->current_location);
                            })
                            ->editColumn('rack',function($data){
                              if ($data->storage==true && $data->borrow==false) {
                                    $ret = $data->rack;
                              }else if ($data->storage==false && $data->borrow==true) {
                                    $emp = DB::table('employee')->where('nik',$data->borrower)->first();

                                    $ret = strtoupper($emp->name)." (".$emp->nik.")";
                              }

                              return $ret;
                            })
                            ->rawColumns(['current_location','rack'])->make(true);
    }

    public function exportStockQc(Request $request){
        $factory_id = $request->factory_id;
        $filterby = $request->filterby;

        $data =  DB::table('ns_report_stockqc')
                            ->where('factory_id',$factory_id);

        if (isset($filterby)) {
            $data = $data->where('barcode_id','LIKE','%'.$filterby.'%')
                          ->orWhere('item','LIKE','%'.$filterby.'%')
                          ->orWhere('current_location','LIKE','%'.$filterby.'%')
                          ->orWhere('documentno','LIKE','%'.$filterby.'%')
                          ->orWhere('rack','LIKE','%'.$filterby.'%');
        }

        $i = 1;

        
       $filename = "AOI_".$factory_id."_report_stock_qc";
       
      

      

       $data_result = $data->get();
       foreach ($data_result as $data_results) {
          $data_results->no=$i++;
       }

       if ($data_result->count()>0) {
          return (new FastExcel($data_result))->download($filename.'.xlsx',function($row) use($i){

                if ($row->current_location=="qc") {
                    $em= DB::table('employee')->where('nik',$row->borrower)->first();
                    $pic= $em->name." (".$em->nik.")";
                }else{
                    $pic = $row->rack;
                }

                $opt = User::where('id',$row->user_id)->select('name')->first();
            return
              [
                '#'=>$row->no,
                'Barcode_id'=>$row->barcode_id,
                'Item'=>$row->item,
                'Doc. No.'=>$row->documentno,
                'Brand'=>$row->brand,
                'Date Reciept'=>$row->date_reciept,
                'Status'=>$row->status,
                'Color'=>$row->color,
                'Prod. Name'=>$row->product_name,
                'Prod. Ctg.'=>$row->product_category,
                'Location'=>strtoupper($row->current_location),
                'Rack/Borrower'=>$pic,
                'Remark'=>$row->remark,
                'Scan Date'=>Carbon::parse($row->created_at)->format('d-m-Y H:i:s')." (".$opt->name.")"

              ];
          });
       }else{
          return response()->json('no data', 422);
       }

    }
    //qc stock


    // qc borrowing
    public function borrowingQc(){
        $factory = DB::table('factory')->whereNull('deleted_at')->get();
        return view('report.qc_borrowing')->with('factory',$factory); 
    }

    public function ajaxQcBorrowing(Request $request){
        $factory_id = $request->factory_id;
        $date_range = explode('-', preg_replace('/\s+/', '', $request->date_range));
        $from = date_format(date_create($date_range[0]), 'Y-m-d 00:00:00');
        $to = date_format(date_create($date_range[1]), 'Y-m-d 23:59:59');

        $data =  DB::table('ns_report_qc_borrowing')
                            ->where('factory_id',$factory_id)
                            ->whereBetween('created_at',[$from,$to])
                            // ->orderBy('current_location','ASC')
                            ->orderBy('created_at','desc');

        return DataTables::of($data)
                            ->editColumn('current_location',function($data){
                                return strtoupper($data->current_location);
                            })
                            ->addColumn('return',function($data){
                                $dept = DB::table('department')->where('value_name',$data->locator_to)->whereNull('deleted_at')->select('expired')->first();

                                return Carbon::parse($data->created_at)->addDays($dept->expired)->format('d-M-Y');
                            })
                            ->editColumn('created_at',function($data){
                                return Carbon::parse($data->created_at)->format('d-M-Y H:i:s');
                            })
                            ->editColumn('deleted_at',function($data){
                                return Carbon::parse($data->deleted_at)->format('d-M-Y H:i:s');
                            })
                            ->rawColumns(['current_location','return','created_at','deleted_at'])->make(true);
    }

    public function exportQcBorrowing(Request $request){
        $filterby = $request->filterby;
        $factory_id = $request->factory_id;
        $date_range = explode('-', preg_replace('/\s+/', '', $request->date_range));
        $from = date_format(date_create($date_range[0]), 'Y-m-d 00:00:00');
        $to = date_format(date_create($date_range[1]), 'Y-m-d 23:59:59');

        $data =  DB::table('ns_report_qc_borrowing')
                            ->where('factory_id',$factory_id)
                            ->whereBetween('created_at',[$from,$to])
                            ->orderBy('created_at','desc');

        if (isset($filterby)) {
            $data = $data->where('barcode_id','LIKE','%'.$filterby.'%')
                          ->orWhere('item','LIKE','%'.$filterby.'%')
                          ->orWhere('locator_from','LIKE','%'.$filterby.'%')
                          ->orWhere('locator_to','LIKE','%'.$filterby.'%')
                          ->orWhere('documentno','LIKE','%'.$filterby.'%')
                          ->orWhere('borrower','LIKE','%'.$filterby.'%');
        }

        $i = 1;

        
       $filename = "AOI_".$factory_id."_report_qc_borrowing";
       
      

      

       $data_result = $data->get();

      
       foreach ($data_result as $data_results) {
          $data_results->no=$i++;
       }

       if ($data_result->count()>0) {
          return (new FastExcel($data_result))->download($filename.'.xlsx',function($row) use($i){

                $dept = DB::table('department')->where('value_name',$row->locator_to)->whereNull('deleted_at')->select('expired')->first();

            return
              [
                '#'=>$row->no,
                'Barcode_id'=>$row->barcode_id,
                'Item'=>$row->item,
                'Doc. No.'=>$row->documentno,
                'Brand'=>$row->brand,
                'Status'=>$row->status,
                'Color'=>$row->color,
                'Prod. Name'=>$row->product_name,
                'Prod. Ctg.'=>$row->product_category,
                'Locator To'=>strtoupper($row->locator_to),
                'Remark'=>$row->remark,
                'Borrower'=>$row->borrower,
                'Borrowing Date'=>Carbon::parse($row->created_at)->format('d-M-Y H:i:s'),
                'Return Date'=> Carbon::parse($row->deleted_at)->format('d-M-Y H:i:s'),
                'Expired'=>Carbon::parse($row->created_at)->addDays($dept->expired)->format('d-M-Y')

              ];
          });
       }else{
          return response()->json('no data', 422);
       }
    }
    // qc borrowing

}
