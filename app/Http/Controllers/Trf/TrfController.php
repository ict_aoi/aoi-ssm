<?php

namespace App\Http\Controllers\Trf;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use DB;
use Carbon\Carbon;
use DataTables;
use SimpleSoftwareIO\QrCode\Facades\QrCode;
// use App\Models\Trftest;
// use App\Models\TrfDoc;
// use App\Models\TrfMeth;
// use App\Models\TrfCategory;

use App\Models\TRF\Mcategory;
use App\Models\TRF\MBuyer;
use App\Models\TRF\Trftesting;
use App\Models\TRF\TrfDocument;
use App\Models\TRF\TrfMethods;
use App\Models\TRF\Employee;
use App\Models\TRF\Mfact;

class TrfController extends Controller
{
    
    protected  $lab ;
    protected  $erp ;
    protected  $abs ;
    // protected  $api_url = "http://digilab.bbi-apparel.com/api/print/";

    public function __construct(){
        $this->lab = DB::connection('lab');
        $this->erp = DB::connection('erp');
        $this->abs = DB::connection('absen');
    }

    public function index(){
        return view('trf.index');
    }

    public function ajaxGetData(){
        $data = Trftesting::whereNull('deleted_at')->where('asal_specimen','DEVELOPMENT')->where('factory_id',Auth::user()->factory_id)->orderBy('created_at','desc');

        return DataTables::of($data)
                            ->editColumn('created_at',function($data){
                                return Carbon::parse($data->created_at)->format('d M Y H:i:s');
                            })
                            ->editColumn('test_required',function($data){
                                switch ($data->test_required) {
                                    case 'bulktesting_m':
                                        $treq = "1 Bulk Testing Model";
                                        break;
                                    
                                    case 'bulktesting_a':
                                        $treq = "1 Bulk Testing Model Level";
                                        break;

                                    case 'bulktesting_m':
                                        $treq = "1 Bulk Testing Article Level";
                                        break;

                                    case 'bulktesting_selective':
                                        $treq = "1 Bulk Testing Selective";
                                        break;

                                    case 'reordertesting_m':
                                        $treq = "Re-Order Testing Model Level";
                                        break;

                                    case 'reordertesting_a':
                                        $treq = "Re-Order Testing Model Level";
                                        break;

                                    case 'reordertesting_selective':
                                        $treq = "Re-Order Testing Selective";
                                        break;

                                    case 'retest':
                                        $treq = "Retest from ".$data->previous_trf;
                                        break;
                                    
                                }

                                return $treq;
                            })
                            ->addColumn('testing_method_id',function($data){
                                $getmth = $this->lab->table('trf_testing_methods')
                                                        ->join('master_method','trf_testing_methods.master_method_id','=','master_method.id')
                                                        ->where('trf_testing_methods.trf_id',$data->id)
                                                        ->whereNull('trf_testing_methods.deleted_at')
                                                        ->groupBy('trf_testing_methods.trf_id')
                                                        ->select(DB::raw("string_agg(concat(master_method.method_code,' (',master_method.method_name,')'),', ') as method_name"))->first();
                                return $getmth->method_name;
                            })
                             ->editColumn('status',function($data){
                                if ($data->status=="OPEN") {
                                    return '<span class="label label-default">OPEN</span>';
                                }else if ($data->status=="VERIFIED") {
                                   

                                   return '<span class="label label-primary">VERIFIED by '.$this->getAbs($data->verified_lab_by).'</span>';

                                }else if ($data->status=="ONPROGRESS") {
                                   
                                   return '<span class="label label-info">ONPROGRESS</span>';

                                }else if ($data->status=="CLOSED") {
                                   
                                   return '<span class="label label-success">CLOSED</span>';

                                }else if ($data->status=="REJECT") {
                                 
                                   return '<span class="label label-warning">REJECT by '.$this->getAbs($data->verified_lab_by).'</span>';
                                }else{
                                    return '<span class="label label-danger">ERROR</span>';
                                }
                            })
                            ->editColumn('date_information',function($data){
                                switch ($data->date_information_remark) {
                                    case 'buy_ready':
                                        return 'BUY READY  <br>'.date_format(date_create($data->date_information),'d-m-Y');
                                        break;

                                    case 'podd':
                                        return 'PODD <br>'.date_format(date_create($data->date_information),'d-m-Y');
                                        break;

                                    case 'output_sewing':
                                        return 'Output Sewing <br>'.date_format(date_create($data->date_information),'d-m-Y');
                                        break;
                                    
                                    default:
                                        return 'ERROR';
                                        break;
                                }
                            })
                            ->editColumn('category',function($data){
                                $ctg = Mcategory::where('id',$data->id_category)->first();

                                return '<b>Category </b> : '.$ctg->category.'<br><b>Category Specimen</b> : '.$ctg->category_specimen.'<br><b>Type Specimen</b> : '.$ctg->type_specimen;
                            })
                            ->addColumn('action',function($data){
                                if ($data->last_status!="REJECT") {
                                   return view('_action',[
                                                    'trfBarcode'=>route('trf.printBarcode',['id'=>$data->id]), 
                                                    'trfDetail'=>route('trf.printDetail',['id'=>$data->id]), 
                                                    'trfReport'=>route('trf.report',['id'=>$data->id]) ,
                                                ]);
                                }
                            })
                            ->rawColumns(['created_at','testing_method_id','status','action','date_information','category'])
                            ->make(true);
    }

    public function formCreateTrf(){
        
        $fact = Mfact::whereNull('deleted_at')->get();
        $buyer = MBuyer::whereNull('deleted_at')->get();

        return view('trf.create')->with('buyer',$buyer)
                                    ->with('fact',$fact);
       
    }

    public function ajaxGetCategory(Request $req){


        $data = $this->lab->table('ns_req_catg_method')
                                    ->where('buyer',trim($req->buyer))
                                    ->groupBy('category')
                                    ->select('category')
                                    ->orderBy('category')
                                    ->get();

        return response()->json(['data'=>$data],200);
    }

    public function ajaxGetCtgSpes(Request $req){


        $data = $this->lab->table('ns_req_catg_method')
                                    ->where('buyer',trim($req->buyer))
                                    ->where('category',trim($req->category))
                                    ->groupBy('category_specimen')
                                    ->select('category_specimen')
                                    ->orderBy('category_specimen')
                                    ->get();


                                    // dd($data,$ctg);
         return response()->json(['data'=>$data],200);
    }

    public function ajaxGetTypeSpc(Request $req){
        $data = $this->lab->table('ns_req_catg_method')
                                    ->where('buyer',trim($req->buyer))
                                    ->where('category',trim($req->category))
                                    ->where('category_specimen',trim($req->category_specimen))
                                    ->groupBy('type_specimen','master_category_id')
                                    ->select('type_specimen','master_category_id')
                                    ->orderBy('type_specimen')
                                    ->get();

                                    // dd($data);
         return response()->json(['data'=>$data],200);
    }


    public function ajaxGetTestMethod(Request $req){

        $data = $this->lab->table('ns_req_catg_method')
                    ->where('buyer',trim($req->buyer))
                    ->where('category',trim($req->category))
                    ->where('category_specimen',trim($req->category_specimen))
                    ->where('type_specimen',trim($req->type_specimen))
                    ->groupBy('master_method_id','method_code','method_name')
                    ->select('master_method_id','method_code','method_name')
                    ->get();
       
        return response()->json(['data'=>$data],200);
    }


    public function getDocument(Request $request){
        $type = $request->type;
        $docno = trim($request->docno);

        $load = $this->getDocErp($docno,$type);

        return response()->json(['data'=>$load],200);
    }

    public function addTrf(Request $request){



        $buyer = $request->buyer;
        $asal = $request->asal;
        $origin = $request->origin;
        $category_spesimen = $request->category_specimen;
        $type_spesimen = $request->type_specimen;
        $category = $request->category;
        $lab_loc = $request->labloction;
        $test_req = $request->test_req;
        $radio_date_info = $request->radio_date_info;
        $date_info = date_format(date_create($request->date_info),'Y-m-d');
        $part_test = trim($request->part_test);
        $returntest = $request->returntest;
        $no_trf = trim($request->no_trf);
        $datas = $request->datas;
        $test_methods = $request->test_methods;
        $code = $this->getCode($category_spesimen,$asal);
        
        $getCtg = Mcategory::where('category',$category)->where('category_specimen',$category_spesimen)->where('type_specimen',$type_spesimen)->whereNull('deleted_at')->first();

        $chkTrf = Trftesting::where('trf_id',$code)->whereNull('deleted_at')->exists();

        if ($chkTrf) {
            return response()->json('TRF Number was already ! ! !',422);
        }
  
        try {
            DB::begintransaction();
                $test = array(
                            'trf_id'=>$code,
                            'buyer'=>$buyer,
                            'lab_location'=>$lab_loc,
                            'nik'=>auth::user()->nik,
                            'platform'=>$origin,
                            'factory_id'=>auth::user()->factory_id,
                            'asal_specimen'=>$asal,
                            'category_specimen'=>$category_spesimen,
                            'category'=>$category,
                            'type_specimen'=>$type_spesimen,
                            'date_information'=>$date_info,
                            'date_information_remark'=>$radio_date_info,
                            'test_required'=>$test_req,
                            'part_of_specimen'=>$part_test,
                            'created_at'=>carbon::now(),
                            'return_test_sample'=>$returntest,
                            'status'=>'OPEN',
                            'previous_trf_id'=>isset($no_trf)  ? $no_trf : null,
                            'id_category'=>$getCtg->id,
                            'username'=>auth::user()->name
                        );

                $intest = Trftesting::firstorcreate($test);

                foreach ($datas as $dt) {
                  
                    $indocs = array(
                                'trf_id'=>$intest->id,
                                'document_type'=>$dt['mddoctype'],
                                'document_no'=>$dt['mddocno'],
                                'style'=>trim($dt['mdstyle']),
                                'article_no'=>trim($dt['mdarticle']),
                                'size'=>trim($dt['mdsize']),
                                'color'=>trim($dt['mdcolor']),
                                'fibre_composition'=>trim($dt['mdfibre']),
                                'fabric_finish'=>trim($dt['mdfabfin']),
                                'gauge'=>trim($dt['mdgauge']),
                                'fabric_weight'=>trim($dt['mdweight']),
                                'plm_no'=>trim($dt['mdplmno']),
                                'care_instruction'=>trim($dt['mdcare']),
                                'manufacture_name'=>trim($dt['mdmanuf']),
                                'export_to'=>trim($dt['mdexport']),
                                'nomor_roll'=>trim($dt['mdroll']),
                                'batch_number'=>trim($dt['mdbatch']),
                                'created_at'=>carbon::now(),
                                'item'=>trim($dt['mditem']),
                                'barcode_supplier'=>trim($dt['mdposupp']),
                                'additional_information'=>trim($dt['mdaddinfo']),
                                'yds_roll'=>trim($dt['mdydsroll']),
                                'description'=>trim($dt['mddescript']),
                                'season'=>trim($dt['mdseason'])
                            );
                
                    TrfDocument::firstOrCreate($indocs);
                }

                foreach ($test_methods as $tm){

                    $inmeth = array(
                                'trf_id'=>$intest->id,
                                'master_method_id'=>$tm,
                                'created_at'=>Carbon::now(),
                                'created_by'=>auth::user()->nik
                            );

                    TrfMethods::firstorcreate($inmeth);
                }
            DB::commit();

            $data_response = [
                            'status' => 200,
                            'output' => 'Save Trf Success No TRF '.$code
                          ];
        } catch (Exception $ex) {
             DB::rollback();
            $message = $ex->getMessage();
            ErrorHandler::db($message);

            $data_response = [
                            'status' => 422,
                            'output' => 'Save Trf Field No TRF . . .'.$message
                          ];
        }

        return response()->json(['data'=>$data_response]);

    }


    private function getCode($spcode,$asal){
      
        $gefac = $this->lab->table('factory')->where('id',Auth::user()->factory_id)->first();
        $sp = $this->lab->table('master_spesiment_code')->where('platform',$asal)->where('spesimen',$spcode)->whereNull('deleted_at')->first();
        $date = Carbon::now()->format('ymd');
        $last = Trftesting::where('factory_id',auth::user()->factory_id)->where('asal_specimen',$asal)->whereDate('created_at',Carbon::now()->format('Y-m-d'))->whereNull('deleted_at')->first();
        // $last = $this->lab->table('master_trf_testings')->first();

        if ($last==null) {
            $x = (int)0;
        }else{
            $x = (int)substr($last->trf_id,-3);

        }

        $rcode = "TRF/".$gefac->factory_name."/".$date."-".$sp->code.sprintf("%03s", $x+1);

        return $rcode;
    }


    private function getAbs($nik){
        $data = $this->abs->table('adt_bbigroup_emp_new')->where('nik',$nik)->first();

        $ret = isset($data->name) ? strtoupper($data->name) : $nik;

        return $ret;
    }


    public function printBarcode(Request $request){
        
        $data = Trftesting::where('id',trim($request->id))->where('deleted_at')->first();

        

        return view('trf.print_barcode',['data'=>$data]);
        // $url = "http://digilab.bbi-apparel.com/api/print/barcode?id=".trim($request->id);
        // return redirect()->to($url)->send();
    }

    // public function printDetail(Request $request){
    //     $id = trim($request->id);

    //     $data = Trftesting::where('id',$id)->whereNull('deleted_at')->first();
        
    //     switch ($data->status) {
    //         case 'OPEN' || 'ONPROGRESS' || 'CLOSED':
    //             $remk = "Status : ".$data->status;
    //         break;

    //         case 'VERIFIED':
    //             $remk = "Status : ".$data->status." Verified By : ".$this->getAbs($data->verified_lab_by)." Verified at : ". carbon::parse($data->verified_lab_date)->format('d F Y H-i-s');
    //         break;

    //         case 'REJECT':
    //             $remk = "Status : ".$data->status." Reject By : ".$this->getAbs($data->reject_by)." Verified at : ". carbon::parse($data->reject_date)->format('d F Y H-i-s');
    //         break;


    //         default:
    //             $remk = "ERROR";
    //             break;
            
    //     }



    //     $meth = $this->lab->table('trf_testing_methods')
    //                     ->where('trf_id',$data->id)
    //                     ->whereNull('deleted_at')
    //                     ->select('master_method_id')
    //                     ->get()->toArray();

    //     $docm = $this->lab->table('trf_testing_document')->where('trf_id',$data->id)->whereNull('deleted_at')->get();

    //     // $abs = $this->abs->table('adt_bbigroup_emp_new')->where('nik',$data->nik)->first();

    //     $filename = "TRF_DETAIL_".$data->trf_id;

    //     $pdf = \PDF::loadView('trf.form_trf',['data'=>$data,'method'=>$meth,'docm'=>$docm,'submit_by'=>$this->getAbs($data->nik),'remark'=>$remk])->setPaper('A4','Potrait');
    //     return $pdf->stream($filename);
    // }

    public function printDetail(Request $request){
         $id= trim($request->id);

        $data = $this->lab->table('trf_testings')
                            ->join('master_category','trf_testings.id_category','=','master_category.id')
                            ->join('factory','trf_testings.factory_id','=','factory.id')
                            ->where('trf_testings.id',$id)
                            ->whereNull('trf_testings.deleted_at')
                            ->select(
                                    'trf_testings.id',
                                    'trf_testings.trf_id',
                                    'trf_testings.lab_location',
                                    'trf_testings.buyer',
                                    'trf_testings.nik',
                                    'trf_testings.platform',
                                    'trf_testings.asal_specimen',
                                    'trf_testings.date_information',
                                    'trf_testings.date_information_remark',
                                    'trf_testings.test_required',
                                    'trf_testings.part_of_specimen',
                                    'trf_testings.created_at',
                                    'trf_testings.return_test_sample',
                                    'trf_testings.status',
                                    'trf_testings.verified_lab_date',
                                    'trf_testings.verified_lab_by',
                                    'trf_testings.reject_by',
                                    'trf_testings.reject_date',
                                    'trf_testings.additional_information',
                                    'trf_testings.previous_trf_id',
                                    'trf_testings.remarks',
                                    'master_category.category',
                                    'master_category.category_specimen',
                                    'master_category.type_specimen',
                                    'factory.factory_name'
                                )->first();


        $url = "http://digilab.bbi-apparel.com/api/print/barcode?id=".$data->id;
        $qrcode =  QrCode::format('svg')->size(150)->errorCorrection('H')->generate($url);

        // $testreq = HelperController::testRequired($data->test_required,$data->previous_trf_id);
        switch ($data->test_required) {
            case 'developtesting_t1':
                $testreq = "Develop Testing T1";
            break;

            case 'developtesting_t2':
                $testreq = "Develop Testing T2";
            break;

            case 'developtesting_selective':
                $testreq = "Develop Testing Selective";
            break;

            case 'bulktesting_m':
                $testreq = "1st Bulk Testing M (Model Level)";
            break;


            case 'bulktesting_a':
                $testreq = "1st Bulk Testing A (Article Level)";
            break;


            case 'bulktesting_selective':
                $testreq = "1st Bulk Testing Selective";
            break;

            case 'reordertesting_m':
                $testreq = "Re-Order Testing M (Model Level)";
            break;

            case 'reordertesting_a':
                $testreq = "Re-Order Testing A (Article Level)";
            break;

            case 'reordertesting_selective':
                $testreq = "Re-Order Testing Selective";
            break;

            case 'retest':
                $testreq = "Re-Test ".isset($data->previous_trf_id);
            break;

            default : 
                $testreq = 'ERROR';
            break;
            
          
        }

        // $dateinfo = HelperController::dateInfoSet($data->date_information,$data->date_information_remark);

        switch ($data->date_information_remark) {
            case 'buy_ready':
                $dateinfo = 'BUY READY  <br>'.date_format(date_create($data->date_information),'d-m-Y');
                break;

            case 'podd':
                $dateinfo = 'PODD <br>'.date_format(date_create($data->date_information),'d-m-Y');
                break;

            case 'output_sewing':
                $dateinfo = 'Output Sewing <br>'.date_format(date_create($data->date_information),'d-m-Y');
                break;
            
            default:
                $dateinfo = 'ERROR';
                break;
        }

        $meth = $this->lab->table('trf_testing_methods')
                        ->join('master_method','trf_testing_methods.master_method_id','=','master_method.id')
                        ->whereNull('trf_testing_methods.deleted_at')
                        ->where('trf_testing_methods.trf_id',$data->id)
                        ->select('master_method.id','master_method.method_code','master_method.method_name','master_method.category','master_method.type')->get()->toArray();

        $docm = TrfDocument::where('trf_id',$data->id)->whereNull('deleted_at')->get();

        $users = Employee::where('nik', $data->nik)->first();

       


        $vefpiclab = isset($data->verified_lab_by) ?  Employee::where('nik', $data->verified_lab_by)->first()['name'] : "";
        $vefdatelab = isset($data->verified_lab_date) ? carbon::parse($data->verified_lab_date)->format('d-m-Y H:i:s') : "";
        $filename = "TRF_DETAIL_".$data->trf_id;
        
        $pdf = \PDF::loadView('trf.detail_trf',['data'=>$data,'method'=>$meth,'docm'=>$docm,'submit_by'=>isset($users) ? $users->name : $data->nik,'test_required'=>$testreq,'dateinfo'=>$dateinfo,'labdate'=>$vefdatelab,'labpic'=>$vefpiclab,'qrcode'=>base64_encode($qrcode)])->setPaper('A4','Potrait');
        return $pdf->stream($filename);
    }


    static function getDocErp($docno,$type){
        $return = [];
        switch ($type) {
            case 'PO BUYER':
                $data = DB::connection('erp')->select("select * From digilab_trf_so('".$docno."')");

                break;

            case 'MO':
                $data = DB::connection('erp')->select("select * From digilab_trf_mo('".$docno."')");

                break;

            case 'ITEM':
                $data = DB::connection('erp')->select("select * From digilab_trf_item('".$docno."')");

                break;
            
            case 'PO SUPPLIER':
                $data = DB::connection('erp')->select("select * From digilab_trf_po('".$docno."')");

                break;
        }

        $i = 1;
        foreach ($data as $key => $value) {

            
            $xd['no']=$i++;
            $xd['documentno']= isset($value->documentno) ? $value->documentno: '';
            $xd['style']= isset($value->style) ? $value->style: '';
            $xd['article']= isset($value->article) ? $value->article: '';
            $xd['size']= isset($value->size) ? $value->size: '';
            $xd['color']= isset($value->color) ? $value->color: '';
            $xd['item']= isset($value->item) ? $value->item: '';
            $xd['season']= isset($value->season) ? $value->season: '';
            $xd['doctype']= isset($value->doctype) ? $value->doctype: '';
            $xd['supplier']= isset($value->supplier) ? $value->supplier: '';
            $xd['buyer']= isset($value->buyer) ? $value->buyer: '';
            $xd['export_to']= isset($value->export_to) ? $value->export_to: '';



            $return[]=$xd;

        }

        return $return;
        
    }

    public function report(Request $req){
        $id = $req->id;

        $trfdoc = [];
        $filename="TRF_REPORT_".$id;

                foreach ($this->getTrfDoc($id) as $tdc) {

                        switch ($tdc->date_information_remark) {
                            case 'buy_ready':
                                $dateinfo = 'BUY READY  <br>'.date_format(date_create($tdc->date_information),'d-m-Y');
                                break;

                            case 'podd':
                                $dateinfo = 'PODD <br>'.date_format(date_create($tdc->date_information),'d-m-Y');
                                break;

                            case 'output_sewing':
                                $dateinfo = 'Output Sewing <br>'.date_format(date_create($tdc->date_information),'d-m-Y');
                                break;
                            
                            default:
                                $dateinfo = 'ERROR';
                                break;
                        }


                        switch ($tdc->test_required) {
                            case 'developtesting_t1':
                                $testreq = "Develop Testing T1";
                            break;

                            case 'developtesting_t2':
                                $testreq = "Develop Testing T2";
                            break;

                            case 'developtesting_selective':
                                $testreq = "Develop Testing Selective";
                            break;

                            case 'bulktesting_m':
                                $testreq = "1st Bulk Testing M (Model Level)";
                            break;


                            case 'bulktesting_a':
                                $testreq = "1st Bulk Testing A (Article Level)";
                            break;


                            case 'bulktesting_selective':
                                $testreq = "1st Bulk Testing Selective";
                            break;

                            case 'reordertesting_m':
                                $testreq = "Re-Order Testing M (Model Level)";
                            break;

                            case 'reordertesting_a':
                                $testreq = "Re-Order Testing A (Article Level)";
                            break;

                            case 'reordertesting_selective':
                                $testreq = "Re-Order Testing Selective";
                            break;

                            case 'retest':
                                $testreq = "Re-Test ".isset($tdc->previous_trf_id);
                            break;

                            default : 
                                $testreq = 'ERROR';
                            break;
                        }

                    $api = 'https://digilab.bbi-apparel.com/api/print/report-specimen?id='.$tdc->id."&docid=".$tdc->doc_id;
                    $qrcode =  QrCode::format('svg')->size(120)->errorCorrection('H')->generate($api);
                    $fac = $this->lab->table('factory')->where('id',$tdc->factory_id)->first();
                    $dx['trfid']= $tdc->id;
                    $dx['notrf']= $tdc->trf_id;
                    $dx['buyer']= $tdc->buyer;
                    $dx['lab_location']= $tdc->lab_location;
                    $dx['nik']= $tdc->nik;
                    $dx['platform']= $tdc->platform;
                    $dx['factory_id']= $tdc->factory_id;
                    $dx['fact']= $fac->factory_name;
                    $dx['asal_specimen']= $tdc->asal_specimen;
                    $dx['date_information']= $tdc->date_information;
                    $dx['date_information_remark']= $tdc->date_information_remark;
                    $dx['set_date_info']= $dateinfo;
                    $dx['test_required']= $tdc->test_required;
                    $dx['set_testreq']= $testreq;
                    $dx['part_of_specimen']= $tdc->part_of_specimen;
                    $dx['created_at']= $tdc->created_at;
                    $dx['updated_at']= $tdc->updated_at;
                    $dx['return_test_sample']= $tdc->return_test_sample;
                    $dx['status']= $tdc->status;
                    $dx['remarks']= $tdc->remarks;
                    $dx['previous_trf_id']= $tdc->previous_trf_id;
                    $dx['id_category']= $tdc->id_category;
                    $dx['pic_test']= $tdc->pic_test;
                    $dx['approval_test']= $tdc->approval_test;
                    $dx['doc_id']= $tdc->doc_id;
                    $dx['document_type']= $tdc->document_type;
                    $dx['document_no']= $tdc->document_no;
                    $dx['season']= $tdc->season;
                    $dx['style']= $tdc->style;
                    $dx['article_no']= $tdc->article_no;
                    $dx['size']= $tdc->size;
                    $dx['color']= $tdc->color;
                    $dx['fibre_composition']= $tdc->fibre_composition;
                    $dx['fabric_finish']= $tdc->fabric_finish;
                    $dx['gauge']= $tdc->gauge;
                    $dx['fabric_weight']= $tdc->fabric_weight;
                    $dx['plm_no']= $tdc->plm_no;
                    $dx['care_instruction']= $tdc->care_instruction;
                    $dx['manufacture_name']= $tdc->manufacture_name;
                    $dx['export_to']= $tdc->export_to;
                    $dx['nomor_roll']= $tdc->nomor_roll;
                    $dx['batch_number']= $tdc->batch_number;
                    $dx['item']= $tdc->item;
                    $dx['barcode_supplier']= $tdc->barcode_supplier;
                    $dx['additional_information']= $tdc->additional_information;
                    $dx['yds_roll']= $tdc->yds_roll;
                    $dx['description']= $tdc->description;
                    $dx['category']= $tdc->category;
                    $dx['category_specimen']= $tdc->category_specimen;
                    $dx['type_specimen']= $tdc->type_specimen;
                    $dx['test_pic']= isset($tdc->pic_test) ? $this->lab->table('users')->where('id',$tdc->pic_test)->first()['name'] :"";
                    $dx['test_appv']= isset($tdc->pic_test) ? $this->lab->table('users')->where('id',$tdc->approval_test)->first()['name'] :"";
                    $dx['qrcode']= base64_encode($qrcode);

                        $i=0;
                        foreach ($this->getTrfRest($tdc->doc_id) as $res) {

                            $tr['no']=++$i;
                            $tr['imgdir']='imagespc/'.$res->id.'.png';
                            $tr['requirment']=$this->operatorVal($res->requirement_id);
                            $tr['before_test']=$res->before_test;
                            $tr['supplier_result']=$res->supplier_result;
                            $tr['result']=$res->result;
                            $tr['method_code']=$res->method_code;
                            $tr['method_name']=$res->method_name;
                            $tr['set_result']=$this->ReportResult($res->requirement_id,$res->result);
                            $dx['result_test'][]= $tr;
                        }

                    $trfdoc[] = $dx;
               }
        
        $pdf = \PDF::loadView('trf.report',['data'=>$trfdoc])->setPaper('A4','Potrait');
       return $pdf->stream($filename);
    }

    private function getTrfDoc($id){
        $gtrdoc = $this->lab->table('trf_testings')
                            ->join('trf_testing_document','trf_testings.id','=','trf_testing_document.trf_id')
                            ->join('master_category','trf_testings.id_category','=','master_category.id')
                            ->where('trf_testings.id',$id)
                            ->whereNull('trf_testings.deleted_at')
                            ->whereNull('trf_testing_document.deleted_at')
                            ->select(
                                'trf_testings.id',
                                'trf_testings.trf_id',
                                'trf_testings.buyer',
                                'trf_testings.lab_location',
                                'trf_testings.nik',
                                'trf_testings.platform',
                                'trf_testings.factory_id',
                                'trf_testings.asal_specimen',
                                'trf_testings.date_information',
                                'trf_testings.date_information_remark',
                                'trf_testings.test_required',
                                'trf_testings.part_of_specimen',
                                'trf_testings.created_at',
                                'trf_testings.updated_at',
                                'trf_testings.return_test_sample',
                                'trf_testings.status',
                                'trf_testings.remarks',
                                'trf_testings.previous_trf_id',
                                'trf_testings.id_category',
                                'trf_testings.pic_test',
                                'trf_testings.approval_test',
                                DB::raw('trf_testing_document.id as doc_id'),
                                'trf_testing_document.document_type',
                                'trf_testing_document.document_no',
                                'trf_testing_document.season',
                                'trf_testing_document.style',
                                'trf_testing_document.article_no',
                                'trf_testing_document.size',
                                'trf_testing_document.color',
                                'trf_testing_document.fibre_composition',
                                'trf_testing_document.fabric_finish',
                                'trf_testing_document.gauge',
                                'trf_testing_document.fabric_weight',
                                'trf_testing_document.plm_no',
                                'trf_testing_document.care_instruction',
                                'trf_testing_document.manufacture_name',
                                'trf_testing_document.export_to',
                                'trf_testing_document.nomor_roll',
                                'trf_testing_document.batch_number',
                                'trf_testing_document.item',
                                'trf_testing_document.barcode_supplier',
                                'trf_testing_document.additional_information',
                                'trf_testing_document.yds_roll',
                                'trf_testing_document.description',
                                'master_category.category',
                                'master_category.category_specimen',
                                'master_category.type_specimen'
                                );
        return $gtrdoc->get();
    }


    private function getTrfRest($docid){
        $getRest = $this->lab->table('trf_result_test')
                            ->join('trf_requirement_set','trf_result_test.set_req_id','=','trf_requirement_set.id')
                            ->join('trf_testing_methods','trf_requirement_set.trf_meth_id','=','trf_testing_methods.id')
                            ->join('master_method','trf_testing_methods.master_method_id','=','master_method.id')
                            ->where('trf_result_test.trf_doc_id',$docid)
                            ->whereNull('trf_result_test.deleted_at')
                            ->whereNull('trf_requirement_set.deleted_at')
                            ->whereNull('trf_testing_methods.deleted_at')
                            ->select(
                                'trf_result_test.id',
                                'trf_result_test.set_req_id',
                                'trf_result_test.trf_doc_id',
                                'trf_result_test.supplier_result',
                                'trf_result_test.result',
                                'trf_result_test.before_test',
                                'trf_result_test.special_id',
                                'trf_result_test.number_test',
                                'trf_result_test.image_ext',
                                'trf_requirement_set.requirement_id',
                                'master_method.method_code',
                                'master_method.method_name',
                                'master_method.category'
                            )->get();
        return $getRest;
    }

    private function operatorVal($req_id){
        $masreq = $this->labe->table('master_requirements')
                            ->join('master_operators','master_requirements.operator','=','master_operators.id')
                            ->where('master_requirements.id',$req_id)
                            ->whereNull('master_requirements.deleted_at')
                            ->select(
                                'master_requirements.id',
                                'master_requirements.operator',
                                'master_requirements.value1',
                                'master_requirements.value2',
                                'master_requirements.value3',
                                'master_requirements.value4',
                                'master_requirements.value5',
                                'master_requirements.value6',
                                'master_requirements.value7',
                                'master_requirements.false',
                                'master_operators.operator_name',
                                'master_operators.symbol'
                            )->first();

        switch ($masreq->operator) {
            case '1':
                return $masreq->value1;
                break;

            case '2':
                return $masreq->symbol." ".$masreq->value1;
                break;
            case '3':
                return $masreq->symbol." ".$masreq->value1;
                break;

            case '4':

                $arr = array($masreq->value1,$masreq->value2,$masreq->value3,$masreq->value4,$masreq->value5,$masreq->value6,$masreq->value7);
                return implode(",",array_filter($arr));
                break;

            case '5':
                return $masreq->value1." ".$masreq->symbol." ".$masreq->value2;
                break;
            
        }

        // return implode(",",array_filter($arr));
    }

    private function ReportResult($req_id,$rest_val){
        $mreq = $this->lab->table('master_requirements')->where('id',$req_id)->first();

        if ($rest_val!='null' || $rest_val!=null || $rest_val!='') {
            switch ($mreq->operator) {
                case '1':
                        if ($rest_val== strval($mreq->value1)) {
                            $return ="PASS";
                        }else{
                            $return ="FAILED";
                        }
                    break;

                case '2':
                        if (floatval($rest_val)<= floatval($mreq->value1)) {
                          $return ="PASS";
                        }else{
                            $return ="FAILED";
                        }
                    break;

                case '3':
                        if (floatval($rest_val)>= floatval($mreq->value1)) {
                           $return ="PASS";
                        }else{
                            $return ="FAILED";
                        }
                    break;


                case '4':
                        $requ = array($mreq->value1,$mreq->value2,$mreq->value2,$mreq->value3,$mreq->value4,$mreq->value5,$mreq->value6,$mreq->value7);
            
                        $tes = in_array($rest_val,$requ);
                
                        if ($tes==true) {
                          $return ="PASS";
                        }else{
                             $return ="FAILED";
                        }
                    break;



                case '5':
                        if ( ( floatval($rest_val)>= floatval($mreq->value1)) && ( floatval($rest_val)<= floatval($mreq->value2))) {
                            $return ="PASS";
                        }else{
                            $return ="FAILED";
                        }
                    break;

                default : 
                    return 'ERROR';
                break;
                
                
            }
        }else{
            $return = "";
        }

        return $return;
    }




   

}
