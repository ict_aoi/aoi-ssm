<?php

namespace App\Http\Controllers\Packinglist;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use DB;
use Carbon\Carbon;
use DataTables;
use Excel;
// use Illuminate\Support\Facades\Storage;

use App\Models\Invoices;
use App\Models\Package;
use App\Models\PackDetail;

class PackingController extends Controller
{
    public function index(){
        return view('packinglist.index');
    } 

    public function getDatInv(Request $request){
        $par = $request->all();

        if ($request->request!=null) {
            $this->validate($request,[
                'invoice'=>'required|min:4',
            ]);



             $data = Invoices::where('invoice','LIKE','%'.$par['invoice'])
                                    ->whereNull('deleted_at');

        }else{
            $data = array();
        }

        return DataTables::of($data)
                            ->addColumn('qty',function($data){
                                $count = Package::where('invoice_id',$data->id)->whereNull('deleted_at')->groupBy('invoice_id')->count('carton_id');

                                if ($count==0) {
                                   return '<span class="badge badge-default">0</span>';
                                }else{
                                    return '<span class="badge badge-success">'.$count.'</span>';
                                }
                            })
                            ->addColumn('action',function($data){
                                $ctn = Package::where('invoice_id',$data->id)->whereNull('deleted_at')->groupBy('invoice_id')->count('carton_id');
                                if ($ctn==0) {
                                    return view('_action',[
                                                'model'=>$data,
                                                'uppl'=>['id'=>$data->id,'inv'=>$data->invoice]
                                            ]);
                                }else{
                                    return view('_action',[
                                                'model'=>$data,
                                                // 'uppl'=>['id'=>$data->id,'inv'=>$data->invoice]
                                                'detpl'=>route('packlist.detailPackinglist',
                                                                [
                                                                    'invoice_id'=>$data->id,
                                                                    'invoice'=>$data->invoice
                                                                ])
                                            ]);
                                }
                                
                            })
                            ->rawColumns(['qty','action'])
                            ->make(true);
    }

    public function uploadPL(Request $request){

        $invoice = $request->invoice;

        if ($request->hasFile('file')) {
            $extension = \File::extension($request->file->getClientOriginalName());

            if ($extension=="xlsx" || $extension=="xls") {
                $path = $request->file->getRealPath();
                $data = \Excel::selectSheetsByIndex(0)->load($path)->get();

                
            }
        }

     
        try {
            DB::begintransaction();
            $ctn=0;
            $barcode = "";
                foreach ($data as $dt) {
                    $ctn_no = (int) trim($dt->ctn_no);
                    $po_number = trim($dt->po_number);
                    $season = trim($dt->season);
                    $style = trim($dt->style);
                    $article = trim($dt->article);
                    $size = trim($dt->size);
                    $qty = trim($dt->qty);
                    $uom = trim($dt->units);
                   

                    
                        if ($ctn_no!="" && $po_number!=""  && $invoice!=""  && $style!=""  && $article!=""  && $size!=""  && $qty!=""    && $uom!="" ) {
                        
                            if ($ctn_no!=$ctn) {
                                $cekpk = Package::where('invoice_id',$invoice)->where('carton_no',$ctn_no)->whereNull('deleted_at')->exists();

                                if ($cekpk) {
                                    throw new \Exception('Carton Number is already');
                                }

                                $barcode = $this->_generateBarcode();
                                $ctn = $ctn_no;

                                $package = array(
                                    'carton_id'=>$barcode,
                                    'invoice_id'=>$invoice,
                                    'carton_no'=>$ctn,
                                    'location'=>'preparation',
                                    'status'=>'onprogress',
                                    'created_at'=>Carbon::now()
                                );

                                Package::firstorcreate($package);

                                $packdet = array(
                                    'carton_id'=>$barcode,
                                    'po_number'=>$po_number,
                                    'style'=>$style,
                                    'article'=>$article,
                                    'size'=>$size,
                                    'qty'=>$qty,
                                    'season'=>$season,
                                    'uom'=>$uom,
                                    'created_at'=>Carbon::now()
                                );

                                PackDetail::firstorcreate($packdet);

                            }else{
                                $packdet = array(
                                    'carton_id'=>$barcode,
                                    'po_number'=>$po_number,
                                    'style'=>$style,
                                    'article'=>$article,
                                    'size'=>$size,
                                    'qty'=>$qty,
                                    'season'=>$season,
                                    'uom'=>$uom,
                                    'created_at'=>Carbon::now()
                                );

                                PackDetail::firstorcreate($packdet);
                            }
                        }
                    
                }

            DB::commit();
            $data_response = [
                                'status'=>200,
                                'output'=>"Upload Packinglist Success ! ! !"
                                ];
        } catch (Exception $ex) {
            DB::rollback();
            $message = $ex->getMessage();
            ErrorHandler::db($message);

            $data_response = [
                                'status'=>422,
                                'output'=>"Upload Field ".$message
                                ];
        }

        return response()->json(['data'=>$data_response]);
    }

    private function _generateBarcode(){
        $max = Package::whereDate('created_at',Carbon::now()->format('Y-m-d'))->whereNull('deleted_at')->max('carton_id');

        if ($max ==null) {
            $last = 0;
        }else{
            $last = (int) substr($max, 9,5);
        }

        $date = Carbon::now()->format('ymd');

        $carton_id = "CTN".$date.sprintf("%05s", $last+1);

        return $carton_id;
    } 

    public function detailPackinglist(Request $request){
        $id = $request->invoice_id;
        $invoice = $request->invoice;

        return view('packinglist.detail')->with('id',$id)->with('invoice',$invoice);
    }

    public function ajaxGetDetailPl(Request $request){
        $id = $request->id;
  
        $data = DB::table('invoices')
                            ->join('package','invoices.id','=','package.invoice_id')
                            ->where('invoices.id',$id)
                            ->whereNull('invoices.deleted_at')
                            ->whereNull('package.deleted_at')
                            ->select('invoices.invoice','invoices.id','invoices.country_to','invoices.customer_number','invoices.shipmode','package.carton_id','package.carton_no','package.location','package.status');
        return DataTables::of($data)
                            ->addColumn('checkbox',function($data){
                                return '<input type="checkbox" class="ctn_id" name="selector[]" id="Inputselector" value="'.$data->carton_id.'" data-id="'.$data->carton_id.'">';
                            }) 
                            ->editColumn('customer_number',function($data){
                                return $data->customer_number;
                            })
                            ->addColumn('inner_pack',function($data){
                                $inner = PackDetail::where('carton_id',$data->carton_id)->whereNull('deleted_at')->groupBy('carton_id')->sum('qty');
                                return $inner;
                            })
                            ->rawColumns(['checkbox','customer_number','inner_pack'])
                            ->make(true);
    }

    public function printShipmark(Request $request){
        $data = explode(";",trim($request->data));
        $invid = trim($request->invid);

        // $ctncount = Package::whereNull('deleted_at')->where('invoice_id',$invid)->groupBy('invoice_id')->count('carton_id');
        $ctn = Package::whereNull('deleted_at')->whereIn('carton_id',$data)->select('carton_id','carton_no')->get();

        $invoices = DB::table('invoices')
                            ->join('package','invoices.id','=','package.invoice_id')
                            ->whereNull('package.deleted_at')
                            ->whereNull('invoices.deleted_at')
                            ->where('invoices.id',$invid)
                            ->groupBy('invoices.invoice','invoices.country_to','invoices.customer_number','invoices.id')
                            ->select('invoices.invoice','invoices.country_to','invoices.customer_number','invoices.id',DB::raw('count(package.carton_id) as totctn'))
                            ->first();
        $packdet = PackDetail::whereNull('deleted_at')->whereIn('carton_id',$data)->select('carton_id','po_number','style','article','qty')->get();
        $filename = "Shippingmark_".$invoices->invoice;
        $pdf = \PDF::loadView('packinglist.smark',
                                [
                                    'ctn' => $ctn,
                                    'total_ctn' => $invoices->totctn,
                                    'invoice' => $invoices->invoice,
                                    'country' => $invoices->country_to,
                                    'cust_no' => $invoices->customer_number,
                                    'packdet'=>$packdet
                                ])
                       ->setPaper('A4', 'potrait');

        return $pdf->stream($filename);
    }

    public function templatePl(){
        try {
            $path = storage_path('template/template_pl.xlsx');
            return response()->download($path);
        } catch (Exception $e) {
            $e->getMessage();
        }
        
    }
}
