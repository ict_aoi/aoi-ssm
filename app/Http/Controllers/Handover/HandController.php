<?php

namespace App\Http\Controllers\Handover;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use DB;
use Carbon\Carbon;
use DataTables;

use App\Models\GarmentId;
use App\Models\GarmentMove;

class HandController extends Controller
{
    private function cek_barcode($barcode){
        $dt_bar = DB::table('barcode_garment')
                    ->join('master_mo','barcode_garment.mo_id','=','master_mo.id')
                    ->where('barcode_garment.barcode_id',$barcode)
                    ->where('barcode_garment.factory_id',Auth::user()->factory_id)
                    ->whereNull('barcode_garment.deleted_at')
                    ->whereNull('master_mo.deleted_at')
                    ->select('barcode_garment.barcode_id',
                                'barcode_garment.mo_id',
                                'barcode_garment.current_location',
                                'master_mo.documentno',
                                'master_mo.season',
                                'master_mo.style','master_mo.article',
                                'master_mo.size',
                                'master_mo.status',
                                'barcode_garment.checkin_md',
                                'barcode_garment.storage',
                                'barcode_garment.borrow',
                                'barcode_garment.factory_id')->first();

        return $dt_bar;
    }

    private function barcodeGarment($barcode_id,$type,$ip,$fctfrom = null, $fctto = null){

        $last = GarmentMove::where('barcode_id',$barcode_id)->whereNull('deleted_at')->where('is_canceled',false)->orderBy('created_at','asc')->first();

        switch ($type) {
            case 'out':
                $up = array(
                    'factory_id'=>$fctto,
                    'storage'=>false,
                    'borrow'=>false,
                    'current_location'=>'handover'
                );

                $move = array(
                    'barcode_id'=>$barcode_id,
                    'locator_from'=>$last->locator_to,
                    'locator_to'=>"handover",
                    'description'=>"Handover out ",
                    'created_at'=>carbon::now(),
                    'user_id'=>Auth::user()->id,
                    'ip_address'=>$ip,
                    'factory_to'=>$fctto,
                    'factory_from'=>$fctfrom
                );    
            break;

            case 'in':
                // $cekfact = DB::table('garment_movements')
                //                 ->join('users','garment_movements.user_id','=','users.id')
                //                 ->join('factory','users.factory_id','=','factory.id')
                //                 ->where('garment_movements.barcode_id',$barcode_id)
                //                 ->where('garment_movements.locator_to','handover')
                //                 ->where('garment_movements.is_canceled',false)
                //                 ->orderBy('garment_movements.created_at','desc')
                //                 ->select('factory.factory_name')
                //                 ->first();

                $up = array(
                    'current_location'=>'checkin_md'
                );

                $move = array(
                    'barcode_id'=>$barcode_id,
                    'locator_from'=>$last->locator_to,
                    'locator_to'=>"checkin_md",
                    'description'=>"Handover in ",
                    'created_at'=>carbon::now(),
                    'user_id'=>Auth::user()->id,
                    'ip_address'=>$ip,
                    'factory_to'=>$fctto,
                    'factory_from'=>$last->factory_from
                );    
            break;
        }

        try {
            DB::begintransaction();
                GarmentId::where('barcode_id',$barcode_id)->update($up);
                GarmentMove::where('id',$last->id)->update(['deleted_at'=>Carbon::now()]);
                GarmentMove::firstorcreate($move);
            DB::commit();
        } catch (Exception $ex) {
             DB::rollback();
            $message = $ex->getMessage();
            ErrorHandler::db($message);
        }
        return true;
    }

    public function checkout(){
        $fac = DB::table('factory')->whereNull('deleted_at')->whereNotIn('id',[Auth::user()->factory_id])->orderBy('id','asc')->get();
        return view('handover.out.index')->with('factory',$fac);
    }

    public function ajaxCheckOut(Request $request){
        $barcode = trim($request->bargar);
        $factory = $request->factory;
        $ip = $request->ip();
        $cek = $this->cek_barcode($barcode);

        
        if ($cek==null || $cek->factory_id!=Auth::user()->factory_id) {
             return response()->json("Garment not found ! ! !",422);
        }else if (($cek->current_location=="storage_md" || $cek->current_location=="qc_storage") && $cek->storage==true && $cek->borrow==false ) {
            $getFac = DB::table('factory')->where('id',$factory)->first();
            
            $data = array(
                'barcode_id'=>$cek->barcode_id,
                'docno'=>$cek->documentno,
                'season'=>$cek->season,
                'style'=>$cek->style,
                'article'=>$cek->article,
                'size'=>$cek->size,
                'status'=>$cek->status,
                'loc'=>"Hand Over to ".$getFac->factory_name
            );

            try {
                DB::begintransaction();
                    $update = $this->barcodeGarment($cek->barcode_id,"out",$ip,auth::user()->factory_id, $factory);





                    if (!$update) {
                          throw new \Exception('Update movement garment error ! ! !');
                    }
                DB::commit();

            } catch (Exception $ex) {
                DB::rollback();
                $message = $ex->getMessage();
                ErrorHandler::db($message);
            }

            return view('handover.out.ajax_list')->with('data',$data);
        }else{
             return response()->json("Garment location in ".strtoupper($cek->current_location),422);
        }
        
    }


    public function checkin(){
        return view('handover.in.index');
    }

    public function ajaxCheckIn(Request $request){
        $barcode = trim($request->bargar);
        $ip = $request->ip();
        $cek = $this->cek_barcode($barcode);

        
        if ($cek==null || $cek->factory_id!=Auth::user()->factory_id) {
             return response()->json("Garment not found ! ! !",422);
        }else if ($cek->current_location=="handover" && $cek->storage==false && $cek->borrow==false ) {
            
            $data = array(
                'barcode_id'=>$cek->barcode_id,
                'docno'=>$cek->documentno,
                'season'=>$cek->season,
                'style'=>$cek->style,
                'article'=>$cek->article,
                'size'=>$cek->size,
                'status'=>$cek->status,
                'loc'=>"Checkin Md From Handover"
            );

            try {
                DB::begintransaction();
                    // $update = $this->barcodeGarment($cek->barcode_id,"in",$ip);

                     $update = $this->barcodeGarment($cek->barcode_id,"in",$ip,null,auth::user()->factory_id);

                    if (!$update) {
                          throw new \Exception('Update movement garment error ! ! !');
                    }
                DB::commit();
            } catch (Exception $ex) {
                DB::rollback();
                $message = $ex->getMessage();
                ErrorHandler::db($message);
            }

            return view('handover.in.ajax_list')->with('data',$data);
        }else{
             return response()->json("Garment location in ".strtoupper($cek->current_location),422);
        }
        
    }
}
