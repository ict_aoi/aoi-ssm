<?php

namespace App\Http\Controllers\Borrowing;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use DB;
use Carbon\Carbon;
use DataTables;

use App\Models\GarmentId;
use App\Models\GarmentMove;

class BorrowingController extends Controller
{
    public function borrow(){
        $dept = DB::table('department')->whereNull('deleted_at')->get();
        return view('borrowing.checkin.index')->with('dept',$dept);
    }

    public function ajaxBorrow(Request $request){
    	$bargar = $request->bargar;
    	$loct = $request->loct;
    	$ip = $request->ip();
        $nik = $request->nik;

    	$check = $this->cek_barcode($bargar);

    	if (!$check) {
    		return response()->json("Barcode garment not found",422);
    	}

    	if ($check->current_location!="storage_md") {
    		return response()->json("Barcode garment current location on ".$check->current_location." ! ! !",422);
    	}else{
            $dept = DB::table('department')->where('value_name',$loct)->whereNull('deleted_at')->first();
            
    		$data = array(
                    'barcode_id'=>$check->barcode_id,
                    'docno'=>$check->documentno,
                    'season'=>$check->season,
                    'style'=>$check->style,
                    'article'=>$check->article,
                    'size'=>$check->size,
                    'status'=>$check->status,
                    'loct'=>strtoupper($loct),
                    'type'=>"checkout",
                    'expd'=>Carbon::now()->addDays($dept->expired)->format('Y-m-d')
                );

    		$query = $this->barcodeGarment($check->barcode_id,"checkout",$loct,$ip,$nik);

    		if (!$query) {
    			return response()->json("Update movement garment error ! ! !",422);
    		}
    	}



    	
    	return view('borrowing.checkin.ajax_list')->with('data',$data);

    }

    private function cek_barcode($barcode){
        $dt_bar = DB::table('barcode_garment')
                    ->join('master_mo','barcode_garment.mo_id','=','master_mo.id')
                    ->where('barcode_garment.barcode_id',$barcode)
                    ->where('barcode_garment.factory_id',Auth::user()->factory_id)
                    ->whereNull('barcode_garment.deleted_at')
                    ->whereNull('master_mo.deleted_at')
                    ->select('barcode_garment.barcode_id',
                                'barcode_garment.mo_id',
                                'barcode_garment.current_location',
                                'master_mo.documentno',
                                'master_mo.season',
                                'master_mo.style','master_mo.article',
                                'master_mo.size',
                                'master_mo.status',
                                'barcode_garment.checkin_md',
                                'barcode_garment.storage',
                                'barcode_garment.borrow')->first();

        return $dt_bar;
    }


    private function barcodeGarment($barcode_id,$type,$loct,$ipaddress,$nik){
        #query update & movement
        $last = GarmentMove::where('barcode_id',$barcode_id)->whereNull('deleted_at')->where('is_canceled',false)->orderBy('created_at','asc')->first();


        switch ($type) {
            case 'checkout':
                $desc = "Checkout to ".$loct;
                break;           
 
        }
        

        try {
            DB::begintransaction();
                $update = array(
                    'current_location'=>$loct,
                    'updated_at'=>Carbon::now(),
                    'borrow'=>true,
                    'storage'=>false
                );

                $move = array(
                    'barcode_id'=>$barcode_id,
                    'locator_from'=>$last->locator_to,
                    'locator_to'=>$loct,
                    'description'=>$desc,
                    'created_at'=>Carbon::now(),
                    'user_id'=>Auth::user()->id,
                    'ip_address'=>$ipaddress,
                    'borrower'=>$nik
                );

                GarmentId::where('barcode_id',$barcode_id)->update($update);
                GarmentMove::where('id',$last->id)->update(['deleted_at'=>Carbon::now()]);
                GarmentMove::firstorcreate($move);
            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            $message = $ex->getMessage();
            ErrorHandler::db($message);
        }

        return true;
    }

    public function getNik(Request $request){
        $nik = $request->nik;

        $data = DB::table('employee')->where('nik',$nik)->whereNull('deleted_at')->first();
        if ($data!=null) {
            return response()->json($data,200);
        }else{
            return response()->json("Borrower not found ! ! !",422);
        }
    }


}
