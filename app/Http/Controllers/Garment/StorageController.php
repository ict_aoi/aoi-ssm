<?php

namespace App\Http\Controllers\Garment;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use DB;
use Carbon\Carbon;
use DataTables;

use App\Models\GarmentId;
use App\Models\GarmentMove;
use App\Models\Locator; 

class StorageController extends Controller
{
    public function index(){
    	return view('garment_storage.checkin.index');
    }

    public function ajaxCheckinStorage(Request $request){
    	$bargar = trim($request->bargar);
        $ip = $request->ip();

    	$chek = $this->cek_barcode($bargar);
    	
    	if (!$chek) {
    		return response()->json("Barcode garment not found",422);
    	}

        if ($chek->current_location=='reservation' && $chek->checkin_md==false) {

            $sugest = $this->_suggestion($chek->barcode_id,$chek->id,'checkin');

           
            $data = array(
                'barcode_id'=>$chek->barcode_id,
                'docno'=>$chek->documentno,
                'season'=>$chek->season,
                'style'=>$chek->style,
                'article'=>$chek->article,
                'size'=>$chek->size,
                'status'=>$chek->status,
                'loc'=>"checkin",
                'sugest'=>isset($sugest->name) ? $sugest->name : ""
            );


            try {
                DB::begintransaction();
                    $update = $this->barcodeGarment($bargar,"checkin_md",$ip,null);

                    if (!$update) {
                          throw new \Exception('Update movement garment error ! ! !');
                    }
                DB::commit();
            } catch (Exception $ex) {
                DB::rollback();
                $message = $ex->getMessage();
                ErrorHandler::db($message);
            }

            return view('garment_storage.checkin.ajax_list')->with('data',$data);

        }else if (($chek->borrow==true && $chek->storage==false) || ($chek->borrow==false && $chek->storage==true && $chek->current_location!=='storage_md')) {
            
                $sugest = $this->_suggestion($chek->barcode_id,$chek->id,'reverse');
                $data = array(
                    'barcode_id'=>$chek->barcode_id,
                    'docno'=>$chek->documentno,
                    'season'=>$chek->season,
                    'style'=>$chek->style,
                    'article'=>$chek->article,
                    'size'=>$chek->size,
                    'status'=>$chek->status,
                    'loc'=>"checkin",
                    'sugest'=>isset($sugest->name) ? $sugest->name : ""
                );

                try {
                DB::begintransaction();
                        $update = $this->barcodeGarment($bargar,"reverse",$ip,null);

                        if (!$update) {
                              throw new \Exception('Update movement garment error ! ! !');
                        }
                    DB::commit();
                } catch (Exception $ex) {
                    DB::rollback();
                    $message = $ex->getMessage();
                    ErrorHandler::db($message);
                }

                return view('garment_storage.checkin.ajax_list')->with('data',$data);
        }else{
            return response()->json(" Barcode last location on ".$chek->current_location."  ! ! ! ",422);
        }

    }

    public function storageGarment(){
        return view('garment_storage.storage.index');
    }

    public function ajaxPlaceStorage(Request $request){
        $bargar = trim($request->barcode_id);
        $idloc = $request->idloc;
        $ip = $request->ip();
        $status = $request->status;
        

        
        $chek = $this->cek_barcode($bargar);

        if (!$chek) {
            return response()->json("Barcode garment not found",422);
        }
        
        
        switch ($status) {
            //storage
            case 'storage':
               if ($chek->current_location=='checkin_md' && $chek->checkin_md==true && $chek->storage==false) {
                    $loc = Locator::where('id',$idloc)->whereNull('deleted_at')->first();

                    $data = array(
                        'barcode_id'=>$chek->barcode_id,
                        'docno'=>$chek->documentno,
                        'season'=>$chek->season,
                        'style'=>$chek->style,
                        'article'=>$chek->article,
                        'size'=>$chek->size,
                        'status'=>$chek->status,
                        'loc'=>$loc->name,
                        'act_status'=>"storage"
                    );

                  ;
                    try {
                        DB::begintransaction();
                            $update = $this->barcodeGarment($bargar,"storage_md",$ip,$idloc);

                            if (!$update) {
                                  throw new \Exception('Update movement garment error ! ! !');
                            }
                        DB::commit();
                    } catch (Exception $ex) {
                        DB::rollback();
                        $message = $ex->getMessage();
                        ErrorHandler::db($message);
                    }

                    return view('garment_storage.storage.ajax_list')->with('data',$data);

                }else if ($chek->current_location=='storage_md'  && $chek->storage==true && ($chek->locator_id!=$idloc)) {
                    $loc = Locator::where('id',$idloc)->whereNull('deleted_at')->first();

                    $data = array(
                        'barcode_id'=>$chek->barcode_id,
                        'docno'=>$chek->documentno,
                        'season'=>$chek->season,
                        'style'=>$chek->style,
                        'article'=>$chek->article,
                        'size'=>$chek->size,
                        'status'=>$chek->status,
                        'loc'=>$loc->name,
                        'act_status'=>"storage"
                    );

                  
                    try {
                        DB::begintransaction();
                            $update = $this->barcodeGarment($bargar,"move_storage",$ip,$idloc);

                            if (!$update) {
                                  throw new \Exception('Update movement garment error ! ! !');
                            }
                        DB::commit();
                    } catch (Exception $ex) {
                        DB::rollback();
                        $message = $ex->getMessage();
                        ErrorHandler::db($message);
                    }

                    return view('garment_storage.storage.ajax_list')->with('data',$data);
                }else{
                     return response()->json("Barcode last location in ".$chek->current_location." ! ! !",422);
                } 
            break;
            //storage

            //cancel
            case 'cancel':
                if ($chek->current_location=='storage_md' && $chek->storage==true) {
             

                    $data = array(
                        'barcode_id'=>$chek->barcode_id,
                        'docno'=>$chek->documentno,
                        'season'=>$chek->season,
                        'style'=>$chek->style,
                        'article'=>$chek->article,
                        'size'=>$chek->size,
                        'status'=>$chek->status,
                        'loc'=>null,
                        'act_status'=>"cancel"
                    );

                  ;
                    try {
                        DB::begintransaction();
                            $update = $this->cancelMovements($bargar,$ip);

                            if (!$update) {
                                  throw new \Exception('Update movement garment error ! ! !');
                            }
                        DB::commit();
                    } catch (Exception $ex) {
                        DB::rollback();
                        $message = $ex->getMessage();
                        ErrorHandler::db($message);
                    }

                    return view('garment_storage.storage.ajax_list')->with('data',$data);

                }else{
                     return response()->json("Barcode last location in ".$chek->current_location." ! ! !",422);
                }
            break;
            //cancel
        }
        
        

    }


    public function setLocator(Request $request){
        $barcode_id =trim($request->barcode_id);
        
        $data = Locator::where('id',$barcode_id)->whereNull('deleted_at')->first();

        if ($data!=null) {
            return response()->json($data,200);
        }else{
            return response()->json("Locator not found ! ! !",422);
        }

        
    }


    private function cek_barcode($barcode){
    	$dt_bar = DB::table('barcode_garment')
                    ->join('master_mo','barcode_garment.mo_id','=','master_mo.id')
                    ->leftjoin('garment_movements','barcode_garment.barcode_id','=','garment_movements.barcode_id')
                    ->where('barcode_garment.barcode_id',$barcode)
                    ->where('barcode_garment.factory_id',Auth::user()->factory_id)
                    ->whereNull('barcode_garment.deleted_at')
                    ->whereNull('master_mo.deleted_at')
                    ->whereNull('garment_movements.deleted_at')
                    ->where('garment_movements.is_canceled',false)
                    ->select('barcode_garment.barcode_id',
                                'barcode_garment.mo_id',
                                'barcode_garment.current_location',
                                'master_mo.documentno',
                                'master_mo.season',
                                'master_mo.style','master_mo.article',
                                'master_mo.size',
                                'master_mo.status',
                                'master_mo.id',
                                'barcode_garment.checkin_md',
                                'barcode_garment.storage',
                                'barcode_garment.borrow',
                                'garment_movements.locator_id')->first();

    	return $dt_bar;
    }

    private function barcodeGarment($barcode_id,$location,$ipaddress,$idloc= null){
        $last = GarmentMove::where('barcode_id',$barcode_id)->whereNull('deleted_at')->where('is_canceled',false)->orderBy('created_at','asc')->first();
        if ($location=='checkin_md') {
            $upgar = array(
                'current_location'=>$location,
                'checkin_md'=>true,
                'updated_at'=>Carbon::now()
            );

            $move = array(
                'barcode_id'=>$barcode_id,
                'locator_from'=>$last->locator_to,
                'locator_to'=>$location,
                'description'=>"checkin on md sample",
                'created_at'=>Carbon::now(),
                'user_id'=>Auth::user()->id,
                'ip_address'=>$ipaddress
            );

            $lasup = array('deleted_at'=>Carbon::now());
        }
        else if ($location=='storage_md') {
            $upgar = array(
                'current_location'=>$location,
                'storage'=>true,
                'updated_at'=>Carbon::now()
            );

            $move = array(
                'barcode_id'=>$barcode_id,
                'locator_from'=>$last->locator_to,
                'locator_to'=>$location,
                'locator_id'=>$idloc,
                'description'=>"Placement on storage MD",
                'created_at'=>Carbon::now(),
                'user_id'=>Auth::user()->id,
                'ip_address'=>$ipaddress
            );

            $lasup = array('deleted_at'=>Carbon::now());
        }
        else if ($location=="reverse") {
            $picborw = GarmentMove::where('barcode_id',$barcode_id)->where('is_canceled',false)->whereNotNull('borrower')->orderBy('created_at')->first();

            $upgar = array(
                'current_location'=>"checkin_md",
                'storage'=>false,
                'borrow'=>false,
                'updated_at'=>Carbon::now()
            );

            $move = array(
                'barcode_id'=>$barcode_id,
                'locator_from'=>$last->locator_to,
                'locator_to'=>"checkin_md",
                'locator_id'=>$idloc,
                'description'=>"Return from ".$last->locator_to,
                'created_at'=>Carbon::now(),
                'user_id'=>Auth::user()->id,
                'ip_address'=>$ipaddress,
                'borrower'=>isset($picborw->borrower) ? $picborw->borrower : null
            );

            $lasup = array('deleted_at'=>Carbon::now());
        }else if ($location=="move_storage") {
            $upgar = array(
                'current_location'=>"storage_md",
                'storage'=>true,
                'updated_at'=>Carbon::now()
            );

            $move = array(
                'barcode_id'=>$barcode_id,
                'locator_from'=>"checkin_md",
                'locator_to'=>"storage_md",
                'locator_id'=>$idloc,
                'description'=>"move rack on storage MD",
                'created_at'=>Carbon::now(),
                'user_id'=>Auth::user()->id,
                'ip_address'=>$ipaddress
            );

            $lasup = array('deleted_at'=>Carbon::now(),'is_canceled'=>true);
        }

        try {
            DB::begintransaction();
                GarmentId::where('barcode_id',$barcode_id)->update($upgar);
                GarmentMove::where('id',$last->id)->update($lasup);
                GarmentMove::firstorcreate($move);
            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            $message = $ex->getMessage();
            ErrorHandler::db($message);
        }

        return true;
    }

    private function cancelMovements($barcode_id,$ip){
        $move = GarmentMove::where('barcode_id',$barcode_id)->where('is_canceled',false)->orderBy('created_at','desc')->skip(0)->take(2)->get();

        try {
            DB::begintransaction();
                foreach ($move as $mv) {
                    if ($mv->locator_to=="storage_md") {
                       GarmentMove::where('id',$mv->id)->update(['deleted_at'=>Carbon::now(),'is_canceled'=>true]);
                    }else if ($mv->locator_to=="checkin_md") {
                                               
                        GarmentMove::where('id',$mv->id)->update(['deleted_at'=>null]);
                    }
                }
               
                GarmentId::where('barcode_id',$mv->barcode_id)->Update(['current_location'=>"checkin_md",'storage'=>false]);
            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            $message = $ex->getMessage();
            ErrorHandler::db($message);
        }

        return true;
    }

    private function _suggestion($barcode_id,$mo_id,$status){
        
        switch ($status) {
            case 'reverse':
                $sug = DB::table('garment_movements')
                            ->join('locator','locator.id','=','garment_movements.locator_id')
                            ->where('garment_movements.barcode_id',$barcode_id)
                            ->where('garment_movements.is_canceled',false)
                            ->where('garment_movements.locator_to','storage_md')
                            ->orderBy('garment_movements.created_at','desc')
                            ->select('locator.name')
                            ->first();
                break;
            
            case 'checkin':
                $sug = DB::table('master_mo')
                            ->join('barcode_garment','barcode_garment.mo_id','=','master_mo.id')
                            ->join('garment_movements','barcode_garment.barcode_id','=','garment_movements.barcode_id')
                            ->leftjoin('locator','locator.id','=','garment_movements.locator_id')
                            ->where('master_mo.id',$mo_id)
                            ->where('garment_movements.is_canceled',false)
                            ->where('garment_movements.locator_to','storage_md')
                            ->orderBy('garment_movements.created_at','desc')
                             ->select('locator.name')
                            ->first();
                break;
         
        }
        

        return $sug;
    }




}
