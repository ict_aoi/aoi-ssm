<?php

namespace App\Http\Controllers\Bapb;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use DB;
use Carbon\Carbon;
use DataTables;

use App\Models\GarmentId;
use App\Models\GarmentMove;

class DestroyController extends Controller
{
    public function destroy(){
        return view('bapb.index');
    }

    public function ajaxScanDestory(Request $request){
        $barcode = $request->bargar;
        $ip = $request->ip();

        $cek = $this->cek_barcode($barcode);

        if ($cek==null || $cek->factory_id!=auth::user()->factory_id) {
            return response()->json("Garment not found ! ! !",422);
        }else if($cek->current_location=="storage_md" || $cek->current_location=="qc_storage" || $cek->current_location=="lab_storage"){

            $data = array(
                'barcode_id'=>$cek->barcode_id,
                'docno'=>$cek->documentno,
                'season'=>$cek->season,
                'style'=>$cek->style,
                'article'=>$cek->article,
                'size'=>$cek->size,
                'status'=>$cek->status,
                'loc'=>"Destory BAPB"
            );

            try {
                DB::begintransaction();
                    $update = $this->barcodeGarment($cek->barcode_id,"destory",$ip);

                    if (!$update) {
                          throw new \Exception('Update movement garment error ! ! !');
                    }
                DB::commit();

            } catch (Exception $ex) {
                DB::rollback();
                $message = $ex->getMessage();
                ErrorHandler::db($message);
            }

            return view('bapb.ajax_list')->with('data',$data);
        }else{
             return response()->json("Garment current location in ".strtoupper($cek->current_location),422);
        }
    }

    private function cek_barcode($barcode){
        $dt_bar = DB::table('barcode_garment')
                    ->join('master_mo','barcode_garment.mo_id','=','master_mo.id')
                    ->where('barcode_garment.barcode_id',$barcode)
                    ->where('barcode_garment.factory_id',Auth::user()->factory_id)
                    ->whereNull('barcode_garment.deleted_at')
                    ->whereNull('master_mo.deleted_at')
                    ->select('barcode_garment.barcode_id',
                                'barcode_garment.mo_id',
                                'barcode_garment.current_location',
                                'master_mo.documentno',
                                'master_mo.season',
                                'master_mo.style','master_mo.article',
                                'master_mo.size',
                                'master_mo.status',
                                'barcode_garment.checkin_md',
                                'barcode_garment.storage',
                                'barcode_garment.borrow',
                                'barcode_garment.factory_id')->first();

        return $dt_bar;
    }

    private function barcodeGarment($barcode_id,$type,$ip){

        $last = GarmentMove::where('barcode_id',$barcode_id)->whereNull('deleted_at')->where('is_canceled',false)->orderBy('created_at','asc')->first();

        switch ($type) {
            case 'destory':
                $up = array(
                    'storage'=>false,
                    'borrow'=>false,
                    'current_location'=>'bapb'
                );

                $move = array(
                    'barcode_id'=>$barcode_id,
                    'locator_from'=>$last->locator_to,
                    'locator_to'=>"bapb",
                    'description'=>"Destory BAPB",
                    'created_at'=>carbon::now(),
                    'user_id'=>Auth::user()->id,
                    'ip_address'=>$ip
                );    
            break;
        }

        try {
            DB::begintransaction();
                GarmentId::where('barcode_id',$barcode_id)->update($up);
                GarmentMove::where('id',$last->id)->update(['deleted_at'=>Carbon::now()]);
                GarmentMove::firstorcreate($move);
            DB::commit();
        } catch (Exception $ex) {
             DB::rollback();
            $message = $ex->getMessage();
            ErrorHandler::db($message);
        }
        return true;
    }
}
