<?php

namespace App\Http\Controllers\Locator;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use DataTables;
use Auth;
use Carbon\Carbon;

use App\Models\Area;
use App\Models\Locator;


class AreaLocatorController extends Controller
{
    public function index(){
    	return view('locator.area');
    }

    public function getDataArea(){
    	$data = Area::whereNull('deleted_at')->where('factory_id',Auth::user()->factory_id);

    	return DataTables::of($data)
    					->addColumn('action',function($data){
    						return view('_action',[
    										'model'=>$data,
    										'editArea'=>route('area.formEditArea',[
    											'id'=>$data->id
    										])
    									]);
    					})
    					->rawColumns(['action'])
    					->make(true);
    }

    public function addArea(Request $request){
    	$name = $request->name;
    	$desc = $request->desc;
    	$factory_id = Auth::user()->factory_id;

    	try {
    		db::begintransaction();
    			$in = array('name'=>$name,'description'=>$desc,'factory_id'=>$factory_id,'created_at'=>carbon::now());

    			Area::firstorcreate($in);

    			$data_response = [
                            'status' => 200,
                            'output' => 'Add Area Success . . .'
                          ];
    		db::commit();
    	} catch (Exception $ex) {
    		DB::rollback();
            $message = $ex->getMessage();
            ErrorHandler::db($message);
            $data_response = [
                            'status' => 422,
                            'output' => 'Add Area filed ! ! !'
                          ];
    	}

    	return response()->json(['data'=>$data_response]);
    }

    public function formEditArea(Request $request){
    	$id = $request->id;
    	
    	$area = Area::where('id',$id)->whereNull('deleted_at')->first();

    	return view('locator.edit_area',['area'=>$area]);
    }

    public function editArea(Request $request){
    	$id = $request->id;
    	$name = $request->name;
    	$desc = $request->desc;

    	try {
    		db::begintransaction();
    			$edit = array('name'=>$name,'description'=>$desc,'updated_at'=>carbon::now());

    			Area::where('id',$id)->update($edit);

    			$data_response = [
                            'status' => 200,
                            'output' => 'Edit Area Success . . .'
                          ];
    		db::commit();
    	} catch (Exception $ex) {
    		DB::rollback();
            $message = $ex->getMessage();
            ErrorHandler::db($message);
            $data_response = [
                            'status' => 422,
                            'output' => 'Edit Area filed ! ! !'
                          ];
    	}

    	return response()->json(['data'=>$data_response]);
    }




    public function locator(){
    	return view('locator.locator');
    }

    public function getDataLocator(Request $request){
    	$data = Locator::where('area_id',$request->area)->whereNull('deleted_at');

    	return DataTables::of($data)
    					->addColumn('checkbox',function($data){
    						return '<input type="checkbox" class="barcode" name="selector[]" id="Inputselector" value="'.$data->id.'" data-id="'.$data->id.'">';
    					})
    					->editColumn('area_id',function($data){
    						$area = Area::where('id',$data->area_id)->first();

    						return $area->name;
    					})
    					->addColumn('action',function($data){
    						return view('_action',[
    										'model'=>$data,
    										'editLocator'=>route('area.formEditLocator',[
    											'id'=>$data->id
    										])
    									]);

    						
    					})
    					->rawColumns(['action','area_id','checkbox'])
    					->make(true);
    }

    public function ajaxGetArea(){
    	$dtarea = Area::where('factory_id',Auth::user()->factory_id)->whereNull('deleted_at')->get();

    	return response()->json(['area'=>$dtarea],200);
    }

    public function addLocator(Request $request){
    	$name = $request->name;
    	$desc = $request->desc;
    	$area_id = $request->area;
    	$factory_id = Auth::user()->factory_id;

    	try {
    		db::begintransaction();
    			
    			$barcode_id = $this->generateLocator($factory_id);

    			$in = array('id'=>$barcode_id,'name'=>$name,'description'=>$desc,'factory_id'=>$factory_id,'area_id'=>$area_id,'created_at'=>carbon::now());

    			Locator::firstorcreate($in);

    			$data_response = [
                            'status' => 200,
                            'output' => 'Add Locator Success . . .'
                          ];
    		db::commit();
    	} catch (Exception $ex) {
    		DB::rollback();
            $message = $ex->getMessage();
            ErrorHandler::db($message);
            $data_response = [
                            'status' => 422,
                            'output' => 'Add Locator filed ! ! !'
                          ];
    	}

    	return response()->json(['data'=>$data_response]);
    }

    public function generateLocator($factory_id){
        $maxloc = Locator::where('factory_id',$factory_id)->max('id');

        if ($maxloc==null) {
            $last = 0;
        }else{
            $last = (int) substr($maxloc, 2,5);
        }

        
        $barcode_id = "R".$factory_id.sprintf("%05s", $last+1);

        return $barcode_id;
    }


    public function formEditLocator(Request $request){
    	$id = $request->id;

    	$dlocat = Locator::where('id',$id)->whereNull('deleted_at')->first();

    	return view('locator.edit_locator',['locator'=>$dlocat]);
    }

    public function editLocator(Request $request){
    	$name = $request->name;
    	$desc = $request->desc;
    	$area_id = $request->area;
    	$id = $request->id;

    	try {
    		db::begintransaction();
    			
    	

    			$uploc = array('name'=>$name,'description'=>$desc,'area_id'=>$area_id,'updated_at'=>carbon::now());

    			Locator::where('id',$id)->update($uploc);

    			$data_response = [
                            'status' => 200,
                            'output' => 'Update Locator Success . . .'
                          ];
    		db::commit();
    	} catch (Exception $ex) {
    		DB::rollback();
            $message = $ex->getMessage();
            ErrorHandler::db($message);
            $data_response = [
                            'status' => 422,
                            'output' => 'Update Locator filed ! ! !'
                          ];
    	}

    	return response()->json(['data'=>$data_response]);
    }

    public function printLocator(Request $request){
    	$data = $request->data;
       
        $expl = explode(";",$data);
        foreach ($expl as $ex) {
            if ($ex!="") {
                $arr[]=$ex;
            }
        }

        
        $loc =  DB::table('locator')->whereIn('id',$arr)->whereNull('deleted_at')->get();

        $filename = "Print_locator";

        $pdf = \PDF::loadView('locator.barcode_locator',['data'=>$loc])->setPaper('A4','potrait');
        return $pdf->stream($filename);

        // return view('locator.barcode_locator',['data'=>$loc]);
    }
   
}
