<?php

namespace App\Http\Controllers\Lab;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use DB;
use Carbon\Carbon;
use DataTables;

use App\Models\GarmentId;
use App\Models\GarmentMove;
use App\Models\Locator;

class LabController extends Controller
{
    public function index(){
    	return view('lab.storage.index');
    }

    public function ajaxScan(Request $request){
    	$barcode = $request->bargar;
    	$status = $request->status;
        $rack = $request->rack;
        $ip = $request->ip();

    	$check = $this->cek_barcode($barcode);

    	if (!$check) {
    		return response()->json("Barcode garment not found",422);
    	}

        if ($status=="storage") {
            if ($check->current_location=="lab" && $check->storage==false && $check->borrow==true) {
               $locid = Locator::where('id',$rack)->whereNull('deleted_at')->first();

               $data = array(
                    'barcode_id'=>$check->barcode_id,
                    'docno'=>$check->documentno,
                    'season'=>$check->season,
                    'style'=>$check->style,
                    'article'=>$check->article,
                    'size'=>$check->size,
                    'status'=>$check->status,
                    'type'=>"storage",
                    'rack'=>$locid->name
                );

                $query = $this->barcodeGarment($barcode,"lab_storage",$ip,$rack);

                if (!$query) {
                    return response()->json("Update movement garment error ! ! !",422);
                }
            }else{
                return response()->json("Barcode last location ".$check->current_location."  ! ! !",422);
            }
        }else if ($status=="reverse"){
            if ($check->current_location=="lab_storage" && $check->storage==true && $check->borrow==false) {
                $data = array(
                    'barcode_id'=>$check->barcode_id,
                    'docno'=>$check->documentno,
                    'season'=>$check->season,
                    'style'=>$check->style,
                    'article'=>$check->article,
                    'size'=>$check->size,
                    'status'=>$check->status,
                    'type'=>"reverse"
                );

                $query = $this->barcodeGarment($barcode,"lab",$ip,null);

                if (!$query) {
                    return response()->json("Update movement garment error ! ! !",422);
                }
            }else{
                 return response()->json("Barcode last location ".$check->current_location."  ! ! !",422);
            }
        }

    	
         return view('lab.storage.ajax_list')->with('data',$data);
    }


    private function cek_barcode($barcode){
        $dt_bar = DB::table('barcode_garment')
                    ->join('master_mo','barcode_garment.mo_id','=','master_mo.id')
                    ->where('barcode_garment.barcode_id',$barcode)
                    ->where('barcode_garment.factory_id',Auth::user()->factory_id)
                    ->whereNull('barcode_garment.deleted_at')
                    ->whereNull('master_mo.deleted_at')
                    ->select('barcode_garment.barcode_id',
                                'barcode_garment.mo_id',
                                'barcode_garment.current_location',
                                'master_mo.documentno',
                                'master_mo.season',
                                'master_mo.style','master_mo.article',
                                'master_mo.size',
                                'master_mo.status',
                                'barcode_garment.checkin_md',
                                'barcode_garment.storage',
                                'barcode_garment.borrow')->first();

        return $dt_bar;
    }

    private function barcodeGarment($barcode_id,$location,$ipaddress,$idloc= null){
    	#query update & movement
    	$last = GarmentMove::where('barcode_id',$barcode_id)->whereNull('deleted_at')->where('is_canceled',false)->orderBy('created_at','asc')->first();

    	
    	

    	try {
            DB::begintransaction();
            switch ($location) {
                case 'lab_storage':
                    $desc = "Checkin on lab storage";
                    $borrower = null;
                    $update = array(
                        'current_location'=>$location,
                        'borrow'=>false,
                        'storage'=>true,
                        'updated_at'=>Carbon::now()
                    );
                    break;

                case 'lab':
                    $desc = "Reverse to lab";
                    $bor= GarmentMove::where('barcode_id',$barcode_id)->where('locator_to','lab')->orderBy('created_at','desc')->fist();
                    $borrower = $bor->borrower;

                    $update = array(
                        'current_location'=>$location,
                        'borrow'=>true,
                        'storage'=>false,
                        'updated_at'=>Carbon::now()
                    );
                    break;
            }
            	

	            $move = array(
	            	'barcode_id'=>$barcode_id,
	                'locator_from'=>$last->locator_to,
	                'locator_to'=>$location,
                    'locator_id'=>$idloc,
	                'description'=>$desc,
	                'created_at'=>Carbon::now(),
	                'user_id'=>Auth::user()->id,
                    'ip_address'=>$ipaddress,
                    'borrower'=>$borrower
	            );

                GarmentId::where('barcode_id',$barcode_id)->update($update);
                GarmentMove::where('id',$last->id)->update(['deleted_at'=>Carbon::now()]);
                GarmentMove::firstorcreate($move);
            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            $message = $ex->getMessage();
            ErrorHandler::db($message);
        }

        return true;
    }

    public function getRack(Request $request){
        $barcode = $request->barcode;
        $loc = Locator::where('id',$barcode)->where('factory_id',Auth::user()->factory_id)->whereNull('deleted_at')->first();

        if ($loc!=null) {
            return response()->json($loc,200);
        }else{
             return response()->json("Location not Found ! ! !",422);
        }
        
    }

    public function borrow(){
        $dept = DB::table('department')->whereNull('deleted_at')->get();
        return view('lab.borrow.borrow')->with('dept',$dept);
    }

    public function ajaxBorrow(Request $request){
        $barcode = $request->bargar;
        $status = $request->status;
        $dept = $request->dept;
        $ip = $request->ip();
        $nik = $request->nik;

        $check = $this->cek_barcode($barcode);

        if (!$check) {
            return response()->json("Barcode garment not found",422);
        }

        if ($status=="borrow") {
            if ($check->current_location=="lab_storage" && $check->storage==true && $check->borrow==false) {

                $data = array(
                    'barcode_id'=>$check->barcode_id,
                    'docno'=>$check->documentno,
                    'season'=>$check->season,
                    'style'=>$check->style,
                    'article'=>$check->article,
                    'size'=>$check->size,
                    'status'=>$check->status,
                    'type'=>"out",
                    'loct'=>strtoupper($dept)
                );
                $query = $this->barcodeBorrow($barcode,"out",$dept,$ip,$nik);

                if (!$query) {
                     return response()->json("pdate movement garment error ! ! !",422);
                }
            }else{
                return response()->json("Barcode last location ".$check->current_location." ! ! !",422);
            }
        }else if ($status=="return") {
            if (($check->current_location!="lab_storage" || $check->current_location!="lab") && $check->storage==false && $check->borrow==true) {

                $data = array(
                    'barcode_id'=>$check->barcode_id,
                    'docno'=>$check->documentno,
                    'season'=>$check->season,
                    'style'=>$check->style,
                    'article'=>$check->article,
                    'size'=>$check->size,
                    'status'=>$check->status,
                    'type'=>"in"
                );
                $query = $this->barcodeBorrow($barcode,"in","lab",$ip);

                if (!$query) {
                     return response()->json("pdate movement garment error ! ! !",422);
                }
            }else{
                return response()->json("Barcode last location ".$check->current_location." ! ! !",422);
            }
        }

        return view('lab.borrow.ajax_list')->with('data',$data);
    }

    private function barcodeBorrow($barcode,$type,$location,$ipaddress,$nik){
        $last = GarmentMove::where('barcode_id',$barcode)->whereNull('deleted_at')->where('is_canceled',false)->orderBy('created_at','asc')->first();

        try {
            DB::begintransaction();
                switch ($type) {
                    case 'out':
                        $desc = "Checkout to ".$location;
                        $update = array(
                            'current_location'=>$location,
                            'borrow'=>true,
                            'storage'=>false,
                            'updated_at'=>Carbon::now()
                        );

                        $move = array(
                            'barcode_id'=>$barcode,
                            'locator_from'=>$last->locator_to,
                            'locator_to'=>$location,
                            'description'=>$desc,
                            'created_at'=>Carbon::now(),
                            'user_id'=>Auth::user()->id,
                            'ip_address'=>$ipaddress,
                            'borrower'=>$nik
                        );
                        break;

                    case 'in':
                        $desc = "Return to ".$location;
                        $bor= GarmentMove::where('barcode_id',$barcode_id)->where('locator_to','lab')->orderBy('created_at','desc')->fist();
                        $borrower = $bor->borrower;

                        $update = array(
                            'current_location'=>$location,
                            'borrow'=>true,
                            'storage'=>false,
                            'updated_at'=>Carbon::now()
                        );

                        $move = array(
                            'barcode_id'=>$barcode,
                            'locator_from'=>$last->locator_to,
                            'locator_to'=>$location,
                            'description'=>$desc,
                            'created_at'=>Carbon::now(),
                            'user_id'=>Auth::user()->id,
                            'ip_address'=>$ipaddress,
                            'borrower'=>$borrower
                        );
                        break;
                }

                

                GarmentId::where('barcode_id',$barcode)->update($update);
                GarmentMove::where('id',$last->id)->update(['deleted_at'=>Carbon::now()]);
                GarmentMove::firstorcreate($move);
            DB::commit();
        } catch (Exception $er) {
            DB::rollback();
            $message = $er->getMessage();
            ErrorHandler::db($message);
        }

        return true;
    }

    public function getNik(Request $request){
        $nik = $request->nik;

        $data = DB::table('employee')->where('nik',$nik)->whereNull('deleted_at')->first();
        if ($data!=null) {
            return response()->json($data,200);
        }else{
            return response()->json("Borrower not found ! ! !",422);
        }
    }
}
