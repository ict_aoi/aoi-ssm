<?php

namespace App\Http\Controllers\Carton;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use DB;
use Carbon\Carbon;
use DataTables;
use File;
use StdClass;
use Excel;
use Illuminate\Support\Facades\Storage;

use App\Models\Packinglist;
use App\Models\PackageId;

class CartonController extends Controller
{
    public function index(){
    	return view('carton.index');
    }

    public function uploadPL(Request $request){
    	$plids = array();
    	$results = array();
    	if($request->hasFile('file')){
          $extension = \File::extension($request->file->getClientOriginalName());
          if ($extension == "xlsx" || $extension == "xls") {
              $path = $request->file->getRealPath();
              $datax = \Excel::selectSheetsByIndex(0)->load($path)->get();
             
          }
	    }

	    try {
	    	DB::begintransaction();
				foreach ($datax as $dt) {

			    	$custno = trim($dt->cust_no);
			    	$country = trim($dt->country);
			    	$length = $dt->length/100;
			    	$width = $dt->width/100;
			    	$height = $dt->height/100;
			    	$ctn_qty = $dt->ctn_qty;
			    	$crd =date_format(date_create($dt->crd),'Y-m-d');

			    	if ($custno!=null && $country!=null && $ctn_qty!=null && $crd!=null) {

			    		$dt_pl = array(
			    					'customer_number'=>strtoupper($custno) ,
			    					'country'=>strtoupper($country) ,
			    					'length'=>$length ,
			    					'width'=>$width ,
			    					'height'=>$height ,
			    					'ctn_qty'=>$ctn_qty ,
			    					'crd'=>$crd ,
			    					'factory_id'=>Auth::user()->factory_id ,
			    					'created_at'=>Carbon::now()
			    				);

			    		$plid = Packinglist::firstorcreate($dt_pl);
			    		$plids[] = $plid->id; 
			    		for ($i=1; $i <=$ctn_qty ; $i++) { 
			    				$package = array(
			    								'ctn_id'=>$this->barcode_ctn($i),
			    								'pl_id'=>$plid->id ,
			    								'current_location'=>"preparation" ,
			    								'current_status'=>"onprogress" ,
			    								'prepartion'=>"onprogress" ,
			    								'filling'=>null ,
			    								'inventory'=>null ,
			    								'shipping'=>null ,
			    								'created_at'=>Carbon::now()
			    							);
			    				PackageId::firstorcreate($package);
			    		}
			    	}
		    	
				}

				$data_response = [
                            'status' => 200,
                            'output' => 'Carton created Success . . .',
                            'plids'=>$plids
                          ];
	    	DB::commit();
	    } catch (Exception $ex) {
	    	DB::rollback();
            $message = $ex->getMessage();
            ErrorHandler::db($message);
            $data_response = [
                            'status' => 200,
                            'output' => 'Carton created Field . . .'.$message
                          ];
	    }

	    return response()->json(['data'=>$data_response]);
	    
    }

    private function barcode_ctn($i){
    	$mxctn = PackageId::whereDate('created_at',Carbon::now()->format('Y-m-d'))->max('ctn_id');

    	if ($mxctn==null) {
    		$last =0;
    	}else{
    		$last = (int) substr($mxctn, 7,5);
    	}

    	$date = Carbon::now()->format('ymd');

    	$ctn_id = "C".$date.sprintf("%05s", $last+1);

    	return $ctn_id;
    }

    public function getDataPL(Request $request){
    	$radio = $request->radio;
    	$data = Packinglist::whereNull('deleted_at');
    	switch ($radio) {
    		case 'customer':
    			$data = $data->where('customer_number','LIKE','%'.$request->search);
    			break;

    		case 'country':
    			$data = $data->where('country','LIKE','%'.$request->search);
    			break;

    		case 'upload':
    			$data = $data->whereIN('id',$request->search);
    			break;
    	}

    	$data = $data->orderBy('created_at','desc');

    	return DataTables::of($data)
    						->addColumn('dimension',function($data){
    							$dim = ($data->length*100).'X'.($data->width*100).'X'.($data->height*100).' (cm)';

    							return $dim;
    						})
    						->addColumn('action',function($data){
    							return view('_action',[
                                            'model'=>$data,
                                            'ctndet'=>route('carton.detailCtn',[
                                                            'id'=>$data->id
                                                        ])
                                        ]);
    						})
    						->rawColumns(['dimension','action'])
    						->make(true);
    }

    public function detailCtn(Request $request){
       $id = $request->id;
       
       return view('carton.detail')->with('id',$id);
    }

    public function ajaxGetDetailCtn(Request $request){
        $data = DB::table('packinglist')
                        ->join('package','packinglist.id','=','package.pl_id')
                        ->where('packinglist.id',$request->id)
                        ->whereNull('packinglist.deleted_at')
                        ->whereNull('package.deleted_at')
                        ->orderBy('package.ctn_id','asc');

        return DataTables::of($data)
                            ->addColumn('dimension',function($data){
                                $dim = ($data->length*100).'X'.($data->width*100).'X'.($data->height*100).' (cm)';

                                return $dim;
                            })
                            ->editColumn('current_location',function($data){
                                return $data->current_location;
                            })
                            ->editColumn('current_status',function($data){
                                return $data->current_status;
                            })
                            ->rawColumns(['dimension','current_location','current_status'])
                            ->make(true);
    }

    public function printCtn(Request $request){
        $id = $request->id;

        $data = DB::table('packinglist')
                        ->join('package','packinglist.id','=','package.pl_id')
                        ->where('packinglist.id',$request->id)
                        ->whereNull('packinglist.deleted_at')
                        ->whereNull('package.deleted_at')
                        ->select('packinglist.customer_number','packinglist.country','packinglist.crd','package.ctn_id')
                        ->get();
        $filename = "Ctn_".$id;

        $pdf = \PDF::loadView('carton.template.carton',['data'=>$data])->setPaper('A4','potrait');
        return $pdf->stream($filename);
    }
}
