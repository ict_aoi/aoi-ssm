<?php

namespace App\Http\Controllers\Master;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use DB;
use Carbon\Carbon;
use Excel;
use Illuminate\Filesystem\Filesystem;
use Telegram\Bot\Laravel\Facades\Telegram;

use App\Models\MasterMO;
use App\Models\Invoices;
use App\Models\Cronjobs;
use App\Models\SampleReq;
use App\Models\SrDetail;
use App\Models\SrCsi;


class MasterDataController extends Controller
{
	public function __construct(){
	    ini_set('max_execution_time', 3600);
        $this->erp = DB::connection('replication_erp');
	}

      // master MO erp
      public function getMoERP($id){
        $listmo = [];
        switch ($id) {
            case '1':
                $from = Carbon::now()->subDays(1)->format('Y-m-d 14:00:00');
                $to = Carbon::now()->format('Y-m-d 06:59:59');
            break;

            case '2':
                $from = Carbon::now()->format('Y-m-d 07:00:00');
                $to = Carbon::now()->format('Y-m-d 09:59:59');
            break;

            case '3':
                $from = Carbon::now()->format('Y-m-d 10:00:00');
                $to = Carbon::now()->format('Y-m-d 13:59:59');
            break;

            case '4':
                $from = Carbon::now()->subDays(1)->format('Y-m-d 14:00:00');
                $to = Carbon::now()->format('Y-m-d 13:59:59');
            break;
        }


            $presale = $this->MoPrsell();

              $this->cron_jobs('erp_mo','start','ongoing','-');
              $erp = DB::connection('replication_erp')->table('bw_dss_mo_development')
                            ->where(function ($query) use ($from,$to){
                                $query->whereBetween('created',[$from,$to])
                                  ->orwhereBetween('updated',[$from,$to])
                                  ->orwhereBetween('docstatusexecutedate',[$from,$to]);
                            })
                            ->whereNotIn('documentno',$presale)
                            // ->whereIn('documentno',['MO/DEV/1018217','MO/DEV/1017833'])
                              // ->whereIN('docstatus',['CO','VO'])
                            //   ->where('tgl_kirim','2022-01-28')
                            ->orderBy('created','asc')
                            ->orderBy('updated','asc')
                            ->get();

            	try {
            		DB::begintransaction();
            			foreach ($erp as $erp) {
            				$mastermo = MasterMO::where('documentno',$erp->documentno)->whereNull('deleted_at')->first();

            				if ($mastermo==null && ($erp->docstatus=='CO' ||$erp->docstatus=='VO') ) {
            					$insert = array(
            						'documentno'=>$erp->documentno,
      							'season'=>$erp->season,
      							'style'=>$erp->style,
      							'article'=>$erp->article,
      							'size'=>$erp->size,
      							'brand'=>$erp->brand,
      							'qty'=>(int) $erp->qty,
      							'uom'=>$erp->uom,
      							'status'=>$erp->status,
      							'product_name'=>$erp->product_name,
      							'product_category'=>$erp->product_category,
      							'color'=>trim(explode('(',$erp->colorway)[0]),
      							'mo_created'=>$erp->created,
      							'mo_updated'=>$erp->updated,
      							'created_at'=>Carbon::now(),
      							'docstatus'=>$erp->docstatus
            					);

            					MasterMO::firstOrCreate($insert);

            				}else if ($mastermo!=null && ($erp->docstatus=='CO' ||$erp->docstatus=='VO')){
            					$update = array(
            						'documentno'=>$erp->documentno,
      							'season'=>$erp->season,
      							'style'=>$erp->style,
      							'article'=>$erp->article,
      							'size'=>$erp->size,
      							'brand'=>$erp->brand,
      							'qty'=>(int) $erp->qty,
      							'uom'=>$erp->uom,
      							'status'=>$erp->status,
      							'product_name'=>$erp->product_name,
      							'product_category'=>$erp->product_category,
      							'color'=>trim(explode('(',$erp->colorway)[0]),
      							'mo_created'=>$erp->created,
      							'mo_updated'=>$erp->updated,
      							'updated_at'=>Carbon::now(),
      							'docstatus'=>$erp->docstatus
            					);

            					MasterMO::where('id',$mastermo->id)->update($update);
            				}

                            $ceksr = SampleReq::where('documentno',$erp->documentno)->whereNull('deleted_at')->first();

                            if ($ceksr==null && $erp->docstatus=='CO') {
                                $sampReq = array(
                                        'documentno'=>$erp->documentno,
                                        'buyer'=>$erp->brand,
                                        'season'=>$erp->season,
                                        'style'=>$erp->style,
                                        'article'=>$erp->article,
                                        'size'=>$erp->size,
                                        'status'=>$erp->status,
                                        'qty'=>(int)$erp->qty,
                                        'shipment_date'=>$erp->tgl_kirim,
                                        'description'=>$erp->description,
                                        'image_id'=>$erp->ad_image_id,
                                        'mo_pic'=>$erp->createdby,
                                        'product_category'=>$erp->product_category,
                                        'created_at'=>carbon::now(),
                                        'mo_created'=>$erp->created,
                                        'mo_updated'=>$erp->updated,
                                        'color'=>trim(explode('(',$erp->colorway)[0]),
                                        'completed_date'=>$erp->docstatusexecutedate,
                                        'docstatus'=>$erp->docstatus,
                                        'allocation'=>$erp->warehouse,
                                        'pattern_status'=>"OPEN",
                                        'ori_date'=>isset($erp->ori_date) ? $erp->ori_date : null,
                                        'tarikan'=>$id,
                                        'marker_need'=> $erp->ismarker=='Y' ? true : false
                                    );

                                // dd($sampReq);

                                SampleReq::firstOrCreate($sampReq);
                                // $listmo[]=trim($erp->documentno);
                            }else if ($ceksr!=null && ($erp->docstatus=='CO' || $erp->docstatus=='VO')  ){
                                $cekup = $this->CekUpdate($ceksr,$erp);

                                if ($cekup>0) {
                                    $UpsampReq = array(
                                        'documentno'=>$erp->documentno,
                                        'buyer'=>$erp->brand,
                                        'season'=>$erp->season,
                                        'style'=>$erp->style,
                                        'article'=>$erp->article,
                                        'size'=>$erp->size,
                                        'status'=>$erp->status,
                                        'qty'=>(int)$erp->qty,
                                        'shipment_date'=>$erp->tgl_kirim,
                                        'description'=>$erp->description,
                                        'image_id'=>$erp->ad_image_id,
                                        'mo_pic'=>$erp->createdby,
                                        'product_category'=>$erp->product_category,
                                        'updated_at'=>Carbon::now(),
                                        'mo_created'=>$erp->created,
                                        'mo_updated'=>$erp->updated,
                                        'is_updated'=>true,
                                        'color'=>trim(explode('(',$erp->colorway)[0]),
                                        'completed_date'=>$erp->docstatusexecutedate,
                                        'docstatus'=>$erp->docstatus,
                                        'allocation'=>$erp->warehouse,
                                        'ori_date'=>isset($erp->ori_date) ? $erp->ori_date : null,
                                        'tarikan'=>$id,
                                        'marker_need'=> $erp->ismarker=='Y' ? true : false
                                    );

                                    SampleReq::where('id',$ceksr->id)->update($UpsampReq);

                                    // if ($erp->docstatus=='CO') {
                                    //    $listmo[]=trim($erp->documentno);
                                    // }
                                    
                                }
                            }

                            $listmo[]=trim($erp->documentno);
            			}
            		    $this->cron_jobs('erp_mo','done','done','-');
            		DB::commit();
            	} catch (Exception $ex) {
            		DB::rollback();
                  $message = $ex->getMessage();
                  ErrorHandler::db($message);
                  $this->cron_jobs('erp_mo','done','error',$message);
            	}

                $this->getSampleRequest($listmo);
                $this->getNotif();
      }
      // master MO erp
      static function CekUpdate($sr,$erp){
           $point = 0;

           if ($sr->status!=$erp->status) {
               $point = $point+1;
           }


           if ((int)$sr->qty!=(int)$erp->qty) {
               $point = $point+1;
           }


           if (carbon::parse($sr->shipment_date)->format('Y-m-d')!=carbon::parse($erp->tgl_kirim)->format('Y-m-d')) {
               $point = $point+1;
           }


           if ($sr->description!=$erp->description) {
               $point = $point+1;
           }

           if ($sr->image_id!=$erp->ad_image_id) {
               $point = $point+1;
           }


           if ($sr->product_category!=$erp->product_category) {
               $point = $point+1;
           }

           if ($sr->size!=$erp->size) {
               $point = $point+1;
           }

           // if (carbon::parse($sr->mo_updated)->format('Y-m-d')!=carbon::parse($erp->updated)->format('Y-m-d')) {
           //     $point = $point+1;
           // }

           if ($sr->color!=trim(explode('(',$erp->colorway)[0])) {
               $point = $point+1;
           }

           if (carbon::parse($sr->completed_date)->format('Y-m-d H:i:s')!=carbon::parse($erp->docstatusexecutedate)->format('Y-m-d H:i:s')) {
               $point = $point+1;
           }

           if ($sr->docstatus!=$erp->docstatus) {
               $point = $point+1;
           }

           if ($sr->allocation!=$erp->warehouse) {
               $point = $point+1;
           }

           if (($sr->marker_need==true && $erp->ismarker=='N') || ($sr->marker_need==false && $erp->ismarker=='Y')) {
               $point = $point+1;
           }

           return $point;
      }
      // master employee absensi
      public function employee(){

            $from = Carbon::now()->subDays(1)->format('Y-m-d 00:00:00');
            $to = Carbon::now()->format('Y-m-d 23:59:59');

            $this->cron_jobs('employee','start','ongoing','-');
            $abs = DB::connection('absen')->table('ns_employee')
                                                // ->whereIn('factory',['AOI1','AOI2'])
                                                ->whereIn('department_name',
                                                      [
                                                        'PPC',
                                                        'IE',
                                                        'QA & QC',
                                                        'MARKETING & MERCHANDISER',
                                                        'ORDER MANAGEMENT',
                                                        'OPERATION',
                                                        'PRODUCT DEVELOPMENT',
                                                        'ORDER MANAGEMENT'
                                                      ])
                                                // ->whereIn('nik',['201000070','191000028'])
                                                // ->where('status','aktif')
                                                ->whereBetween('created_at',[$from,$to])
                                                ->where(function($query){
                                                    $query->whereNull('termintation_date')
                                                                ->orWhere('status','aktif');
                                                })
                                                ->select(
                                                    'nik',
                                                    'name',
                                                    'factory',
                                                    'department_name',
                                                    DB::Raw('subdept as subdept_name')
                                                )
                                                ->get();


            try {
                  db::begintransaction();
                        foreach ($abs as $ab) {
                             $emp = DB::table('employee')->where('nik',$ab->nik)->exists();

                             switch ($ab->factory) {
                                   case 'AOI1':
                                         $fac = 1;
                                         break;

                                    case 'AOI2':
                                         $fac = 2;
                                         break;
                                        
                                    case 'AOI3':
                                        $fac = 3;
                                        break;

                                    case 'DC':
                                        $fac = 4;
                                        break;

                             }

                             if(!$emp){
                                $dt['nik']=$ab->nik;
                                $dt['name']=$ab->name;
                                $dt['department_name']=$ab->department_name;
                                $dt['subdept_name']=$ab->subdept_name;
                                $dt['factory_id']=$fac;
                                $dt['created_at']=Carbon::now();
                                DB::table('employee')->insert($dt);
                             }

                             // if ($emp) {
                             //       $up = array(
                             //              'name'=>$ab->name,
                             //              'department_name'=>$ab->department_name,
                             //              'subdept_name'=>$ab->subdept_name,
                             //              'factory_id'=>$fac,
                             //              'updated_at'=>Carbon::now()
                             //       );

                             //       DB::table('employee')->where('nik',$ab->nik)->update($up);
                             // }else{
                             //        $dt['nik']=$ab->nik;
                             //        $dt['name']=$ab->name;
                             //        $dt['department_name']=$ab->department_name;
                             //        $dt['subdept_name']=$ab->subdept_name;
                             //        $dt['factory_id']=$fac;
                             //        $dt['created_at']=Carbon::now();
                             //        DB::table('employee')->insert($dt);
                             // }
                        }

                        $this->cron_jobs('employee','done','done','-');
                  db::commit();
            } catch (Exception $er) {
                  DB::rollback();
                  $message = $er->getMessage();
                  ErrorHandler::db($message);
                  $this->cron_jobs('employee','done','error',$message);
            }
      }
      // master employee absensi

      //master invoice ERP
      public function getInvERP(){
          $from = date(Carbon::now()->subDays(7)->format('Y-m-d'));
          $to = date(Carbon::now()->format('Y-m-d'));

          $this->cron_jobs('erp_inv','start','ongoing','-');

          $erp = DB::connection('replication_erp')->table('bw_invoice_shipment_md')
                            ->whereBetween('created',[$from,$to])
                            ->orwhereBetween('updated',[$from,$to])
                            ->whereNotNull('order_reference')
                            ->OrderBy('created','desc')
                            ->OrderBy('updated','desc')
                            ->get();

          try {
            DB::begintransaction();
                foreach ($erp as $ep) {
                    $inv = Invoices::where('invoice',$ep->order_reference)->where('docstatus','<>','VO')->first();



                    if ($inv==null) {
                        $new = array(
                          'invoice'=>$ep->order_reference ,
                          'documentno'=>$ep->documentno ,
                          'document_type'=>$ep->document_type ,
                          'docstatus'=>$ep->docstatus ,
                          'country_to'=>$ep->country_to ,
                          'customer_number'=>$ep->customer_number ,
                          'shipmode'=>$ep->shipping_method ,
                          'created'=>$ep->created ,
                          'updated'=>$ep->updated ,
                          'created_at'=>Carbon::now()
                        );

                        Invoices::firstOrCreate($new);
                    }else{
                        $up = array(
                          'documentno'=>$ep->documentno ,
                          'document_type'=>$ep->document_type ,
                          'docstatus'=>$ep->docstatus ,
                          'country_to'=>$ep->country_to ,
                          'customer_number'=>$ep->customer_number ,
                          'shipmode'=>$ep->shipping_method ,
                          'created'=>$ep->created ,
                          'updated'=>$ep->updated ,
                          'updated_at'=>Carbon::now()
                        );

                        Invoices::where('id',$inv->id)->update($up);
                    }
                }

                $this->cron_jobs('erp_inv','done','done','-');
            DB::commit();
          } catch (Exception $ex) {
              DB::rollback();
              $message = $ex->getMessage();
              ErrorHandler::db($message);
              $this->cron_jobs('erp_inv','done','error',$message);
          }
      }
      //master invoice ERP

      private function getNotif(){
        $filename = "Notifikasi_Sample_Request";
        // $last =  "Last Update ".Carbon::now()->format('d-M-Y H:i');
        $i = 1;

        $from = Carbon::now()->subDays(7)->format('Y-m-d 00:00:00');
        // $from = '2024-10-25 00:00:00';
        $to = Carbon::now()->format('Y-m-d 23:59:59');

        // $data = SampleReq::whereBetween('updated_at',[$from,$to])
        //                     ->orwhereBetween('created_at',[$from,$to])
        //                     ->orderBy('created_at','desc')
        //                     ->get();

        $data = SampleReq::whereBetween('created_at',[$from,$to])
                                ->orWhere(function($query) use ($from,$to){
                                    $query->whereBetween('updated_at',[$from,$to])
                                            ->where('is_updated',true);
                                })
                                ->orderBy('created_at','desc')
                                ->get();
                                
        $path = storage_path('exports/Notifikasi_Sample_Request');

        $file = new Filesystem;
        $file->cleanDirectory($path);

        $export = \Excel::create($filename,function($excel) use ($data,$i){
            $excel->sheet('notif',function($sheet) use ($data,$i){
                $sheet->appendRow(array(
                    '#',
                    'Mo Created',
                    'Mo Updated',
                    'Tarikan',
                    'Tgl Mo Completed',
                    'Document No.',
                    'Season',
                    'Style',
                    'Article',
                    'Size',
                    'Status Sample',
                    'Qty Mo',
                    'PIC',
                    'Marker Remark',
                    'Description',
                    'Product Ctg.',
                    'Color',
                    'Allocation',
                    'Doc. Status'
                ))->setAllBorders(true);

                $sheet->getStyle('A1:R1')->applyFromArray(array(
                    'font'=>array('bold'=>true),
                    'borders'=>array('allborder'=>true)
                )); 

                foreach ($data as $dt) {
                    switch ($dt->tarikan) {
                        case '4':
                            $trk = "Tambahan";
                            break;
                        
                        default:
                            $trk = $dt->tarikan;
                            break;
                    }
                   $sheet->appendRow(array(
                        $i++,
                        date_format(date_create($dt->mo_created),'d-M-Y'),
                        date_format(date_create($dt->mo_updated),'d-M-Y'),
                        date_format(date_create($dt->updated_at),'d-M-Y')." Tarikan ".$trk,
                        date_format(date_create($dt->completed_date),'d-M-Y H:i'),
                        $dt->documentno,
                        $dt->season,
                        $dt->style,
                        $dt->article,
                        $dt->size,
                        $dt->status,
                        $dt->qty,
                        $dt->mo_pic,
                        $dt->marker_need==true ? "Need Print Marker" : "Not Need Print Marker",
                        $dt->description,
                        $dt->product_category,
                        $dt->color,
                        $dt->allocation,
                        $dt->docstatus
                    ));

                   $rws = 'A'.$i.':R'.$i;
                   if ($dt->docstatus=='CO') {
                       if ($dt->is_updated==true) {
                           $sheet->cell($rws,function($row){
                                $row->setBackground('#ff884d');
                                
                           });
                       }else{

                            $sheet->cell($rws,function($row){
                                $row->setBackground('#ffffff');
                            

                           });
                       }
                   }else{
                        $sheet->cell($rws,function($row){
                                $row->setBackground('#ffff99');
                            

                           });
                   }
                   
                   $sheet->setBorder($rws,'thin');
                }
            });
        })->store('xlsx');

      }

      private function getSampleRequest($listmo){
        $this->cron_jobs('erp_samplereq','start','ongoing','-');
            $date = Carbon::now()->format('Y-m-d');

            $smpl = SampleReq::whereIn('documentno',$listmo)
                                        // ->orWhereDate('updated_at',$date)
                                        ->whereNull('deleted_at')
                                        ->select('documentno','id')
                                        ->get();


            // try {
            //     DB::begintransaction();
                    foreach ($smpl as $sr) {
                        SrDetail::where('sr_id',$sr->id)->delete();
                        $this->getSrDetl($sr->documentno,$sr->id);

                        SrCsi::where('sr_id',$sr->id)->delete();
                        $this->getSrCsi($sr->documentno,$sr->id);
                    }

                    $this->cron_jobs('erp_samplereq','done','done','-');
            //     DB::commit();
            // } catch (Exception $ex) {
            //      DB::rollback();
            //   $message = $ex->getMessage();
            //   ErrorHandler::db($message);
            //   $this->cron_jobs('erp_samplereq','done','error',$message);
            // }


      }

      private function getSrDetl($docno,$id){
        $cekDt = SrDetail::where('sr_id',$id)->whereNull('deleted_at')->count();

        if ($cekDt==0) {
            $erpDetl = $this->erp->table('bw_ssm_sample_request')
                                            ->where('documentno',$docno)
                                            ->whereNotNull(['part','part_no'])
                                            ->get();

            try {
                 DB::begintransaction();
                    foreach ($erpDetl as $dt) {

                        switch ($dt->part_name) {
                            case 'Fabric':
                                $ty = "Fabric";
                                break;

                            case 'Label':
                                $ty = "Label";
                                break;

                            default:
                                $ty = "Trim";
                                break;
                        }

                        $compdet = array(
                            'sr_id'=>$id,
                            'partname'=>$dt->part,
                            'partno'=>$dt->part_no,
                            'material'=>$dt->material,
                            'yield'=>$dt->yield,
                            'uom'=>$dt->uom,
                            'color'=>$dt->color,
                            'description'=>$dt->description,
                            'width'=>$dt->width_size,
                            'note'=>$dt->desc,
                            'created_at'=>carbon::now(),
                            'type'=>$ty
                        );

                        SrDetail::firstOrCreate($compdet);
                    }
                 DB::commit();
            } catch (Exception $ex) {
                DB::rollback();
              $message = $ex->getMessage();
              ErrorHandler::db($message);
            }


        }

        return true;

      }

      private function getSrCsi($docno,$id){
        $cekCs = SrCsi::where('sr_id',$id)->whereNull('deleted_at')->count();

        if ($cekCs==0) {
            $erpCsi = $this->erp->table('bw_ssm_csi')
                                            ->where('documentno',$docno)
                                            // ->whereNotNull(['material','part_no'])
                                            ->get();

            try {
                 DB::begintransaction();
                    foreach ($erpCsi as $csi) {



                        $csidet = array(
                            'sr_id'=>$id,
                            'part_no'=>$csi->part_no,
                            'material_code'=>$csi->code_raw_material,
                            'material'=>$csi->material,
                            'uom'=>$csi->uom_product,
                            'fbc'=>$csi->fbc,
                            'note'=>$csi->description,
                            'created_at'=>carbon::now(),
                            'usage'=>$csi->usage

                        );

                        SrCsi::firstOrCreate($csidet);
                    }
                 DB::commit();
            } catch (Exception $ex) {
                DB::rollback();
              $message = $ex->getMessage();
              ErrorHandler::db($message);
            }
        }


            return true;
      }


      // schaduler
      private function cron_jobs($cron_name,$type,$status,$error){
        $today = Carbon::now()->format('Y-m-d');

        switch ($type) {
          case 'start':
                $start =  array(
                  'cron_name'=>$cron_name,
                  'status'=>$status,
                  'start'=>Carbon::now(),
                  'created_at'=>Carbon::now(),
                  'error_status'=>$error
                );
               Cronjobs::firstOrCreate($start);
                // DB::table('cron_jobs')->insert($start);
               $txt = "Cron ".$cron_name." Start At : ".Carbon::now()->format('d M Y H:i:s').".";
            //    $this->notifikasi($txt);
              break;
          case 'done':
            $cron =  Cronjobs::wheredate('created_at',$today)->where('cron_name',$cron_name)->where('status','ongoing')->first();
                $done =  array(
                  'status'=>$status,
                  'finish'=>isset($status)=='done' ? Carbon::now() : null,
                  'updated_at'=>Carbon::now(),
                  'error_status'=>$error
                );
               Cronjobs::where('id',$cron->id)->update($done);
               $txt = "Cron ".$cron_name." Finish At : ".Carbon::now()->format('d M Y H:i:s')." ERROR : ".$error;
            //    $this->notifikasi($txt);
              break;
        }

        return true;

      }
      // schaduler


      static function MoPrsell(){
            return array(
                    'MO/DEV/1016725','MO/DEV/1016767','MO/DEV/1016937','MO/DEV/1016938','MO/DEV/1016939','MO/DEV/1016940','MO/DEV/1016941','MO/DEV/1016942','MO/DEV/1016969','MO/DEV/1017087','MO/DEV/1017088','MO/DEV/1017089','MO/DEV/1017090','MO/DEV/1017091','MO/DEV/1017092','MO/DEV/1017093','MO/DEV/1017379','MO/DEV/1017380','MO/DEV/1017393','MO/DEV/1017394','MO/DEV/1017395','MO/DEV/1017396','MO/DEV/1017397','MO/DEV/1017398','MO/DEV/1017399','MO/DEV/1017400','MO/DEV/1017401','MO/DEV/1017405','MO/DEV/1017403','MO/DEV/1017404','MO/DEV/1017406','MO/DEV/1017407','MO/DEV/1017408','MO/DEV/1017409','MO/DEV/1017410','MO/DEV/1017411','MO/DEV/1017412','MO/DEV/1017413','MO/DEV/1017414','MO/DEV/1017415','MO/DEV/1017417','MO/DEV/1017418','MO/DEV/1017421','MO/DEV/1017422','MO/DEV/1017435','MO/DEV/1017436','MO/DEV/1017437','MO/DEV/1017438','MO/DEV/1017445','MO/DEV/1017446','MO/DEV/1017447','MO/DEV/1017448','MO/DEV/1017449','MO/DEV/1017450','MO/DEV/1017451','MO/DEV/1017452','MO/DEV/1017453','MO/DEV/1017454','MO/DEV/1017455','MO/DEV/1017456','MO/DEV/1017457','MO/DEV/1017458','MO/DEV/1017459','MO/DEV/1017460','MO/DEV/1017461','MO/DEV/1017462','MO/DEV/1017463','MO/DEV/1017464','MO/DEV/1017465','MO/DEV/1017466','MO/DEV/1017467','MO/DEV/1017468','MO/DEV/1017469','MO/DEV/1017470','MO/DEV/1017471','MO/DEV/1017472','MO/DEV/1017473','MO/DEV/1017474','MO/DEV/1017475','MO/DEV/1017476','MO/DEV/1017477','MO/DEV/1017478','MO/DEV/1017479','MO/DEV/1017480','MO/DEV/1017481','MO/DEV/1017482','MO/DEV/1017483','MO/DEV/1017484','MO/DEV/1017485','MO/DEV/1017486','MO/DEV/1017487','MO/DEV/1017488','MO/DEV/1017489','MO/DEV/1017490','MO/DEV/1017491','MO/DEV/1017492','MO/DEV/1017493','MO/DEV/1017494','MO/DEV/1017495','MO/DEV/1017496','MO/DEV/1017497','MO/DEV/1017498','MO/DEV/1017499','MO/DEV/1017500','MO/DEV/1017501','MO/DEV/1017502','MO/DEV/1017503','MO/DEV/1017504','MO/DEV/1017505','MO/DEV/1017506','MO/DEV/1017507','MO/DEV/1017508','MO/DEV/1017509','MO/DEV/1017510','MO/DEV/1017511','MO/DEV/1017512','MO/DEV/1017513','MO/DEV/1017514','MO/DEV/1017515','MO/DEV/1017516','MO/DEV/1017517','MO/DEV/1017518','MO/DEV/1017519','MO/DEV/1017520','MO/DEV/1017521','MO/DEV/1017522','MO/DEV/1017523','MO/DEV/1017524','MO/DEV/1017525','MO/DEV/1017526','MO/DEV/1017527','MO/DEV/1017528','MO/DEV/1017529','MO/DEV/1017530','MO/DEV/1017531','MO/DEV/1017532','MO/DEV/1017533','MO/DEV/1017534','MO/DEV/1017535','MO/DEV/1017536','MO/DEV/1017537','MO/DEV/1017538','MO/DEV/1017539','MO/DEV/1017540','MO/DEV/1017541','MO/DEV/1017542','MO/DEV/1017543','MO/DEV/1017544','MO/DEV/1017545','MO/DEV/1017546','MO/DEV/1017547','MO/DEV/1017548','MO/DEV/1017549','MO/DEV/1017550','MO/DEV/1017551','MO/DEV/1017552','MO/DEV/1017553','MO/DEV/1017554','MO/DEV/1017555','MO/DEV/1017556','MO/DEV/1017557','MO/DEV/1017558','MO/DEV/1017559','MO/DEV/1017560','MO/DEV/1017561','MO/DEV/1017562','MO/DEV/1017563','MO/DEV/1017564','MO/DEV/1017565','MO/DEV/1017566','MO/DEV/1017567','MO/DEV/1017568','MO/DEV/1017569','MO/DEV/1017570','MO/DEV/1017571','MO/DEV/1017572','MO/DEV/1017573','MO/DEV/1017574','MO/DEV/1017575','MO/DEV/1017576','MO/DEV/1017577','MO/DEV/1017578','MO/DEV/1017579','MO/DEV/1017580','MO/DEV/1017581','MO/DEV/1017582','MO/DEV/1017583','MO/DEV/1017584','MO/DEV/1017585','MO/DEV/1017586','MO/DEV/1017587','MO/DEV/1017588','MO/DEV/1017589','MO/DEV/1017590','MO/DEV/1017591','MO/DEV/1017592','MO/DEV/1017593','MO/DEV/1017594','MO/DEV/1017595','MO/DEV/1017596','MO/DEV/1017597','MO/DEV/1017598','MO/DEV/1017599','MO/DEV/1017600','MO/DEV/1017601','MO/DEV/1017602','MO/DEV/1017603','MO/DEV/1017610','MO/DEV/1017611','MO/DEV/1017612','MO/DEV/1017613','MO/DEV/1017614','MO/DEV/1017615','MO/DEV/1017620','MO/DEV/1017621','MO/DEV/1017622','MO/DEV/1017623','MO/DEV/1017624','MO/DEV/1017656','MO/DEV/1017657','MO/DEV/1017658','MO/DEV/1017659','MO/DEV/1017660','MO/DEV/1017661','MO/DEV/1017662','MO/DEV/1017701','MO/DEV/1017702','MO/DEV/1017703','MO/DEV/1017704','MO/DEV/1017705','MO/DEV/1017706','MO/DEV/1017707','MO/DEV/1017708','MO/DEV/1017783','MO/DEV/1017784','MO/DEV/1017785','MO/DEV/1017786','MO/DEV/1017787','MO/DEV/1017788','MO/DEV/1017815','MO/DEV/1017816','MO/DEV/1017817','MO/DEV/1017818','MO/DEV/1017819','MO/DEV/1017820','MO/DEV/1017821','MO/DEV/1017823','MO/DEV/1017824','MO/DEV/1017825','MO/DEV/1017826','MO/DEV/1017827','MO/DEV/1017833','MO/DEV/1017834','MO/DEV/1017835','MO/DEV/1017823','MO/DEV/1017824','MO/DEV/1017825','MO/DEV/1017826','MO/DEV/1017827','MO/DEV/1016725','MO/DEV/1016767','MO/DEV/1016937','MO/DEV/1016938','MO/DEV/1016939','MO/DEV/1016940','MO/DEV/1016941','MO/DEV/1016942','MO/DEV/1016969','MO/DEV/1017087','MO/DEV/1017088','MO/DEV/1017089','MO/DEV/1017090','MO/DEV/1017091','MO/DEV/1017092','MO/DEV/1017093','MO/DEV/1017379','MO/DEV/1017380','MO/DEV/1017435','MO/DEV/1017436','MO/DEV/1017437','MO/DEV/1017438','MO/DEV/1017656','MO/DEV/1017657','MO/DEV/1017658','MO/DEV/1017659','MO/DEV/1017660','MO/DEV/1017661','MO/DEV/1017662','MO/DEV/1017701','MO/DEV/1017702','MO/DEV/1017703','MO/DEV/1017704','MO/DEV/1017705','MO/DEV/1017706','MO/DEV/1017707','MO/DEV/1017708','MO/DEV/1017783','MO/DEV/1017784','MO/DEV/1017785','MO/DEV/1017786','MO/DEV/1017787','MO/DEV/1017788','MO/DEV/1017815','MO/DEV/1017833','MO/DEV/1017834','MO/DEV/1017835','MO/DEV/1017783','MO/DEV/1017784','MO/DEV/1017785','MO/DEV/1017786','MO/DEV/1017787','MO/DEV/1017788','MO/DEV/1017701','MO/DEV/1017702','MO/DEV/1017703','MO/DEV/1017704','MO/DEV/1017705','MO/DEV/1017706','MO/DEV/1017707','MO/DEV/1017708','MO/DEV/1017661','MO/DEV/1017662','MO/DEV/1017786','MO/DEV/1017787','MO/DEV/1017788','MO/DEV/1017815','MO/DEV/1017833','MO/DEV/1017834','MO/DEV/1017835','MO/DEV/1017661','MO/DEV/1017662','MO/DEV/1017701','MO/DEV/1017087','MO/DEV/1017088','MO/DEV/1017089','MO/DEV/1017090','MO/DEV/1017091','MO/DEV/1017092','MO/DEV/1017093','MO/DEV/1016969','MO/DEV/1017435','MO/DEV/1017436','MO/DEV/1017437','MO/DEV/1017438','MO/DEV/1017656','MO/DEV/1017657','MO/DEV/1017658','MO/DEV/1017659','MO/DEV/1017660','MO/DEV/1017783','MO/DEV/1017784','MO/DEV/1017785','MO/DEV/1018217','MO/DEV/1017393','MO/DEV/1017394','MO/DEV/1017395','MO/DEV/1017399','MO/DEV/1017400','MO/DEV/1017401','MO/DEV/1017405','MO/DEV/1017403','MO/DEV/1017406','MO/DEV/1017407','MO/DEV/1017408','MO/DEV/1017409','MO/DEV/1017411','MO/DEV/1017412','MO/DEV/1017413','MO/DEV/1017414','MO/DEV/1017415','MO/DEV/1017417','MO/DEV/1017418','MO/DEV/1017422','MO/DEV/1017620','MO/DEV/1017621','MO/DEV/1017622','MO/DEV/1017623','MO/DEV/1017624','MO/DEV/1017445','MO/DEV/1017446','MO/DEV/1017447','MO/DEV/1017448','MO/DEV/1017449','MO/DEV/1017450','MO/DEV/1017451','MO/DEV/1017452','MO/DEV/1017453','MO/DEV/1017454','MO/DEV/1017455','MO/DEV/1017456','MO/DEV/1017457','MO/DEV/1017458','MO/DEV/1017459','MO/DEV/1017460','MO/DEV/1017461','MO/DEV/1017462','MO/DEV/1017463','MO/DEV/1017464','MO/DEV/1017465','MO/DEV/1017466','MO/DEV/1017467','MO/DEV/1017468','MO/DEV/1017469','MO/DEV/1017470','MO/DEV/1017471','MO/DEV/1017472','MO/DEV/1017473','MO/DEV/1017474','MO/DEV/1017475','MO/DEV/1017476','MO/DEV/1017477','MO/DEV/1017478','MO/DEV/1017614','MO/DEV/1017612','MO/DEV/1017610','MO/DEV/1017615','MO/DEV/1017611','MO/DEV/1017613','MO/DEV/1017479','MO/DEV/1017480','MO/DEV/1017481','MO/DEV/1017482','MO/DEV/1017483','MO/DEV/1017484','MO/DEV/1017485','MO/DEV/1017486','MO/DEV/1017487','MO/DEV/1017488','MO/DEV/1017489','MO/DEV/1017490','MO/DEV/1017491','MO/DEV/1017492','MO/DEV/1017493','MO/DEV/1017494','MO/DEV/1017495','MO/DEV/1017496','MO/DEV/1017497','MO/DEV/1017498','MO/DEV/1017499','MO/DEV/1017500','MO/DEV/1017501','MO/DEV/1017502','MO/DEV/1017503','MO/DEV/1017504','MO/DEV/1017505','MO/DEV/1017506','MO/DEV/1017507','MO/DEV/1017508','MO/DEV/1017509','MO/DEV/1017510','MO/DEV/1017511','MO/DEV/1017512','MO/DEV/1017513','MO/DEV/1017514','MO/DEV/1017515','MO/DEV/1017516','MO/DEV/1017517','MO/DEV/1017518','MO/DEV/1017519','MO/DEV/1017520','MO/DEV/1017521','MO/DEV/1017522','MO/DEV/1017527','MO/DEV/1017528','MO/DEV/1017529','MO/DEV/1017537','MO/DEV/1017602','MO/DEV/1017603','MO/DEV/1017530','MO/DEV/1017531','MO/DEV/1017532','MO/DEV/1017533','MO/DEV/1017534','MO/DEV/1017535','MO/DEV/1017536','MO/DEV/1017538','MO/DEV/1017539','MO/DEV/1017540','MO/DEV/1017541','MO/DEV/1017542','MO/DEV/1017543','MO/DEV/1017544','MO/DEV/1017545','MO/DEV/1017546','MO/DEV/1017547','MO/DEV/1017548','MO/DEV/1017549','MO/DEV/1017550','MO/DEV/1017551','MO/DEV/1017552','MO/DEV/1017553','MO/DEV/1017554','MO/DEV/1017555','MO/DEV/1017556','MO/DEV/1017557','MO/DEV/1017558','MO/DEV/1017559','MO/DEV/1017560','MO/DEV/1017561','MO/DEV/1017562','MO/DEV/1017563','MO/DEV/1017564','MO/DEV/1017565','MO/DEV/1017566','MO/DEV/1017567','MO/DEV/1017568','MO/DEV/1017569','MO/DEV/1017570','MO/DEV/1017571','MO/DEV/1017572','MO/DEV/1017573','MO/DEV/1017574','MO/DEV/1017575','MO/DEV/1017576','MO/DEV/1017577','MO/DEV/1017578','MO/DEV/1017579','MO/DEV/1017580','MO/DEV/1017581','MO/DEV/1017582','MO/DEV/1017583','MO/DEV/1017584','MO/DEV/1017585','MO/DEV/1017586','MO/DEV/1017587','MO/DEV/1017588','MO/DEV/1017589','MO/DEV/1017590','MO/DEV/1017591','MO/DEV/1017592','MO/DEV/1017593','MO/DEV/1017594','MO/DEV/1017595','MO/DEV/1017596','MO/DEV/1017597','MO/DEV/1017598','MO/DEV/1017599','MO/DEV/1017600','MO/DEV/1017601','MO/DEV/1017402');
      }


    static function notifikasi($text){
        $response = Telegram::getMe();

        $response = Telegram::sendMessage([
          'chat_id' => '@nsnotif', 
          'text' => $text
        ]);

        $messageId = $response->getMessageId();
    }
}
