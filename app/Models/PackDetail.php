<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Uuids;

class PackDetail extends Model
{
    use Uuids;
	public $incrementing = false;
	protected $guarded = ['id'];
	protected $table = 'detail_package';
	protected $fillable = ['carton_id','po_number','style','article','size','qty','created_at','updated_at','deleted_at','season','uom'];
}
