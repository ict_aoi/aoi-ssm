<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GarmentId extends Model
{

	public $incrementing = false;
	protected $table = 'barcode_garment';
	protected $fillable = ['barcode_id','mo_id','current_location','created_at','updated_at','deleted_at','is_print','factory_id','sets','checkin_md','storage','borrow','carton_id','reciept_date'];
}
