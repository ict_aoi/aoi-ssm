<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Uuids;

class SampleReq extends Model
{
    use Uuids;
    public $incrementing = false;
    protected $guarded = ['id'];
    protected $table = 'sample_request';
    protected $fillable = ['documentno','buyer','season','style','article','size','status','qty','shipment_date','description','image_id','mo_pic','created_at','updated_at','deleted_at','product_category','is_sr','is_csi','mo_created','mo_updated','is_updated','status','docstatus','completed_date','color','allocation','marker_need','tarikan'];
}
