<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Uuids;

class GarmentMove extends Model
{
    use Uuids;
	public $incrementing = false;
	protected $guarded = ['id'];
	protected $table = 'garment_movements';
	protected $fillable = ['barcode_id','locator_from','locator_to','locator_id','description','is_canceled','created_at','updated_at','deleted_at','user_id','ip_address','borrower','line','promise_return','factory_to','factory_from'];
}
