<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Uuids;

class Cronjobs extends Model
{
    use Uuids;
	public $incrementing = false;
	protected $guarded = ['id'];
	protected $table = 'cron_jobs';
	protected $fillable = ['cron_name','status','start','finish','error_status','created_at','updated_at','deleted_at'];
}
