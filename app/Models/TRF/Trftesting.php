<?php

namespace App\Models\TRF;

use Illuminate\Database\Eloquent\Model;
use App\Uuids;

class Trftesting extends Model
{
    use Uuids;
    public $incrementing = false;
    protected $connection = 'lab';
    protected $guarded = ['id'];
    protected $table = 'trf_testings';
    protected $fillable = ['trf_id','buyer','lab_location','nik','platform','factory_id','asal_specimen','category_specimen','type_specimen','category','date_information','date_information_remark','test_required','part_of_specimen','return_test_sample','status','verified_lab_date','verified_lab_by','reject_by','reject_date','is_handover','status_handover','remarks','previous_trf_id','result_t2','additional_information','id_category','pic_test','approval_test','username'];
}
