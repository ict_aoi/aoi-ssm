<?php

namespace App\Models\TRF;

use Illuminate\Database\Eloquent\Model;
use App\Uuids;

class TrfDocument extends Model
{
    use Uuids;
    public $incrementing = false;
    protected $connection = 'lab';
    protected $guarded = ['id'];
    protected $table = 'trf_testing_document';
    protected $fillable = ['trf_id','document_type','document_no','style','article_no','size','color','fibre_composition','fabric_finish','gauge','fabric_weight','plm_no','care_instruction','manufacture_name','export_to','nomor_roll','batch_number','is_repetation','item','barcode_supplier','additional_information','yds_roll','description','season','deleted_by'];
}
