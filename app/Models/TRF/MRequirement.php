<?php

namespace App\Models\TRF;

use Illuminate\Database\Eloquent\Model;
use App\Uuids;


class MRequirement extends Model
{
    use Uuids;
    public $incrementing = false;
    protected $connection = 'lab';
    protected $guarded = ['id'];
    protected $table = 'master_requirements';
    protected $fillable = ['master_method_id','master_category_id','komposisi_specimen','parameter','perlakuan_test','operator','uom','value1','value2','value3','value4','value5','value6','value7','remarks','created_at','updated_at','deleted_at','created_by','deleted_by','buyer','sequence','false'];
}
