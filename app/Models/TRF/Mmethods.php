<?php

namespace App\Models\TRF;

use Illuminate\Database\Eloquent\Model;
use App\Uuids;

class Mmethods extends Model
{
    use Uuids;
    public $incrementing = false;
    protected $connection = 'lab';
    protected $guarded = ['id'];
    protected $table = 'master_method';
    protected $fillable = ['method_code','method_name','category','created_at','updated_at','deleted_at','created_by','sequence','type'];
}
