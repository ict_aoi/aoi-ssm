<?php

namespace App\Models\TRF;

use Illuminate\Database\Eloquent\Model;

class Mlablocation extends Model
{
    public $incrementing = true;
    protected $connection = 'lab';
    protected $guarded = ['id'];
    protected $table = 'master_lab_location';
    protected $fillable = ['company_name','created_at','updated_at','deleted_at'];
}
