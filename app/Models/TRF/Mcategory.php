<?php

namespace App\Models\TRF;

use Illuminate\Database\Eloquent\Model;
use App\Uuids;

class Mcategory extends Model
{
    use Uuids;
    public $incrementing = false;
    protected $connection = 'lab';
    protected $guarded = ['id'];
    protected $table = 'master_category';
    protected $fillable = ['category','category_specimen','type_specimen','created_at','created_by','updated_at','deleted_at','deleted_by','sequence'];
}
