<?php

namespace App\Models\TRF;

use Illuminate\Database\Eloquent\Model;

class Mfact extends Model
{
    protected $connection = 'lab';
    protected $guarded = ['id'];
    protected $table = 'factory';
}
