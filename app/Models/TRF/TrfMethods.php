<?php

namespace App\Models\TRF;

use Illuminate\Database\Eloquent\Model;
use App\Uuids;


class TrfMethods extends Model
{
    use Uuids;
    public $incrementing = false;
    protected $connection = 'lab';
    protected $guarded = ['id'];
    protected $table = 'trf_testing_methods';
    protected $fillable = ['trf_id','master_method_id','created_at','updated_at','deleted_at','created_by','deleted_by'];
}
