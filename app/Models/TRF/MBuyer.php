<?php

namespace App\Models\TRF;

use Illuminate\Database\Eloquent\Model;


class MBuyer extends Model
{

    public $incrementing = true;
    protected $connection = 'lab';
    protected $guarded = ['id'];
    protected $table = 'master_buyer';
    protected $fillable = ['buyer','created_at','updated_at','deleted_at'];
}
