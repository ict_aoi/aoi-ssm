<?php

namespace App\Models\TRF;

use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    protected $connection = 'absen';
    protected $table = 'adt_new_employee';
}
