<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Uuids;

class MasterMO extends Model
{
    use Uuids;
	public $incrementing = false;
	protected $guarded = ['id'];
	protected $table = 'master_mo';
	protected $fillable = ['documentno','season','style','article','size','brand','qty','uom','status','product_name','product_category','color','mo_created','mo_updated','created_at','updated_at','deleted_at','docstatus','remark','topbot','fabric1','merch','technician','fabric2','is_reciept'];
}
