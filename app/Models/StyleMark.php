<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Uuids;

class StyleMark extends Model
{
     use Uuids;
    public $incrementing = false;
    protected $guarded = ['id'];
    protected $table = 'marker_style';
    protected $fillable = ['season','style','release','podd','pic','remark','user_id','ip_address','created_at','updated_at','deleted_at'];
}
