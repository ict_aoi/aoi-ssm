<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Uuids;

class SrCsi extends Model
{
     use Uuids;
    public $incrementing = false;
    protected $guarded = ['id'];
    protected $table = 'sr_csi_detail';
    protected $fillable = ['sr_id','part_no','material_code','material','uom','fbc','note','created_at','updated_at','deleted_at','usage'];
}
