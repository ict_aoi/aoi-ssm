<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Uuids;

class SrDetail extends Model
{
    use Uuids;
    public $incrementing = false;
    protected $guarded = ['id'];
    protected $table = 'sr_component_detail';
    protected $fillable = ['sr_id','partname','partno','material','yield','uom','color','description','width','note',
'created_at','updated_at','deleted_at','type'];

}
