<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Uuids;

class Invoices extends Model
{
   	use Uuids;
	public $incrementing = false;
	protected $guarded = ['id'];
	protected $table = 'invoices';
	protected $fillable = ['invoice','documentno','document_type','docstatus','country_to','customer_number','shipmode','created','updated','created_at','updated_at','deleted_at'];
}
