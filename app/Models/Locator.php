<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Locator extends Model
{
    public $incrementing = false;
	protected $table = 'locator';
	protected $fillable = ['id','area_id','name','description','factory_id','created_at','updated_at','deleted_at'];
}
