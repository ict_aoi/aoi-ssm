<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SrInfo extends Model
{
    public $incrementing = false;
    protected $table = 'sr_info';
    protected $fillable = ['id','created_at','updated_at','deleted_at','pattern_fu','pattern_remark','pattern_status','pattern_date','marker_length','date_plot','marker_release','recever','pic_marker','pattern_marker','ori_date','pattern_stage','marker_status'];
}
