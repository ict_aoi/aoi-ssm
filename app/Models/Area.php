<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Uuids;

class Area extends Model
{
    use Uuids;
	public $incrementing = false;
	protected $guarded = ['id'];
	protected $table = 'area';
	protected $fillable = ['name','factory_id','description','created_at','updated_at','deleted_at'];
}
