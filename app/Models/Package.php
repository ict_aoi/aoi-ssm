<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Package extends Model
{
    public $incrementing = false;
	protected $table = 'package';
	protected $fillable = ['carton_id','invoice_id','carton_no','location','status','created_at','updated_at','deleted_at'];
}
