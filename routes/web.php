<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    if (Auth::check()) {
    	return redirect()->route('home.index');
    }

    return redirect('/login');
});

Route::get('/auth', 'Auth\LoginController@showLogin')->name('auth.showLogin');
Route::post('/auth/loginaction', 'Auth\LoginController@loginAction')->name('auth.loginAction');
Route::post('/auth/logoutaction', 'Auth\LoginController@logoutAction')->name('auth.logoutAction');

Auth::routes();

Route::middleware('auth')->group(function(){
	Route::prefix('/dashboard')->group(function(){
		Route::get('', 'DashboardController@index')->name('home.index');
		Route::get('/get-data', 'DashboardController@getData')->name('home.getData');
		Route::get('/detail', 'DashboardController@detailDash')->name('home.detailDash');
		Route::get('/detail/get-data', 'DashboardController@ajaxGetDetail')->name('home.ajaxGetDetail');
	});
	

	//account setting
	Route::prefix('/user')->group(function(){
		Route::get('/myAccount', 'User\AccountController@myAccount')->name('account.myAccount');
		Route::post('/myAccount/edit', 'User\AccountController@editPassword')->name('account.editPassword');
	});

	Route::prefix('/admin')->middleware(['permission:menu-user-management'])->group(function(){
		//user account
		Route::get('/user-account', 'Admin\AdminController@user_account')->name('admin.user_account');
		Route::get('/user-account/get-data', 'Admin\AdminController@getDataUser')->name('admin.getDataUser');
		Route::get('/user-account/ajaxGetRole', 'Admin\AdminController@ajaxGetRole')->name('admin.ajaxGetRole');
		Route::get('/user-account/add-user', 'Admin\AdminController@addUser')->name('admin.addUser');
		Route::get('/user-account/edit', 'Admin\AdminController@formEditUser')->name('admin.formEditUser');
		Route::get('/user-account/edit/edit-user', 'Admin\AdminController@editAccount')->name('admin.editAccount');
		Route::get('/user-account/edit/reset-password', 'Admin\AdminController@passwordReset')->name('admin.passwordReset');
		Route::get('/user-account/innactive', 'Admin\AdminController@innactiveUser')->name('admin.innactiveUser');

		//role
		Route::get('/role', 'Admin\AdminController@formRole')->name('admin.formRole');
		Route::get('/role/get-data', 'Admin\AdminController@ajaxRoleData')->name('admin.ajaxRoleData');
		Route::get('/role/new-role', 'Admin\AdminController@newRole')->name('admin.newRole');
		Route::post('/role/add-role', 'Admin\AdminController@addRole')->name('admin.addRole');
		Route::get('/role/edit', 'Admin\AdminController@editRoleUser')->name('admin.editRoleUser');
		Route::post('/role/update-role', 'Admin\AdminController@updateRole')->name('admin.updateRole');
	});

	Route::prefix('/sample-reciept')->middleware(['permission:menu-sample-reciept'])->group(function(){
		
		Route::get('', 'Sample\SampleAdminConttroler@index')->name('reciept.index');

		//search
		Route::get('/search/get-mo', 'Sample\SampleAdminConttroler@getMO')->name('reciept.getMO');
		Route::get('/search/det-mo', 'Sample\SampleAdminConttroler@getDetMo')->name('reciept.getDetMo');
		Route::get('/search/reciept', 'Sample\SampleAdminConttroler@reciept')->name('reciept.reciept');
		

		//upload
		Route::get('/upload/template-upload','Sample\SampleAdminConttroler@templateUpload')->name('reciept.templateUpload');
		Route::post('/upload/upload-mo','Sample\SampleAdminConttroler@ajaxUpload')->name('reciept.ajaxUpload');
		Route::get('/upload/getDataUpload','Sample\SampleAdminConttroler@getDataUpload')->name('reciept.getDataUpload');
		Route::get('/upload/print','Sample\SampleAdminConttroler@printUploadBarcode')->name('reciept.printUploadBarcode');

		Route::get('/detail-mo','Sample\SampleAdminConttroler@detailMO')->name('reciept.detailMO');
		Route::get('/detail-mo/ajaxGetDetail','Sample\SampleAdminConttroler@ajaxGetDetail')->name('reciept.ajaxGetDetail');
		Route::get('/detail-mo/print-barcode','Sample\SampleAdminConttroler@printBarcode')->name('reciept.printBarcode');
		Route::post('/detail-mo/ajaxRecieptDate','Sample\SampleAdminConttroler@ajaxRecieptDate')->name('reciept.ajaxRecieptDate');

		// delete reciept
		Route::get('/delete-reciept/','Sample\SampleAdminConttroler@deleteReciept')->name('reciept.deleteReciept');
		Route::get('/delete-reciept/get-data','Sample\SampleAdminConttroler@ajaxGetDelete')->name('reciept.ajaxGetDelete');
		Route::post('/delete-reciept/delete','Sample\SampleAdminConttroler@ajaxDeleteMo')->name('reciept.ajaxDeleteMo');

		
	});

	Route::prefix('/area')->middleware(['permission:menu-area-locator'])->group(function(){
		//user account
		Route::get('', 'Locator\AreaLocatorController@index')->name('area.index');
		Route::get('/get-data', 'Locator\AreaLocatorController@getDataArea')->name('area.getDataArea');
		Route::post('/add-area', 'Locator\AreaLocatorController@addArea')->name('area.addArea');
		Route::get('/form-edit-area', 'Locator\AreaLocatorController@formEditArea')->name('area.formEditArea');
		Route::post('/edit-area', 'Locator\AreaLocatorController@editArea')->name('area.editArea');

		Route::get('/locator', 'Locator\AreaLocatorController@locator')->name('area.locator');
		Route::get('/locator/get-data', 'Locator\AreaLocatorController@getDataLocator')->name('area.getDataLocator');
		Route::get('/locator/ajaxGetArea', 'Locator\AreaLocatorController@ajaxGetArea')->name('area.ajaxGetArea');
		Route::get('/locator/add-locator', 'Locator\AreaLocatorController@addLocator')->name('area.addLocator');
		Route::get('/locator/form-edit-locator', 'Locator\AreaLocatorController@formEditLocator')->name('area.formEditLocator');
		Route::post('/locator/edit-locator', 'Locator\AreaLocatorController@editLocator')->name('area.editLocator');
		Route::get('/locator/print-locator', 'Locator\AreaLocatorController@printLocator')->name('area.printLocator');
		
	});

	Route::prefix('/storage')->middleware(['permission:menu-md-storage'])->group(function(){
		Route::get('', 'Garment\StorageController@index')->name('storage.checkin');
		Route::post('/ajaxCheckinStorage', 'Garment\StorageController@ajaxCheckinStorage')->name('storage.ajaxCheckinStorage');

		Route::get('/storage', 'Garment\StorageController@storageGarment')->name('storage.storageGarment');
		Route::post('/storage/ajaxPlaceStorage', 'Garment\StorageController@ajaxPlaceStorage')->name('storage.ajaxPlaceStorage');
		Route::post('/storage/setlocator', 'Garment\StorageController@setLocator')->name('storage.setLocator');
		
		
	});

	Route::prefix('/borrowing')->middleware(['permission:menu-borrowing'])->group(function(){
		Route::get('/borrow', 'Borrowing\BorrowingController@borrow')->name('borrow.borrow');
		Route::post('/ajaxBorrow', 'Borrowing\BorrowingController@ajaxBorrow')->name('borrow.ajaxBorrow');
		Route::post('/ajaxGetNik', 'Borrowing\BorrowingController@getNik')->name('borrow.getNik');
				
		
	});

	Route::prefix('/pattern-marker')->middleware(['permission:menu-pattern-marker,menu-marker-style'])->group(function(){
		Route::get('', 'Marker\MarkerController@index')->name('marker.index');
		Route::post('/ajaxScan', 'Marker\MarkerController@ajaxScan')->name('marker.ajaxScan');	
		Route::get('/setlocation', 'Marker\MarkerController@getRack')->name('marker.getRack');	

		Route::get('/style', 'Marker\MarkerController@markerStyle')->name('markerstyle.index');
		Route::get('/style/ajaxGetData', 'Marker\MarkerController@ajaxGetData')->name('markerstyle.ajaxGetData');
		Route::post('/style/ajaxUploadStyle', 'Marker\MarkerController@ajaxUploadStyle')->name('markerstyle.ajaxUploadStyle');
		Route::get('/style/getTemplate', 'Marker\MarkerController@getTemplate')->name('markerstyle.getTemplate');
		Route::post('/style/editMarkerStyle', 'Marker\MarkerController@editMarkerStyle')->name('markerstyle.editMarkerStyle');	
		
	});

	Route::prefix('/laboratory')->middleware(['permission:menu-loc-lab'])->group(function(){
		// storage
		Route::get('', 'Lab\LabController@index')->name('lab.index');
		Route::get('/ajaxGetRack', 'Lab\LabController@getRack')->name('lab.getRack');
		Route::post('/ajaxScan', 'Lab\LabController@ajaxScan')->name('lab.ajaxScan');	

		// borrow
		Route::get('/borrow', 'Lab\LabController@borrow')->name('lab.borrow');
		Route::post('/borrow/ajaxBorrow', 'Lab\LabController@ajaxBorrow')->name('lab.ajaxBorrow');
		Route::post('/borrow/ajaxGetNik', 'Lab\LabController@getNik')->name('lab.getNik');
			
		
	});

	Route::prefix('/quality-control')->middleware(['permission:menu-loc-qc'])->group(function(){
		// storage
		Route::get('', 'Qc\QcController@index')->name('qc.index');
		Route::get('/ajaxGetRack', 'Qc\QcController@getRack')->name('qc.getRack');
		Route::post('/ajaxScan', 'Qc\QcController@ajaxScan')->name('qc.ajaxScan');	

		// borrow
		Route::get('/borrow', 'Qc\QcController@borrow')->name('qc.borrow');
		Route::post('/borrow/ajaxBorrow', 'Qc\QcController@ajaxBorrow')->name('qc.ajaxBorrow');
		Route::post('/borrow/ajaxGetNik', 'Qc\QcController@getNik')->name('qc.getNik');
		Route::get('/borrow/ajaxGetDept', 'Qc\QcController@ajaxGetDept')->name('qc.ajaxGetDept');
		Route::get('/borrow/ajaxGetLine', 'Qc\QcController@ajaxGetLine')->name('qc.ajaxGetLine');
	});

	Route::prefix('/packinglist')->middleware(['permission:menu-packinglist'])->group(function(){

		Route::get('', 'Packinglist\PackingController@index')->name('packlist.index');
		Route::get('/getDatInv', 'Packinglist\PackingController@getDatInv')->name('packlist.getDatInv');
		Route::post('/uploadPL', 'Packinglist\PackingController@uploadPL')->name('packlist.uploadPL');
		Route::get('/detail-packinglist', 'Packinglist\PackingController@detailPackinglist')->name('packlist.detailPackinglist');
		Route::get('/detail-packinglist/ajaxGetDetailPl', 'Packinglist\PackingController@ajaxGetDetailPl')->name('packlist.ajaxGetDetailPl');
		Route::get('/detail-packinglist/printShipmark', 'Packinglist\PackingController@printShipmark')->name('packlist.printShipmark');
		Route::get('/template-pl', 'Packinglist\PackingController@templatePl')->name('packlist.templatePl');

	
	});

	Route::prefix('/report')->middleware(['permission:menu-report'])->group(function(){

		// reciept
		Route::get('/reciept', 'Report\RecieptController@index')->name('report.reciept.index');
		Route::get('/reciept/get-data', 'Report\RecieptController@ajaxGetData')->name('report.reciept.ajaxGetData');
		Route::get('/reciept/export-data', 'Report\RecieptController@exportReciept')->name('report.reciept.exportReciept');
		// reciept

		// stockmd
		Route::get('/stock/md', 'Report\StockController@stockMD')->name('report.stock.stockMD');
		Route::get('/stock/md/get-data', 'Report\StockController@ajaxGetStockMD')->name('report.stock.ajaxGetStockMD');
		Route::get('/stock/md/export-data', 'Report\StockController@exportStockMD')->name('report.stock.exportStockMD');
	
		// stockmd

		// history borrowing
		Route::get('/history-borrowing', 'Report\StockController@borrowingHistory')->name('report.histoborrow.borrowingHistory');
		Route::get('/history-borrowing/get-data', 'Report\StockController@ajaxGetHistoryBorrowing')->name('report.histoborrow.ajaxGetHistoryBorrowing');
		Route::get('/history-borrowing/export-data', 'Report\StockController@exportHistoryBorrowing')->name('report.histoborrow.exportHistoryBorrowing');
	
		// history borrowing

		// borrowing
		Route::get('/history', 'Report\StockController@borrowing')->name('report.borrow.borrowing');
		Route::get('/history/get-data', 'Report\StockController@ajaxGetBorrowing')->name('report.borrow.ajaxGetBorrowing');
		Route::get('/history/export-data', 'Report\StockController@exportBorrowing')->name('report.borrow.exportBorrowing');
		// borrowing

		// stock pattern
		Route::get('/stock/pattern', 'Report\StockController@patternStock')->name('report.stock.patternStock');
		Route::get('/stock/pattern/get-data', 'Report\StockController@ajaxGetStockPattern')->name('report.stock.ajaxGetStockPattern');
		Route::get('/stock/pattern/export-data', 'Report\StockController@exportStockPattern')->name('report.stock.exportStockPattern');
		// stock pattern

		

		//stock global
		Route::get('/stock/all', 'Report\StockController@globalStock')->name('report.stock.globalStock');
		Route::get('/stock/all/get-data', 'Report\StockController@ajaxGetGlobalStock')->name('report.stock.ajaxGetGlobalStock');
		Route::get('/stock/all/export-data', 'Report\StockController@exportGlobalStock')->name('report.stock.exportGlobalStock');
		//stock global


		//receiving
		Route::get('/stock/receiving', 'Report\StockController@receiving')->name('report.stock.receiving');
		Route::get('/stock/receiving/ajaxGetReceiving', 'Report\StockController@ajaxGetReceiving')->name('report.stock.ajaxGetReceiving');
		Route::get('/stock/receiving/exportReceiving', 'Report\StockController@exportReceiving')->name('report.stock.exportReceiving');
		//receiving


		// stock qc
		Route::get('/stock/qc', 'Report\QcController@stockQc')->name('report.qc.stockQc');
		Route::get('/stock/qc/get-data', 'Report\QcController@ajaxGetStockQc')->name('report.qc.ajaxGetStockQc');
		Route::get('/stock/qc/export-data', 'Report\QcController@exportStockQc')->name('report.qc.exportStockQc');
		// stock qc

		// qc borrowing
		Route::get('/borrowing/qc', 'Report\QcController@borrowingQc')->name('report.qc.borrowingQc');
		Route::get('/borrowing/qc/get-data', 'Report\QcController@ajaxQcBorrowing')->name('report.qc.ajaxQcBorrowing');
		Route::get('/borrowing/qc/export-data', 'Report\QcController@exportQcBorrowing')->name('report.qc.exportQcBorrowing');
		// qc borrowing

		// handover
		Route::get('/handover', 'Report\HandoverController@handover')->name('report.handover.handover');
		Route::get('/handover/get-data', 'Report\HandoverController@ajaxGetData')->name('report.handover.ajaxGetData');
		Route::get('/handover/export-data', 'Report\HandoverController@exportData')->name('report.handover.exportData');
		// handover


		//shipping
		Route::get('/shipping', 'Report\ShipController@index')->name('report.shipp.index');
		Route::get('/shipping/getData', 'Report\ShipController@getData')->name('report.shipp.getData');
		Route::get('/shipping/exportShip', 'Report\ShipController@exportShip')->name('report.shipp.exportShip');
		//shipping


		//mutation
		Route::get('/mutation','Report\StockController@reportMutation')->name('report.mutation.index');
		Route::get('/mutation/get-data','Report\StockController@ajaxGetMutation')->name('report.mutation.getData');
		Route::get('/mutation/export-data','Report\StockController@exportMutation')->name('report.mutation.exportMutation');

		//mutation
	
	});

	Route::prefix('/master')->middleware(['permission:menu-master'])->group(function(){

		Route::get('', 'Sample\SampleAdminConttroler@syncDoc')->name('mastersync.syncDoc');
		Route::get('/get-data', 'Sample\SampleAdminConttroler@ajaxGetDocSync')->name('mastersync.ajaxGetDocSync');
		Route::post('/sync-data', 'Sample\SampleAdminConttroler@syncDocument')->name('mastersync.syncDocument');

	});

	Route::prefix('/handover')->middleware(['permission:menu-handover'])->group(function(){

		Route::get('/out', 'Handover\HandController@checkout')->name('handover.out');
		Route::post('/out/ajaxCheckOut', 'Handover\HandController@ajaxCheckOut')->name('handover.out.ajaxOut');
		
		Route::get('/in', 'Handover\HandController@checkin')->name('handover.in');
		Route::post('/in/ajaxCheckIn', 'Handover\HandController@ajaxCheckIn')->name('handover.out.ajaxIn');

	});

	Route::prefix('/bapb')->middleware(['permission:menu-bapb'])->group(function(){

		Route::get('/destory', 'Bapb\DestroyController@destroy')->name('bapb.destroy');
		Route::post('/destory/ajaxScan', 'Bapb\DestroyController@ajaxScanDestory')->name('bapb.ajaxScanDestory');
	});
	
	Route::prefix('/warehouse')->middleware(['permission:menu-scan-pack'])->group(function(){

		Route::get('/scan-pack', 'Warehouse\Packing@scanPack')->name('whs.scan.scanPack');
		Route::get('/scan-pack/ajaxGetCarton', 'Warehouse\Packing@ajaxGetCarton')->name('whs.scan.ajaxGetCarton');
		Route::post('/scan-pack/ajaxScan', 'Warehouse\Packing@ajaxScan')->name('whs.scan.ajaxScan');

		Route::get('/stuffing', 'Warehouse\Packing@stuffing')->name('whs.stuff.stuffing');
		Route::post('/stuffing/ajaxScanStuffing', 'Warehouse\Packing@ajaxScanStuffing')->name('whs.stuff.ajaxScanStuffing');
	
	});

	// Route::prefix('/trf')->middleware(['permission:menu-trf-testing'])->group(function(){

	// 		Route::get('', 'Trf\TrfController@index')->name('trf.index');
	// 		Route::get('/ajaxGetData', 'Trf\TrfController@ajaxGetData')->name('trf.ajaxGetData');
	// 		Route::get('/create', 'Trf\TrfController@formCreateTrf')->name('trf.createTrf');
	// 		Route::get('/ajaxGetCtgSpes', 'Trf\TrfController@ajaxGetCtgSpes')->name('trf.ajaxGetCtgSpes');
	// 		Route::get('/ajaxGetType', 'Trf\TrfController@ajaxGetType')->name('trf.ajaxGetType');
	// 		Route::get('/ajaxGetCategory', 'Trf\TrfController@ajaxGetCategory')->name('trf.ajaxGetCategory');
	// 		Route::get('/ajaxGetTestMethod', 'Trf\TrfController@ajaxGetTestMethod')->name('trf.ajaxGetTestMethod');
	// 		Route::post('/addTrf', 'Trf\TrfController@addTrf')->name('trf.addTrf');
	// 		Route::get('/print-barcode', 'Trf\TrfController@printBarcode')->name('trf.printBarcode');
	// 		Route::get('/print-detail', 'Trf\TrfController@printDetail')->name('trf.printDetail');


	// 		Route::get('/getMoErp', 'Trf\TrfController@getMoErp')->name('trf.getMoErp');
	// 		Route::get('/getErpItem', 'Trf\TrfController@getErpItem')->name('trf.getErpItem');
	// 		Route::get('/getColorErp', 'Trf\TrfController@getColorErp')->name('trf.getColorErp');
	
	// });

	Route::prefix('/trf')->middleware(['permission:menu-trf-testing'])->group(function(){

			Route::get('', 'Trf\TrfController@index')->name('trf.index');
			Route::get('/ajaxGetData', 'Trf\TrfController@ajaxGetData')->name('trf.ajaxGetData');


			Route::get('/create', 'Trf\TrfController@formCreateTrf')->name('trf.createTrf');
			Route::get('/ajaxGetCategory', 'Trf\TrfController@ajaxGetCategory')->name('trf.ajaxGetCategory');
			Route::get('/ajaxGetCtgSpes', 'Trf\TrfController@ajaxGetCtgSpes')->name('trf.ajaxGetCtgSpes');
			Route::get('/ajaxGetTypeSpc', 'Trf\TrfController@ajaxGetTypeSpc')->name('trf.ajaxGetTypeSpc');
			Route::get('/ajaxGetTestMethod', 'Trf\TrfController@ajaxGetTestMethod')->name('trf.ajaxGetTestMethod');
			Route::post('/addTrf', 'Trf\TrfController@addTrf')->name('trf.addTrf');
			
			Route::get('/print-barcode', 'Trf\TrfController@printBarcode')->name('trf.printBarcode');
			Route::get('/print-detail', 'Trf\TrfController@printDetail')->name('trf.printDetail');


			Route::get('/getDocErp', 'Trf\TrfController@getDocErp')->name('trf.getDocErp');

			Route::get('/report','Trf\TrfController@report')->name('trf.report');
	});

	Route::prefix('/sample-request')->middleware(['permission:menu-sample-request'])->group(function(){
			Route::get('/notifikasi', 'Sample\SampleReqController@getNotif')->name('samreq.getNotif');

			Route::get('/sample-request', 'Sample\SampleReqController@sreq')->name('samreq.sreq');
			Route::get('/sample-csi', 'Sample\SampleReqController@csi')->name('samreq.csi');
			Route::get('/ajaxGetData', 'Sample\SampleReqController@ajaxGetData')->name('samreq.ajaxGetData');
			Route::get('/downloadSr', 'Sample\SampleReqController@downloadSr')->name('samreq.printsr');
			Route::get('/downloadCsi', 'Sample\SampleReqController@downloadCsi')->name('samreq.printcsi');


			Route::get('/report-request', 'Sample\SampleReqController@reportRequest')->name('samreq.reportRequest');
			Route::get('/report-request/get-data', 'Sample\SampleReqController@getDataReq')->name('samreq.getDataReq');
			Route::get('/report-request/export', 'Sample\SampleReqController@exportReq')->name('samreq.exportReq');
			Route::get('/report-request/get-data-req', 'Sample\SampleReqController@getSamReq')->name('samreq.getSamReq');
			Route::post('/report-request/update-data', 'Sample\SampleReqController@updateSampleReq')->name('samreq.updateSampleReq');

	});

	Route::prefix('/shipping')->middleware(['permission:menu-shipping'])->group(function(){

			Route::get('', 'Shipping\ScanshipController@scanShip')->name('scanship.scanShip');
			Route::get('/ajaxGetNik', 'Shipping\ScanshipController@ajaxGetNik')->name('scanship.ajaxGetNik');
			Route::get('/ajaxScan', 'Shipping\ScanshipController@ajaxScan')->name('scanship.ajaxScan');
	
	});

});

//display
Route::prefix('/display')->group(function(){
		Route::get('/borrower/md/{factory_id}', 'Display\DisplayController@indexMd')->name('display.md.index');
		Route::get('/borrower/qc/{factory_id}', 'Display\DisplayController@indexqc')->name('display.qc.index');
		// Route::get('/borrower/get-data', 'Display\DisplayController@getData')->name('display.getData');
});
//display


//cron
Route::prefix('/cron')->group(function(){
		Route::get('/erp/mo/{id}', 'Master\MasterDataController@getMoERP')->name('masterErp.getMo');
		Route::get('/erp/invoice', 'Master\MasterDataController@getInvERP')->name('masterErp.getInv');
		Route::get('/absensi/emp', 'Master\MasterDataController@employee')->name('masterAbs.employee');
		// Route::get('/erp/samplereq', 'Master\MasterDataController@getSampleRequest')->name('masterErp.getSampleRequest');

		// Route::get('/test', 'Master\MasterDataController@getNotif')->name('test.getNotif');
		
});
//cron
