<tr>
	<td>{{ $data['barcode_id'] }}</td>
	<td>{{ $data['docno'] }}</td>
	<td>{{ $data['season'] }}</td>
	<td>{{ $data['style'] }}</td>
	<td>{{ $data['article'] }}</td>
	<td>{{ $data['size'] }}</td>
	<td>{{ $data['status'] }}</td>
	<td>{{ $data['returndate'] }}</td>
	<td>
		@if($data['type']=="out")
			<span class="label label-primary">Check out to {{$data['loct'] }}</span>
		@elseif($data['type']=="in")
			<span class="label label-success">RETURN TO QC</span>
		@endif
	</td>
</tr>