<tr>
	<td>{{ $data['barcode_id'] }}</td>
	<td>{{ $data['docno'] }}</td>
	<td>{{ $data['season'] }}</td>
	<td>{{ $data['style'] }}</td>
	<td>{{ $data['article'] }}</td>
	<td>{{ $data['size'] }}</td>
	<td>{{ $data['status'] }}</td>
	<td>
		@if($data['type']=="storage")
			<span class="label label-primary">{{$data['rack'] }}</span>
		@elseif($data['type']=="reverse")
			<span class="label label-success">REVERSE TO QC</span>
		@endif
	</td>
</tr>