@extends('layouts.app', ['active' => 'pattern_maker'])
@section('header')
<div class="page-header page-header-default">
	<div class="breadcrumb-line">
		<ul class="breadcrumb">
			<li><a href="#"><i class="icon-home2 position-left"></i> Pattern Marker</a></li>
		</ul>
	</div>
</div>
@endsection

@section('content')
<div class="content">

	<div class="row">
		<div class=" panel panel-flat">
			<div class="panel-body">
				<div class="row">
					<div class="col-lg-4">
						<button class="btn btn-default" id="btn-upload"><span class="icon-file-upload2"></span> Upload</button>
						<!-- <button class="btn bg-teal-400" id="btn-download"><span class="icon-file-download2"></span> Download</button> -->
						<a href="{{ route('markerstyle.getTemplate') }}" type="button" class="btn bg-teal-400" id="btn-download"><span class="icon-file-download2"></span> Download</a>
					</div>

				</div>
				<div class="row table-responsive">
					<table class ="table table-basic table-condensed" id="table-list">
						<thead>
							<tr>
								<th>#</th>
								<th>Season</th>
								<th>Style</th>
								<th>Pattern Release</th>
								<th>PODD</th>
								<th>Expired</th>
								<th>PIC</th>
								<th>Action</th>
							</tr>
						</thead>
						<tbody></tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>

@endsection

@section('modal')
<div id="modal_upload" class="modal fade">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h5 class="modal-title">Upload Style</h5>
			</div>

			<div class="modal-body">
				<form action="{{ route('markerstyle.ajaxUploadStyle') }}" id="form-upload" enctype="multipart/form-data" method="POST">
					@csrf
					<div class="row">
						<input type="file" name="file_upload" id="file_upload" class="file-styled">
					</div>
					<div class="row">
						<button type="submit" class="btn btn-primary" id="btn-save">Save</button>

					</div>
				</form>
			</div>
		</div>
	</div>
</div>



<div id="modal_edit" class="modal fade">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h5 class="modal-title">Edit Style</h5>
			</div>

			<div class="modal-body">
				<form action="{{ route('markerstyle.editMarkerStyle') }}" id="form-edit" method="POST">
					@csrf

					<div class="row">
						<div class="col-lg-12">
							<label><b>Season</b></label>
							<input type="text" name="season" class="form-control season" id="season" required>
							<input type="hidden" name="ids" id="ids" class="ids">
						</div>
					</div>
					<div class="row">
						<div class="col-lg-12">
							<label><b>Style</b></label>
							<input type="text" name="style" class="form-control style" id="style" required>
						</div>
					</div>
					<div class="row">
						<div class="col-lg-12">
							<label><b>Marker Release</b></label>
							<input type="text" name="marker" id="marker" class="form-control pickadate marker">
						</div>
					</div>
					<div class="row">
						<div class="col-lg-12">
							<label><b>Marker Release</b></label>
							<input type="text" name="podd" id="podd" class="form-control pickadate podd">
						</div>
					</div>
					<div class="row">
						<div class="col-lg-12">
							<label><b>PIC</b></label>
							<input type="text" name="pic" class="form-control pic" id="pic" required>
						</div>
					</div>
					<div class="row">
						<div class="col-lg-12">
							<button type="submit" class="btn btn-primary" id="btn-save">Save</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
@endsection


@section('js')
<script type="text/javascript">
$(document).ready(function(){

	var table = $('#table-list').DataTable({
		processing:true,
		serverSide:true,
		deferRender:true,
		autoWidth: true,
		dom:'<"datatable-header"fBl><t><"datatable-footer"ip>',
		language: {
	        search: '<span>Filter:</span> _INPUT_',
	        searchPlaceholder: 'Type to filter...',
	        lengthMenu: '<span>Show:</span> _MENU_',
	        paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
	    },
        ajax: {
	        type: 'GET',
	        url: "{{ route('markerstyle.ajaxGetData') }}",
	    },
        fnCreatedRow: function (row, data, index) {
            var info = table.page.info();
            var value = index+1+info.start;
            $('td', row).eq(0).html(value);
        },
        columnDefs: [
            {
                className: 'dt-center'
            }
        ],
        columns: [
	        {data: null, sortable: false, orderable: false, searchable: false},
	        {data: 'season', name: 'season'},
	        {data: 'style', name: 'style'},
	        {data: 'release', name: 'release'},
	        {data: 'podd', name: 'podd'},
	        {data: 'expired', name: 'expired'},
	        {data: 'pic', name: 'pic'},
	        {data: 'action', name: 'action'}
	    ]
    });


    $('#btn-upload').click(function(){
    	$('#modal_upload').modal('show');
    });


    $('#form-upload').submit(function(event){
		event.preventDefault();
		// var FormData =  new FormData(this);

		$.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: 'post',
            url : $('#form-upload').attr('action'),
            contentType: false,
            processData: false,
            data :new FormData(this) ,
            beforeSend: function() {
                loading();
            },
            success: function(response) {
                $.unblockUI();
                $('#modal_upload').modal('hide');
              	var notif = response.data;
	            alert(notif.status,notif.output);

                window.location.reload();

            },
            error: function(response) {

                $.unblockUI();
	           var notif = response.data;
	            alert(notif.status,notif.output);
            }
        });
	});



	$('#table-list').on('click','.editmarker',function(){
		var id = $(this).data('id');
		var season = $(this).data('season');
		var style = $(this).data('style');
		var release = $(this).data('release');
		var podd = $(this).data('podd');
		var pic = $(this).data('pic');


		// console.log(id,season,style,release,podd,pic,remark_date);
		$('#ids').val(id);
		$('#season').val(season);
		$('#style').val(style);
		// $('#marker').val(release);
		mkrset = $('#marker').pickadate('picker');
		mkrset.set('select', release, { format: 'yyyy-mm-dd' });
		// $('#podd').val(podd);
		podset = $('#podd').pickadate('picker');
		podset.set('select', podd, { format: 'yyyy-mm-dd' });
		$('#pic').val(pic);

		$('#modal_edit').modal('show');
	});


	$('#form-edit').submit(function(event){
		event.preventDefault();
		// var FormData =  new FormData(this);

		$.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: 'post',
            url : $('#form-edit').attr('action'),
            data :{
            	id:$('#ids').val(),
            	season:$('#season').val(),
            	style:$('#style').val(),
            	release:$('#marker').val(),
            	podd:$('#podd').val(),
            	pic:$('#pic').val()
            } ,
            beforeSend: function() {
                loading();
            },
            success: function(response) {
                $.unblockUI();
                $('#modal_edit').modal('hide');
              	var notif = response.data;
	            alert(notif.status,notif.output);

                window.location.reload();

            },
            error: function(response) {

                $.unblockUI();
	           var notif = response.data;
	            alert(notif.status,notif.output);
            }
        });
	});

});


</script>
@endsection
