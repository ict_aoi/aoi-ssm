<tr>
	<td>{{ $data['barcode_id'] }}</td>
	<td>{{ $data['docno'] }}</td>
	<td>{{ $data['season'] }}</td>
	<td>{{ $data['style'] }}</td>
	<td>{{ $data['article'] }}</td>
	<td>{{ $data['size'] }}</td>
	<td>{{ $data['status'] }}</td>
	<td>
		@if($data['sugest']!="")
			<span class="label label-success">{{ $data['sugest'] }}</span>
		@endif
	</td>
	<td>
		@if($data['loc']=="checkin")
			<span class="label label-primary">ON PROGRESS </span> 
		@else
			<span class="label label-success">COMPLETED</span>
		@endif
	</td>
</tr>