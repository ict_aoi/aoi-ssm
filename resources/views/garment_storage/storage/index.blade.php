 @extends('layouts.app', ['active' => 'storage_garment'])
@section('header')
<div class="page-header page-header-default">
	<div class="breadcrumb-line">
		<ul class="breadcrumb">
			<li><a href="#"><i class="icon-home2 position-left"></i> Germent Storage</a></li>
			<li class="active">Storage</li>
		</ul>
	</div>
</div>
@endsection

@section('content')
<div class="content">
	<div class="row">
		<div class=" panel panel-flat">
			<div class="panel-body">
				<div class="row">
					<div class="col-lg-10">
						<form action="{{ route('storage.ajaxPlaceStorage') }}" id="form-storage">
							@csrf
							<div class="col-lg-10">
								<label><b>Scan Barcode</b></label>
								<input type="text" name="barcode_id" id="barcode_id" class="form-control input-new" placeholder="#scan in here" required="">
							</div>
							<div class="col-lg-2">
								<div class="locar">
									<label><b>Locator</b></label>
									<input type="text" name="loc" id="loc" class="form-control" disabled="" style="text-align: center; font-size: 20px; color: black;">
									<input type="text" name="idloc" id="idloc" class="form-control hidden" >
								</div>
								
							</div>
						</form>
					</div>
					<div class="col-lg-2">

						<input type="text" name="_count" id="_count" class="form-control" value="0" readonly="" style="text-align: center; font-size: 28px; margin-top: 27px;">
					</div>
				</div>
				<div class="row form-group">
					<div class="col-lg-12">
						<label class="radio-inline"><input type="radio" name="radio_status" checked="checked" value="storage"><b> PLANCEMENT RACK</b></label>
	        			<label class="radio-inline"><input type="radio" name="radio_status" value="cancel"><b> CANCEL</b></label>
					</div>
				</div>
				
			</div>
		</div>
	</div>
	<div class="row">
		<div class=" panel panel-flat">
			<div class="panel-body">
				<div class="table-responsive">
					<table class ="table table-basic table-condensed" id="table-list">
						<thead>
							<tr>
								<th>Barcode ID</th>
								<th>Doc. No.</th>
								<th>Season</th>
								<th>Style</th>
								<th>Article</th>
								<th>Size</th>
								<th>Status</th>
								<th></th>
							</tr>
						</thead>
						<tbody></tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>

@endsection



@section('js')
<script type="text/javascript">
$(document).ready(function(){
	$('#barcode_id').focus();

	$('input[type=radio][name=radio_status]').change(function(){
		if (this.value=="storage") {
			if ($('.locar').hasClass('hidden')) {
				$('.locar').removeClass('hidden');
			}
		}else{
			if ($('.locar').hasClass('hidden')) {
				$('.locar').removeClass('hidden');
				$('.locar').addClass('hidden');
			}else{
				$('.locar').addClass('hidden')
			}
		}
	});

	$('.input-new').keypress(function(event){
		if (event.which==13) {
			event.preventDefault();

			var barcode_id = $('#barcode_id').val();
			var idloc = $('#idloc').val();
			var status = $('input[name=radio_status]:checked').val();
			var bcd = barcode_id.substr(0,1);

			if (status=="storage") {
				if ($('#loc').val()=="") {
				
					if (bcd=="R") {
						setLoc(barcode_id);
					}else{
						alert(422,"Scan Barcode Locator First ! ! !");
						$('#barcode_id').focus();
				        $('#barcode_id').val('');
				        $.unblockUI();
						return false;
					}
				}else if ($('#loc').val()!="" && bcd=="R") {
					setLoc(barcode_id);
				}else{
					scanBarcode(barcode_id,idloc,status);
				}
			}else if (status=="cancel") {
				scanBarcode(barcode_id,idloc,status);
			}	
		}
	});

	
});

function setLoc(barcode_id){
	$.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
        type: 'post',
        url : "{{ route('storage.setLocator') }}",
        data:{barcode_id:barcode_id},
        beforeSend : function(){
        	loading();
        	$('#barcode_id').focus();
        	$('#barcode_id').val('');
        },
        success: function(response) {
        	$.unblockUI();
           	$('#idloc').val(response.id);
           	$('#loc').val(response.name);

        	
        },
        error: function(response) {
        	$.unblockUI();
           	alert(response.status,response.responseText);
            
        }
    });
}

function scanBarcode(barcode_id,idloc,status){
	$.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
        type: 'post',
        url : $('#form-storage').attr('action'),
        data:{barcode_id:barcode_id,idloc:idloc,status:status},
        beforeSend : function(){
        	loading();
        	$('#barcode_id').focus();
        	$('#barcode_id').val('');
        },
        success: function(response) {
        	$.unblockUI();
        	$('#table-list > tbody').prepend(response);
        	var counts = +$('#_count').val()+1;

        	$('#_count').val(counts);
        	
        },
        error: function(response) {
        	$.unblockUI();
           	alert(response.status,response.responseText);
            
        }
    });
}
</script>
@endsection