<tr>
	<td>{{ $data['barcode_id'] }}</td>
	<td>{{ $data['docno'] }}</td>
	<td>{{ $data['season'] }}</td>
	<td>{{ $data['style'] }}</td>
	<td>{{ $data['article'] }}</td>
	<td>{{ $data['size'] }}</td>
	<td>{{ $data['status'] }}</td>
	<td>
		@if($data['act_status']=="storage")
			<span class="label label-success">{{ $data['loc'] }}</span>
			
		@else
			<span class="label label-warning">CANCEL</span>
		@endif
	</td>
</tr>