<ul class="navigation navigation-main navigation-accordion">
	
	<!-- Main -->
	@permission(['menu-dashboard','menu-md-storage','menu-loc-qc'])
	<li class="{{ $active == 'dashboard' ? 'active' : ''}}"><a href="{{ route('home.index') }}"><i class="icon-home4"></i> <span>Dashboard</span></a></li>
	@permission(['menu-md-storage'])
	<li class="#"><a href="{{ route('display.md.index',['factory_id'=> Auth::user()->factory_id]) }}" target="_blank"><i class="icon-library2"></i> <span>Borrwing Info MD</span></a></li>
	@endpermission
	@permission(['menu-loc-qc'])
	<li class="#"><a href="{{ route('display.qc.index',['factory_id'=> Auth::user()->factory_id]) }}" target="_blank"><i class="icon-office"></i> <span>Borrwing Info QC/QA</span></a></li>
	@endpermission
	@endpermission

	@permission(['menu-user-management'])
	<li class="navigation-header"><span>User Management</span> <i class="icon-menu"></i></li>
	<li class="{{ $active == 'user_account' ? 'active' : ''}}"><a href="{{ route('admin.user_account')}}"><i class="icon-theater"></i> <span>User Account</span></a></li>
	<li class="{{ $active == 'role_user' ? 'active' : ''}}"><a href="{{route('admin.formRole')}}"><i class="icon-menu2"></i> <span>Role</span></a></li>
	@endpermission

	@permission(['menu-master'])
	<li class="navigation-header"><span>Menu Master</span> <i class="icon-menu"></i></li>
	<li class="{{ $active == 'master_mo' ? 'active' : ''}}"><a href="{{ route('mastersync.syncDoc') }}"><i class="icon-android"></i> <span>Sync MO</span></a></li>
	@endpermission

	@permission(['menu-sample-reciept'])
	<li class="navigation-header"><span>Sample Reciept</span> <i class="icon-menu"></i></li>
	<li class="{{ $active == 'sample_reciept' ? 'active' : ''}}"><a href="{{ route('reciept.index') }}"><i class="icon-cart-add2"></i> <span>Sample Reciept</span></a></li>
	<li class="{{ $active == 'delete_reciept' ? 'active' : ''}}"><a href="{{ route('reciept.deleteReciept') }}"><i class="icon-trash"></i> <span>Delete Reciept</span></a></li>
	@endpermission

	@permission(['menu-sample-request'])
	<li class="navigation-header"><span>Sample Request</span> <i class="icon-menu"></i></li>
	<li class="{{ $active == 'sample_sreq' ? 'active' : ''}}"><a href="{{ route('samreq.sreq') }}"><i class="icon-design"></i> <span>Sample Request</span></a></li>
	<li class="{{ $active == 'sample_csi' ? 'active' : ''}}"><a href="{{ route('samreq.csi') }}"><i class="icon-scissors"></i> <span>CSI</span></a></li>
	<li class="{{ $active == 'report_request' ? 'active' : ''}}"><a href="{{ route('samreq.reportRequest') }}"><i class="icon-server"></i> <span>Report Request</span></a></li>
	@endpermission

	@permission(['menu-area-locator'])
	<li class="navigation-header"><span>Area & Locator</span> <i class="icon-menu"></i></li>
	@permission(['menu-set-area'])
	<li class="{{ $active == 'area' ? 'active' : ''}}"><a href="{{ route('area.index')}}"><i class="icon-location4"></i> <span>Area</span></a></li>
	@endpermission
	<li class="{{ $active == 'locator' ? 'active' : ''}}"><a href="{{ route('area.locator') }}"><i class="icon-server"></i> <span>Locator</span></a></li>
	@endpermission


	@permission(['menu-md-storage'])
	<li class="navigation-header"><span>Garment Storage</span> <i class="icon-menu"></i></li>
	<li class="{{ $active == 'checkin_garment' ? 'active' : ''}}"><a href="{{ route('storage.checkin') }}"><i class="icon-folder-download"></i> <span>Checkin</span></a></li>
	<li class="{{ $active == 'storage_garment' ? 'active' : ''}}"><a href="{{ route('storage.storageGarment') }}"><i class="icon-cabinet"></i> <span>Storage</span></a></li>
	@endpermission

	


	@permission(['menu-borrowing'])
	<li class="navigation-header"><span>Borrow</span> <i class="icon-menu"></i></li>
	<li class="{{ $active == 'borrow' ? 'active' : ''}}"><a href="{{route('borrow.borrow')}}"><i class="icon-exit"></i> <span>Borrowing</span></a></li>
	@endpermission

	@permission(['menu-pattern-marker'])
	<li class="navigation-header"><span>Pattern Maker</span> <i class="icon-menu"></i></li>
	<li class="{{ $active == 'pattern_maker' ? 'active' : ''}}"><a href="{{ route('marker.index') }}"><i class="icon-puzzle3"></i> <span>Pattern Maker Storage</span></a></li>
	@permission(['menu-marker-style'])
	<li class="{{ $active == 'marker_style' ? 'active' : ''}}"><a href="{{ route('markerstyle.index') }}"><i class="icon-dots"></i> <span>Marker Info Style</span></a></li>
	@endpermission
	@endpermission

	@permission(['menu-loc-lab'])
	<li class="navigation-header"><span>Laboratory</span> <i class="icon-menu"></i></li>
	<li class="{{ $active == 'lab_area' ? 'active' : ''}}"><a href="{{ route('lab.index') }}"><i class="icon-droplet"></i> <span>Laboratory Storage</span></a></li>
	<li class="{{ $active == 'lab_borrow' ? 'active' : ''}}"><a href="{{ route('lab.borrow') }}"><i class="icon-store2"></i> <span>Lab Borrowing</span></a></li>
	@endpermission

	@permission(['menu-loc-qc'])
	<li class="navigation-header"><span>QA</span> <i class="icon-menu"></i></li>
	<li class="{{ $active == 'qc_area' ? 'active' : ''}}"><a href="{{ route('qc.index') }}"><i class="icon-shield-check"></i> <span>QC Storage</span></a></li>
	<li class="{{ $active == 'qc_borrow' ? 'active' : ''}}"><a href="{{ route('qc.borrow') }}"><i class="icon-store"></i> <span>QC Borrowing</span></a></li>
	@endpermission

	@permission(['menu-handover'])
	<li class="navigation-header"><span>Handover</span> <i class="icon-menu"></i></li>
	<li class="{{ $active == 'handoverout' ? 'active' : ''}}"><a href="{{ route('handover.out') }}"><i class="icon-circle-up2"></i> <span>Handover Out</span></a></li>
	<li class="{{ $active == 'handoverin' ? 'active' : ''}}"><a href="{{ route('handover.in') }}"><i class="icon-circle-down2"></i> <span>Handover In</span></a></li>
	@endpermission

	@permission(['menu-bapb'])
	<li class="navigation-header"><span>BAPB</span> <i class="icon-menu"></i></li>
	<li class="{{ $active == 'destroybapb' ? 'active' : ''}}"><a href="{{ route('bapb.destroy') }}"><i class="icon-fire"></i> <span>Destroy BAPB</span></a></li>
	@endpermission
	
	@permission(['menu-packinglist'])
	<li class="navigation-header"><span>Packinglist</span> <i class="icon-menu"></i></li>
	<li class="{{ $active == 'packinglist' ? 'active' : ''}}"><a href="{{ route('packlist.index') }}"><i class="icon-dropbox"></i> <span>Packinglist</span></a></li>
	@endpermission

	
	<li class="navigation-header"><span>Report</span> <i class="icon-menu"></i></li>
	
	@permission(['menu-md-storage','menu-report'])
	<li class="{{ $active == 'report_reciept' ? 'active' : ''}}"><a href="{{ route('report.reciept.index') }}"><i class="icon-drawer-in"></i> <span>Report Reciept</span></a></li>
	<li class="{{ $active == 'report_receiving' ? 'active' : ''}}"><a href="{{ route('report.stock.receiving') }}"><i class="icon-file-check2"></i> <span>Report Receiving</span></a></li>
	<li class="{{ $active == 'report_stockmd' ? 'active' : ''}}"><a href="{{ route('report.stock.stockMD') }}"><i class="icon-library2"></i> <span>Report Stock MD</span></a></li>
	<li class="{{ $active == 'report_borrowing' ? 'active' : ''}}"><a href="{{ route('report.borrow.borrowing') }}"><i class="icon-cart"></i> <span>Report Borrowing</span></a></li>
	<li class="{{ $active == 'report_histoborrowing' ? 'active' : ''}}"><a href="{{ route('report.histoborrow.borrowingHistory') }}"><i class="icon-stack-up"></i> <span>Report History Borrowing</span></a></li>
	<li class="{{ $active == 'report_activegar' ? 'active' : ''}}"><a href="{{ route('report.stock.globalStock') }}"><i class="icon-book"></i> <span>Report Global Stock</span></a></li>
	<li class="{{ $active == 'report_shipping' ? 'active' : ''}}"><a href="{{route('report.shipp.index')}}"><i class="icon-ship"></i> <span>Report Shipping</span></a></li>
	@endpermission

	@permission(['menu-pattern-marker','menu-report'])
	<li class="{{ $active == 'report_stockpattern' ? 'active' : ''}}"><a href="{{ route('report.stock.patternStock') }}"><i class="icon-google-drive"></i> <span>Report Pattern Stock</span></a></li>
	@endpermission

	@permission(['menu-loc-qc','menu-report'])
	<li class="{{ $active == 'report_qc' ? 'active' : ''}}"><a href="{{ route('report.qc.stockQc') }}"><i class="icon-trophy3"></i> <span>Report QC/QA</span></a></li>
	<li class="{{ $active == 'report_qc_borrowing' ? 'active' : ''}}"><a href="{{ route('report.qc.borrowingQc') }}"><i class="icon-file-text3"></i> <span>Report QC/QA Borrowing</span></a></li>
	@endpermission

	@permission(['menu-handover','menu-report'])
	<li class="{{ $active == 'report_handover' ? 'active' : ''}}"><a href="{{ route('report.handover.handover') }}"><i class="icon-transmission"></i> <span>Report History Handover</span></a></li>
	@endpermission

	@permission(['menu-report-mutation','menu-report'])
	<li class="{{ $active == 'report_mutation' ? 'active' : ''}}"><a href="{{ route('report.mutation.index')}}"><i class="icon-stackoverflow"></i> <span>Report Mutation</span></a></li>
	@endpermission
	

	@permission(['menu-scan-pack'])
	<li class="navigation-header"><span>SCAN PACK</span> <i class="icon-menu"></i></li>
	<li class="{{ $active == 'scanpack' ? 'active' : ''}}"><a href="{{ route('whs.scan.scanPack')}}"><i class="icon-dropbox"></i> <span>Scan Pack</span></a></li>
	<li class="{{ $active == 'scanstuffing' ? 'active' : ''}}"><a href="{{ route('whs.stuff.stuffing')}}"><i class="icon-truck"></i> <span>Stuffing</span></a></li>
	@endpermission

	@permission(['menu-shipping'])
	<li class="navigation-header"><span>SHIPPING</span> <i class="icon-menu"></i></li>
	<li class="{{ $active == 'scanship' ? 'active' : ''}}"><a href="{{ route('scanship.scanShip')}}"><i class="icon-truck"></i> <span>Shipping</span></a></li>
	@endpermission


	@permission(['menu-trf-testing'])
	<li class="navigation-header"><span>LAB TRF TESTING</span> <i class="icon-menu"></i></li>
	<li class="{{ $active == 'trf' ? 'active' : ''}}"><a href="{{ route('trf.index') }}"><i class="fa fa-hourglass-half"></i> <span>TRF Testing</span></a></li>
	@endpermission



	<!-- /page kits -->

</ul>