@permission(['menu-scan-location'])
	<li class="navigation-header"><span>Dept. Location</span> <i class="icon-menu"></i></li>

	@permission(['menu-loc-qc'])
	<li class="{{ $active == 'qc_area' ? 'active' : ''}}"><a href="{{ route('qc.index') }}"><i class="icon-shield-check"></i> <span>QC</span></a></li>
	@endpermission

	@permission(['menu-loc-lab'])
	<li class="{{ $active == 'lab_area' ? 'active' : ''}}"><a href="{{ route('lab.index') }}"><i class="icon-droplet"></i> <span>Laboratory</span></a></li>
	@endpermission

	@permission(['menu-packing-sample'])
	<li class="{{ $active == 'packsample' ? 'active' : ''}}"><a href="{{route('packsample.index')}}"><i class="icon-home7"></i> <span>Packing Sample</span></a></li>
	@endpermission

	@permission(['menu-ie-dev'])
	<li class="{{ $active == 'ie' ? 'active' : ''}}"><a href="{{ route('ie.index') }}"><i class="icon-git-branch"></i> <span>IE</span></a></li>
	@endpermission

	@permission(['menu-pattern-marker'])
	<li class="{{ $active == 'pattern_maker' ? 'active' : ''}}"><a href="{{ route('marker.index') }}"><i class="icon-puzzle3"></i> <span>Pattern Maker</span></a></li>
	@endpermission

	@permission(['menu-line-sewing'])
	<li class="{{ $active == 'sewing' ? 'active' : ''}}"><a href="{{ route('sewing.index') }}"><i class="icon-arrow-down8"></i> <span>Sewing</span></a></li>
	@endpermission

	<li class="{{ $active == 'cutting' ? 'active' : ''}}"><a href="#"><i class="icon-map4"></i> <span>Cutting</span></a></li>

	<li class="{{ $active == 'ppa' ? 'active' : ''}}"><a href="#"><i class="icon-pushpin"></i> <span>Secondary Proccess</span></a></li>


	@endpermission