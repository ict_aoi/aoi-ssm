<ul class="icons-list">
	<li class="dropdown">
		<a href="#" class="dropdown-toggle" data-toggle="dropdown">
			<i class="icon-menu9"></i>
		</a>

		<ul class="dropdown-menu dropdown-menu-right">
			
			@if(isset($edituser))
			<li><a href="{!! $edituser !!}"><i class="icon-paragraph-justify2"></i> Edit User</a></li>
			@endif

			@if(isset($innactive))
			<li><a href="{!! $innactive !!}"><i class="icon-x"  class="ignore-click innactive"></i> Innactive User</a></li>
			@endif

			@if(isset($editrole))
			<li><a href="{!! $editrole !!}"><i class="icon-paragraph-justify2"  class="ignore-click"></i> Edit Role</a></li>
			@endif

			@if(isset($reciept))
			<li><a href="#" class="ignore-click reciept" data-id="{!! $reciept !!}"><i class="icon-plus-circle2"></i> Reciept</a></li>
			@endif

			@if(isset($detailMo))
			<li><a href="{!! $detailMo !!}" target="_blank"><i class="icon-list"></i> Detail</a></li>
			@endif

			@if(isset($editArea))
			<li><a href="{!! $editArea !!}"><i class="icon-paragraph-justify2"></i> Edit Area</a></li>
			@endif

			@if(isset($editLocator))
			<li><a href="{!! $editLocator !!}"><i class="icon-paragraph-justify2"></i> Edit Locator</a></li>
			@endif

			@if(isset($ctndet))
			<li><a href="{!! $ctndet !!}" target="_blank"><i class="icon-search4"></i> Detail</a></li>
			@endif

			@if(isset($uppl))
			<li><a href="#" class="ignore-click uppl" data-id="{!! $uppl['id'] !!}" data-inv="{!! $uppl['inv'] !!}"><i class="icon-file-upload2"></i> Upload</a></li>
			@endif

			@if(isset($detpl))
			<li><a href="{!! $detpl !!}" target="_blank" ><i class="icon-paragraph-justify3"></i> Detail</a></li>
			@endif


			@if(isset($trfBarcode))
			<li><a href="{!! $trfBarcode !!}" target="_blank" ><i class="icon-printer"></i> Print Barcode</a></li>
			@endif

			@if(isset($trfDetail))
			<li><a href="{!! $trfDetail !!}" target="_blank" ><i class="icon-printer2"></i> Print Detail</a></li>
			@endif

			@if(isset($trfReport))
			<li><a href="{!! $trfReport !!}" target="_blank" ><i class="icon-list"></i> Report</a></li>
			@endif

			@if(isset($downsr))
			<li><a  class="ignore-click downsr" data-id="{!! $downsr !!}"  ><i class="icon-user-check"></i> Print SR</a></li>
			@endif

			@if(isset($downcsi))
			<li><a class="ignore-click downcsi" data-id="{!! $downcsi !!}"  ><i class="icon-scissors"></i> Print CSI</a></li>
			@endif
			
			@if(isset($editmarker))
			<li><a class="ignore-click editmarker" data-id="{!! $editmarker['id'] !!}" data-season="{!! $editmarker['season'] !!}" data-style="{!! $editmarker['style'] !!}" data-release="{!! $editmarker['release'] !!}" data-podd="{!! $editmarker['podd'] !!}" data-pic="{!! $editmarker['pic'] !!}"  ><i class="icon-pencil3"></i> Edit</a></li>
			@endif

		</ul>
	</li>
</ul>