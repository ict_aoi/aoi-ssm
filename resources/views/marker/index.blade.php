@extends('layouts.app', ['active' => 'pattern_maker'])
@section('header')
<div class="page-header page-header-default">
	<div class="breadcrumb-line">
		<ul class="breadcrumb">
			<li><a href="#"><i class="icon-home2 position-left"></i> Pattern Marker</a></li>
		</ul>
	</div>
</div>
@endsection

@section('content')
<div class="content">
	<div class="row">
		<div class=" panel panel-flat">
			<div class="panel-body">
				<form action="{{ route('marker.ajaxScan') }}" id="form-scan">
					@csrf
					<div class="row form-group">
						<div class="col-lg-8">
							<label><b>Scan Barcode</b></label>
							<input type="text" name="barcode_id" id="barcode_id" class="form-control input-new" placeholder="#scan in here" required="">
						</div>
						<div class="col-lg-2">
							<input type="text" name="_count" id="_count" class="form-control" value="0" readonly="" style="text-align: center; font-size: 28px; margin-top: 30px;">
						</div>
						<div class="col-lg-2">
							<div class="locar">
								<label><b>Locator</b></label>
								<input type="text" name="rack" id="rack" class="form-control" readonly="" style="text-align: center; font-size: 20px; color: black;">
								<input type="text" name="locat" id="locat" class="form-control hidden" >
							</div>
							
						</div>
					</div>
					<div class="row form-group">
						<label class="radio-inline"><input type="radio" name="radio_status" checked="checked" value="storage"><b> STORAGE</b></label>
	        			<label class="radio-inline"><input type="radio" name="radio_status" value="reverse"><b> REVERSE</b></label>
					</div>
				</form>
			</div>
		</div>
	</div>
	<div class="row">
		<div class=" panel panel-flat">
			<div class="panel-body">
				<div class="table-responsive">
					<table class ="table table-basic table-condensed" id="table-list">
						<thead>
							<tr>
								<th>Barcode ID</th>
								<th>Doc. No.</th>
								<th>Season</th>
								<th>Style</th>
								<th>Article</th>
								<th>Size</th>
								<th>Status</th>
								<th></th>
							</tr>
						</thead>
						<tbody></tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>

@endsection



@section('js')
<script type="text/javascript">
$(document).ready(function(){
	$('#barcode_id').focus();

	$('input[type=radio][name=radio_status]').change(function(){
		if (this.value=="storage") {
			if ($('.locar').hasClass('hidden')) {
				$('.locar').removeClass('hidden');
			}
		}else{
			if ($('.locar').hasClass('hidden')) {
				$('.locar').removeClass('hidden');
				$('.locar').addClass('hidden');
			}else{
				$('.locar').addClass('hidden')
			}
		}
	});

	$('.input-new').keypress(function(event){
		if (event.which==13) {
			event.preventDefault();

			var bargar = $('#barcode_id').val();
			var status = $('input[name=radio_status]:checked').val();
			var locat = $('#locat').val();
			var bcd = bargar.substr(0,1);

			if (bargar=="") {
				alert(422,"Barcode required ! ! !");
				$('#barcode_id').val('');
				$('#barcode_id').focus();
				return false;

			}

			if (status=='storage' && locat=="") {

				if (bcd!="R") {
					alert(422,"Select Rack First ! ! !");
					$('#barcode_id').val('');
					$('#barcode_id').focus();
					return false;
				}else{
					setLocation(bargar);
				}
				$('#barcode_id').val();
			}else if (status=='storage' && locat!="") {
				if (bcd=="R") {
					setLocation(bargar);
				}else{
					ajaxScan(bargar,status,locat);
				}
			}else if (status=='reverse') {
				ajaxScan(bargar,status,locat);
			}
			


			$('#barcode_id').val('');
			$('#barcode_id').focus();
		}


	});

	
});

function setLocation(barcode){
	$.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
        type: 'get',
        url : "{{ route('marker.getRack') }}",
        data:{barcode:barcode},
        beforeSend : function(){
        	loading();
        },
        success: function(response) {
        	$.unblockUI();
        	$('#locat').val(response.id);
        	$('#rack').val(response.name);
        },
        error: function(response) {
        	$.unblockUI();
           	alert(response.status,response.responseText);
            
        }
    });
}

function ajaxScan(bargar,status,rack){
	$.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
        type: 'post',
        url : $('#form-scan').attr('action'),
        data:{bargar:bargar,status:status,rack:rack},
        beforeSend : function(){
        	loading();
        	$('#barcode_id').focus();
        	$('#barcode_id').val('');
        },
        success: function(response) {
        	$.unblockUI();
        	$('#table-list > tbody').prepend(response);
        	var counts = +$('#_count').val()+1;

        	$('#_count').val(counts);

        	
        },
        error: function(response) {
        	$.unblockUI();
           	alert(response.status,response.responseText);
            
        }
    });
}
</script>
@endsection