@extends('layouts.app', ['active' => 'destroybapb'])
@section('header')
<div class="page-header page-header-default">
	<div class="breadcrumb-line">
		<ul class="breadcrumb">
			<li><a href="#"><i class="icon-home2 position-left"></i> BAPB</a></li>
			<li class="active">Destroy</li>
		</ul>
	</div>
</div>
@endsection

@section('content')
<div class="content">
	<div class="row">
		<div class=" panel panel-flat">
			<div class="panel-body">
				<div class="col-lg-10">
					<form action="{{ route('bapb.ajaxScanDestory') }}" id="form-checkin">
						@csrf
						<label><b>Scan Barcode</b></label>
						<input type="text" name="barcode_id" id="barcode_id" class="form-control input-new" placeholder="#scan in here" required="">
					</form>
				</div>
				<div class="col-lg-2">

					<input type="text" name="_count" id="_count" class="form-control" value="0" readonly="" style="text-align: center; font-size: 28px; margin-top: 27px;">
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class=" panel panel-flat">
			<div class="panel-body">
				<div class="table-responsive">
					<table class ="table table-basic table-condensed" id="table-list">
						<thead>
							<tr>
								<th>Barcode ID</th>
								<th>Doc. No.</th>
								<th>Season</th>
								<th>Style</th>
								<th>Article</th>
								<th>Size</th>
								<th>Status</th>
								<th></th>
							</tr>
						</thead>
						<tbody></tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>

@endsection



@section('js')
<script type="text/javascript">
$(document).ready(function(){
	$('#barcode_id').focus();

	$('.input-new').keypress(function(event){
		if (event.which==13) {
			event.preventDefault();

			var bargar = $('#barcode_id').val();

			$.ajaxSetup({
		        headers: {
		            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		        }
		    });
		    $.ajax({
		        type: 'post',
		        url : $('#form-checkin').attr('action'),
		        data:{bargar:bargar},
		        beforeSend : function(){
		        	loading();
		        	$('#barcode_id').focus();
		        	$('#barcode_id').val('');
		        },
		        success: function(response) {
		        	$.unblockUI();
		        	$('#table-list > tbody').prepend(response);
		        	var counts = +$('#_count').val()+1;

		        	$('#_count').val(counts);

		        	
		        },
		        error: function(response) {
		        	$.unblockUI();
		           	alert(response.status,response.responseText);
		            
		        }
		    });
		}
	});

	
});
</script>
@endsection