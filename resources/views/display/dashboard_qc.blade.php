<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
     <link rel="icon"  href="{{url('assets/icon/icon_baju.png')}}">
    <title>S S M</title>
    <!-- {{url('')}} -->
    <!-- Global stylesheets -->
    <link href="{{ url('assets/css/googleapis.css')}}" rel="stylesheet" type="text/css">
    <link href="{{url('assets/css/icons/icomoon/styles.css')}}" rel="stylesheet" type="text/css">
    <link href="{{url('assets/css/icons/fontawesome/styles.min.css')}}" rel="stylesheet" type="text/css">
    <link href="{{url('assets/css/bootstrap.css')}}" rel="stylesheet" type="text/css">
    <link href="{{url('assets/css/core.css')}}" rel="stylesheet" type="text/css">
    <link href="{{url('assets/css/components.css')}}" rel="stylesheet" type="text/css">
    <link href="{{url('assets/css/colors.css')}}" rel="stylesheet" type="text/css">
    <!-- /global stylesheets -->
    <style type="text/css">
        .row{
            margin: 0px;
            padding: 3px;
        }
    </style>
    <!-- Core JS files -->
    <script type="text/javascript" src="{{url('assets/js/plugins/loaders/pace.min.js')}}"></script>
    <script type="text/javascript" src="{{url('assets/js/core/libraries/jquery.min.js')}}"></script>
    <script type="text/javascript" src="{{url('assets/js/core/libraries/bootstrap.min.js')}}"></script>
    <script type="text/javascript" src="{{url('assets/js/plugins/loaders/blockui.min.js')}}"></script>
    <!-- /core JS files -->

    <!-- Theme JS files -->
    <script type="text/javascript" src="{{url('assets/js/plugins/visualization/d3/d3.min.js')}}"></script>
    <script type="text/javascript" src="{{url('assets/js/plugins/visualization/d3/d3_tooltip.js')}}"></script>
    <script type="text/javascript" src="{{url('assets/js/plugins/forms/styling/switchery.min.js')}}"></script>
    <script type="text/javascript" src="{{url('assets/js/plugins/forms/styling/uniform.min.js')}}"></script>

    <script type="text/javascript" src="{{url('assets/js/plugins/ui/moment/moment.min.js')}}"></script>
    <script type="text/javascript" src="{{url('assets/js/plugins/pickers/daterangepicker.js')}}"></script>
    <script type="text/javascript" src="{{url('assets/js/plugins/notifications/bootbox.min.js')}}"></script>
    <script type="text/javascript" src="{{url('assets/js/core/libraries/jquery_ui/interactions.min.js')}}"></script>

    <script type="text/javascript" src="{{url('assets/js/plugins/forms/selects/bootstrap_select.min.js')}}"></script>
    <script type="text/javascript" src="{{url('assets/js/plugins/tables/datatables/datatables.min.js')}}"></script>
    <script type="text/javascript" src="{{url('assets/js/plugins/tables/datatables/extensions/responsive.min.js')}}"></script>
  
    <script type="text/javascript" src="{{url('assets/js/plugins/forms/selects/select2.min.js')}}"></script>

    <script type="text/javascript" src="{{url('assets/js/plugins/pickers/anytime.min.js')}}"></script>
    <script type="text/javascript" src="{{url('assets/js/plugins/pickers/pickadate/picker.js')}}"></script>
    <script type="text/javascript" src="{{url('assets/js/plugins/pickers/pickadate/picker.date.js')}}"></script>
    <script type="text/javascript" src="{{url('assets/js/plugins/pickers/pickadate/picker.time.js')}}"></script>
    <script type="text/javascript" src="{{url('assets/js/plugins/pickers/pickadate/legacy.js')}}"></script>

    <script type="text/javascript" src="{{url('assets/js/pages/picker_date.js')}}"></script>

    <script type="text/javascript" src="{{url('assets/js/core/app.js')}}"></script>
    <script type="text/javascript" src="{{url('assets/js/pages/form_inputs.js')}}"></script>
    <script type="text/javascript" src="{{url('assets/js/pages/form_select2.js')}}"></script>
    <script type="text/javascript" src="{{url('assets/js/plugins/ui/ripple.min.js')}}"></script>
    <script type="text/javascript" src="{{url('assets/js/pages/form_bootstrap_select.js')}}"></script>
   
    

    <!-- /theme JS files -->
    <script type="text/javascript" src="{{url('assets/swal/sweetalert2.min.js')}}"></script>
    <script type="text/javascript" src="{{url('assets/swal/bootbox.min.js')}}"></script>
    <script type="text/javascript" src="{{url('assets/js/pages/components_modals.js')}}"></script>





    <script type="text/javascript">
        function alert(code,text) {
             if (code==200) {
             
                Swal.fire({
                  icon: 'success',
                  title: 'Good Job . . .',
                  text: text,
                  showConfirmButton: false,
                  timer: 1500,
                  width: '50rem'
                });
             }else if (code==422){
                Swal.fire({
                  icon: 'error',
                  title: 'Oopsss . . .',
                  text: text,
                  showConfirmButton: true,
                  width: '50rem'
                });
             }else if (code==500){
                Swal.fire({
                  icon: 'error',
                  title: 'Oopsss . . .',
                  text: text,
                  showConfirmButton: true,
                  width: '50rem'
                });
             }

        }

        function loading(){
            $.blockUI({ 
                message: '<i class="icon-spinner4 spinner"></i>',
                overlayCSS: {
                    backgroundColor: '#1b2024',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    color: '#fff',
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        }
    </script>
   

</head>
<style type="text/css">
table  thead  th {
  font-size: 22px;
  font-weight: bold;
}

table  tbody  td {
  font-size: 18px;
  font-weight: bold;
  text-align: center;
}

.bg-yellow{
  background-color: #ffc107;
  border-color: #ffc107;
}
</style>

<body >

  


    <!-- Page container -->
    <div class="page-container">

        <!-- Page content -->
        <div class="page-content">

            <!-- Main content -->
            <div class="content-wrapper">

                <div class="row">
                   <div class="col-lg-12">
                       <center><label style=" font-size: 45px; padding-top: 10px;">SAMPLE BORROWING INFO QC/QA</label></center>
                   </div>
               </div>
               
               <div class="row">
                 
                 <input type="text" name="no" id="no" class="hidden"  value="0">
                 <input type="text" name="factory_id" class="hidden" id="factory_id"  value="{{ $factory_id }}">
               </div>  

                <div class="row">
                        
                  <div class="table-responsive">
                    <table  class ="table table-basic table-condensed"  id="table-list" width="100%">
                          <thead>
                              <th><center>Style</center></th>
                              <th><center>Qty</center></th>
                              <th><center>Department</center></th>
                              <th><center>Borrower</center></th>
                              <th><center>Borrowing Date</center></th>
                              <th><center>Promise Date</center></th>
                          </thead>
                          <tbody id="last">
                             
                          </tbody>
                     </table>
                  </div> 
                </div>   
          
            </div>
            <!-- /main content -->

        </div>
        <!-- /page content -->

    </div>
<script type="text/javascript">
$(document).ready(function(){
    var d_list =<?php echo json_encode($data); ?>;

 


   
   
    table(d_list);
    

    
});

function table(listing){


  var chunk = listing;

  loading();
    $('#table-list > tbody:last').empty();
    for (var i = 0; i < chunk.length; i++) {
        // console.log(color(chunk[i]['status']));
       $('#table-list > tbody:last').append('<tr '+color(chunk[i]['status'])+'> <td>'+chunk[i]['style']+'</td> <td>'+chunk[i]['qty']+'</td> <td>'+chunk[i]['dept_name']+'</td> <td>'+chunk[i]['borrower']+'</td> <td>'+chunk[i]['date_bor']+'</td> <td>'+chunk[i]['promise']+'</td> </tr>');
    }

  $.unblockUI();
}

function color(status){
    if (status==true) {
        return 'style="background-color: #F44336; color:black; font-weight:bold;"';
    }else{
        
         return 'style="background-color: #eeeded; color:black; font-weight:bold;"';
    }
}

</script>
</body>
</html>
