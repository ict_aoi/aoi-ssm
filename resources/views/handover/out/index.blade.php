@extends('layouts.app', ['active' => 'handoverout'])
@section('header')
<div class="page-header page-header-default">
	<div class="breadcrumb-line">
		<ul class="breadcrumb">
			<li><a href="#"><i class="icon-home2 position-left"></i> Handover</a></li>
			<li class="active">Checkout</li>
		</ul>
	</div>
</div>
@endsection

@section('content')
<div class="content">
	<div class="row">
		<div class=" panel panel-flat">
			<div class="panel-body">
				<div class="col-lg-8">
					<form action="{{ route('handover.out.ajaxOut') }}" id="form-checkout">
						@csrf
						<label><b>Scan Barcode</b></label>
						<input type="text" name="barcode_id" id="barcode_id" class="form-control input-new" placeholder="#scan in here" required="">
					</form>
				</div>
				<div class="col-lg-2">
					<label><b>To Factory</b></label>
					<select id="factory_id" class="form-control select">
						<option value="">--Select Factory--</option>

						@foreach($factory as $fac)
						<option value="{{$fac->id}}">{{$fac->factory_name}}</option>

						@endforeach
					</select>
				</div>
				<div class="col-lg-2">

					<input type="text" name="_count" id="_count" class="form-control" value="0" readonly="" style="text-align: center; font-size: 28px; margin-top: 27px;">
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class=" panel panel-flat">
			<div class="panel-body">
				<div class="table-responsive">
					<table class ="table table-basic table-condensed" id="table-list">
						<thead>
							<tr>
								<th>Barcode ID</th>
								<th>Doc. No.</th>
								<th>Season</th>
								<th>Style</th>
								<th>Article</th>
								<th>Size</th>
								<th>Status</th>
								<th></th>
							</tr>
						</thead>
						<tbody></tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>

@endsection



@section('js')
<script type="text/javascript">
$(document).ready(function(){
	$('#barcode_id').focus();

	$('.input-new').keypress(function(event){
		if (event.which==13) {
			event.preventDefault();

			var bargar = $('#barcode_id').val();
			var factory = $('#factory_id').val();

			if (factory=="") {
				alert(422,"Select Factory destination first ! ! !");
				$('#barcode_id').val('');
				$('#barcode_id').focus();
				return false;
			}

			$.ajaxSetup({
		        headers: {
		            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		        }
		    });
		    $.ajax({
		        type: 'post',
		        url : $('#form-checkout').attr('action'),
		        data:{bargar:bargar,factory:factory},
		        beforeSend : function(){
		        	loading();
		        	$('#barcode_id').focus();
		        	$('#barcode_id').val('');
		        },
		        success: function(response) {
		        	$.unblockUI();
		        	$('#table-list > tbody').prepend(response);
		        	var counts = +$('#_count').val()+1;

		        	$('#_count').val(counts);

		        	
		        },
		        error: function(response) {
		        	$.unblockUI();
		           	alert(response.status,response.responseText);
		            
		        }
		    });
		}
	});

	
});
</script>
@endsection