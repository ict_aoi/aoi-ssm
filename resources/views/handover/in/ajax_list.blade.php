<tr>
	<td>{{ $data['barcode_id'] }}</td>
	<td>{{ $data['docno'] }}</td>
	<td>{{ $data['season'] }}</td>
	<td>{{ $data['style'] }}</td>
	<td>{{ $data['article'] }}</td>
	<td>{{ $data['size'] }}</td>
	<td>{{ $data['status'] }}</td>
	<td>
		<span class="label label-success">{{ $data['loc'] }}</span>
	</td>
</tr>