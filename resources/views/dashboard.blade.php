@extends('layouts.app', ['active' => 'dashboard'])
@section('header')
<div class="page-header page-header-default">
	<div class="breadcrumb-line">
		<ul class="breadcrumb">
			<li><a href="index.html"><i class="icon-home2 position-left"></i> Home</a></li>
			<li class="active">Dashboard</li>
		</ul>
	</div>

</div>
@endsection

@section('content')
<div class="content">
	<!-- Main charts -->
	<div class="panel panel-flat">
		<div class="panel-heading">
			<div class="row">
				<div class="col-lg-9">
					<h5>DASHBOARD</h5>
				</div>
				<div class="col-lg-2" >
					<select class="select form-control {{ Auth::user()->admin_role===false ? 'hidden' : ''}}" id="factory_id">
						<option value="1" {{auth::user()->factory_id==1 ? "selected" : ""}}>AOI 1</option>
						<option value="2" {{auth::user()->factory_id==2 ? "selected" : ""}}>AOI 2</option>
					</select>
				</div>
			</div>
		</div>
		<div class="panel-body">
			<center><h1>COBA</h1></center>
		</div>
	</div>

</div>
@endsection