@extends('layouts.app', ['active' => 'packinglist'])
@section('header')
<div class="page-header page-header-default">
	<div class="breadcrumb-line">
		<ul class="breadcrumb">
			<li><a href="#"><i class="icon-home2 position-left"></i> Packinglist</a></li>
			<li class="active">Detail Packinglist</li>
		</ul>
	</div>
</div>
@endsection

@section('content')
<div class="content">
	

	<div class="row">
	

		<!-- by search -->
		
		<div class="panel panel-flat">
			<div class="panel-heading">
				<h3>Detail Packinglist #{{$invoice}}</h3>
				<input type="text" name="txid" id="txid" class="hidden" value="{{ $id }}">
			</div>
			<div class="panel-body">
				<div class="row">
					<button class="btn btn-success" id="btn-print" ><span class="icon-printer4"></span> Print</button>
				</div>
				<div class="table-responsive">
					<table class ="table table-basic table-condensed" id="table-list" width="100%">
						<thead>
							<tr>
								<th>#</th>
								<th>Carton ID</th>
								<th>Carton No.</th>
								<th>Invoice</th>
								<th>Country</th>
								<th>Customer</th>
								<th>Inner Pack</th>
							</tr>
						</thead>
					</table>
				</div>
			</div>
		</div>
		
		<!-- by search -->
	</div>



</div>
<a href="{{ route('packlist.printShipmark') }}" id="shipmark"></a>
@endsection

@section('js')
<script type="text/javascript">
$(document).ready(function(){
	


	//by search
	var tableS = $('#table-list').DataTable({
		processing:true,
		serverSide:true,
		deferRender:true,
		autoWidth: true,
		dom:'<"datatable-header"fBl><t><"datatable-footer"ip>',
		language: {
	        search: '<span>Filter:</span> _INPUT_',
	        searchPlaceholder: 'Type to filter...',
	        lengthMenu: '<span>Show:</span> _MENU_',
	        paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
	    },
	    ajax: {
	        type: 'GET',
	        url: "{{ route('packlist.ajaxGetDetailPl') }}",
	        data: function (d) {
	            return $.extend({},d,{
	                "id": $('#txid').val()
	            });
	        }
	    },
	    fnCreatedRow: function (row, data, index) {
	        var info = tableS.page.info();
	        var value = index+1+info.start;
	        // $('td', row).eq(0).html(value);
	        $('td', row).eq(1).css('max-width','50px');
	    },
	    columns: [
	        // {data: null, sortable: false, orderable: false, searchable: false},
	        {data: 'checkbox',name:'checkbox', sortable: false, orderable: false, searchable: false},
	        {data: 'carton_id', name: 'carton_id'},
	        {data: 'carton_no', name: 'docucarton_nomentno'},
	        {data: 'invoice', name: 'invoice'},
	        {data: 'country_to', name: 'country_to'},
	        {data: 'customer_number', name: 'customer_number'},
	        {data: 'inner_pack', name: 'inner_pack'}
	    ]
	});



	tableS.on('preDraw', function() {
		loading();
	    Pace.start();
	})
	.on('draw.dt', function() {
	    $.unblockUI();
	    Pace.stop();
	});


	
	$('#btn-print').click(function(event){
		event.preventDefault();

		var data = "";
		var invid = $('#txid').val();

		$('.ctn_id:checked').each(function(){
			var str = $(this).val()+";";
			data = data+str;
		});

		var url = $('#shipmark').attr('href')+'?invid='+invid+'&data='+data;
		if (data!="") {
			window.open(url);
		}else{
			alert(422,"checked carton first ! ! !");
		}
	})
	
	

	
	
});
</script>
@endsection