@extends('layouts.app', ['active' => 'packinglist'])
@section('header')
<div class="page-header page-header-default">
	<div class="breadcrumb-line">
		<ul class="breadcrumb">
			<li><a href="#"><i class="icon-home2 position-left"></i> Packinglist</a></li>
			<li class="active">Upload Packinglist</li>
		</ul>
	</div>
</div>
@endsection

@section('content')
<div class="content">
	

	<div class="row">
	

		<!-- by search -->
		
		<div class="panel panel-flat">
			<div class="panel-heading">
				<form action="{{ route('packlist.getDatInv') }}" id="form-search">
					@csrf
					<div class="form-group" >
		                <label><b>Invoice NO</b></label>
		                <div class="input-group">
		                    <input type="text" class="form-control" name="invoice" id="invoice" required="" placeholder="#Invoice No">
		                    <div class="input-group-btn">
		                        <button type="submit" class="btn btn-primary">Filter</button>
		                        <button type="button" class="btn btn-success" id="btn-temp"><i class="icon-file-text3"></i> Template</button>
		                    </div>
		                </div>
		            </div>
				</form>
			</div>
			<div class="panel-body">
				<div class="table-responsive">
					<table class ="table table-basic table-condensed" id="table-search" width="100%">
						<thead>
							<tr>
								<th>#</th>
								<th>Invoice No</th>
								<th>Doc. No.</th>
								<th>Doc. type</th>
								<th>Doc. Status</th>
								<th>Country</th>
								<th>Cust. No.</th>
								<th>Shipmode</th>
								<th>Qty</th>
								<th>Action</th>
							</tr>
						</thead>
					</table>
				</div>
			</div>
		</div>
		
		<!-- by search -->
	</div>



</div>
<a href="{{ route('packlist.templatePl') }}" id="ajaxTemplatePl"></a>
@endsection

@section('modal')

<div id="modal_upload" class="modal fade">
	<div class="modal-dialog modal-md">
		<div class="modal-content">
			<div class="modal-header">
				<center><h4>Upload PackingList</h4> <h4 class ="txinv"></h4></center>
			</div>
			<div class="modal-body">
				<form action="{{ route('packlist.uploadPL') }}" method="POST" id="form_upload" enctype="multipart/form-data">
					@csrf
					<div class="row">
						<label><b>File Upload :</b></label>
						<input type="text" name="txid" id="txid" class="hidden txid" >
					</div>
					<div class="row">
						<div class="col-lg-8">
							<input type="file" name="file" id="file" class="file-styled">
						</div>
						<div class="col-lg-4">
							<button class="btn btn-success" type="submit" id="btn-submit">Upload</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
@endsection

@section('js')
<script type="text/javascript">
$(document).ready(function(){
	


	//by search
	var tableS = $('#table-search').DataTable({
		processing:true,
		serverSide:true,
		deferRender:true,
		autoWidth: true,
		dom:'<"datatable-header"fBl><t><"datatable-footer"ip>',
		language: {
	        search: '<span>Filter:</span> _INPUT_',
	        searchPlaceholder: 'Type to filter...',
	        lengthMenu: '<span>Show:</span> _MENU_',
	        paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
	    },
	    ajax: {
	        type: 'GET',
	        url: $('#form-search').attr('action'),
	        data: function (d) {
	            return $.extend({},d,{
	                "invoice": $('#invoice').val()
	            });
	        }
	    },
	    fnCreatedRow: function (row, data, index) {
	        var info = tableS.page.info();
	        var value = index+1+info.start;
	        $('td', row).eq(0).html(value);
	    },
	    columns: [
	        {data: null, sortable: false, orderable: false, searchable: false},
	        {data: 'invoice', name: 'invoice'},
	        {data: 'documentno', name: 'documentno'},
	        {data: 'document_type', name: 'document_type'},
	        {data: 'docstatus', name: 'docstatus'},
	        {data: 'country_to', name: 'country_to'},
	        {data: 'customer_number', name: 'customer_number'},
	        {data: 'shipmode', name: 'shipmode'},
	        {data: 'qty', name: 'qty'},
	        {data: 'action', name: 'action', searchable:false, sortable:false, orderable:false}
	    ]
	});



	tableS.on('preDraw', function() {
		loading();
	    Pace.start();
	})
	.on('draw.dt', function() {
	    $.unblockUI();
	    Pace.stop();
	});

	$('#form-search').submit(function(event){
		event.preventDefault();
		
		tableS.clear();
		tableS.draw();
	});

	$('#table-search').on('click','.uppl',function(event){
		event.preventDefault();

		var id = $(this).data('id');
		var inv = $(this).data('inv');

		$('#txinv').val(inv);
		$('#txid').val(id);

		$('#modal_upload').modal('show');
	});

	
	// $('#table-search').on('click','.upload',function(event){
	// 	event.preventDefault();
	// 	var str = $(this).data('id');
	// 	var id = str.split('&')[0];
	// 	var inv = str.split('&')[1];
	// 	$('#modal_upload .txinv').text('');
	// 	$('#modal_upload .txid').text('');
	// 	$('#modal_upload').modal('show');
	// 	$('#modal_upload .txinv').text(inv);
	// 	$('#modal_upload .txid').val(id);

		
	// });

	$('#form_upload').submit(function(event){
		event.preventDefault();

		var formData = new FormData($(this)[0]);

		formData.append('invoice',$('#txid').val());
		console.log(formData);
		$.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: 'POST',
            url : $('#form_upload').attr('action'),
            data: formData,
            contentType: false,
            processData: false,
            beforeSend: function() {
               loading();
            },
            success: function(response) {

            	$('#modal_upload').modal('hide');
            	$.unblockUI();
            	var notif = response.data;
	            alert(notif.status,notif.output);
	            $('#form-search').submit();
	            
            },
            error: function(response) {
                
            }
        });
	});
	
	// $('#table-search').on('click','.detpl',function(){
		
	// });
	
	
});
</script>
@endsection