<style type="text/css">
@page{
    margin : 15 15 15 15;

}

/*@font-face {
    font-family: 'impact' !important;
    src: url({{ storage_path('fonts/impact.ttf') }}) format('truetype');
}*/




.page-break {
    page-break-after: always;
}

.print-friendly {
    line-height: 30px;
    width: 100%;
}

table.print-friendly tr td, table.print-friendly tr th,
table.print-friendly-single tr td, table.print-friendly-single tr th, {
    page-break-inside: avoid;
}

.barcode {
    line-height: 16px;
    /*padding-right:  -10px;*/
    float: right;
    padding-bottom: 5px;
}

.img_barcode {
    display: block;
    padding: 0px;
}

.img_barcode > img {
    width: 180px;
    height:30px;
}

.barcode_number {
    font-family: sans-serif;
    font-size: 14px !important;
}

</style>

@if(isset($ctn))
    @foreach($ctn as $key => $value)
    <table class="print-friendly">
        <tr>
            <td colspan="3">
               <b>{{$invoice}} / {{$cust_no}} / {{$country}} </b>
            </td>
            <td colspan="1">
                <div class="barcode">
                    <div class="img_barcode">
                        <img src="data:image/png;base64,{{ DNS1D::getBarcodePNG($value->carton_id, 'C128',2,35) }}" alt="barcode"   />
                    </div>
                    <div class="row">
                        <span class="barcode_number">{{ $value->carton_id }}</span >
                    </div>
                </div>
            </td>
        </tr>
        <tr>
            <td>
                <b>
                 CARTON
                </b>
            </td>
            <td>
                <center>
                <b>
                 {{$value->carton_no}}
                </b>
                </center>
            </td>
            <td>
                <center>
                <b>
                    OF
                </b>
                </center>
            </td>
            <td>
                <center>
                <b>
                    {{ $total_ctn }}
                </b>
                </center>
            </td>
        </tr>
        <tr>
            <td colspan="4"> 
                <table width="100%">
                    <thead>
                        <tr>
                            <th ><center>STYLE NO</center></th>
                            <th ><center>P.O.NO.</center></th>
                            <th ><center>ART NO.</center></th>
                            <th ><center>QTY</center></th>
                        </tr>
                    </thead>
                    <tbody>
                        @php($tot = 0)
                        @foreach($packdet as $pd => $val)
                            @if($val->carton_id == $value->carton_id)
                            <tr>
                                <td><center>{{$val->style}}</center></td>
                                <td><center>{{$val->po_number}}</center></td>
                                <td><center>{{$val->article}}</center></td>
                                <td><center>{{$val->qty}}</center></td>
                            </tr>
                            @php($tot = $tot + $val->qty)
                            @endif
                        @endforeach
                            <tr>
                                <td colspan="2"></td>
                                <td><center><b>Total</b></center></td>
                                <td><center><b>{{$tot}}</b></center></td>
                            </tr>
                    </tbody>
                </table>
            </td>
        </tr>
    </table>
    @if($key != (count($ctn) - 1))
    <div class="page-break"></div>
    @endif
    @endforeach

@endif



