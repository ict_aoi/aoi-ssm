<style type="text/css">
@page {
    margin: 0 20 0 20;
}

p{
    line-height: 1;
}

.table{
	line-height: 20px;
	padding-top: 10px;
}



.barcode {
    line-height: 16px;
}

.img_barcode {
    display: block;
    padding: 0px;
}

.img_barcode > img {
    width: 180px;
    height:30px;
}

.barcode_number {
    font-family: sans-serif;
    font-size: 14px !important;
}	
</style>
@php($chunk = array_chunk($data->toArray(),3))
@foreach($chunk as $key => $value)
	<table class="table">
		
		<tr>
			@if(isset($value[0]))
			<td style="padding-right: 25px; padding-left: 25px; border: 2px solid black;">
				<p>Cust. No. : {{ $value[0]->customer_number }}</p>
				<p>Country : {{ $value[0]->country }}</p>
				<p>Crd : {{ $value[0]->crd }}</p>
				<div class="barcode">
	                <div class="img_barcode">
	                    <img src="data:image/png;base64,{{ DNS1D::getBarcodePNG($data[0]->ctn_id, 'C128',2,35) }}" alt="barcode"   />
	                </div>
	                <div class="row">
	                    <span class="barcode_number">{{  $data[0]->ctn_id }}</span >
	                </div>                
	            </div>
			</td>
			@endif

			@if(isset($value[1]))
			<td style="padding-right: 25px; padding-left: 25px; border: 2px solid black;">
				<p>Cust. No. : {{ $value[1]->customer_number }}</p>
				<p>Country : {{ $value[1]->country }}</p>
				<p>Crd : {{ $value[1]->crd }}</p>
				<div class="barcode">
	                <div class="img_barcode">
	                    <img src="data:image/png;base64,{{ DNS1D::getBarcodePNG($data[1]->ctn_id, 'C128',2,35) }}" alt="barcode"   />
	                </div>
	                <div class="row">
	                    <span class="barcode_number">{{  $data[1]->ctn_id }}</span >
	                </div>                
	            </div>
			</td>
			@endif

			@if(isset($value[2]))
			<td style="padding-right: 25px; padding-left: 25px; border: 2px solid black;">
				<p>Cust. No. : {{ $value[2]->customer_number }}</p>
				<p>Country : {{ $value[2]->country }}</p>
				<p>Crd : {{ $value[2]->crd }}</p>
				<div class="barcode">
	                <div class="img_barcode">
	                    <img src="data:image/png;base64,{{ DNS1D::getBarcodePNG($data[2]->ctn_id, 'C128',2,35) }}" alt="barcode"   />
	                </div>
	                <div class="row">
	                    <span class="barcode_number">{{  $data[2]->ctn_id }}</span >
	                </div>                
	            </div>
			</td>
			@endif

		</tr>

	</table>
@endforeach