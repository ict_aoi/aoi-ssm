@extends('layouts.app', ['active' => 'create_carton'])
@section('header')
<div class="page-header page-header-default">
	<div class="breadcrumb-line">
		<ul class="breadcrumb">
			<li><a href="#"><i class="icon-home2 position-left"></i> Carton</a></li>
			<li class="active">Create Carton</li>
		</ul>
	</div>
</div>
@endsection

@section('content')
<div class="content">
	<div class="row">
		<div class=" panel panel-flat">
			<div class="panel-body">
				<div class="col-lg-10">
					<div class="row">
						<!-- <form action="{{ route('carton.getDataPL') }}" id="form-search">
							@csrf -->
							<label id="lb-fil" style="font-weight: bold;">Filter by Customer Number</label>
							<div class="input-group">
			                    <input type="text" name="key" id="key" class="form-control" required="">
			                    <div class="input-group-btn">
			                        <button type="submit" class="btn btn-primary" id="btn-filter">Filter</button>
			                    </div>
			                </div>
						<!-- </form> -->
					</div>
					<div class="row">
						<label class="radio-inline"><input type="radio" name="radio_status" checked="checked" value="customer">Filter by Customer Number</label>
                		<label class="radio-inline"><input type="radio" name="radio_status" value="country">Filter by Country</label>
					</div>
					
				</div>
				<div class="col-lg-2">
					<button id="btn-upload" class="btn btn-success" style="margin-top: 30px;"><span class="icon-upload"></span> Upload PL</button>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class=" panel panel-flat">
			<div class="panel-body">
				<div class="table-responsive">
					<table class ="table table-basic table-condensed" id="table-list">
						<thead>
							<tr>
								<th>#</th>
								<th>Cust. No.</th>
								<th>Country</th>
								<th>Dimension</th>
								<th>Crd</th>
								<th>Qty</th>
								<th>Action</th>
							</tr>
						</thead>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>

@endsection

@section('modal')
<div id="modal_upload" class="modal fade">
	<div class="modal-dialog modal-md">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h5 class="modal-title">Upload Packinglist</h5>
			</div>

			<div class="modal-body">
				<form action="{{ route('carton.uploadPL') }}" id="form-upload" method="POST" enctype="multipart/form-data">
					@csrf
					<div class="row">
						<input type="file" name="file" id="file" class="file-styled" >
					</div>
					<div class="row">
						<button type="submit" class="btn btn-primary" id="btn-save">Save</button>
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
@endsection


@section('js')
<script type="text/javascript">
$(document).ready(function(){

	



	$('#btn-filter').click(function(){
		
		var key = $('#key').val();
		var radio = $('input[type=radio][name=radio_status]').val();
		if (key.trim()=="") {
			alert(422,"Keyword required ! ! ! ");
		}else{
			settable(key,radio);
		}

		
	});




	$('#btn-upload').on('click',function(){
		$('#modal_upload').modal('show');
	});

	$('#form-upload').submit(function(event){
		event.preventDefault();

		var formData = new FormData($(this)[0]);

		$.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: 'POST',
            url : $('#form-upload').attr('action'),
            data: formData,
            contentType: false,
            processData: false,
            beforeSend: function() {
                $('#modal_upload').modal('hide');
                loading();
            },
            success: function(response) {
                var data = response.data;
                if (data.status==200) {
                	alert(data.status,data.output);
                	setList(data.plids);
                	$.unblockUI();
                }else{
                	alert(data.status,data.output);
                	$.unblockUI();
                }
            },
            error: function(response) {
            	var data = response.data;
            	alert(data.status,data.output);
                $.unblockUI();
            }
        });

	});

	$('input[type=radio][name=radio_status]').change(function(){
		if (this.value=='customer') {
			$('#lb-fil').text('Filter By Customer Number');
		}else{
			$('#lb-fil').text('Filter By Country');
		}
	});







	

	
});

function setList(plids){
	console.log(plids);

	var tablel = $('#table-list').DataTable({
		processing:true,
		serverSide:true,
		deferRender:true,
		dom:'<"datatable-header"fBl><t><"datatable-footer"ip>',
		language: {
	        search: '<span>Filter:</span> _INPUT_',
	        searchPlaceholder: 'Type to filter...',
	        lengthMenu: '<span>Show:</span> _MENU_',
	        paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
	    },
	    ajax: {
	        type: 'GET',
	        url: "{{ route('carton.getDataPL') }}",
	        data: function (d) {
	            return $.extend({},d,{
	                "search": plids,
	                "radio":"upload"
	            });
	        }
	    },
	    fnCreatedRow: function (row, data, index) {
	        var info = tablel.page.info();
	        var value = index+1+info.start;
	        $('td', row).eq(0).html(value);
	    },
	    columns: [
	        {data: null, sortable: false, orderable: false, searchable: false},
	        {data: 'customer_number', name: 'customer_number'},
	        {data: 'country', name: 'country'},
	        {data: 'dimension', name: 'dimension'},
	        {data: 'crd', name: 'crd'},
	        {data: 'ctn_qty', name: 'ctn_qty'},
	        {data: 'action', name: 'action', searchable:false, sortable:false, orderable:false}
	    ]
	});

	// tablel.clear();
	// tablel.draw();
}

function settable(search,radio){
	
	var table = $('#table-list').DataTable({
		processing:true,
		serverSide:true,
		deferRender:true,
		dom:'<"datatable-header"fBl><t><"datatable-footer"ip>',
		language: {
	        search: '<span>Filter:</span> _INPUT_',
	        searchPlaceholder: 'Type to filter...',
	        lengthMenu: '<span>Show:</span> _MENU_',
	        paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
	    },
	    ajax: {
	        type: 'GET',
	        url: "{{ route('carton.getDataPL') }}",
	        data: function (d) {
	            return $.extend({},d,{
	                "search": search,
	                "radio":radio
	            });
	        }
	    },
	    fnCreatedRow: function (row, data, index) {
	        var info = table.page.info();
	        var value = index+1+info.start;
	        $('td', row).eq(0).html(value);
	    },
	    columns: [
	        {data: null, sortable: false, orderable: false, searchable: false},
	        {data: 'customer_number', name: 'customer_number'},
	        {data: 'country', name: 'country'},
	        {data: 'dimension', name: 'dimension'},
	        {data: 'crd', name: 'crd'},
	        {data: 'ctn_qty', name: 'ctn_qty'},
	        {data: 'action', name: 'action', searchable:false, sortable:false, orderable:false}
	    ]
	});

	// table.clear();
	// table.draw();
}
</script>
@endsection