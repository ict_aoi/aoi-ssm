@extends('layouts.app', ['active' => 'create_carton'])
@section('header')
<div class="page-header page-header-default">
	<div class="breadcrumb-line">
		<ul class="breadcrumb">
			<li><a href="#"><i class="icon-home2 position-left"></i> Carton</a></li>
			<li class="active">Create Carton</li>
		</ul>
	</div>
</div>
@endsection

@section('content')
<div class="content">
	<div class="row">
		<div class=" panel panel-flat">
			<div class="panel-body">
				<div class="row">
					<div class="col-lg-11">
						<input type="text" name="plid" id="plid" value="{{ $id }}" hidden="">
					</div>
					<div class="col-lg-1">
						<button class="btn btn-primary" id="print" style="margin-top:15px;"><span class="icon-printer2"></span> Print</button>
					</div>
				</div>
				<hr>
				<div class="table-responsive">
					<table class ="table table-basic table-condensed" id="table-list">
						<thead>
							<tr>
								<th>#</th>
								<th>Carton Id</th>
								<th>Cust. No.</th>
								<th>Country</th>
								<th>Dimension</th>
								<th>Location</th>
								<th></th>
							</tr>
						</thead>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
<a href="{{ route('carton.printCtn') }}" id="ajaxprint"></a>
@endsection




@section('js')
<script type="text/javascript">
$(document).ready(function(){

	var table = $('#table-list').DataTable({
		processing:true,
		serverSide:true,
		deferRender:true,
		dom:'<"datatable-header"fBl><t><"datatable-footer"ip>',
		language: {
	        search: '<span>Filter:</span> _INPUT_',
	        searchPlaceholder: 'Type to filter...',
	        lengthMenu: '<span>Show:</span> _MENU_',
	        paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
	    },
	    ajax: {
	        type: 'GET',
	        url: "{{ route('carton.ajaxGetDetailCtn') }}",
	        data: function (d) {
	            return $.extend({},d,{
	                "id": $('#plid').val()
	            });
	        }
	    },
	    fnCreatedRow: function (row, data, index) {
	        var info = table.page.info();
	        var value = index+1+info.start;
	        $('td', row).eq(0).html(value);
	    },
	    columns: [
	        {data: null, sortable: false, orderable: false, searchable: false},
	        {data: 'ctn_id', name: 'ctn_id'},
	        {data: 'customer_number', name: 'customer_number'},
	        {data: 'country', name: 'country'},
	        {data: 'dimension', name: 'dimension',sortable: false, orderable: false, searchable: false},
	        {data: 'current_location', name: 'current_location',sortable: false, orderable: false, searchable: false},
	        {data: 'current_status', name: 'current_status',sortable: false, orderable: false, searchable: false}
	    ]
	});

	$('#print').on('click',function(){
		var url = $('#ajaxprint').attr('href')+"?id="+$('#plid').val();
		window.open(url,'_blank');
	});

	
});


</script>
@endsection