@extends('layouts.app', ['active' => 'dashboard'])
@section('header')
<div class="page-header page-header-default">
	<div class="breadcrumb-line">
		<ul class="breadcrumb">
			<li><a href="#"><i class="icon-home2 position-left"></i> Home</a></li>
			<li class="active">Dashboard</li>
		</ul>
	</div>

</div>
@endsection

@section('content')
<div class="content">
	<!-- Main charts -->
	<div class="panel panel-flat">
		<div class="panel-heading">
			<div class="row">
				<div class="col-lg-9">
					<h2><i class="fa fa-dashboard position-left"></i> <b>DASHBOARD</b></h2>
				</div>
			</div>
		</div>
		<div class="panel-body">
			<div class="row">
				<form action="{{ route('home.getData') }}" id="form-search">
					@csrf
					<div class="row form-group" id="by_docno">
		                <label><b>Document NO</b></label>
		                <div class="input-group">
		                    <input type="text" class="form-control" name="docno" id="docno" placeholder="Document No.">
		                    <div class="input-group-btn">
		                        <button type="submit" class="btn btn-primary">Filter</button>
		                    </div>
		                </div>
		            </div>
		            <div class="row form-group hidden" id="by_style">
		                <label><b>Style</b></label>
		                <div class="input-group">
		                    <input type="text" class="form-control" name="txstyle" id="txstyle" placeholder="Style">
		                    <div class="input-group-btn">
		                        <button type="submit" class="btn btn-primary">Filter</button>
		                    </div>
		                </div>
		            </div>
		            <div class="row form-group">
		            	<label class="radio-inline"><input type="radio" name="radio_search" checked="checked" value="no_mo"><b>Document No</b></label>
	            		<label class="radio-inline"><input type="radio" name="radio_search" value="style"><b>Style</b></label>
		            </div>
				</form>
			</div>

			<div class="row {{ Auth::user()->admin_role ===false ? 'hidden' : '' }}" >
				<div class="col-lg-3">
					<label><b>Factory : </b></label>
					<select id="factory_id" class="form-control select">
						@foreach($factory as $fc)
							<option value="{{ $fc->id}}" {{ $fc->id === Auth::user()->factory_id ? 'selected' : ''}}>{{ $fc->factory_name}}</option>
						@endforeach
					</select>
				</div>
			</div>
			<div class="row table-resposive">
				<table class="table table-basic table-condensed" id="table-list" style="text-align: center;">
					<thead>
						<tr>
							<th rowspan="2">#</th>
							<th rowspan="2" width="100" style="text-align:center">Doc. NO</th>
							<th rowspan="2" width="250" style="text-align:center">Item </th>
							<th rowspan="2" style="text-align:center">Brand</th>
							<th rowspan="2" style="text-align:center">Status</th>
							<th rowspan="2" width="50" style="text-align:center"> Qty</th>
							<th colspan="16" style="text-align:center">Location</th>
							<th rowspan="2">Action</th>
						</tr>

						<tr>
							<th style="text-align:center" class="col-xs-1">Reserv</th>
							<th style="text-align:center" class="col-xs-1">MD</th>
							<th style="text-align:center" class="col-xs-1">QA</th>
							<th style="text-align:center" class="col-xs-1">Laboratory</th>
							<th style="text-align:center" class="col-xs-1">Marker</th>
							<th style="text-align:center" class="col-xs-1">Cutting</th>
							<th style="text-align:center" class="col-xs-1">Pack. Sample</th>
							<th style="text-align:center" class="col-xs-1">IE</th>
							<th style="text-align:center" class="col-xs-1">PPC</th>
							<th style="text-align:center" class="col-xs-1">PPA</th>
							<th style="text-align:center" class="col-xs-1">Sewing</th>
							<th style="text-align:center" class="col-xs-1">Sample Room</th>
							<th style="text-align:center" class="col-xs-1">OM</th>
							<th style="text-align:center" class="col-xs-1">Shipping</th>
							<th style="text-align:center" class="col-xs-1">BAPB</th>
							<th style="text-align:center" class="col-xs-1">Handover</th>
						</tr>
					</thead>
				</table>
			</div>
		
		</div>
	</div>

</div>
<a href="{{route('home.detailDash')}}" id="ajaxDetail"></a>
@endsection

@section('js')
<script type="text/javascript">
$(document).ready(function(){
	$('input[type=radio][name=radio_search]').change(function(){
		if (this.value=='no_mo') {
			if ($('#by_docno').hasClass('hidden')) {
				$('#by_docno').removeClass('hidden');
			}
			
			$('#by_style').addClass('hidden');
		}else if (this.value=='style') {
			if ($('#by_style').hasClass('hidden')) {
				$('#by_style').removeClass('hidden');
			}

			$('#by_docno').addClass('hidden');
		}
	});
	
	$.extend( $.fn.dataTable.defaults, {
        // scrollX: true,
        // scrollY: "50vh",
        scrollCollapse: true,
        stateSave: true,
        autoWidth: false,
        autoLength: false,
        processing: true,
        serverSide: true,
        paging: false, 
        scrollY:        "300px",
        scrollX:        true,
        scrollCollapse: true,
        fixedColumns:   {
            leftColumns: 6
        },
        sDom: 'tF',
    });

	var table = $('#table-list').DataTable({
	    ajax: {
	        type: 'GET',
	        url: $('#form-search').attr('action'),
	        data: function (d) {
	            return $.extend({},d,{
	                "radio_search": $('input[name=radio_search]:checked').val(),
	                "docno": $('#docno').val(),
	                "style": $('#txstyle').val(),
	                'factory' : $('#factory_id').val()
	            });
	        }
	    },
	    fnCreatedRow: function (row, data, index) {
	        var info = table.page.info();
	        var value = index+1+info.start;
	        $('td', row).eq(0).html(value);
	        $('td', row).eq(1).css('min-width','100px');
	        $('td', row).eq(2).css('min-width','250px');
	        $('td', row).eq(5).css('min-width','50px');

	    },
	    columns: [
	        {data: null, sortable: false, orderable: false, searchable: false},
	        {data: 'documentno', name: 'documentno', sortable:false, orderable:false},
	        {data: 'item_code', name: 'item_code' , sortable:false, orderable:false},
	        {data: 'brand', name: 'brand', sortable:false, orderable:false},
	        {data: 'status', name: 'status', sortable:false, orderable:false},
	        {data: 'qty', name: 'qty', searchable:false, sortable:false, orderable:false},
	        {data: 'reserv', name: 'reserv', searchable:false, sortable:false, orderable:false},
	        {data: 'md', name: 'md', searchable:false, sortable:false, orderable:false},
	        {data: 'qc', name: 'qc', searchable:false, sortable:false, orderable:false},
	        {data: 'lab', name: 'lab', searchable:false, sortable:false, orderable:false},
	        {data: 'marker', name: 'marker', searchable:false, sortable:false, orderable:false},
	        {data: 'cutting', name: 'cutting', searchable:false, sortable:false, orderable:false},
	        {data: 'pack_sample', name: 'pack_sample', searchable:false, sortable:false, orderable:false},
	        {data: 'ie', name: 'ie', searchable:false, sortable:false, orderable:false},
	        {data: 'ppc', name: 'ppc', searchable:false, sortable:false, orderable:false},
	        {data: 'ppa', name: 'ppa', searchable:false, sortable:false, orderable:false},
	        {data: 'sewing', name: 'sewing', searchable:false, sortable:false, orderable:false},
	        {data: 'sample_room', name: 'sample_room', searchable:false, sortable:false, orderable:false},
	        {data: 'om', name: 'om', searchable:false, sortable:false, orderable:false},
	        {data: 'ship', name: 'ship', searchable:false, sortable:false, orderable:false},
	        {data: 'bapb', name: 'bapb', searchable:false, sortable:false, orderable:false},
	        {data: 'handover', name: 'handover', searchable:false, sortable:false, orderable:false},
	        {data: 'action', name: 'action', searchable:false, sortable:false, orderable:false}
	    ]
	});

	table.on('preDraw', function() {
		loading();
	    Pace.start();
	})
	.on('draw.dt', function() {
	    $.unblockUI();
	    Pace.stop();
	});

	$('#form-search').submit(function(event){
		event.preventDefault();

		table.clear();
		table.draw();
	});

	$('#factory_id').change(function(event){
		event.preventDefault();

		table.clear();
		table.draw();
	})
	
	$('#table-list').on('click','.btn-detail',function(event){
		event.preventDefault();

		var moid = $(this).data('id');
		var fact = $(this).data('fact');
		var url = $('#ajaxDetail').attr('href')+'?moid='+moid+'&factory='+fact;
		window.open(url);
	});
});
</script>

@endsection