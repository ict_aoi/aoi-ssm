@extends('layouts.app', ['active' => 'dashboard'])
@section('header')
<div class="page-header page-header-default">
	<div class="breadcrumb-line">
		<ul class="breadcrumb">
			<li><a href="#"><i class="icon-home2 position-left"></i> Home</a></li>
			<li class="active">Dashboard</li>
		</ul>
	</div>

</div>
@endsection

@section('content')
<div class="content">
	<!-- Main charts -->
	<div class="panel panel-flat">
		<div class="panel-heading">
			<div class="row">
				<div class="col-lg-9">
					<h4><b>Dashboard Details #{{$docno}} ( AOI {{$factory}} )</b></h4>
					<input type="text" name="txdoc" id="txdoc" class="hidden" value="{{$docno}}">
					<input type="text" name="txmoid" id="txmoid" class="hidden" value="{{$moid}}">
					<input type="text" name="txfact" id="txfac" class="hidden" value="{{$factory}}">
				</div>
			</div>
		</div>
		<div class="panel-body">
			<div class="panel-body">
				<div class="table-responsive">
					<table class ="table table-basic table-condensed" id="table-list">
						<thead>
							<tr>
								<th>#</th>
								<th>Barcode ID</th>
								<th>Document No</th>
								<th>Item</th>
								<th>Brand</th>
								<th>Status</th>
								<th>Location</th>
								<th></th>
							</tr>
						</thead>
					</table>
				</div>
			</div>
		</div>
	</div>

</div>
@endsection

@section('js')
<script type="text/javascript">
$(document).ready(function(){
	var table = $('#table-list').DataTable({
		processing:true,
		serverSide:true,
		deferRender:true,
		"searching": false,
		dom:'<"datatable-header"fBl><t><"datatable-footer"ip>',
		language: {
	        lengthMenu: '<span>Show:</span> _MENU_',
	        paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
	    },
	    ajax: {
	        type: 'GET',
	        url: "{{ route('home.ajaxGetDetail') }}",
	        data: function (d) {
	            return $.extend({},d,{
	                "moid": $('#txmoid').val(),
	                "factory": $('#txfac').val()
	            });
	        }
	    },
	    fnCreatedRow: function (row, data, index) {
	        var info = table.page.info();
	        var value = index+1+info.start;
	        $('td', row).eq(0).html(value);
	    },
	    columns: [
	        {data: null, sortable: false, orderable: false, searchable: false},
	        {data: 'barcode_id', name: 'barcode_id'},
	        {data: 'documentno', name: 'documentno'},
	        {data: 'item', name: 'item'},
	        {data: 'brand', name: 'brand'},
	        {data: 'status', name: 'status'},
	        {data: 'location', name: 'location'},
	        {data: 'rack', name: 'rack'}
	    ]
	});
});
</script>

@endsection