@extends('layouts.app', ['active' => 'sample_reciept'])
@section('header')
<div class="page-header page-header-default">
	<div class="breadcrumb-line">
		<ul class="breadcrumb">
			<li><a href="#"><i class="icon-home2 position-left"></i> Admin Sample</a></li>
			<li class="active">Sample Reciept</li>
		</ul>
	</div>
</div>
@endsection

@section('content')
<div class="content">


	<div class="row">
		<div class="panel panel-flat">
			<div class="panel-heading">

				<h3>Detail MO # {{$docno}}</h3>
				<input type="text" name="moid" id="moid" class="hidden" value="{{$moid}}">
			</div>
			<div class="panel-body">
				<div class="row">
					<div class="col-md-3">
						<label><b>Template</b></label>
						<select class="select form-control" id="temp_card">
							<option value="">--Choose Template--</option>
							<option value="aoi_tempt">Internal AOI (A4)</option>
							<option value="sealing">Sealing (F4)</option>
							<option value="porto">Porto (F4)</option>
							<option value="wash_test">Wash Test (F4)</option>
							<option value="counter_sample">Counter Sample (F4)</option>
						</select>
					</div>
					<div class="col-md-3">
						<button class="btn btn-default" id="btn-print" style="margin-top: 27px;"><i class="icon-printer4"></i> Print</button>
					</div>
					

				</div>
				<div class="table-responsive">
					<table class ="table table-basic table-condensed" id="table-list" width="100%">
						<thead>
							<tr>
								<th><input type="checkbox" name="checkall" id="checkall"></th>
								<th>Barcode ID</th>
								<th>Reciept Date</th>
								<th>Document No</th>
								<th>Item</th>
								<th>Brand</th>
								<th>Status</th>
								<th>Location</th>
							</tr>
						</thead>
					</table>
				</div>
			</div>
		</div>
	</div>



</div>
<a href="{{route('reciept.printBarcode')}}" id="printBarcode" target="_blank"></a>
@endsection



@section('js')
<script type="text/javascript">
$(document).ready(function() {

	$(window).on('load',function(){
		tableL.clear();
		tableL.draw();
	});

	var tableL = $('#table-list').DataTable({
		processing:true,
		serverSide:true,
		deferRender:true,
		autoWidth: true,
		dom:'<"datatable-header"fBl><t><"datatable-footer"ip>',
		language: {
	        search: '<span>Filter:</span> _INPUT_',
	        searchPlaceholder: 'Type to filter...',
	        lengthMenu: '<span>Show:</span> _MENU_',
	        paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
	    },
	    ajax: {
	        type: 'GET',
	        url: "{{ route('reciept.ajaxGetDetail') }}",
	        data: function (d) {
	            return $.extend({},d,{
	                "moid": $('#moid').val()
	            });
	        }
	    },
	    fnCreatedRow: function (row, data, index) {
	        var info = tableL.page.info();
	        // var value = index+1+info.start;
	        // $('td', row).eq(0).html(value);
	    },
	    columns: [
	        // {data: null, sortable: false, orderable: false, searchable: false},
	        {data: 'checkbox', name: 'checkbox', sortable:false, orderable:false,searchable:false},
	        {data: 'barcode_id', name: 'barcode_id'},
	        {data: 'reciept_date', name: 'reciept_date'},
	        {data: 'documentno', name: 'documentno'},
	        {data: 'item', name: 'item'},
	        {data: 'brand', name: 'brand'},
	        {data: 'status', name: 'status'},
	        {data: 'location', name: 'location'}
	    ]
	});	

	$('#btn-print').click(function(event){
		event.preventDefault();

		var data = "";
		var tempt = $('#temp_card').val();

		$('.barcode:checked').each(function() {
        
	        var str = $(this).val()+";"
	        data = data+str;
	    });

	    if (data=="" ||tempt=="") {
	    	alert(422,"Template or Barcode garment not selected ! ! !");
	    }else{
	    	var url = $('#printBarcode').attr('href')+'?tempt='+tempt+'&data='+data;
	    	window.open(url);

	    	location.reload();
	    }
	});

	$('#checkall').click(function(){
		if ($(this).is(':checked')) {
			$('#table-list .barcode').prop("checked",true);
		}else{
			$('#table-list .barcode').prop("checked",false);
		}
	});

	$('#table-list').on('blur','.input_date',function(event){
		event.preventDefault();

		var barcode = $(this).data('barcode');
		var olddate = $(this).data('dater');
		var date = $(this).val();

		if (date!=olddate) {
			$.ajaxSetup({
		        headers: {
		            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		        }
		    });
		    $.ajax({
		        type: 'post',
		        url : "{{ route('reciept.ajaxRecieptDate') }}",
		        data:{barcode_id:barcode,reciept_date:date},
		        success: function(response) {
		        	
		         	var notif = response.data;
		            alert(notif.status,notif.output);
		        },
		        error: function(response) {
		        	console.log(response);
		           // var notif = response.data;
		            alert(response.status,response.responseJSON['message']); 
		        }
		    });
		}
		
	});
});

function check(e){
	
	var date = e.getAttribute('data-reciept');
	var barc = e.getAttribute('data-barcode');
	var chb = document.getElementById(e.id);


	if (chb.checked==true && date=='') {
		alert(422,"Date Reciept Not Set ! ! !");
		chb.checked = false;
	}
}
</script>
@endsection
