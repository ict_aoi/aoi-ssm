@extends('layouts.app', ['active' => 'delete_reciept'])
@section('header')
<div class="page-header page-header-default">
	<div class="breadcrumb-line">
		<ul class="breadcrumb">
			<li><a href="#"><i class="icon-home2 position-left"></i> Admin Sample</a></li>
			<li class="active">Sample Reciept</li>
		</ul>
	</div>
</div>
@endsection

@section('content')
<div class="content">
	<div class="panel panel-flat">
		<div class="panel-heading">
			<form action="{{ route('reciept.ajaxGetDelete') }}" id="form-search">
				@csrf
				<div class="form-group" >
	                <label><b>Document NO</b></label>
	                <div class="input-group">
	                    <input type="text" class="form-control" name="docno" id="docno" required="" placeholder=" #Document Number">
	                    <div class="input-group-btn">
	                        <button type="submit" class="btn btn-primary">Filter</button>
	                    </div>
	                </div>
	            </div>
			</form>
		</div>
		<div class="panel-body">
			<div class="table-responsive">
				<table class ="table table-basic table-condensed" id="table-search" width="100%">
					<thead>
						<tr>
							<th>#</th>
							<th>Document No</th>
							<th>Item</th>
							<th>Brand</th>
							<th>Qty</th>
							<th>Date Reciept</th>
							<th>Action</th>
						</tr>
					</thead>
				</table>
			</div>
		</div>
	</div>
</div>


@endsection



@section('js')
<script type="text/javascript">
$(document).ready(function(){
	


	//by search
	var tableS = $('#table-search').DataTable({
		processing:true,
		serverSide:true,
		deferRender:true,
		autoWidth: true,
		dom:'<"datatable-header"fBl><t><"datatable-footer"ip>',
		language: {
	        search: '<span>Filter:</span> _INPUT_',
	        searchPlaceholder: 'Type to filter...',
	        lengthMenu: '<span>Show:</span> _MENU_',
	        paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
	    },
	    ajax: {
	        type: 'GET',
	        url: $('#form-search').attr('action'),
	        data: function (d) {
	            return $.extend({},d,{
	                "docno": $('#docno').val()
	            });
	        }
	    },
	    fnCreatedRow: function (row, data, index) {
	        var info = tableS.page.info();
	        var value = index+1+info.start;
	        $('td', row).eq(0).html(value);
	    },
	    columns: [
	        {data: null, sortable: false, orderable: false, searchable: false},
	        {data: 'documentno', name: 'documentno'},
	        {data: 'item', name: 'item'},
	        {data: 'brand', name: 'brand'},
	        {data: 'qty', name: 'qty'},
	        {data: 'date_reciept', name: 'date_reciept'},
	        {data: 'action', name: 'action', searchable:false, sortable:false, orderable:false}
	    ]
	});



	tableS.on('preDraw', function() {
		loading();
	    Pace.start();
	})
	.on('draw.dt', function() {
	    $.unblockUI();
	    Pace.stop();
	});

	$('#form-search').submit(function(event){
		event.preventDefault();
		
		tableS.clear();
		tableS.draw();
	});
	
});

function deletMo(e){
	var id = e.getAttribute('data-id');

	$.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
        type: 'post',
        url : "{{ route('reciept.ajaxDeleteMo') }}",
        data:{id:id},
        beforeSend : function(){
        	loading();
        },
        success: function(response) {

        	$.unblockUI();
         	var notif = response.data;
            alert(notif.status,notif.output);
            $('#form-search').submit();
        },
        error: function(response) {

        	$.unblockUI();
           var notif = response.data;
            alert(notif.status,notif.output);
            
        }
    });  
}




</script>
@endsection