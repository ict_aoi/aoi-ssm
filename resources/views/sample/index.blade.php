@extends('layouts.app', ['active' => 'sample_reciept'])
@section('header')
<div class="page-header page-header-default">
	<div class="breadcrumb-line">
		<ul class="breadcrumb">
			<li><a href="#"><i class="icon-home2 position-left"></i> Admin Sample</a></li>
			<li class="active">Sample Reciept</li>
		</ul>
	</div>
</div>
@endsection

@section('content')
<div class="content">
	<div class="row">
		<div class="panel panel-flat">
			<div class="panel-body">
				<label><b>Reservasion Method</b></label>
				<div class="form-group">
					<label class="radio-inline"><input type="radio" name="radio_status" checked="checked" value="upload"><b>By Upload</b></label>
		            <label class="radio-inline"><input type="radio" name="radio_status" value="search"><b>By Searching</b></label>
				</div>
			</div>
		</div>
	</div>
	<!-- choose metode -->

	<div class="row">
		<!-- by upload -->
		<div class="form-group " id="by_upload">
			<div class="panel panel-flat">
				<div class="panel-heading">
					<button id="btn-md-upload" class="btn btn-primary"><i class="icon-file-upload2"></i> UPLOAD</button>
					<button id="btn-template" class="btn bg-slate "><i class="icon-file-text3"></i> Template</button>
					<button id="btn-up-print" class="btn btn-success" ><i class="icon-printer4"></i> Print</button>
				</div>
				<div class="panel-body">
					<div class="table-responsive">
						<table class ="table table-basic table-condensed" id="table-upload" width="100%">
							<thead>
								<tr>
									<th><input type="checkbox" name="checkall" id="checkall"></th>
									<th>Document No</th>
									<th>Doc. Status</th>
									<th>Season</th>
									<th>Style</th>
									<th>Article</th>
									<th>Size</th>
									<th>Brand</th>
									<th>Status</th>
									<th>Qty</th>
								</tr>
							</thead>
						</table>
					</div>
				</div>
			</div>
		</div>
		<!-- by upload -->

		<!-- by search -->
		<div class="form-group hidden" id="by_search">
			<div class="panel panel-flat">
				<div class="panel-heading">
					<form action="{{ route('reciept.getMO') }}" id="form-search">
						@csrf
						<div class="row form-group" id="by_docno">
			                <label><b>Document NO</b></label>
			                <div class="input-group">
			                    <input type="text" class="form-control" name="docno" id="docno" placeholder="Document No.">
			                    <div class="input-group-btn">
			                        <button type="submit" class="btn btn-primary">Filter</button>
			                    </div>
			                </div>
			            </div>
			            <div class="row form-group hidden" id="by_style">
			                <label><b>Style</b></label>
			                <div class="input-group">
			                    <input type="text" class="form-control" name="s_style" id="s_style" placeholder="Style">
			                    <div class="input-group-btn">
			                        <button type="submit" class="btn btn-primary">Filter</button>
			                    </div>
			                </div>
			            </div>
			            <div class="row form-group">
			            	<label class="radio-inline"><input type="radio" name="radio_search" checked="checked" value="no_mo"><b>Document No</b></label>
		            		<label class="radio-inline"><input type="radio" name="radio_search" value="style"><b>Style</b></label>
			            </div>
					</form>
					<br>
					<hr>
				</div>
				<div class="panel-body">
					<div class="table-responsive">
						<table class ="table table-basic table-condensed" id="table-search" width="100%">
							<thead>
								<tr>
									<th>#</th>
									<th>Document No</th>
									<th>Doc. Status</th>
									<th>Season</th>
									<th>Style</th>
									<th>Article</th>
									<th>Size</th>
									<th>Brand</th>
									<th>Status</th>
									<th>Qty</th>
									<th>Action</th>
								</tr>
							</thead>
						</table>
					</div>
				</div>
			</div>
		</div>
		<!-- by search -->
	</div>



</div>

<a href="{{ route('reciept.templateUpload') }}" id="ajaxTemplateUpload"></a>
<a href="{{ route('reciept.printUploadBarcode') }}" id="printUploadBarcode"></a>
@endsection

@section('modal')
<div id="modal_reciept" class="modal fade">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h5 class="modal-title">Reciept Garment</h5>
			</div>

			<div class="modal-body">
				<form action="{{ route('reciept.reciept') }}" id="form-save">
					@csrf
					<div class="row">
						<div class="col-lg-4">
							<label><b>Document No</b></label>
							<input type="text" name="txdocno" id="txdocno" readonly="" class="form-control">
						</div>
						<div class="col-lg-4">
							<label><b>Season</b></label>
							<input type="text" name="txseason" id="txseason" readonly="" class="form-control">
						</div>
						<div class="col-lg-4">
							<label><b>Style</b></label>
							<input type="text" name="txstyle" id="txstyle" readonly="" class="form-control">
						</div>
						
					</div>
					<div class="row">
						<div class="col-lg-4">
							<label><b>Article</b></label>
							<input type="text" name="txarticle" id="txarticle" readonly="" class="form-control">
							<input type="text" name="txid" id="txid" class="hidden">
							<input type="text" name="txfac" id="txfac" class="hidden">
						</div>
						<div class="col-lg-4">
							<label><b>Size</b></label>
							<input type="text" name="txsize" id="txsize" readonly="" class="form-control">
						</div>
						<div class="col-lg-4">
							<label><b>Qty</b></label>
							<input type="text" name="txqty" id="txqty" readonly="" class="form-control">
						</div>
						
					</div>
					
					<div class="row">
						<!-- <div class="col-lg-6">
							<label><b>Reciept Date</b></label>
							<input type="text" name="datereciept" id="datereciept" class="form-control pickadate" required>
						</div> -->
						
						

						<div class="col-lg-3">
							<div class="checkbox">
								<label>
									<input type="checkbox" name="sets" id="sets"> <b>SETS</b>
								</label>
							</div>
						</div>
						
					</div>
					<div class="row" id="fabnosets">

						<div class="col-md-6">
							<label><b>Fabric</b></label>
							<input type="text" name="fabric" id="fabric" class="form-control" placeholder="Fabric Material">
						</div>
					</div>
					<div class="row hidden" id="fabsets">

						<div class="col-md-6">
							<label><b>Fabric TOP</b></label>
							<input type="text" name="fabrictop" id="fabrictop" class="form-control" placeholder="Fabric Material">
						</div>
						<div class="col-md-6" >
							<label><b>Fabric BOTTOM</b></label>
							<input type="text" name="fabricbot" id="fabricbot" class="form-control" placeholder="Fabric Material">
						</div>
					</div>
					<div class="row">
						
						<div class="col-lg-6">
							<label><b>T1 Merch</b></label>
							<input type="text" name="merch" id="merch" class="form-control" placeholder="T1 Merch" required>
						</div>
						<div class="col-lg-6">
							<label><b>T1 Technician</b></label>
							<!-- <input type="text" name="technician" id="technician" class="form-control" placeholder="T1 Technician" required> -->
							<select class="form-control select" id="technician" hidden="">
								<option value="">--Choose Technician--</option>
								<option value="PARSIANI">PARSIANI</option>
								<option value="WIDAYANTI">WIDAYANTI</option>
								<option value="RUSDA">RUSDA</option>
								<option value="NOVA">NOVA</option>
								<option value="JORGE">JORGE</option>
								<option value="AKBAR">AKBAR</option>
								<option value="WANDA">WANDA</option>
							</select>
							
						</div>
					</div>
					<div class="row">
						<label><b>Remark</b></label>
						<input type="text" name="remark" id="remark" class="form-control" placeholder="Remark (optional)">
					</div>
					<div class="row">
						<button type="submit" class="btn btn-primary" id="btn-save">Save</button>
						<button type="button" class="btn btn-default" id="btn-close">Close</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>


<div id="modal_upload" class="modal fade">
	<div class="modal-dialog modal-md">
		<div class="modal-content">
			<div class="modal-header">
				<center><h4>UPLOAD DOCUMENT NO</h4></center>
			</div>
			<div class="modal-body">
				<form action="{{ route('reciept.ajaxUpload') }}" method="POST" id="form_upload" enctype="multipart/form-data">
					@csrf
					<div class="row">
						<label><b>File Upload :</b></label>
					</div>
					<div class="row">
						<div class="col-lg-8">
							<input type="file" name="file" id="file" class="file-styled">
						</div>
						<div class="col-lg-4">
							<button class="btn btn-success" type="submit" id="btn-submit">Upload</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>

<div id="modal_print" class="modal fade">
	<div class="modal-dialog modal-md">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<center><h5 class="modal-title">PRINT HANG TAG GARMENT</h5></center>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-lg-6">
						<label><b>TEMPLATE HANGTAG</b></label>
						<select class="select form-control" id="temp_card">
							<option value="">--Choose Template--</option>
							<option value="aoi_tempt">Internal AOI (A4)</option>
							<option value="sealing">Sealing (F4)</option>
							<option value="porto">Porto (F4)</option>
							<option value="wash_test">Wash Test (F4)</option>
							<option value="counter_sample">Counter Sample (F4)</option>
						</select>
					</div>
					<div class="col-lg-6">
						<button class="btn btn-success" id="btn-print" style="margin-top: 27px;"><i class="icon-printer4"></i> PRINT</button>
					</div>
				</div>
				
			</div>
		</div>
	</div>
</div>
@endsection

@section('js')
<script type="text/javascript">
$(document).ready(function(){

	$('input[type=radio][name=radio_status]').change(function(){
		if (this.value=='upload') {
			if ($('#by_upload').hasClass('hidden')) {
				$('#by_upload').removeClass('hidden');
			}
			
			$('#by_search').addClass('hidden');
		}else if (this.value=='search') {
			if ($('#by_search').hasClass('hidden')) {
				$('#by_search').removeClass('hidden');
			}

			$('#by_upload').addClass('hidden');
		}
	});

	$('input[type=radio][name=radio_search]').change(function(){
		if (this.value=='no_mo') {
			if ($('#by_docno').hasClass('hidden')) {
				$('#by_docno').removeClass('hidden');
			}
			
			$('#by_style').addClass('hidden');
		}else if (this.value=='style') {
			if ($('#by_style').hasClass('hidden')) {
				$('#by_style').removeClass('hidden');
			}

			$('#by_docno').addClass('hidden');
		}
	});

	$('#sets').on('change',function(event){
		var checksets = $('input[name=sets]:checked').val();

		if (checksets=="on") {
			if ($('#fabsets').hasClass('hidden')) {
				$('#fabsets').removeClass('hidden');
			}
			$('#fabnosets').addClass('hidden');
		}else{
			if ($('#fabnosets').hasClass('hidden')) {
				$('#fabnosets').removeClass('hidden');
			}
			
			$('#fabsets').addClass('hidden');
		}
	});


	//by search
	var tableS = $('#table-search').DataTable({
		processing:true,
		serverSide:true,
		deferRender:true,
		autoWidth: true,
		dom:'<"datatable-header"fBl><t><"datatable-footer"ip>',
		language: {
	        search: '<span>Filter:</span> _INPUT_',
	        searchPlaceholder: 'Type to filter...',
	        lengthMenu: '<span>Show:</span> _MENU_',
	        paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
	    },
	    ajax: {
	        type: 'GET',
	        url: $('#form-search').attr('action'),
	        data: function (d) {
	            return $.extend({},d,{
	                "radio_search": $('input[name=radio_search]:checked').val(),
	                "docno": $('#docno').val(),
	                "s_style": $('#s_style').val()
	            });
	        }
	    },
	    fnCreatedRow: function (row, data, index) {
	        var info = tableS.page.info();
	        var value = index+1+info.start;
	        $('td', row).eq(0).html(value);
	    },
	    columns: [
	        {data: null, sortable: false, orderable: false, searchable: false},
	        {data: 'documentno', name: 'documentno'},
	        {data: 'docstatus', name: 'docstatus'},
	        {data: 'season', name: 'season'},
	        {data: 'style', name: 'style'},
	        {data: 'article', name: 'article'},
	        {data: 'size', name: 'size'},
	        {data: 'brand', name: 'brand'},
	        {data: 'status', name: 'status'},
	        {data: 'qty', name: 'qty'},
	        {data: 'action', name: 'action', searchable:false, sortable:false, orderable:false}
	    ]
	});



	tableS.on('preDraw', function() {
		loading();
	    Pace.start();
	})
	.on('draw.dt', function() {
	    $.unblockUI();
	    Pace.stop();
	});

	$('#form-search').submit(function(event){
		event.preventDefault();
		
		tableS.clear();
		tableS.draw();
	});

	$('#btn-close').click(function(){
		dispose();
	});

	$('#table-search').on('click','.reciept', function(event){
		event.preventDefault();

		var str = $(this).data('id');
		var id = str.split('$')[0];
		// var topbot = str.split('$')[1];
		// var date = str.split('$')[2];
		// var season = str.split('$')[1];
		// var style = str.split('$')[2];
		// var article = str.split('$')[3];
		// var size = str.split('$')[4];
		// var docno = str.split('$')[5];
		// // var factory_id = str.split('$')[6];
		// var qty = str.split('$')[6];
		// var date = str.split('$')[7];
		// var remark = str.split('$')[8];
		// var topbot = str.split('$')[9];

		$.ajaxSetup({
	        headers: {
	            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	        }
	    });
	    $.ajax({
	        type: 'get',
	        url : "{{ route('reciept.getDetMo') }}",
	        data:{id:id},
	        beforeSend : function(){
	        	loading();
	        },
	        success: function(response) {
	        	var data = response.data;
	   			var topbot = data['topbot'];

	        	if (data['is_reciept']==true) {
					// $('#datereciept').val(data['date_reciept']);
					$('#remark').val(data['remark']);
					// document.getElementById("datereciept").disabled = true;
					document.getElementById("remark").readOnly  = true;
					document.getElementById("btn-save").disabled = true;
					document.getElementById("fabric").readOnly  = true;
					document.getElementById("fabrictop").readOnly  = true;
					document.getElementById("fabricbot").readOnly  = true;
					document.getElementById("merch").readOnly  = true;
					document.getElementById("technician").disabled  = true;
					document.getElementById("sets").disabled  = true;

					
				}

				if (topbot===true) {
					
					document.getElementById("sets").checked = true;
					if ($('#fabsets').hasClass('hidden')) {
						$('#fabsets').removeClass('hidden');
					}
					$('#fabnosets').addClass('hidden');
					$('#fabrictop').val(data['fabric1']);
					$('#fabricbot').val(data['fabric2']);
				}else if (topbot===false){
					
					document.getElementById("sets").checked = false;
					if ($('#fabnosets').hasClass('hidden')) {
						$('#fabnosets').removeClass('hidden');
					}
					$('#fabsets').addClass('hidden');
					$('#fabric').val(data['fabric1']);
				}else{
					if ($('#fabnosets').hasClass('hidden')) {
						$('#fabnosets').removeClass('hidden');
					}
					$('#fabsets').addClass('hidden');
					document.getElementById("sets").checked = false;
				}

				$('#txid').val(data['id']);
				$('#txdocno').val(data['documentno']);
				$('#txseason').val(data['season']);
				$('#txstyle').val(data['style']);
				$('#txarticle').val(data['article']);
				$('#txsize').val(data['size']);
				// $('#txfac').val(factory_id);
				$('#txqty').val(data['qty']);
				
				$('#merch').val(data['merch']);
				$('#technician').val(data['technician']).trigger('change');
				


				$.unblockUI();
				$('#modal_reciept').modal('show');
	        },
	        error: function(response) {
	        	$.unblockUI();
	        	alert(422,response);
	        	console.log(response);
	            
	        }
	    });  

		
	});

	$('#form-save').submit(function(event){
		event.preventDefault();

		// if ($('#datereciept').val()==='') {
		// 	alert(422,"Date Reciept required ! ! !");

		// 	return false;
		// }

		if ($('#merch').val()==='') {
			alert(422,"T1 Merch required ! ! !");

			return false;
		}

		if ($('#technician').val()==='') {
			alert(422,"T1 Technician required ! ! !");

			return false;
		}

		var fabsets = $('input[name=sets]:checked').val();

		if (fabsets=="on") {
			var fabric1= $('#fabrictop').val();
			var fabric2= $('#fabricbot').val();

			if (fabric1==="" || fabric2==="") {
				alert(422,"Fabric TOP and BOTTOM required ! ! !");

				return false;
			}
		}else{
			var fabric1= $('#fabric').val();
			var fabric2= null;
			if (fabric1==="") {
				alert(422,"Fabric required ! ! !");

				return false;
			}
		}

		$.ajaxSetup({
	        headers: {
	            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	        }
	    });
	    $.ajax({
	        type: 'get',
	        url : $('#form-save').attr('action'),
	        data:{id:$('#txid').val(),qty:$('#txqty').val(),remark:$('#remark').val(),sets:$('input[name=sets]:checked').val(),merch:$('#merch').val(),tech:$('#technician').val(),fabric1:fabric1,fabric2:fabric2},
	        beforeSend : function(){
	        	loading();
	        },
	        success: function(response) {
	        	dispose();
	        	$('#form-search').submit();
	        	$.unblockUI();
	         	var notif = response.data;
	            alert(notif.status,notif.output);
	            $('#form-search').submit();
	        },
	        error: function(response) {
	        	dispose();
	        	$.unblockUI();
	           var notif = response.data;
	            alert(notif.status,notif.output);
	            
	        }
	    });  
	});

	
	//by search



	// by upload
	$('#btn-template').click(function(){
		window.location.href=$('#ajaxTemplateUpload').attr('href');
	});

	$('#btn-md-upload').click(function(){
		$('#modal_upload').modal('show');
	});

	$('#form_upload').submit(function(event){
		event.preventDefault();
		// var FormData =  new FormData(this);

		$.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: 'post',
            url : $('#form_upload').attr('action'),
            contentType: false,
            processData: false,
            data :new FormData(this) ,
            beforeSend: function() {
                loading();
            },
            success: function(response) {
                $.unblockUI();
                
                tbUpload(response.data);
                 
                $('#modal_upload').modal('hide');
                 
            },
            error: function(response) {
                $.unblockUI();
                alert(422,"Upload field ! ! ! ");
                console.log(response);
            }
        });

  		
	});
	var data;
	$('#btn-up-print').click(function(){
		// event.preventDefault();
		data = "";
		// var data = "";
		$('#modal_print').modal('show');
		
	});

	$('#btn-print').on('click',function(){
		$('.moid:checked').each(function() {
        
	        var str = $(this).val()+";"
	        data = data+str;
	    });

	    var temp = $('#temp_card').val();

	    if (data=="" || temp=="" ) {
	    	alert(422,"Template Garment Card Or Document number not selected ! ! !");
	    }else{
	    	var url = $('#printUploadBarcode').attr('href')+'?temp='+temp+'&data='+data;
	    	window.open(url);
	    	
	    }

	    
	})
	// by upload

	// checkall
	$('#checkall').click(function(){
		if ($(this).is(':checked')) {
			$('#table-upload .moid').prop("checked",true);
		}else{
			$('#table-upload .moid').prop("checked",false);
		}
	});

	
});


function dispose(){
	$('#modal_reciept').modal('hide');
    $('#txid').val('');
	$('#txdocno').val('');
	$('#txseason').val('');
	$('#txstyle').val('');
	$('#txarticle').val('');
	$('#txsize').val('');
	$('#txqty').val('');
	$('#datereciept').val('');
	$('#remark').val('');
	$('#fabric').val('');
	$('#merch').val('');
	$('#technician').val('').trigger('change');
	$('#fabric1').val('');
	$('#fabric2').val('');
	document.getElementById("sets").disabled  = false;
	document.getElementById("sets").checked = false;
	document.getElementById("fabric").disabled = false;
	document.getElementById("fabrictop").disabled = false;
	document.getElementById("fabricbot").disabled = false;
	document.getElementById("merch").disabled = false;
	document.getElementById("btn-save").disabled = false;
	document.getElementById("technician").disabled  = false;

	if ($('#fabnosets').hasClass('hidden')) {
		$('#fabnosets').removeClass('hidden');
	}
	$('#fabsets').addClass('hidden');
}

function tbUpload(moid){
	var tableU = $('#table-upload').DataTable({
		processing:true,
		serverSide:true,
		deferRender:true,
		autoWidth: true,
		dom:'<"datatable-header"fBl><t><"datatable-footer"ip>',
		language: {
	        search: '<span>Filter:</span> _INPUT_',
	        searchPlaceholder: 'Type to filter...',
	        lengthMenu: '<span>Show:</span> _MENU_',
	        paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
	    },
	    ajax: {
	        type: 'GET',
	        url: "{{ route('reciept.getDataUpload') }}",
	        data: function (d) {
	            return $.extend({},d,{
	                "moid": moid
	            });
	        }
	    },
	    fnCreatedRow: function (row, data, index) {
	        var info = tableU.page.info();
	        // var value = index+1+info.start;
	        // $('td', row).eq(0).html(value);
	    },
	    columns: [
	        // {data: null, sortable: false, orderable: false, searchable: false},
	        {data: 'checkbox', name: 'checkbox', sortable:false, orderable:false,searchable:false},
	        {data: 'documentno', name: 'documentno'},
	        {data: 'docstatus', name: 'docstatus'},
	        {data: 'season', name: 'season'},
	        {data: 'style', name: 'style'},
	        {data: 'article', name: 'article'},
	        {data: 'size', name: 'size'},
	        {data: 'brand', name: 'brand'},
	        {data: 'status', name: 'status'},
	        {data: 'qty', name: 'qty'}
	    ]
	});
}


</script>
@endsection
