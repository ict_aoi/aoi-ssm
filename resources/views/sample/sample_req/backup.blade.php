<style type="text/css">
@page{
    margin : 5 5 5 5;
    
}

/*@font-face {
   font-family: 'impact';
    font-weight: normal;
    font-style: normal;
    font-variant: normal;
    src: url("impact.ttf") format("truetype");
}
*/
/*table ,tr, td{
    border: 1px solid black;
}
*/

</style>

<table width="100%" >
    <tr rowspan>
        <td width="50%" style="border:2px solid black;" rowspan="2">
           <label style="font-size:25px; font-weight: bold;"><center>PT APPAREL ONE INDONESIA</center></label>
           <br>
           <center>
               <img src="{{ public_path('assets/icon/aoi.png') }}" alt="Image">
           </center>
           <br>
           <center>
               <label style="font-size:18px; font-weight: bold; ">Kawasan Berikat PT. Putra Wijayakusuma Sakti</label><br>
               <label style="font-size:18px; font-weight: bold;">Kawasan Indrustri Wijayakusuma Jalan Raya Semarang-Kendal Km 12 Block B-05,</label><br>
               <label style="font-size:18px; font-weight: bold;">Semarang – Jawa Tengah, Indonesia</label>
           </center>
        </td>
        <td width="50%" style="border:2px solid black;">
            <center>
                <label style="font-size:30px; font-weight:bold;">FORMULIR</label><br>
                <label style="font-size:30px; font-weight:bold;">CONFIDENTIAL</label>
            </center>
        </td>
    </tr>
    <tr>
        <td width="50%" style="border:2px solid black;">
            <center>
                <label style="font-size:30px; font-weight:bold;">SAMPLE REQUEST</label>
            </center>
        </td>
    </tr>

    <tr>
        <td>
            <table width="100%" style="margin-left: 20px;">
                <tr>
                    <td width="200">Documentno MO</td>
                    <td>:</td>
                    <td><b>{{$head->documentno}}</b></td>
                </tr>

                <tr>
                    <td width="200">Buyer</td>
                    <td>:</td>
                    <td><b>{{$head->buyer}} {{$head->season}}</b></td>
                </tr>

                <tr>
                    <td width="200">Style</td>
                    <td>:</td>
                    <td><b>{{$head->style}}</b></td>
                </tr>

                <tr>
                    <td width="200">Article</td>
                    <td>:</td>
                    <td><b>{{$head->article}}</b></td>
                </tr>

                <tr>
                    <td width="200">Description</td>
                    <td>:</td>
                    <td><b>{{$head->product_category}}</b></td>
                </tr>

                <tr>
                    <td width="200">Size</td>
                    <td>:</td>
                    <td><b>{{$head->size}}</b></td>
                </tr>

                <tr>
                    <td width="200">Quantity</td>
                    <td>:</td>
                    <td><b>{{$head->qty}}</b></td>
                </tr>

                <tr>
                    <td width="200">Tgl Pengiriman</td>
                    <td>:</td>
                    <td><b>{{date_format(date_create($head->shipment_date),'d-M-Y')}}</b></td>
                </tr>

                <tr>
                    <td width="200">NOTE</td>
                    <td>:</td>
                    <td><b>{{$head->description}}</b></td>
                </tr>
            </table>
        </td>
        <td height="100">
            <table width="100%" style="margin-left: 10px; margin-top: 0px;">
                <tr>
                    <td width="200">Worksheet / AD</td>
                    <td>:</td>
                    <td><b></b></td>
                </tr>

                <tr>
                    <td width="200">Sample Original</td>
                    <td>:</td>
                    <td><b></b></td>
                </tr>

                <tr>
                    <td width="200">Pattern</td>
                    <td>:</td>
                    <td><b></b></td>
                </tr>

                <tr>
                    <td colspan="3">
                        <center>
                             <img src="data:image/png;base64,{{$img}}" alt="Red dot" width="150" style="border: 3px solid black;" />
                        </center>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>



