@extends('layouts.app', ['active' => 'sample_csi'])
@section('header')
<div class="page-header page-header-default">
	<div class="breadcrumb-line">
		<ul class="breadcrumb">
			<li><a href="#"><i class="icon-home2 position-left"></i> Sample</a></li>
			<li class="active">Cutting Sewing Instruction</li>
		</ul>
	</div>
</div>
@endsection

@section('content')
<div class="content">

	<div class="panel panel-flat">
		<div class="panel-heading">
			<form action="{{ route('samreq.ajaxGetData') }}" id="form-search">
				@csrf
				<div class="row form-group hidden" id="by_docno">
	                <label><b>Document NO</b></label>
	                <div class="input-group">
	                    <input type="text" class="form-control" name="txdoc" id="txdoc" >
	                    <div class="input-group-btn">
	                        <button type="submit" class="btn btn-primary">Filter</button>
	                    </div>
	                </div>
	            </div>
	            <div class="row form-group hidden" id="by_style">
	                <label><b>Style</b></label>
	                <div class="input-group">
	                    <input type="text" class="form-control" name="txstyle" id="txstyle" >
	                    <div class="input-group-btn">
	                        <button type="submit" class="btn btn-primary">Filter</button>
	                    </div>
	                </div>
	            </div>
	            <div class="row form-group hidden" id="by_create">
	                <label><b>MO Created</b></label>
	                <div class="input-group">
	                    <input type="text" class="form-control daterange-basic" name="txmo" id="txmo" >
	                    <div class="input-group-btn">
	                        <button type="submit" class="btn btn-primary">Filter</button>
	                    </div>
	                </div>
	            </div>
	            <div class="row form-group hidden" id="by_shipmnt">
	                <label><b>Shipment Date</b></label>
	                <div class="input-group">
	                    <input type="text" class="form-control daterange-basic" name="txship" id="txship" >
	                    <div class="input-group-btn">
	                        <button type="submit" class="btn btn-primary">Filter</button>
	                    </div>
	                </div>
	            </div>
	            <div class="row form-group hidden" id="by_completed">
	                <label><b>MO Completed</b></label>
	                <div class="input-group">
	                    <input type="text" class="form-control daterange-basic" name="txcompl" id="txcompl" >
	                    <div class="input-group-btn">
	                        <button type="submit" class="btn btn-primary">Filter</button>
	                    </div>
	                </div>
	            </div>
	            <div class="row form-group " id="by_sink">
	                <label><b>Sinkron Date</b></label>
	                <div class="input-group">
	                    <input type="text" class="form-control daterange-basic" name="txsink" id="txsink" >
	                    <div class="input-group-btn">
	                        <button type="submit" class="btn btn-primary">Filter</button>
	                    </div>
	                </div>
	            </div>
	            <div class="row form-group">
	            	<label class="radio-inline"><input type="radio" name="radio_search" checked value="sinkron_date"><b>Sinkron Date</b></label>
	            	<label class="radio-inline"><input type="radio" name="radio_search" value="shipment_date"><b>Shipment Date</b></label>
	            	<label class="radio-inline"><input type="radio" name="radio_search" value="documentno"><b>Document No</b></label>
            		<label class="radio-inline"><input type="radio" name="radio_search" value="style"><b>Style</b></label>
            		<label class="radio-inline"><input type="radio" name="radio_search" value="mo_created"><b>MO Created</b></label>
            		<label class="radio-inline"><input type="radio" name="radio_search" value="completed_date"><b>MO Completed</b></label>
            		
	            </div>
			</form>
		</div>
		<div class="panel-body">
			<div class="table-responsive">
				<table class ="table table-basic table-condensed" id="table-list" width="100%">
					<thead>
						<tr>
							<th><input type="checkbox" name="checkall" id="checkall"></th>
							<th>Document No</th>
							<th>Buyer</th>
							<th>Style</th>
							<th>Article</th>
							<th>Status</th>
							<th>Desc</th>
							<th>Size</th>
							<th>Qty</th>
							<th>Ship Date</th>
							<th>Note</th>
						</tr>
					</thead>
				</table>
			</div>
		</div>
	</div>

</div>

@endsection

@section('js')
<script type="text/javascript" src="{{url('assets/js/plugins/tables/datatables/extensions/buttons.min.js')}}"></script>
<script type="text/javascript">
$(document).ready(function(){

	$('input[type=radio][name=radio_search]').change(function(){
		if (this.value=='documentno') {
			if ($('#by_docno').hasClass('hidden')) {
				$('#by_docno').removeClass('hidden');
			}
			
			$('#by_style').addClass('hidden');
			$('#by_create').addClass('hidden');
			$('#by_shipmnt').addClass('hidden');
			$('#by_sink').addClass('hidden');
			$('#by_completed').addClass('hidden');
		}else if (this.value=='style') {
			if ($('#by_style').hasClass('hidden')) {
				$('#by_style').removeClass('hidden');
			}

			$('#by_docno').addClass('hidden');
			$('#by_create').addClass('hidden');
			$('#by_shipmnt').addClass('hidden');
			$('#by_sink').addClass('hidden');
			$('#by_completed').addClass('hidden');
		}else if (this.value=='mo_created') {
			if ($('#by_create').hasClass('hidden')) {
				$('#by_create').removeClass('hidden');
			}

			$('#by_docno').addClass('hidden');
			$('#by_style').addClass('hidden');
			$('#by_shipmnt').addClass('hidden');
			$('#by_sink').addClass('hidden');
			$('#by_completed').addClass('hidden');
		}else if (this.value=='shipment_date') {
			if ($('#by_shipmnt').hasClass('hidden')) {
				$('#by_shipmnt').removeClass('hidden');
			}

			$('#by_docno').addClass('hidden');
			$('#by_style').addClass('hidden');
			$('#by_create').addClass('hidden');
			$('#by_sink').addClass('hidden');
			$('#by_completed').addClass('hidden');
		}else if (this.value=='sinkron_date') {
			if ($('#by_sink').hasClass('hidden')) {
				$('#by_sink').removeClass('hidden');
			}

			$('#by_docno').addClass('hidden');
			$('#by_style').addClass('hidden');
			$('#by_create').addClass('hidden');
			$('#by_shipmnt').addClass('hidden');
			$('#by_completed').addClass('hidden');
		}else if (this.value=='completed_date') {
			if ($('#by_completed').hasClass('hidden')) {
				$('#by_completed').removeClass('hidden');
			}

			$('#by_docno').addClass('hidden');
			$('#by_style').addClass('hidden');
			$('#by_create').addClass('hidden');
			$('#by_shipmnt').addClass('hidden');
			$('#by_sink').addClass('hidden');
		}
	});

	//by search
	var table = $('#table-list').DataTable({
		processing:true,
		serverSide:true,
		deferRender:true,
		autoWidth: true,
		dom:'<"datatable-header"fBl><t><"datatable-footer"ip>',
		language: {
	        search: '<span>Filter:</span> _INPUT_',
	        searchPlaceholder: 'Type to filter...',
	        lengthMenu: '<span>Show:</span> _MENU_',
	        paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
	    },
	    buttons: [
            {
                text: 'Donwload',
                className: 'btn btn-sm bg-success donwload'
            },
            {
                text: 'Notif',
                className: 'btn btn-sm bg-warning notif',
                action:function(){
                	window.open("{{ route('samreq.getNotif') }}");
                }
            },
	    ],
	    ajax: {
	        type: 'GET',
	        url: $('#form-search').attr('action'),
	        data: function (d) {
	            return $.extend({},d,{
	            	"radio_search": $('input[name=radio_search]:checked').val(),
	                "txdoc": $('#txdoc').val(),
	                "txstyle": $('#txstyle').val(),
	                "txmo": $('#txmo').val(),
	                "txship": $('#txship').val(),
	                "txsink":$('#txsink').val(),
	                "txcompl":$('#txcompl').val()
	            });
	        }
	    },
	    fnCreatedRow: function (row, data, index) {
	        var info = table.page.info();
	        var value = index+1+info.start;
	        // $('td', row).eq(0).html(value);
	    },
	    columns: [
	        // {data: null, sortable: false, orderable: false, searchable: false},
	        {data: 'checkbox', name: 'checkbox', searchable:false, sortable:false, orderable:false},
	        {data: 'documentno', name: 'documentno'},
	        {data: 'buyer', name: 'buyer'},
	        {data: 'style', name: 'style'},
	        {data: 'article', name: 'article'},
	        {data: 'status', name: 'status'},
	        {data: 'product_category', name: 'product_category'},
	        {data: 'size', name: 'size'},
	        {data: 'qty', name: 'qty'},
	        {data: 'shipment_date', name: 'shipment_date'},
	        {data: 'description', name: 'description'},	    ]
	});



	table.on('preDraw', function() {
		loading();
	    Pace.start();
	})
	.on('draw.dt', function() {
	    $.unblockUI();
	    Pace.stop();
	});

	$('#form-search').submit(function(event){
		event.preventDefault();
		
		table.clear();
		table.draw();
	});

	
	

	$('#checkall').click(function(){
		if ($(this).is(':checked')) {
			$('#table-list .srid').prop("checked",true);
		}else{
			$('#table-list .srid').prop("checked",false);
		}
	});



	$('.donwload').on('click',function(event){
		event.preventDefault();
		var data = [];
		var url = "{{ route('samreq.printcsi') }}";
		$('.srid:checked').each(function(){
			var str = $(this).val();
				data.push(str);

		});

		var link = url+'?id='+data.join(",");

		window.open(link);
	});

	
});




</script>
@endsection
