@section('modal')
<div id="modal_edit" class="modal fade">
	<div class="modal-dialog modal-full">
		<div class="modal-content">
			<div class="modal-body">
				<!-- <form action="{{ route('samreq.updateSampleReq') }}" method="POST" id="form_update" >
					@csrf -->
					<div class="row form-group">
						<div class="panel panel-flat">
							<div class="panel-body">
								<div class="row">
									<center><h3 id="txdoc" class="txdoc"></h3></center>
									<br>
								</div>
								<div class="row">
									<div class="col-md-2">
										<label><b>Buyer</b></label>
										<input type="text" name="txbuyer" class="form-control txbuyer" readonly>
										<input type="hidden" name="txid" class="form-control txid" >
									</div>
									<div class="col-md-2">
										<label><b>Season</b></label>
										<input type="text" name="txseason" class="form-control txseason" readonly>
									</div>
									<div class="col-md-2">
										<label><b>Style</b></label>
										<input type="text" name="txstyle" class="form-control txstyle" readonly>
									</div>
									<div class="col-md-2">
										<label><b>Article</b></label>
										<input type="text" name="txarticle" class="form-control txarticle" readonly>
									</div>
									<div class="col-md-2">
										<label><b>Size</b></label>
										<input type="text" name="txsize" class="form-control txsize" readonly>
									</div>
									<div class="col-md-2">
										<label><b>Qty</b></label>
										<input type="text" name="txqty" class="form-control txqty" readonly>
									</div>
								</div>
								<div class="row">
									<div class="col-md-2">
										<label><b>PIC MD</b></label>
										<input type="text" name="txpic" class="form-control txpic" readonly>
									</div>
									<div class="col-md-2">
										<label><b>Status</b></label>
										<!-- <input type="text" name="txstatus" class="form-control txstatus" readonly> -->
										<br>
                                        <label name="txstatus" class=" txstatus" ></label>
									</div>
									<div class="col-md-2">
										<label><b>Description</b></label>
										<input type="text" name="txdesc" class="form-control txdesc" readonly>
									</div>
									<div class="col-md-2">
										<label><b>Shipment Date</b></label>
										<input type="text" name="txship" class="form-control txship" readonly>
									</div>
									<div class="col-md-2">
										<label><b>Note</b></label>
										<br>
										<label name="txnote" class=" txnote" ></label>
									</div>
									<div class="col-md-2">
										<label><b>Allocation</b></label>
										<input type="text" name="txalloct" class="form-control txalloct" readonly>
									</div>
								</div>
							</div>
						</div>

						<div class="panel panel-flat">
							<div class="panel-body">
								<div class="row">
									<center><h3>PATTERN</h3></center>
								</div>
								<div class="row">
									<div class="col-md-2">
										<label><b>Pattern Follow Up</b></label>
										<input type="text" name="txpatternfu" class="form-control txpatternfu" style="text-transform: uppercase;">
									</div>
									<div class="col-md-4">
										<label><b>Pattern Remark</b></label>
										<input type="text" name="txpatternremk" class="form-control txpatternremk">
									</div>
									<div class="col-md-2">
										<label><b>PatternMaker</b></label>
										<input type="text" name="txpatternmarker" class="form-control txpatternmarker">
										<!-- <textarea name="txpatternmarker" class="form-control txpatternmarker" style="height: 80px;"></textarea> -->

									</div>
									<div class="col-md-2">
										<label><b>Pattern Stage</b></label>
										<input type="text" name="txpatternstage" class="form-control txpatternstage" style="text-transform:uppercase;">
									</div>
									<div class="col-md-2">
										<label><b>Pattern Status</b></label>
										<select class="select txpatternstat" id="txpatternstat">
											<option value="OPEN">OPEN</option>
											<option value="CLOSE">CLOSE</option>
										</select>
									</div>
									
								</div>
							</div>
						</div>

						<div class="panel panel-flat">
							<div class="panel-body">
								<div class="row">
									<center><h3>MARKER</h3></center>
								</div>
								<div class="row">
									<div class="col-md-4">
										<label><b>Date Pattern</b></label>
										<input type="text" name="txpatterndate" class="form-control pickadate txpatterndate">
									</div>
									<div class="col-md-4">
										<label><b>Date Marker Plot</b></label>
										<input type="text" name="txplotmarker" class="form-control pickadate txplotmarker">
									</div>
									<div class="col-md-4">
										<label><b>Date Marker Release</b></label>
										<input type="text" name="txmarkerrelease" class="form-control pickadate txmarkerrelease">
									</div>
								</div>
								<div class="row">
									<div class="col-md-4">
										<label><b>PIC Marker</b></label>
										<input type="text" name="txpicmarker" class="form-control txpicmarker" style="text-transform:uppercase;">
									</div>
									
									<div class="col-md-4">
										<label><b>Marker Length (inc)</b></label>
										<textarea name="txmarkerlength" class="form-control txmarkerlength" style="height: 80px;"></textarea>

									</div>
									<div class="col-md-4">
										<label><b>Marker Status</b></label>
										<select class="select txmarkerstat" id="txmarkerstat">
											<option value="OPEN">OPEN</option>
											<option value="CLOSE">CLOSE</option>
										</select>
									</div>
									
								</div>

							</div>
						</div>

						<div class="panel panel-flat">
							<div class="panel-body">
								<div class="row">
									<center><h3>SAMPLE ROOM</h3></center>
								</div>
								<div class="row">
									<div class="col-md-3"></div>
									<div class="col-md-3">
										<label><b>Recever</b></label>
										<input type="text" name="txrecever" class="form-control txrecever" style="text-transform: uppercase;">
									</div>
									<div class="col-md-3">
										<label><b>Recieved Marker</b></label>
										<input type="text" name="txsamplerecv" class="form-control pickadate txsamplerecv">
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="row form-group">
						<center>
							<button type="submit" class="btn btn-primary" id="btn-save" >UPDATE</button>
						</center>
					</div>

				<!-- </form> -->
			</div>
		</div>
	</div>
</div>
@endsection