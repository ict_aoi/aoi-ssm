<style type="text/css">
@page{
    margin : 20 10 20 10;
    /*background-image: url('assets/icon/confidential.png');*/
}

/*body{
    background-image: url('assets/icon/confidential.png');
    background-size: 100%;
    background-repeat: no-repeat;
    position: relative;
}*/
@font-face {
    font-family: 'calibri' !important;
    src: url({{ storage_path('fonts/calibri-regular.ttf') }}) format('truetype');
}
/*table ,tr, td{
    border: 1px solid black;
}
*/
.img_area{
    width: 280px;
    height: 160px;
    border: 2px solid black;
    /*padding: 0 0 0 0;*/
    position: absolute;
/*    margin-left: 35px;
    margin-top: 10px;*/
    padding-top: 5 5 5 5;
}

.tab-head {
  border-collapse: collapse;
  font-family: sans-serif;
  /*display: block;*/
  margin-left: 10px;
}
.tab-head tr  td{
    padding-top: 5px;
    vertical-align: top;
}

.tb-detl {
  border-collapse: collapse;
  font-family: sans-serif;
  margin-top: 35px;
  /*display: block;*/
}

.tb-detl tr td {
  border: 2px solid black;
  padding-left: 2px;
  padding-right: 2px;
  word-wrap: break-word;
}

.dtlab{
    font-size: 11px;
    font-family: arial;
}

.yield{
    /*text-align: left;*/
     font-size: 11px;
    font-family: arial;
}

.lanstd{
    font-size:13px;
    word-wrap: break-word;
}

.lanbld{
    font-size:13px;
    font-weight: bold;
}

.aoi{
    font-family: sans-serif;
   /* display: block;*/
}

.img_style{
    /*margin: 10 10 10 10;*/
    height: 100%;
    /*width: 100%;*/
    max-width: 270px;
    max-width: 150px;
    object-fit: contain;
    vertical-align: middle;
    align-content: center;
}

.page-break {
    page-break-after: always;
}

.watermark{
    position: fixed;
    color: red;
    opacity: 0.2;
    z-index: 1;
    font-size: 120px;
    margin-top: 500px;
    margin-left: 75px;
    font-family: calibri;
    transform: rotate(-20deg);
}
</style>
<body>

    <label class="watermark">CONFIDENTIAL</label>
    <main>
        @foreach($samreq as $srq)

            <table width="100%" class="aoi" >
                <tr rowspan>
                    <td width="50%" style="border:2px solid black;" rowspan="2">
                       <label style="font-size:18px; font-weight: bold;"><center>PT APPAREL ONE INDONESIA</center></label>
                       <br>
                       <center>
                           <img src="{{ public_path('assets/icon/aoi.png') }}" width="125" alt="Image">
                       </center>
                       <br>
                       <center>
                           <label style="font-size:9.5px; font-weight: bold; display: block;">Kawasan Berikat PT. Putra Wijayakusuma Sakti</label>
                           <label style="font-size:9.5px; font-weight: bold; display: block;">Kawasan Indrustri Wijayakusuma Jalan Raya Semarang-Kendal Km 12 Block B-05,</label>
                           <label style="font-size:9.5px; font-weight: bold; display: block;">Semarang – Jawa Tengah, Indonesia</label>
                       </center>
                    </td>
                    <td width="50%" style="border:2px solid black;">
                        <center>
                            <label style="font-size:25px; font-weight:bold;">FORMULIR</label><br>
                            <label style="font-size:25px; font-weight:bold;">CONFIDENTIAL</label>
                        </center>
                    </td>
                </tr>
                <tr>
                    <td width="50%" style="border:2px solid black;">
                        <center>
                            <label style="font-size:25px; font-weight:bold;">SAMPLE REQUEST</label>
                        </center>
                    </td>
                </tr>

            </table>

            <table width="100%" class="tab-head">
                <tr>
                    <td width="140">
                        <label class="lanstd">
                            Documentno MO
                        </label>
                    </td>
                    <td>
                        <label class="lanstd">
                            :
                        </label>
                    </td>
                    <td width="140">

                        <label class="lanbld">
                            {{$srq['head']->documentno}}
                        </label>
                    </td>
                    <td>
                        <label class="lanstd">
                            Worksheet / AD
                        </label>
                    </td>
                    <td>
                        <label class="lanstd">
                            :
                        </label>
                    </td>
                    <td>

                    </td>
                </tr>

                <tr>
                    <td width="140">
                        <label class="lanstd">
                            Buyer
                        </label>
                    </td>
                    <td>
                        <label class="lanstd">
                            :
                        </label>
                    </td>
                    <td width="140">

                        <label class="lanbld">
                           {{$srq['head']->buyer}} {{$srq['head']->season}} {{$srq['head']->color}}
                        </label>
                    </td>
                    <td>

                        <label class="lanstd">
                            Sample Original
                        </label>
                    </td>
                    <td>
                        <label class="lanstd">
                            :
                        </label>
                    </td>
                    <td>

                    </td>
                </tr>

                <tr>
                    <td width="140">

                        <label class="lanstd">
                            Style
                        </label>
                    </td>
                    <td>
                        <label class="lanstd">
                            :
                        </label>
                    </td>
                    <td width="140">

                        <label class="lanbld">
                           {{$srq['head']->style}}
                        </label>
                    </td>
                    <td>

                        <label class="lanstd">
                            Pattern
                        </label>
                    </td>
                    <td>
                        <label class="lanstd">
                            :
                        </label>
                    </td>
                    <td>

                    </td>
                </tr>

                <tr>
                    <td width="140">

                        <label class="lanstd">
                            Article
                        </label>
                    </td>
                    <td>
                        <label class="lanstd">
                            :
                        </label>
                    </td>
                    <td width="140">

                        <label class="lanbld">
                           {{$srq['head']->article}}
                        </label>
                    </td>
                    <td colspan="3" rowspan="6">
                        <center>
                            <div class="img_area">
                                @if($srq['img']!=null)
                                    <img class="img_style" src="data:image/png;base64,{{$srq['img']->binarydata}}" alt="Red dot"  />
                                @endif
                            </div>
                        </center>
                    </td>
                </tr>

                <tr>
                    <td width="140">

                        <label class="lanstd">
                            Status Sample
                        </label>
                    </td>
                    <td>
                        <label class="lanstd">
                            :
                        </label>
                    </td>
                    <td width="140">

                        <label class="lanbld">
                           {{$srq['head']->status}}
                        </label>
                    </td>
                </tr>

                <tr>
                    <td width="140">

                        <label class="lanstd">
                            Description
                        </label>
                    </td>
                    <td>
                        <label class="lanstd">
                            :
                        </label>
                    </td>
                    <td width="140">

                        <label class="lanbld">
                           {{$srq['head']->product_category}}
                        </label>
                    </td>
                </tr>

                <tr>
                    <td width="140">

                        <label class="lanstd">
                            Size
                        </label>
                    </td>
                    <td>
                        <label class="lanstd">
                            :
                        </label>
                    </td>
                    <td width="140">

                        <label class="lanbld">
                           {{$srq['head']->size}}
                        </label>
                    </td>
                </tr>

                <tr>
                    <td width="140">

                        <label class="lanstd">
                            Quantity
                        </label>
                    </td>
                    <td>
                        <label class="lanstd">
                            :
                        </label>
                    </td>
                    <td width="140">

                        <label class="lanbld">
                           {{$srq['head']->qty}}
                        </label>
                    </td>
                </tr>

                <tr>
                    <td width="140">

                        <label class="lanstd">
                            Tgl Pengiriman
                        </label>
                    </td>
                    <td>
                        <label class="lanstd">
                            :
                        </label>
                    </td>
                    <td width="140">

                        <label class="lanbld">
                           {{$srq['head']->shipment_date}}
                        </label>
                    </td>
                </tr>

                <tr>
                    <td width="140">

                        <label class="lanstd">
                            NOTE
                        </label>
                    </td>
                    <td>
                        <label class="lanstd">
                            :
                        </label>
                    </td>
                    <td width="140">

                        <label style="font-size:14px; font-weight:bold; word-wrap: break-word;">
                           {{$srq['head']->description}}
                        </label>

                    </td>
                </tr>




            </table>
<!-- <br> -->
            <table width="100%"  class="tb-detl" >
                <tr>
                    <td><center><label style="font-size:13px; font-weight:bold;">Part Name</label></center></td>
                    <td width="35px"><center><label style="font-size:13px; font-weight:bold;">Part No</label></center></td>
                    <td width="100px"><center><label style="font-size:13px; font-weight:bold;">Material</label></center></td>
                    <td><center><label style="font-size:13px; font-weight:bold;">Yield</label></center></td>
                    <td><center><label style="font-size:13px; font-weight:bold;">UOM</label></center></td>
                    <td><center><label style="font-size:13px; font-weight:bold;">Color</label></center></td>
                    <td width="100px"><center><label style="font-size:13px; font-weight:bold;">Description</label></center></td>
                    <td><center><label style="font-size:13px; font-weight:bold;">Width</label></center></td>
                    <td><center><label style="font-size:13px; font-weight:bold;">Note</label></center></td>
                </tr>
                <tr>
                    <td colspan="9" style="background-color: #80d4ff;"><label style="padding-left:10px; font-size: 14px; font-weight:bold;">Main Component</label></td>
                </tr>

                   @foreach($srq['main'] as $mn)
                      <tr>
                          <td><label class="dtlab">{{$mn->partname}}</label></td>
                          <td><label class="dtlab"><center>{{$mn->partno}}</center></label></td>
                          <td><label class="dtlab">{{$mn->material}}</label></td>
                          <td style="text-align: right;"><label class="yield">{{$mn->yield}}</label></td>
                          <td><label class="dtlab"><center>{{$mn->uom}}</center></label></td>
                          <td><label class="dtlab"><center>{{$mn->color}}</center></label></td>
                          <td><label class="dtlab">{{$mn->description}}</label></td>
                          <td><label class="dtlab"><center>{{$mn->width}}</center></label></td>
                          <td><label class="dtlab">{{$mn->note}}</label></td>
                      </tr>
                   @endforeach

                <tr>

                    <td colspan="9" style="background-color: #80d4ff;"><label style="padding-left:10px; font-size: 14px; font-weight:bold;">Trim</label></td>
                </tr>

                   @foreach($srq['trim'] as $tr)
                      <tr>
                          <td><label class="dtlab">{{$tr->partname}}</label></td>
                          <td><label class="dtlab"><center>{{$tr->partno}}</center></label></td>
                          <td><label class="dtlab">{{$tr->material}}</label></td>
                          <td style="text-align: right;"><label class="yield">{{$tr->yield}}</label></td>
                          <td><label class="dtlab"><center>{{$tr->uom}}</center></label></td>
                          <td><label class="dtlab"><center>{{$tr->color}}</center></label></td>
                          <td><label class="dtlab">{{$tr->description}}</label></td>
                          <td><label class="dtlab"><center>{{$tr->width}}</center></label></td>
                          <td><label class="dtlab">{{$tr->note}}</label></td>
                      </tr>
                   @endforeach

                <tr>

                     <td colspan="9" style="background-color: #80d4ff;"><label style="padding-left:10px; font-size: 14px; font-weight:bold;">Label</label></td>
                </tr>

                   @foreach($srq['labl'] as $lb)
                      <tr>
                          <td><label class="dtlab">{{$lb->partname}}</label></td>
                          <td><label class="dtlab"><center>{{$lb->partno}}</center></label></td>
                          <td><label class="dtlab">{{$lb->material}}</label></td>
                          <td style="text-align: right;"><label class="yield">{{$lb->yield}}</label></td>
                          <td><label class="dtlab"><center>{{$lb->uom}}</center></label></td>
                          <td><label class="dtlab"><center>{{$lb->color}}</center></label></td>
                          <td><label class="dtlab">{{$lb->description}}</label></td>
                          <td><label class="dtlab"><center>{{$lb->width}}</center></label></td>
                          <td><label class="dtlab">{{$lb->note}}</label></td>
                      </tr>
                   @endforeach

            </table>

            <table style="font-family: sans-serif; sans-serif; font-size: 12px; margin-top: 20px; page-break-inside: avoid;">
                <tr>
                    <td colspan="2">
                        <label><b>Note / Comment :</b></label>
                    </td>
                </tr>
                <tr>
                    <td colspan="3">
                        <label style="text-decoration: underline;"><b>Report yang diperlukan dari TE</b></label>
                    </td>
                </tr>
                <tr>

                    <td ><label style="padding-left: 20px;">Time Study</label></td>
                    <td>:</td>
                </tr>
                <tr>

                    <td ><label style="padding-left: 20px;">Consumption Benang</label></td>
                    <td>:</td>
                </tr>
                <tr>

                    <td ><label style="padding-left: 20px;">Sewing Instruction</label></td>
                    <td>:</td>
                </tr>
                <tr>

                    <td><label style="padding-left: 20px;">Dibuat Oleh</label></td>
                    <!-- <td>:</td> -->
                    <td><label>: <b>{{$srq['head']->mo_pic}}</b></label></td>
                </tr>
                <tr>

                    <td><label style="padding-left: 20px;">Diterima Oleh</label></td>
                    <td><label>: <b>Sample Room</b></label></td>
                </tr>
                <tr>
                    <td colspan="2">
                        <table style="border-collapse: collapse; margin-left:25px; margin-top: 10px; font-family: sans-serif; font-size: 12px;">
                            <tr>
                                <td>
                                    <label><center>Part 10</center></label>
                                </td>
                                <td>
                                    <label><center>Part 20</center></label>

                                </td>
                                <td>
                                    <label><center>Part 30</center></label>

                                </td>
                                <td>
                                    <label><center>Part 40</center></label>

                                </td>
                            </tr>
                            <tr>
                                <td style=" border: 2px solid black; width: 65px; height:45px;">

                                </td>
                                <td style=" border: 2px solid black; width: 65px; height:45px;">

                                </td>
                                <td style=" border: 2px solid black; width: 65px; height:45px;">

                                </td>
                                <td style=" border: 2px solid black; width: 65px; height:45px;">

                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>

            <div class="page-break"></div>
        @endforeach
    </main>

</body>

