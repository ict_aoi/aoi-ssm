@extends('layouts.app', ['active' => 'report_request'])
@section('header')
<div class="page-header page-header-default">
	<div class="breadcrumb-line">
		<ul class="breadcrumb">
			<li><a href="#"><i class="icon-home2 position-left"></i> Sample</a></li>
			<li class="active">Report Request</li>
		</ul>
	</div>
</div>
@endsection

@section('content')
<div class="content">

	<div class="panel panel-flat">
		<div class="panel-heading">
			<form action="{{ route('samreq.getDataReq') }}" id="form-search">
				@csrf
				<div class="row form-group hidden" id="by_docno">
	                <label><b>Document NO</b></label>
	                <div class="input-group">
	                    <input type="text" class="form-control" name="txdocf" id="txdocf" >
	                    <div class="input-group-btn">
	                        <button type="submit" class="btn btn-primary">Filter</button>
	                    </div>
	                </div>
	            </div>
	            <div class="row form-group hidden" id="by_style">
	                <label><b>Style</b></label>
	                <div class="input-group">
	                    <input type="text" class="form-control" name="txstylef" id="txstylef" >
	                    <div class="input-group-btn">
	                        <button type="submit" class="btn btn-primary">Filter</button>
	                    </div>
	                </div>
	            </div>
	            <div class="row form-group hidden" id="by_create">
	                <label><b>MO Created</b></label>
	                <div class="input-group">
	                    <input type="text" class="form-control daterange-basic" name="txmof" id="txmof" >
	                    <div class="input-group-btn">
	                        <button type="submit" class="btn btn-primary">Filter</button>
	                    </div>
	                </div>
	            </div>
	            <div class="row form-group hidden" id="by_shipmnt">
	                <label><b>Shipment Date</b></label>
	                <div class="input-group">
	                    <input type="text" class="form-control daterange-basic" name="txshipf" id="txshipf" >
	                    <div class="input-group-btn">
	                        <button type="submit" class="btn btn-primary">Filter</button>
	                    </div>
	                </div>
	            </div>
	            <div class="row form-group hidden" id="by_completed">
	                <label><b>MO Completed</b></label>
	                <div class="input-group">
	                    <input type="text" class="form-control daterange-basic" name="txcomplf" id="txcomplf" >
	                    <div class="input-group-btn">
	                        <button type="submit" class="btn btn-primary">Filter</button>
	                    </div>
	                </div>
	            </div>
	            <div class="row form-group " id="by_sink">
	                <label><b>Sinkron Date</b></label>
	                <div class="input-group">
	                    <input type="text" class="form-control daterange-basic" name="txsinkf" id="txsinkf" >
	                    <div class="input-group-btn">
	                        <button type="submit" class="btn btn-primary">Filter</button>
	                    </div>
	                </div>
	            </div>
	            <div class="row form-group">
	            	<label class="radio-inline"><input type="radio" name="radio_search" checked value="sinkron_date"><b>Sinkron Date</b></label>
	            	<label class="radio-inline"><input type="radio" name="radio_search" value="shipment_date"><b>Shipment Date</b></label>
	            	<label class="radio-inline"><input type="radio" name="radio_search" value="documentno"><b>Document No</b></label>
            		<label class="radio-inline"><input type="radio" name="radio_search" value="style"><b>Style</b></label>
            		<label class="radio-inline"><input type="radio" name="radio_search" value="mo_created"><b>MO Created</b></label>
            		<label class="radio-inline"><input type="radio" name="radio_search" value="completed_date"><b>MO Completed</b></label>

	            </div>
			</form>
		</div>
		<div class="panel-body">
			<div class="table-responsive">
				<table class ="table table-basic table-condensed" id="table-list" width="100%">
					<thead>
						<tr>
							<th>#</th>
							<th>Modified</th>
							<th>Season</th>
							<th>PIC MD</th>
							<th>Doc. No.</th>
							<th>Buyer</th>
							<th>Style</th>
							<th>Article</th>
							<th>Status</th>
							<th>Description</th>
							<th>Size</th>
							<th>Qty</th>
							<th>Ship Date</th>
							<th>Note</th>
							<th>Allocation</th>
							<th>Pattern Follow Up</th>
							<th>Pattern Status</th>
							<th>Pattern Remark</th>
							<th>PIC Marker</th>
							<th>Patternmaker</th>
							<th>Date Pattern</th>
							<th>Pattern Stage</th>
							<th>Marker Length</th>
							<th>Date Plot</th>
							<th>Marker Release</th>
							<th>Marker Status</th>
							<th>Recever</th>
							<th>Marker Recived Date</th>
							<!-- <th>Marker Need</th> -->
						</tr>
					</thead>
				</table>
			</div>
		</div>
	</div>

</div>
<a href="{{ route('samreq.exportReq') }}" id="exportExcel"></a>
@endsection

@include('sample/sample_req/md_reportreq')

@section('js')
<script type="text/javascript" src="{{url('assets/js/plugins/tables/datatables/extensions/buttons.min.js')}}"></script>
<script type="text/javascript">
$(document).ready(function(){

	$('input[type=radio][name=radio_search]').change(function(){
		if (this.value=='documentno') {
			if ($('#by_docno').hasClass('hidden')) {
				$('#by_docno').removeClass('hidden');
			}

			$('#by_style').addClass('hidden');
			$('#by_create').addClass('hidden');
			$('#by_shipmnt').addClass('hidden');
			$('#by_sink').addClass('hidden');
			$('#by_completed').addClass('hidden');
		}else if (this.value=='style') {
			if ($('#by_style').hasClass('hidden')) {
				$('#by_style').removeClass('hidden');
			}

			$('#by_docno').addClass('hidden');
			$('#by_create').addClass('hidden');
			$('#by_shipmnt').addClass('hidden');
			$('#by_sink').addClass('hidden');
			$('#by_completed').addClass('hidden');
		}else if (this.value=='mo_created') {
			if ($('#by_create').hasClass('hidden')) {
				$('#by_create').removeClass('hidden');
			}

			$('#by_docno').addClass('hidden');
			$('#by_style').addClass('hidden');
			$('#by_shipmnt').addClass('hidden');
			$('#by_sink').addClass('hidden');
			$('#by_completed').addClass('hidden');
		}else if (this.value=='shipment_date') {
			if ($('#by_shipmnt').hasClass('hidden')) {
				$('#by_shipmnt').removeClass('hidden');
			}

			$('#by_docno').addClass('hidden');
			$('#by_style').addClass('hidden');
			$('#by_create').addClass('hidden');
			$('#by_sink').addClass('hidden');
			$('#by_completed').addClass('hidden');
		}else if (this.value=='sinkron_date') {
			if ($('#by_sink').hasClass('hidden')) {
				$('#by_sink').removeClass('hidden');
			}

			$('#by_docno').addClass('hidden');
			$('#by_style').addClass('hidden');
			$('#by_create').addClass('hidden');
			$('#by_shipmnt').addClass('hidden');
			$('#by_completed').addClass('hidden');
		}else if (this.value=='completed_date') {
			if ($('#by_completed').hasClass('hidden')) {
				$('#by_completed').removeClass('hidden');
			}

			$('#by_docno').addClass('hidden');
			$('#by_style').addClass('hidden');
			$('#by_create').addClass('hidden');
			$('#by_shipmnt').addClass('hidden');
			$('#by_sink').addClass('hidden');
		}
	});

	//by search
	var table = $('#table-list').DataTable({
		processing:true,
		serverSide:true,
		deferRender:true,
		autoWidth: true,
		dom:'<"datatable-header"fBl><t><"datatable-footer"ip>',
		language: {
	        search: '<span>Filter:</span> _INPUT_',
	        searchPlaceholder: 'Type to filter...',
	        lengthMenu: '<span>Show:</span> _MENU_',
	        paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
	    },
	    buttons: [
            {
                text: 'Export Excel',
                className: 'btn btn-sm bg-success export',
                action: function (e, dt, node, config)
                {
                    var filter = table.search();
                    window.location.href = $('#exportExcel').attr('href')
                                            + '?radio_search=' + $('input[name=radio_search]:checked').val()
                                            + '&filterby=' + filter
                                            + '&txdoc='+ $('#txdocf').val()
                                            + '&txstyle='+ $('#txstylef').val()
                                            + '&txmo='+ $('#txmof').val()
                                            + '&txship='+ $('#txshipf').val()
                                            + '&txsink='+ $('#txsinkf').val()
                                            + '&txcompl='+ $('#txcomplf').val();

                }
            },
            {
                text: 'Notif',
                className: 'btn btn-sm bg-warning notif',
                action:function(){
                	window.open("{{ route('samreq.getNotif') }}");
                }
            },
	    ],
	    ajax: {
	        type: 'GET',
	        url: $('#form-search').attr('action'),
	        data: function (d) {
	            return $.extend({},d,{
	            	"radio_search": $('input[name=radio_search]:checked').val(),
	                "txdoc": $('#txdocf').val(),
	                "txstyle": $('#txstylef').val(),
	                "txmo": $('#txmof').val(),
	                "txship": $('#txshipf').val(),
	                "txsink":$('#txsinkf').val(),
	                "txcompl":$('#txcomplf').val()
	            });
	        }
	    },
	    fnCreatedRow: function (row, data, index) {
	        var info = table.page.info();
	        var value = index+1+info.start;
	        $('td', row).eq(0).html(value);
	        $('td', row).eq(1).css('min-width','200px');
	    },
	    columns: [
	        {data: null, sortable: false, orderable: false, searchable: false},
	        {data: 'created_at', name: 'created_at'},
	        {data: 'season', name: 'season'},
	        {data: 'mo_pic', name: 'mo_pic'},
	        {data: 'documentno', name: 'documentno'},
	        {data: 'buyer', name: 'buyer'},
	        {data: 'style', name: 'style'},
	        {data: 'article', name: 'article'},
	        {data: 'product_category', name: 'product_category'},
	        {data: 'status', name: 'status'},
	        {data: 'size', name: 'size'},
	        {data: 'qty', name: 'qty'},
	        {data: 'shipment_date', name: 'shipment_date'},
	        {data: 'description', name: 'description'},
	        {data: 'allocation', name: 'allocation'},
	        {data: 'pattern_fu', name: 'pattern_fu'},
	        {data: 'pattern_status', name: 'pattern_status'},
	        {data: 'pattern_remark', name: 'pattern_remark'},
	        {data: 'pic_marker', name: 'pic_marker'},
	        {data: 'pattern_marker', name: 'pattern_marker'},
	        {data: 'pattern_date', name: 'pattern_date'},
	        {data: 'pattern_stage', name: 'pattern_stage'},
	        {data: 'marker_length', name: 'marker_length'},
	        {data: 'date_plot', name: 'date_plot'},
	        {data: 'marker_release', name: 'marker_release'},
	        {data: 'marker_status', name: 'marker_status'},
	        {data: 'recever', name: 'recever'},
	        {data: 'sample_recieve_marker', name: 'sample_recieve_marker'}
	        // {data: 'marker_need', name: 'marker_need'}
	         ]
	});



	table.on('preDraw', function() {
		loading();
	    Pace.start();
	})
	.on('draw.dt', function() {
	    $.unblockUI();
	    Pace.stop();
	});

	$('#form-search').submit(function(event){
		event.preventDefault();

		table.clear();
		table.draw();
	});



	$('#table-list tbody').on('click','tr',function(){
		var data = table.row(this).data();
		console.log(data);
		var id = data['sr_id'];

		$.ajaxSetup({
	        headers: {
	            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	        }
	    });
	    $.ajax({
	        type: 'get',
	        url : "{{ route('samreq.getSamReq') }}",
	        data:{id:id},
	        beforeSend:function(){
	        	loading();
	        },
	        success: function(response) {
	        	$.unblockUI();
	        	var dt = response.data;
	        	$('#modal_edit .txdoc').text("Manufacturing Order No. "+dt.documentno);
	        	$('#modal_edit .txbuyer').val(dt.buyer);
	        	$('#modal_edit .txseason').val(dt.season);
	        	$('#modal_edit .txstyle').val(dt.style);
	        	$('#modal_edit .txarticle').val(dt.article);
	        	$('#modal_edit .txsize').val(dt.size);
	        	$('#modal_edit .txqty').val(dt.qty);
	        	$('#modal_edit .txpic').val(dt.mo_pic);
	        	$('#modal_edit .txstatus').text(dt.status);
	        	$('#modal_edit .txdesc').val(dt.product_category);
	        	var dship = $('#modal_edit .txship').pickadate();
	        	var sdship = dship.pickadate('picker').set('select', dt.shipment_date, { format: 'yyyy-mm-dd' });
	        	$('#modal_edit .txnote').text(dt.description);
	        	$('#modal_edit .txalloct').val(dt.allocation);
	        	$('#modal_edit .txid').val(dt.sr_id);

	        	$('#modal_edit .txpatternfu').val(dt.pattern_fu);
	        	$('#modal_edit .txpatternremk').val(dt.pattern_remark);
	        	$('#modal_edit .txpatternstat').val(dt.pattern_status).trigger('change');

	        	var ptdate = $('#modal_edit .txpatterndate').pickadate();
	        	var sptdate = ptdate.pickadate('picker').set('select', dt.pattern_date, { format: 'yyyy-mm-dd' });
	        	$('#modal_edit .txmarkerlength').val(dt.marker_length);
	        	var mkplot = $('#modal_edit .txplotmarker').pickadate();
	        	var smkplot = mkplot.pickadate('picker').set('select', dt.date_plot, { format: 'yyyy-mm-dd' });
	        	var mkrls = $('#modal_edit .txmarkerrelease').pickadate();
	        	var smkrls = mkrls.pickadate('picker').set('select', dt.marker_release, { format: 'yyyy-mm-dd' });
	        	$('#modal_edit .txpicmarker').val(dt.pic_marker);
	        	$('#modal_edit .txpatternmarker').val(dt.pattern_marker);
	        	$('#modal_edit .txpatternstage').val(dt.pattern_stage);
	        	$('#modal_edit .txrecever').val(dt.recever);
	        	$('#modal_edit .txmarkerstat').val(dt.marker_status).trigger('change');

	        	var mkrcv = $('#modal_edit .txsamplerecv').pickadate();
	        	var smkrcv = mkrcv.pickadate('picker').set('select', dt.sample_recieve_marker, { format: 'yyyy-mm-dd' });

	        	$('#modal_edit').modal('show');

	        	

	        },
	        error: function(response) {
	        	console.log(response);
	        } 
	    });
	});


	$('#btn-save').click(function(event){
		$.ajaxSetup({
	        headers: {
	            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	        }
	    });
	    $.ajax({
	        type: 'post',
	        url : "{{ route('samreq.updateSampleReq') }}",
	        data:{
	        	id:$('#modal_edit .txid').val(),
	        	pattern_fu:$('#modal_edit .txpatternfu').val(),
	        	pattern_remark:$('#modal_edit .txpatternremk').val(),
	        	pattern_status:$('#modal_edit .txpatternstat').val(),
	        	pattern_date:$('#modal_edit .txpatterndate').val(),
	        	marker_length:$('#modal_edit .txmarkerlength').val(),
	        	date_plot:$('#modal_edit .txplotmarker').val(),
	        	marker_release:$('#modal_edit .txmarkerrelease').val(),
	        	recever:$('#modal_edit .txrecever').val(),
	        	pic_marker:$('#modal_edit .txpicmarker').val(),
	        	pattern_marker:$('#modal_edit .txpatternmarker').val(),
	        	pattern_stage:$('#modal_edit .txpatternstage').val(),
	        	marker_status:$('#modal_edit .txmarkerstat').val(),
	        	sample_recieve_marker:$('#modal_edit .txsamplerecv').val()
	        },
	        beforeSend:function(){
	        	loading();
	        },
	        success: function(response) {
	        	var data = response.data;
	        	alert(data.status,data.output);

	        	$('#modal_edit').modal('hide');
	        	$.unblockUI();
	        	table.clear();
	        	table.draw();

	        },
	        error: function(response) {
	        	console.log(response);
	        	alert(response['status'],response['responseJSON']['message']);
	        	$.unblockUI();
	        }
	    });
	});

});




</script>
@endsection
