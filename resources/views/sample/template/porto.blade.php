<style type="text/css">
@page{
    margin : 2 7 2 7;
}

/*@font-face {
    font-family: 'Arial' !important;
    src: url({{ storage_path('fonts/Arial.ttf') }}) format('truetype');
}*/

.tag{
	width: 386.8px;
	height: 610px;
	/*border: 1px solid black;*/
    padding-bottom: 0 0 0 0;
   
}

.table{
	line-height: 10px;
}

.lab{
	font-family: Arial; 
	font-size: 8px;
	font-weight: bold;
}

.barcode {
    line-height: 16px;
  

}

.img_barcode {
    display: block;
    padding: 0px;
}

.img_barcode > img {
    width: 120px;
    height:20px;
}

.barcode_number {
    font-family: sans-serif;
    font-size: 12px !important;
}

.area_barcode {
    width: 50%;
}

.page-break {
    page-break-after: always;
}

.separator {
	margin-top: 10px;
    border-bottom: 1px dashed black;
}

.mo{
	padding-left: 125px;
}

.backbold{
	font-size: 17px;
	font-weight: bold;
}

.item{
	font-size: 14px;
	padding-left: 55px;
}

.centerpad{
	padding-left: 10px;
	padding-bottom:  10px;
}
.tab-fab tr td{
	vertical-align: top;
}
</style>
@php($chunk = array_chunk($data->toArray(), 2))
@foreach($chunk as $key => $value)

<!-- Depan -->
@if(isset($value[0]))
<table>
	<tr>
		<!-- comments -->
		<td width="47%">
			<table>
				<div class="tag">
					<table style="padding-left: 10px; padding-right: 10px;">
						<tr>
							<td>
								<h3>Adidas Comments :</h3>
							</td>
							
						</tr>
						<tr>
							<td>
								<br>
								<div style="border-bottom: 1px solid black; width: 355px;"></div>
							</td>
						</tr>
						<tr>
							<td>
								<br>
								<div style="border-bottom: 1px solid black; width: 355px;"></div>
							</td>
						</tr>
						<tr>
							<td>
								<br>
								<div style="border-bottom: 1px solid black; width: 355px;"></div>
							</td>
						</tr>
						<tr>
							<td>
								<br>
								<div style="border-bottom: 1px solid black; width: 355px;"></div>
							</td>
						</tr>
						<tr>
							<td>
								<br>
								<div style="border-bottom: 1px solid black; width: 355px;"></div>
							</td>
						</tr>
						<tr>
							<td>
								<br>
								<div style="border-bottom: 1px solid black; width: 355px;"></div>
							</td>
						</tr>
						<tr>
							<td>
								<br>
								<div style="border-bottom: 1px solid black; width: 355px;"></div>
							</td>
						</tr>
						<tr>
							<td>
								<br>
								<div style="border-bottom: 1px solid black; width: 355px;"></div>
							</td>
						</tr>
						<tr>
							<td>
								<br>
								<div style="border-bottom: 1px solid black; width: 355px;"></div>
							</td>
						</tr>
						<tr>
							<td>
								<br>
								<div style="border-bottom: 1px solid black; width: 355px;"></div>
							</td>
						</tr>
						<tr>
							<td>
								<br>
								<div style="border-bottom: 1px solid black; width: 355px;"></div>
							</td>
						</tr>
						<tr>
							<td>
								<br>
								<div style="border-bottom: 1px solid black; width: 355px;"></div>
							</td>
						</tr>
						<tr>
							<td>
								<br>
								<div style="border-bottom: 1px solid black; width: 355px;"></div>
							</td>
						</tr>
						<tr>
							<td>
								<br>
								<div style="border-bottom: 1px solid black; width: 355px;"></div>
							</td>
						</tr>
						<tr>
							<td>
								<br>
								<div style="border-bottom: 1px solid black; width: 355px;"></div>
							</td>
						</tr>
						<tr>
							<td>
								<br>
								<div style="border-bottom: 1px solid black; width: 355px;"></div>
							</td>
						</tr>
						<tr>
							<td>
								<br>
								<div style="border-bottom: 1px solid black; width: 355px;"></div>
							</td>
						</tr>
						<tr>
							<td>
								<br>
								<div style="border-bottom: 1px solid black; width: 355px;"></div>
							</td>
						</tr>
						<tr>
							<td>
								<br>
								<div style="border-bottom: 1px solid black; width: 355px;"></div>
							</td>
						</tr>
						<tr>
							<td>
								<br>
								<div style="border-bottom: 1px solid black; width: 355px;"></div>
							</td>
						</tr>
						<tr>
							<td>
								<br>
								<div style="border-bottom: 1px solid black; width: 355px;"></div>
							</td>
						</tr>
						<tr>
							<td>
								<br>
								<div style="border-bottom: 1px solid black; width: 355px;"></div>
							</td>
						</tr>

						
						

					</table>
				</div>
			</table>
		</td>

		<td width="6%"></td>
		<!-- identity -->
		<td width="47%">
			<table>
				<div class="tag">
					<div class="head">
						<table>
							
							<tr>
								<td>
									<h5 style="font-family: arial;">SAMPLE CHECKLIST</h5>
								</td>
							</tr>
							<tr>
								<td>
									<label style="font-size: 15px;">Every sample has to be send out with a sample checklist !</label>
								</td>
								
							</tr>
						</table>

						<table>
							<tr>
								<td width="150">
									<label style="font-size: 15px;">Sample status :</label>
									
								</td>

								<td width="90" >
									<label style="font-size: 15px;">Brand :</label>
									
								</td>
							</tr>
							<tr>
								<td width="140">
									
									<label style="font-size: 15px;"><img src="{{public_path('assets/icon/boxs.png')}}" width="20">PPR-Sample</label>
								</td>
								<td width="80" >
									<label style="font-size: 15px;"><img src="{{public_path('assets/icon/check.png')}}" width="20"> adidas</label>
								</td>
							</tr>

							<tr>
								<td width="140">
									
									<label style="font-size: 15px;"><img src="{{public_path('assets/icon/boxs.png')}}" width="20">PFR-Sample</label>
								</td>
								<td width="80" >
									<label style="font-size: 15px;"><img src="{{public_path('assets/icon/boxs.png')}}" width="20">Salomon</label>
								</td>
							</tr>

							<tr>
								<td width="140">
									
									<label style="font-size: 15px;"><img src="{{public_path('assets/icon/boxs.png')}}" width="20">Confirmation Sample</label>
								</td>
								<td width="80">
									<div>
										<label style="font-size: 15px;"><img src="{{public_path('assets/icon/boxs.png')}}" width="20">Bonfire</label>
									</div>
								</td>
							</tr>

							<tr>
								<td width="140">
									
									<label style="font-size: 15px;"><img src="{{public_path('assets/icon/boxs.png')}}" width="20">Size-Set-Sample</label>
								</td>
								<td width="110">
									<div>
										<label style="font-size: 15px;"><img src="{{public_path('assets/icon/boxs.png')}}" width="20">Other : </label>  </label> <label style="border-bottom: 1px solid black;  color: white;">ADIDAS</label>
									</div>
									
								</td>
								
							</tr>

							<tr>
								<td width="250" colspan="2">
									
									<label style="font-size: 15px;"><img src="{{public_path('assets/icon/boxs.png')}}" width="20">Other : </label> <label style="border-bottom: 1px solid black;  font-size: 14px;">{{$value[0]->status}}</label>
								</td>
								
							</tr>
						</table>

					
					</div>

					<div class="body1">
						<table>
							<tr>
								<td width="40">
									<label style="  font-size: 15px;">Factory :</label>
								</td>
								<td>
									<div style="border-bottom: 1px solid black; width: 270px;"><label style="font-size: 15px; font-weight: bold;">PT. APPAREL ONE INDONESIA</label></div>
								</td>
							</tr>

							
						</table>

						<table>
							<tr>
								<td >
									<label style="  font-size: 15px;">Factory Responsible </label>
								</td>
								<td >
									<div style="border-bottom: 1px solid black; width: 200px;"><label style="font-size: 14px; font-weight: bold;"> 24P001 </label></div>
								</td>
							</tr>
						</table>

						<table class="tab-fab">
							<tr>
								<td width="40">
									<label style="  font-size: 15px;">Fabric :</label>
								</td>
								<td>
									@if($value[0]->sets=='' || $value[0]->sets=='TOP')
									<div style=" width: 280px;"><label style="font-size: 11.5px; text-decoration: underline;">{{ $value[0]->fabric1,0,50 }}</label></div>
									@elseif($value[0]->sets=='BOT')
									<div style=" width: 280px;"><label style="font-size: 11.5px; text-decoration: underline;">{{ substr($value[0]->fabric2,0,50) }}</label></div>
									@endif
								</td>
							</tr>
						</table>
					</div>

					<div class="body2" style="margin-top: 2px; height: 115px;">
						<table>
							<tr>
								<td><label style="  font-size: 15px;">Work-No.</label></td>
								<td width="85"><div style="border-bottom: 1px solid black; width: 105px;"><label style="font-size: 12px;">{{ $value[0]->style }}</label></div>
								</td>
								<td></td>
								<td><label style="  font-size: 15px;"> Style :</label></td>
								<td  width="85"><div style="border-bottom: 1px solid black; width: 105px;"><label style="font-size: 12px;">{{ $value[0]->product_name }}</label></div>
								</td>
							</tr>

							<tr>
								<td><label style="  font-size: 15px;">Art. No. :</label></td>
								<td width="85"><div style="border-bottom: 1px solid black; width: 105px;"><label style="font-size: 12px;">{{ $value[0]->article }}</label></div>
								</td>
								<td></td>
								<td><label style="  font-size: 15px;"> Date :</label></td>
								<td  width="85"><div style="border-bottom: 1px solid black; width: 105px;"><label style="font-size: 12px;">{{ date_format(date_create($value[0]->reciept_date),'M d, Y')}}</label></div>
								</td>
							</tr>

							<tr>
								<td><label style="  font-size: 15px;">Season :</label></td>
								<td width="85"><div style="border-bottom: 1px solid black; width: 105px;"><label style="font-size: 12px;">{{ $value[0]->season }}</label></div>
								</td>
								<td></td>
								<td><label style="  font-size: 15px;"> Color :</label></td>
								<td  width="85"><div style="border-bottom: 1px solid black; width: 105px;"><label style="font-size: 12px;">{{ explode("(",$value[0]->color)[0] }}</label></div>
								</td>
							</tr>

							<tr>
								<td><label style="  font-size: 15px;">Size :</label></td>
								<td width="85"><div style="border-bottom: 1px solid black; width: 105px;"><label style="font-size: 12px;">{{ $value[0]->size }}</label></div>
								</td>
								<td></td>
								<td><label style="  font-size: 15px;"> PJM :</label></td>
								<td  width="85"><div style="border-bottom: 1px solid black; width: 105px;"><label style="font-size: 12px; color: white;">ADIDAS</label></div>
								</td>
							</tr>
						</table>
					</div>

					<div class="body3">
						<table>
							<tr>
								<td width="120">
									<label style="  font-size: 15px;">Model pattern created by :</label>
								</td>
								<td width="120">
									<label style="font-size: 15px;"><img src="{{public_path('assets/icon/boxs.png')}}" width="20">Herzogenaurach</label>
								</td>
							</tr>
							<tr>
								<td width="120"></td>
								<td width="120">
									<label style="font-size: 15px;"><img src="{{public_path('assets/icon/boxs.png')}}" width="20">Portland</label>
								</td>
							</tr>
							<tr>
								<td width="120"></td>
								<td width="120">
									<label style="font-size: 15px;"><img src="{{public_path('assets/icon/boxs.png')}}" width="20">Factory</label>
								</td>
							</tr>
							<tr>
								<td width="120">
									<label style="  font-size: 15px;">Responsible QC :</label>
								</td>
								<td width="120">
									<label style="  font-size: 15px; padding-left: 17px;">Responsible Developer :</label>
								</td>
							</tr>

							<tr>
								<td width="120">
									<div style="border-bottom: 1px solid black; width: 160px; margin-top: 15px;"></div>
								</td>
								<td width="120">
									<div style="border-bottom: 1px solid black; width: 160px; margin-left: 17px;  margin-top: 15px;"></div>
								</td>
							</tr>

							<tr>
								<td width="120">
									<div style="border-bottom: 1px solid black; width: 160px; margin-top: 30px;"></div>
								</td>
								<td width="120">
									<div style="border-bottom: 1px solid black; width: 160px; margin-left: 17px;  margin-top: 30px;"></div>
								</td>
							</tr>


						</table>
						<table style="margin-top: 30px;">
							<tr>
								<td width="120">
									<label style="  font-size: 15px;">Date/Signature</label>
								</td>
								<td width="120">
									<label style="  font-size: 15px; padding-left: 25px;">Date/Signature</label>
								</td>
							</tr>
							<tr>
								<td>
									<div class="barcode">
									    <div class="img_barcode">
									        <img src="data:image/png;base64,{{ DNS1D::getBarcodePNG($value[0]->barcode_id, 'C128',2,35) }}" alt="barcode"   />
									    </div>
									    <div class="row">
									        <span class="barcode_number">{{ $value[0]->barcode_id  }} {{ isset($value[0]->sets) ? $value[0]->sets : ""}}</span >
									    </div>
									</div>
								</td>
								<td>
									<div class="mo">
										<label style="font-size: 8px;">{{$value[0]->documentno}}</label>
									</div>
								</td>
							</tr>
						</table>
					</div>

					
				</div>
			</table>
		</td>
	</tr>

</table>

<div class="separator"></div>

@endif


@if(isset($value[1]))
<table>
	<tr>
		<!-- comments -->
		<td width="47%">
			<table>
				<div class="tag">
					<table style="padding-left: 10px; padding-right: 10px;">
						<tr>
							<td>
								<h3>Adidas Comments :</h3>
							</td>
							
						</tr>
						<tr>
							<td>
								<br>
								<div style="border-bottom: 1px solid black; width: 355px;"></div>
							</td>
						</tr>
						<tr>
							<td>
								<br>
								<div style="border-bottom: 1px solid black; width: 355px;"></div>
							</td>
						</tr>
						<tr>
							<td>
								<br>
								<div style="border-bottom: 1px solid black; width: 355px;"></div>
							</td>
						</tr>
						<tr>
							<td>
								<br>
								<div style="border-bottom: 1px solid black; width: 355px;"></div>
							</td>
						</tr>
						<tr>
							<td>
								<br>
								<div style="border-bottom: 1px solid black; width: 355px;"></div>
							</td>
						</tr>
						<tr>
							<td>
								<br>
								<div style="border-bottom: 1px solid black; width: 355px;"></div>
							</td>
						</tr>
						<tr>
							<td>
								<br>
								<div style="border-bottom: 1px solid black; width: 355px;"></div>
							</td>
						</tr>
						<tr>
							<td>
								<br>
								<div style="border-bottom: 1px solid black; width: 355px;"></div>
							</td>
						</tr>
						<tr>
							<td>
								<br>
								<div style="border-bottom: 1px solid black; width: 355px;"></div>
							</td>
						</tr>
						<tr>
							<td>
								<br>
								<div style="border-bottom: 1px solid black; width: 355px;"></div>
							</td>
						</tr>
						<tr>
							<td>
								<br>
								<div style="border-bottom: 1px solid black; width: 355px;"></div>
							</td>
						</tr>
						<tr>
							<td>
								<br>
								<div style="border-bottom: 1px solid black; width: 355px;"></div>
							</td>
						</tr>
						<tr>
							<td>
								<br>
								<div style="border-bottom: 1px solid black; width: 355px;"></div>
							</td>
						</tr>
						<tr>
							<td>
								<br>
								<div style="border-bottom: 1px solid black; width: 355px;"></div>
							</td>
						</tr>
						<tr>
							<td>
								<br>
								<div style="border-bottom: 1px solid black; width: 355px;"></div>
							</td>
						</tr>
						<tr>
							<td>
								<br>
								<div style="border-bottom: 1px solid black; width: 355px;"></div>
							</td>
						</tr>
						<tr>
							<td>
								<br>
								<div style="border-bottom: 1px solid black; width: 355px;"></div>
							</td>
						</tr>
						<tr>
							<td>
								<br>
								<div style="border-bottom: 1px solid black; width: 355px;"></div>
							</td>
						</tr>
						<tr>
							<td>
								<br>
								<div style="border-bottom: 1px solid black; width: 355px;"></div>
							</td>
						</tr>
						<tr>
							<td>
								<br>
								<div style="border-bottom: 1px solid black; width: 355px;"></div>
							</td>
						</tr>
						<tr>
							<td>
								<br>
								<div style="border-bottom: 1px solid black; width: 355px;"></div>
							</td>
						</tr>
						<tr>
							<td>
								<br>
								<div style="border-bottom: 1px solid black; width: 355px;"></div>
							</td>
						</tr>

						
						

					</table>
				</div>
			</table>
		</td>

		<td width="6%"></td>
		<!-- identity -->
		<td width="47%">
			<table>
				<div class="tag">
					<div class="head">
						<table>
							
							<tr>
								<td width="300">
									<center>
										<label style="color: white; font-size: 40px; position: absolute; padding-top: 12px; padding-left:  5px;">CONFIDENTIAL</label>
									</center>
								</td>
							</tr>
						

							

							<tr>
								<td>
									<h5 style="font-family: arial;">SAMPLE CHECKLIST</h5>
								</td>
							</tr>
							
							<tr>
								<td>
									<label style="font-size: 15px;">Every sample has to be send out with a sample checklist !</label>
								</td>
								
							</tr>
						</table>

						<table>
							<tr>
								<td width="150">
									<label style="font-size: 15px;">Sample status :</label>
									
								</td>

								<td width="90" >
									<label style="font-size: 15px;">Brand :</label>
									
								</td>
							</tr>
							<tr>
								<td width="140">
									
									<label style="font-size: 15px;"><img src="{{public_path('assets/icon/boxs.png')}}" width="20">PPR-Sample</label>
								</td>
								<td width="80" >
									<label style="font-size: 15px;"><img src="{{public_path('assets/icon/check.png')}}" width="20"> adidas</label>
								</td>
							</tr>

							<tr>
								<td width="140">
									
									<label style="font-size: 15px;"><img src="{{public_path('assets/icon/boxs.png')}}" width="20">PFR-Sample</label>
								</td>
								<td width="80" >
									<label style="font-size: 15px;"><img src="{{public_path('assets/icon/boxs.png')}}" width="20">Salomon</label>
								</td>
							</tr>

							<tr>
								<td width="140">
									
									<label style="font-size: 15px;"><img src="{{public_path('assets/icon/boxs.png')}}" width="20">Confirmation Sample</label>
								</td>
								<td width="80">
									<div>
										<label style="font-size: 15px;"><img src="{{public_path('assets/icon/boxs.png')}}" width="20">Bonfire</label>
									</div>
								</td>
							</tr>

							<tr>
								<td width="140">
									
									<label style="font-size: 15px;"><img src="{{public_path('assets/icon/boxs.png')}}" width="20">Size-Set-Sample</label>
								</td>
								<td width="110">
									<div>
										<label style="font-size: 15px;"><img src="{{public_path('assets/icon/boxs.png')}}" width="20">Other : </label>  </label> <label style="border-bottom: 1px solid black;  color: white;">ADIDAS</label>
									</div>
									
								</td>
								
							</tr>

							<tr>
								<td width="250" colspan="2">
									
									<label style="font-size: 15px;"><img src="{{public_path('assets/icon/boxs.png')}}" width="20">Other : </label> <label style="border-bottom: 1px solid black;  font-size: 14px;">{{$value[1]->status}}</label>
								</td>
								
							</tr>
						</table>

					
					</div>

					<div class="body1">
						<table>
							<tr>
								<td width="40">
									<label style="  font-size: 15px;">Factory :</label>
								</td>
								<td>
									<div style="border-bottom: 1px solid black; width: 270px;"><label style="font-size: 15px; font-weight: bold;">PT. APPAREL ONE INDONESIA</label></div>
								</td>
							</tr>

							
						</table>

						<table>
							<tr>
								<td >
									<label style="  font-size: 15px;">Factory Responsible </label>
								</td>
								<td >
									<div style="border-bottom: 1px solid black; width: 200px;"><label style="font-size: 14px; font-weight: bold;"> 24P001 </label></div>
								</td>
							</tr>
						</table>

						<table class="tab-fab">
							<tr>
								<td width="40">
									<label style="  font-size: 15px;">Fabric :</label>
								</td>
								<td>
									@if($value[1]->sets=='' || $value[1]->sets=='TOP')
									<div style=" width: 280px;"><label style="font-size: 11.5px; text-decoration: underline;">{{ $value[1]->fabric1,0,50 }}</label></div>
									@elseif($value[1]->sets=='BOT')
									<div style=" width: 280px;"><label style="font-size: 11.5px; text-decoration: underline;">{{ substr($value[1]->fabric2,0,50) }}</label></div>
									@endif
								</td>
							</tr>
						</table>
					</div>

					<div class="body2" style="margin-top: 2px; height: 115px;">
						<table>
							<tr>
								<td><label style="  font-size: 15px;">Work-No.</label></td>
								<td width="85"><div style="border-bottom: 1px solid black; width: 105px;"><label style="font-size: 12px;">{{ $value[1]->style }}</label></div>
								</td>
								<td></td>
								<td><label style="  font-size: 15px;"> Style :</label></td>
								<td  width="85"><div style="border-bottom: 1px solid black; width: 105px;"><label style="font-size: 12px;">{{ $value[1]->product_name }}</label></div>
								</td>
							</tr>

							<tr>
								<td><label style="  font-size: 15px;">Art. No. :</label></td>
								<td width="85"><div style="border-bottom: 1px solid black; width: 105px;"><label style="font-size: 12px;">{{ $value[1]->article }}</label></div>
								</td>
								<td></td>
								<td><label style="  font-size: 15px;"> Date :</label></td>
								<td  width="85"><div style="border-bottom: 1px solid black; width: 105px;"><label style="font-size: 12px;">{{ date_format(date_create($value[1]->reciept_date),'M d, Y')}}</label></div>
								</td>
							</tr>

							<tr>
								<td><label style="  font-size: 15px;">Season :</label></td>
								<td width="85"><div style="border-bottom: 1px solid black; width: 105px;"><label style="font-size: 12px;">{{ $value[1]->season }}</label></div>
								</td>
								<td></td>
								<td><label style="  font-size: 15px;"> Color :</label></td>
								<td  width="85"><div style="border-bottom: 1px solid black; width: 105px;"><label style="font-size: 12px;">{{ explode("(",$value[1]->color)[0] }}</label></div>
								</td>
							</tr>

							<tr>
								<td><label style="  font-size: 15px;">Size :</label></td>
								<td width="85"><div style="border-bottom: 1px solid black; width: 105px;"><label style="font-size: 12px;">{{ $value[1]->size }}</label></div>
								</td>
								<td></td>
								<td><label style="  font-size: 15px;"> PJM :</label></td>
								<td  width="85"><div style="border-bottom: 1px solid black; width: 105px;"><label style="font-size: 12px; color: white;">ADIDAS</label></div>
								</td>
							</tr>
						</table>
					</div>

					<div class="body3">
						<table>
							<tr>
								<td width="120">
									<label style="  font-size: 15px;">Model pattern created by :</label>
								</td>
								<td width="120">
									<label style="font-size: 15px;"><img src="{{public_path('assets/icon/boxs.png')}}" width="20">Herzogenaurach</label>
								</td>
							</tr>
							<tr>
								<td width="120"></td>
								<td width="120">
									<label style="font-size: 15px;"><img src="{{public_path('assets/icon/boxs.png')}}" width="20">Portland</label>
								</td>
							</tr>
							<tr>
								<td width="120"></td>
								<td width="120">
									<label style="font-size: 15px;"><img src="{{public_path('assets/icon/boxs.png')}}" width="20">Factory</label>
								</td>
							</tr>
							<tr>
								<td width="120">
									<label style="  font-size: 15px;">Responsible QC :</label>
								</td>
								<td width="120">
									<label style="  font-size: 15px; padding-left: 17px;">Responsible Developer :</label>
								</td>
							</tr>

							<tr>
								<td width="120">
									<div style="border-bottom: 1px solid black; width: 160px; margin-top: 15px;"></div>
								</td>
								<td width="120">
									<div style="border-bottom: 1px solid black; width: 160px; margin-left: 17px;  margin-top: 15px;"></div>
								</td>
							</tr>

							<tr>
								<td width="120">
									<div style="border-bottom: 1px solid black; width: 160px; margin-top: 30px;"></div>
								</td>
								<td width="120">
									<div style="border-bottom: 1px solid black; width: 160px; margin-left: 17px;  margin-top: 30px;"></div>
								</td>
							</tr>


						</table>
						<table style="margin-top: 30px;">
							<tr>
								<td width="120">
									<label style="  font-size: 15px;">Date/Signature</label>
								</td>
								<td width="120">
									<label style="  font-size: 15px; padding-left: 25px;">Date/Signature</label>
								</td>
							</tr>
							<tr>
								<td>
									<div class="barcode">
									    <div class="img_barcode">
									        <img src="data:image/png;base64,{{ DNS1D::getBarcodePNG($value[1]->barcode_id, 'C128',2,35) }}" alt="barcode"   />
									    </div>
									    <div class="row">
									        <span class="barcode_number">{{ $value[1]->barcode_id  }} {{ isset($value[1]->sets) ? $value[1]->sets : ""}}</span >
									    </div>
									</div>
								</td>
								<td>
									<div class="mo">
										<label style="font-size: 8px;">{{$value[1]->documentno}}</label>
									</div>
								</td>
							</tr>
						</table>
					</div>

					
				</div>
			</table>
		</td>
	</tr>

</table>

<div class="separator"></div>
@endif




<div class="page-break"></div>
<!-- BELAKANG -->
@if(isset($value[0]))
<table>
	<tr>
		<!-- comments -->
		<td width="47%">
			
			<div class="tag">
				<table width="100%">
					<tr>
						<td >
							<label class="backbold">1. Labelling/Hangtags</label>
						</td>
						<td >
							<center><label style="font-weight: bold; font-size: 14px;">Original</label></center>
						</td>
						<td >
							<center><label style="font-weight: bold; font-size: 14px;">Subtitute</label></center>
						</td>
					</tr>
					<tr>
						<td >
							<label class="item">Main Label</label>
						</td>
						<td >
							<center>
								<img src="{{public_path('assets/icon/boxs.png')}}" width="30">
							</center>
						</td>
						<td >
							<center>
								<img src="{{public_path('assets/icon/boxs.png')}}" width="30">
							</center>
						</td>
					</tr>
					<tr>
						<td >
							<label class="item">Care Label</label>
						</td>
						<td >
							<center>
								<img src="{{public_path('assets/icon/boxs.png')}}" width="30">
							</center>
						</td>
						<td >
							<center>
								<img src="{{public_path('assets/icon/boxs.png')}}" width="30">
							</center>
						</td>
					</tr>
					<tr>
						<td >
							<label class="item">Hangtag</label>
						</td>
						<td >
							<center>
								<img src="{{public_path('assets/icon/boxs.png')}}" width="30">
							</center>
						</td>
						<td >
							<center>
								<img src="{{public_path('assets/icon/boxs.png')}}" width="30">
							</center>
						</td>
					</tr>

					<tr>
						<td colspan="3">
							<label class="backbold">2. Material : </label>
						</td>
					</tr>
					<tr>
						<td >
							<label class="item">Shell Fabric</label>
						</td>
						<td >
							<center>
								<img src="{{public_path('assets/icon/boxs.png')}}" width="30">
							</center>
						</td>
						<td >
							<center>
								<img src="{{public_path('assets/icon/boxs.png')}}" width="30">
							</center>
						</td>
					</tr>
					<tr>
						<td >
							<label class="item">Additional Fabric</label>
						</td>
						<td >
							<center>
								<img src="{{public_path('assets/icon/boxs.png')}}" width="30">
							</center>
						</td>
						<td >
							<center>
								<img src="{{public_path('assets/icon/boxs.png')}}" width="30">
							</center>
						</td>
					</tr>
					<tr>
						<td >
							<label class="item">Rips</label>
						</td>
						<td >
							<center>
								<img src="{{public_path('assets/icon/boxs.png')}}" width="30">
							</center>
						</td>
						<td >
							<center>
								<img src="{{public_path('assets/icon/boxs.png')}}" width="30">
							</center>
						</td>
					</tr>
					<tr>
						<td >
							<label class="item">Other : </label><label style="border-bottom: 1px solid black; color: white;">XXXXXXXX</label>
						</td>
						<td >
							<center>
								<img src="{{public_path('assets/icon/boxs.png')}}" width="30">
							</center>
						</td>
						<td >
							<center>
								<img src="{{public_path('assets/icon/boxs.png')}}" width="30">
							</center>
						</td>
					</tr>

					<tr>
						<td colspan="3">
							<label class="backbold">3. Acessories : </label>
						</td>
					</tr>
					<tr>
						<td >
							<label class="item">Zipper/Puller</label>
						</td>
						<td >
							<center>
								<img src="{{public_path('assets/icon/boxs.png')}}" width="30">
							</center>
						</td>
						<td >
							<center>
								<img src="{{public_path('assets/icon/boxs.png')}}" width="30">
							</center>
						</td>
					</tr>
					<tr>
						<td >
							<label class="item">Snapbuttons</label>
						</td>
						<td >
							<center>
								<img src="{{public_path('assets/icon/boxs.png')}}" width="30">
							</center>
						</td>
						<td >
							<center>
								<img src="{{public_path('assets/icon/boxs.png')}}" width="30">
							</center>
						</td>
					</tr>
					<tr>
						<td >
							<label class="item">3 stripes tape</label>
						</td>
						<td >
							<center>
								<img src="{{public_path('assets/icon/boxs.png')}}" width="30">
							</center>
						</td>
						<td >
							<center>
								<img src="{{public_path('assets/icon/boxs.png')}}" width="30">
							</center>
						</td>
					</tr>
					<tr>
						<td >
							<label class="item">Drawcord/piping</label>
						</td>
						<td >
							<center>
								<img src="{{public_path('assets/icon/boxs.png')}}" width="30">
							</center>
						</td>
						<td >
							<center>
								<img src="{{public_path('assets/icon/boxs.png')}}" width="30">
							</center>
						</td>
					</tr>
					<tr>
						<td >
							<label class="item">Closure</label>
						</td>
						<td >
							<center>
								<img src="{{public_path('assets/icon/boxs.png')}}" width="30">
							</center>
						</td>
						<td >
							<center>
								<img src="{{public_path('assets/icon/boxs.png')}}" width="30">
							</center>
						</td>
					</tr>
					<tr>
						<td >
							<label class="item">Others : </label><label style="border-bottom: 1px solid black; color: white;">XXXXXXXX</label>
						</td>
						<td >
							<center>
								<img src="{{public_path('assets/icon/boxs.png')}}" width="30">
							</center>
						</td>
						<td >
							<center>
								<img src="{{public_path('assets/icon/boxs.png')}}" width="30">
							</center>
						</td>
					</tr>

					<tr>
						<td colspan="3">
							<label class="backbold">4. Artworks : </label>
						</td>
					</tr>
					<tr>
						<td >
							<label class="item">Print</label>
						</td>
						<td >
							<center>
								<img src="{{public_path('assets/icon/boxs.png')}}" width="30">
							</center>
						</td>
						<td >
							<center>
								<img src="{{public_path('assets/icon/boxs.png')}}" width="30">
							</center>
						</td>
					</tr>
					<tr>
						<td >
							<label class="item">Embroidery</label>
						</td>
						<td >
							<center>
								<img src="{{public_path('assets/icon/boxs.png')}}" width="30">
							</center>
						</td>
						<td >
							<center>
								<img src="{{public_path('assets/icon/boxs.png')}}" width="30">
							</center>
						</td>
					</tr>
					<tr>
						<td >
							<label class="item">Badges</label>
						</td>
						<td >
							<center>
								<img src="{{public_path('assets/icon/boxs.png')}}" width="30">
							</center>
						</td>
						<td >
							<center>
								<img src="{{public_path('assets/icon/boxs.png')}}" width="30">
							</center>
						</td>
					</tr>
					<tr>
						<td >
							<label class="item">Others : </label><label style="border-bottom: 1px solid black; color: white;">XXXXXXXX</label>
						</td>
						<td >
							<center>
								<img src="{{public_path('assets/icon/boxs.png')}}" width="30">
							</center>
						</td>
						<td >
							<center>
								<img src="{{public_path('assets/icon/boxs.png')}}" width="30">
							</center>
						</td>
					</tr>

					<tr>
						<td colspan="3">
							<label class="backbold">5. Color : </label> <div style="border-bottom: 1px solid black; margin-left: 120px;"></div>
						</td>
						<!-- <td colspan="2" style="border-bottom: 1px solid black;"></td> -->
					</tr>
					<tr>
						<td colspan="3">
							<label class="backbold">6. Pattern : </label> <div style="border-bottom: 1px solid black; margin-left: 120px;"></div>
						</td>
					</tr>
					<tr>
						<td colspan="3">
							<label class="backbold">7. Others : </label> <div style="border-bottom: 1px solid black; margin-left: 120px;"></div>
						</td>
					</tr>
				</table>
			</div>
		</td>

		<td width="6%"></td>
		<!-- identity -->
		<td width="47%">
			<div class="tag">
				<table width="100%">
					<tr>
						<td width="30%">
							
								<label class="backbold" style="float: left;">Comment :</label>
							</center>
						</td>
						<td width="40%">
							
								<label class="backbold">Action Needed :</label>
							</center>
						</td>
						<td width="30%">
							
								<label class="backbold" style="float: right;">By Whom :</label>
							
						</td>
					</tr>
					<tr>
						<td colspan="3" style="border-bottom: 1px solid black;color: white;">A</td>
					</tr>
					<tr>
						<td colspan="3" style="border-bottom: 1px solid black;color: white;">A</td>
					</tr>
					<tr>
						<td colspan="3" style="border-bottom: 1px solid black;color: white;">A</td>
					</tr>

					<tr>
						<td colspan="3">
							<label class="backbold" style="color: white;">A</label>
						</td>
					</tr>
					<tr>
						<td colspan="3" style="border-bottom: 1px solid black;color: white;">A</td>
					</tr>
					<tr>
						<td colspan="3" style="border-bottom: 1px solid black;color: white;">A</td>
					</tr>
					<tr>
						<td colspan="3" style="border-bottom: 1px solid black;color: white;">A</td>
					</tr>
					<tr>
						<td colspan="3" style="border-bottom: 1px solid black;color: white;">A</td>
					</tr>

					<tr>
						<td colspan="3">
							<label class="backbold" style="color: white;">A</label>
						</td>
					</tr>
					<tr>
						<td colspan="3" style="border-bottom: 1px solid black;color: white;">A</td>
					</tr>
					<tr>
						<td colspan="3" style="border-bottom: 1px solid black;color: white;">A</td>
					</tr>
					<tr>
						<td colspan="3" style="border-bottom: 1px solid black;color: white;">A</td>
					</tr>
					<tr>
						<td colspan="3" style="border-bottom: 1px solid black;color: white;">A</td>
					</tr>
					<tr>
						<td colspan="3" style="border-bottom: 1px solid black;color: white;">A</td>
					</tr>
					<tr>
						<td colspan="3" style="border-bottom: 1px solid black;color: white;">A</td>
					</tr>

					<tr>
						<td colspan="3">
							<label class="backbold" style="color: white;">A</label>
						</td>
					</tr>
					<tr>
						<td colspan="3" style="border-bottom: 1px solid black;color: white;">A</td>
					</tr>
					<tr>
						<td colspan="3" style="border-bottom: 1px solid black;color: white;">A</td>
					</tr>
					<tr>
						<td colspan="3" style="border-bottom: 1px solid black;color: white;">A</td>
					</tr>
					<tr>
						<td colspan="3" style="border-bottom: 1px solid black;color: white;">A</td>
					</tr>
					<tr>
						<td colspan="3" style="border-bottom: 1px solid black;color: white;">A</td>
					</tr>

					<tr>
						<td colspan="3" style="border-bottom: 1px solid black;color: white;"><label class="backbold" style="color: white;">A</label></td>
					</tr>
					<tr>
						<td colspan="3" style="border-bottom: 1px solid black;color: white;"><label class="backbold" style="color: white;">A</label></td>
					</tr>
					<!-- <tr>
						<td colspan="3" style="border-bottom: 1px solid black;color: white;"><label class="backbold" style="color: white;">A</label></td>
					</tr> -->

				</table>
			</div>
		</td>
	</tr>

</table>

<div class="separator"></div>

@endif

@if(isset($value[1]))
<table>
	<tr>
		<!-- comments -->
		<td width="47%">
			
			<div class="tag">
				<table width="100%">
					<tr>
						<td >
							<label class="backbold">1. Labelling/Hangtags</label>
						</td>
						<td >
							<center><label style="font-weight: bold; font-size: 14px;">Original</label></center>
						</td>
						<td >
							<center><label style="font-weight: bold; font-size: 14px;">Subtitute</label></center>
						</td>
					</tr>
					<tr>
						<td >
							<label class="item">Main Label</label>
						</td>
						<td >
							<center>
								<img src="{{public_path('assets/icon/boxs.png')}}" width="30">
							</center>
						</td>
						<td >
							<center>
								<img src="{{public_path('assets/icon/boxs.png')}}" width="30">
							</center>
						</td>
					</tr>
					<tr>
						<td >
							<label class="item">Care Label</label>
						</td>
						<td >
							<center>
								<img src="{{public_path('assets/icon/boxs.png')}}" width="30">
							</center>
						</td>
						<td >
							<center>
								<img src="{{public_path('assets/icon/boxs.png')}}" width="30">
							</center>
						</td>
					</tr>
					<tr>
						<td >
							<label class="item">Hangtag</label>
						</td>
						<td >
							<center>
								<img src="{{public_path('assets/icon/boxs.png')}}" width="30">
							</center>
						</td>
						<td >
							<center>
								<img src="{{public_path('assets/icon/boxs.png')}}" width="30">
							</center>
						</td>
					</tr>

					<tr>
						<td colspan="3">
							<label class="backbold">2. Material : </label>
						</td>
					</tr>
					<tr>
						<td >
							<label class="item">Shell Fabric</label>
						</td>
						<td >
							<center>
								<img src="{{public_path('assets/icon/boxs.png')}}" width="30">
							</center>
						</td>
						<td >
							<center>
								<img src="{{public_path('assets/icon/boxs.png')}}" width="30">
							</center>
						</td>
					</tr>
					<tr>
						<td >
							<label class="item">Additional Fabric</label>
						</td>
						<td >
							<center>
								<img src="{{public_path('assets/icon/boxs.png')}}" width="30">
							</center>
						</td>
						<td >
							<center>
								<img src="{{public_path('assets/icon/boxs.png')}}" width="30">
							</center>
						</td>
					</tr>
					<tr>
						<td >
							<label class="item">Rips</label>
						</td>
						<td >
							<center>
								<img src="{{public_path('assets/icon/boxs.png')}}" width="30">
							</center>
						</td>
						<td >
							<center>
								<img src="{{public_path('assets/icon/boxs.png')}}" width="30">
							</center>
						</td>
					</tr>
					<tr>
						<td >
							<label class="item">Other : </label><label style="border-bottom: 1px solid black; color: white;">XXXXXXXX</label>
						</td>
						<td >
							<center>
								<img src="{{public_path('assets/icon/boxs.png')}}" width="30">
							</center>
						</td>
						<td >
							<center>
								<img src="{{public_path('assets/icon/boxs.png')}}" width="30">
							</center>
						</td>
					</tr>

					<tr>
						<td colspan="3">
							<label class="backbold">3. Acessories : </label>
						</td>
					</tr>
					<tr>
						<td >
							<label class="item">Zipper/Puller</label>
						</td>
						<td >
							<center>
								<img src="{{public_path('assets/icon/boxs.png')}}" width="30">
							</center>
						</td>
						<td >
							<center>
								<img src="{{public_path('assets/icon/boxs.png')}}" width="30">
							</center>
						</td>
					</tr>
					<tr>
						<td >
							<label class="item">Snapbuttons</label>
						</td>
						<td >
							<center>
								<img src="{{public_path('assets/icon/boxs.png')}}" width="30">
							</center>
						</td>
						<td >
							<center>
								<img src="{{public_path('assets/icon/boxs.png')}}" width="30">
							</center>
						</td>
					</tr>
					<tr>
						<td >
							<label class="item">3 stripes tape</label>
						</td>
						<td >
							<center>
								<img src="{{public_path('assets/icon/boxs.png')}}" width="30">
							</center>
						</td>
						<td >
							<center>
								<img src="{{public_path('assets/icon/boxs.png')}}" width="30">
							</center>
						</td>
					</tr>
					<tr>
						<td >
							<label class="item">Drawcord/piping</label>
						</td>
						<td >
							<center>
								<img src="{{public_path('assets/icon/boxs.png')}}" width="30">
							</center>
						</td>
						<td >
							<center>
								<img src="{{public_path('assets/icon/boxs.png')}}" width="30">
							</center>
						</td>
					</tr>
					<tr>
						<td >
							<label class="item">Closure</label>
						</td>
						<td >
							<center>
								<img src="{{public_path('assets/icon/boxs.png')}}" width="30">
							</center>
						</td>
						<td >
							<center>
								<img src="{{public_path('assets/icon/boxs.png')}}" width="30">
							</center>
						</td>
					</tr>
					<tr>
						<td >
							<label class="item">Others : </label><label style="border-bottom: 1px solid black; color: white;">XXXXXXXX</label>
						</td>
						<td >
							<center>
								<img src="{{public_path('assets/icon/boxs.png')}}" width="30">
							</center>
						</td>
						<td >
							<center>
								<img src="{{public_path('assets/icon/boxs.png')}}" width="30">
							</center>
						</td>
					</tr>

					<tr>
						<td colspan="3">
							<label class="backbold">4. Artworks : </label>
						</td>
					</tr>
					<tr>
						<td >
							<label class="item">Print</label>
						</td>
						<td >
							<center>
								<img src="{{public_path('assets/icon/boxs.png')}}" width="30">
							</center>
						</td>
						<td >
							<center>
								<img src="{{public_path('assets/icon/boxs.png')}}" width="30">
							</center>
						</td>
					</tr>
					<tr>
						<td >
							<label class="item">Embroidery</label>
						</td>
						<td >
							<center>
								<img src="{{public_path('assets/icon/boxs.png')}}" width="30">
							</center>
						</td>
						<td >
							<center>
								<img src="{{public_path('assets/icon/boxs.png')}}" width="30">
							</center>
						</td>
					</tr>
					<tr>
						<td >
							<label class="item">Badges</label>
						</td>
						<td >
							<center>
								<img src="{{public_path('assets/icon/boxs.png')}}" width="30">
							</center>
						</td>
						<td >
							<center>
								<img src="{{public_path('assets/icon/boxs.png')}}" width="30">
							</center>
						</td>
					</tr>
					<tr>
						<td >
							<label class="item">Others : </label><label style="border-bottom: 1px solid black; color: white;">XXXXXXXX</label>
						</td>
						<td >
							<center>
								<img src="{{public_path('assets/icon/boxs.png')}}" width="30">
							</center>
						</td>
						<td >
							<center>
								<img src="{{public_path('assets/icon/boxs.png')}}" width="30">
							</center>
						</td>
					</tr>

					<tr>
						<td colspan="3">
							<label class="backbold">5. Color : </label> <div style="border-bottom: 1px solid black; margin-left: 120px;"></div>
						</td>
						<!-- <td colspan="2" style="border-bottom: 1px solid black;"></td> -->
					</tr>
					<tr>
						<td colspan="3">
							<label class="backbold">6. Pattern : </label> <div style="border-bottom: 1px solid black; margin-left: 120px;"></div>
						</td>
					</tr>
					<tr>
						<td colspan="3">
							<label class="backbold">7. Others : </label> <div style="border-bottom: 1px solid black; margin-left: 120px;"></div>
						</td>
					</tr>
				</table>
			</div>
		</td>

		<td width="6%"></td>
		<!-- identity -->
		<td width="47%">
			<div class="tag">
				<table width="100%">
					<tr>
						<td width="30%">
							
								<label class="backbold" style="float: left;">Comment :</label>
							</center>
						</td>
						<td width="40%">
							
								<label class="backbold">Action Needed :</label>
							</center>
						</td>
						<td width="30%">
							
								<label class="backbold" style="float: right;">By Whom :</label>
							
						</td>
					</tr>
					<tr>
						<td colspan="3" style="border-bottom: 1px solid black;color: white;">A</td>
					</tr>
					<tr>
						<td colspan="3" style="border-bottom: 1px solid black;color: white;">A</td>
					</tr>
					<tr>
						<td colspan="3" style="border-bottom: 1px solid black;color: white;">A</td>
					</tr>

					<tr>
						<td colspan="3">
							<label class="backbold" style="color: white;">A</label>
						</td>
					</tr>
					<tr>
						<td colspan="3" style="border-bottom: 1px solid black;color: white;">A</td>
					</tr>
					<tr>
						<td colspan="3" style="border-bottom: 1px solid black;color: white;">A</td>
					</tr>
					<tr>
						<td colspan="3" style="border-bottom: 1px solid black;color: white;">A</td>
					</tr>
					<tr>
						<td colspan="3" style="border-bottom: 1px solid black;color: white;">A</td>
					</tr>

					<tr>
						<td colspan="3">
							<label class="backbold" style="color: white;">A</label>
						</td>
					</tr>
					<tr>
						<td colspan="3" style="border-bottom: 1px solid black;color: white;">A</td>
					</tr>
					<tr>
						<td colspan="3" style="border-bottom: 1px solid black;color: white;">A</td>
					</tr>
					<tr>
						<td colspan="3" style="border-bottom: 1px solid black;color: white;">A</td>
					</tr>
					<tr>
						<td colspan="3" style="border-bottom: 1px solid black;color: white;">A</td>
					</tr>
					<tr>
						<td colspan="3" style="border-bottom: 1px solid black;color: white;">A</td>
					</tr>
					<tr>
						<td colspan="3" style="border-bottom: 1px solid black;color: white;">A</td>
					</tr>

					<tr>
						<td colspan="3">
							<label class="backbold" style="color: white;">A</label>
						</td>
					</tr>
					<tr>
						<td colspan="3" style="border-bottom: 1px solid black;color: white;">A</td>
					</tr>
					<tr>
						<td colspan="3" style="border-bottom: 1px solid black;color: white;">A</td>
					</tr>
					<tr>
						<td colspan="3" style="border-bottom: 1px solid black;color: white;">A</td>
					</tr>
					<tr>
						<td colspan="3" style="border-bottom: 1px solid black;color: white;">A</td>
					</tr>
					<tr>
						<td colspan="3" style="border-bottom: 1px solid black;color: white;">A</td>
					</tr>

					<tr>
						<td colspan="3" style="border-bottom: 1px solid black;color: white;"><label class="backbold" style="color: white;">A</label></td>
					</tr>
					<tr>
						<td colspan="3" style="border-bottom: 1px solid black;color: white;"><label class="backbold" style="color: white;">A</label></td>
					</tr>
					<!-- <tr>
						<td colspan="3" style="border-bottom: 1px solid black;color: white;"><label class="backbold" style="color: white;">A</label></td>
					</tr> -->

				</table>
			</div>
		</td>
	</tr>

</table>

<div class="separator"></div>

@endif
<div class="page-break"></div>





@endforeach
