<style type="text/css">
@page{
    margin : 25 15 5 15;
}

/*@font-face {
    font-family: 'Arial' !important;
    src: url({{ storage_path('fonts/Arial.ttf') }}) format('truetype');
}*/

.tag{
	width: 367px;
	height: 530px;
    border-style: solid;
    padding-bottom: 5px;
   
}

.table{
	line-height: 10px;
}

.lab{
	font-family: Arial; 
	font-size: 8px;
	font-weight: bold;
}

.barcode {
    line-height: 16px;
    /*padding-left: 5px;*/
    padding-top: 22px;
}

.img_barcode {
    display: block;
    padding: 0px;
}

.img_barcode > img {
    width: 120px;
    height:20px;
}

.barcode_number {
    font-family: sans-serif;
    font-size: 12px !important;
}

.area_barcode {
    width: 50%;
}

.page-break {
    page-break-after: always;
}

.separator {
    margin: 25 0 0 0;
}

.box{
	border: 1px solid black;

	height: 11px;
}

.com{
	margin-top: 8px;
	border-bottom: 1px solid black;
}

.name{
	font-size: 8px;
	padding-left: 7px;
}

.mo{
	font-size: 7px;
}
</style>

@php($chunk = array_chunk($data->toArray(), 4))
@foreach($chunk as $key => $value)
<table>
	<tr>
		<td>
			@if(isset($value[0]))
				<div class="tag">
					<div class="head" style="padding-left: 10px;">
						<table>
							<tr>
								<td colspan="2">
									<label style="font-size: 9px; "><b>GARMENT WASH TEST</b></label>
								</td>
								<td>
									<img src="{{ public_path('assets/icon/adidas_group.png') }}" alt="Image"width="50" style="padding-left: 70px;">
								</td>
							</tr>
							<tr>
								<td colspan="3">
									<center>
										<label style="color: white; font-size: 45px; position: absolute; padding-top: 5px; padding-left:  5px;">CONFIDENTIAL</label>
									</center>
								</td>
							</tr>
							
						</table>
						<br>
						<table>
							
							<tr>
								<td>
									<br>
									<br>
									<label class="lab">Submission NO :</label>
								</td>
								<td colspan="2">
									<br>
									<br>
									<div style="border-bottom: 1px solid black; width: 30px;"><label style="font-size: 10px;">1</label></div>
									
								</td>
							</tr>

							<tr>
								<td>
									<br>
									<label class="lab">LO :</label>
								</td>
								<td colspan="2">
									<br>
									<div style="border-bottom: 1px solid black; width: 228px;"><label style="font-size: 10px;">INDONESIA</label></div>
									
								</td>
							</tr>
						</table>
					</div>

					<br>
					<div class="body1" style="padding-left: 10px;">
						<table class="table">
							<tr>
								<td>
									<label class="lab">Supplier / Supplier Code :</label>
								</td>
								<td>
									<div style="border-bottom: 1px solid black; width: 195px;"><label style="font-size: 10px;">24P001 - APPAREL ONE INDONESIA</label></div>
								</td>
							</tr>
						</table>

						@if($value[0]->sets=='' ||$value[0]->sets=='TOP' )
							<table class="table" style="height: 40px;">
								
								<tr>
									<td width="50">
										<label style="font-family:arial; font-size:8px; font-weight: bold; position: absolute;">Fabric :</label>
									</td>
									<td>
										<div style="width: 225px;"><label style="font-size: 9px; text-decoration: underline; text-align: justify-all; line-height: 12px;">{{ $value[0]->fabric1 }}</label></div>
									</td>
								</tr>
								
								
							</table>
						@elseif($value[0]->sets=='BOT' )
							<table class="table" style="height: 40px;">
								<tr>
									<td width="50">
										<label style="font-family:arial; font-size:8px; font-weight: bold; position: absolute;">Fabric :</label>
									</td>
									<td>
										<div style="width: 225px;"><label style="font-size: 9px; text-decoration: underline; text-align: justify-all; line-height: 12px;">{{ $value[0]->fabric2 }}</label></div>
									</td>
								</tr>
							</table>
						@endif

						
						<div class="body2" style="height: 105px;">
							<table class="table">
								<tr>
									<td width="50">
										<label class="lab">Work-No :</label>
									</td>
									<td>
										@if(isset($value[0]->sets))

											<div style="border-bottom: 1px solid black; width: 225px;"><label style="font-size: 10px;">{{ $value[0]->style }}  {{ $value[0]->sets }} </label></div>
										@else
											<div style="border-bottom: 1px solid black; width: 225px;"><label style="font-size: 10px;">{{ $value[0]->style }} </label></div>
										@endif
									</td>
								</tr>

								<tr>
									<td width="50">
										<label class="lab">Art-No :</label>
									</td>
									<td>
										<div style="border-bottom: 1px solid black; width: 225px;"><label style="font-size: 10px;">{{ $value[0]->article }}</label></div>
									</td>
								</tr>

								<tr>
									<td width="50">
										
										<label class="lab">Season :</label>
									</td>
									<td>
										<div style="border-bottom: 1px solid black; width: 225px;"><label style="font-size: 10px;">{{ $value[0]->season }}</label></div>
									</td>
								</tr>

								<tr>
									<td width="50">
										
										<label class="lab">Size:</label>
									</td>
									<td>
										<div style="border-bottom: 1px solid black; width: 225px;"><label style="font-size: 10px;">{{ $value[0]->size }}</label></div>
									</td>
								</tr>

								<tr>
									<td width="50">
										<label class="lab">Style Name:</label>
									</td>
									<td>
										<div style="border-bottom: 1px solid black; width: 225px;"><label style="font-size: 10px;">{{ $value[0]->product_name }}</label></div>
									</td>
								</tr>

								<tr>
									<td width="50">
										<label class="lab">Date:</label>
									</td>
									<td>
										<div style="border-bottom: 1px solid black; width: 225px;"><label style="font-size: 10px;">{{ date_format(date_create($value[0]->reciept_date),'M d, Y')}}</label></div>
									</td>
								</tr>

								
								<tr>
									<td width="50">
										<label class="lab">Color:</label>
									</td>
									<td>
										<div style=" width: 225px; border-bottom: 1px solid black;"><label style="font-size: 10px; text-decoration: underline;">{{ $value[0]->color }}</label></div>
									</td>
								</tr>
								
							</table>
						</div>

						<br>

						<br>
						<div class="footer">
							<table class="table">
								<tr>
									<td colspan="2">
										<label class="lab">Approved By:</label>
									</td>
								</tr>

								<tr>
									<td width="50">
										<label class="lab">T1 Merch:</label>
									</td>
									<td>
										<div style="border-bottom: 1px solid black; width: 270px;"><label style="font-size: 10px;">{{ $value[0]->merch }}</label></div>
									</td>
								</tr>

								<tr>
									<td width="50">
										<label class="lab">T1 Technician:</label>
									</td>
									<td>
										<div style="border-bottom: 1px solid black; width: 270px;"><label style="font-size: 10px;">{{ $value[0]->technician }}</label></div>
									</td>
								</tr>

								<tr>
									<td width="50">
										<label class="lab">LO/OC/CCDA:</label>
									</td>
									<td>
										<div style="border-bottom: 1px solid black; width: 270px;"><label style="font-size: 10px; color:  white;">A</label></div>
									</td>
								</tr>

								<tr>
									<td width="50">
										<label class="lab">Aproval Date:</label>
									</td>
									<td>
										<div style="border-bottom: 1px solid black; width: 270px;"><label style="font-size: 10px; color:  white;">A</label></div>
									</td>
								</tr>

								<tr>
									<td colspan="2">
										<label class="lab">Comments:</label>
									</td>
								</tr>
								<tr>
									<td colspan="2">
										<div class="barcode">
				                            <div class="row">
				                                <span class="mo">{{ $value[0]->documentno }}</span >
				                            </div>
				                            <div class="img_barcode">
				                                <img src="data:image/png;base64,{{ DNS1D::getBarcodePNG($value[0]->barcode_id, 'C128',2,35) }}" alt="barcode"   />
				                            </div>
				                            <div class="row">
				                                <span class="barcode_number">{{ $value[0]->barcode_id }} {{ $value[0]->sets }}</span >
				                            </div>
				                        </div>
									</td>
								</tr>
							</table>
						</div>
					</div>
				</div>
				<label style="font-weight: bold; font-family: Arial; font-size: 10px; padding-left: 280px;">FR.01-01-MD/00</label>
				@endif

		</td>

		<td>
			<div class="tengah" style="padding-left: 10px; padding-right: 10px;"></div>
		</td>

		<td>
			@if(isset($value[1]))
				<div class="tag">
					<div class="head" style="padding-left: 10px;">
						<table>
							<tr>
								<td colspan="2">
									<label style="font-size: 9px; "><b>GARMENT WASH TEST</b></label>
								</td>
								<td>
									<img src="{{ public_path('assets/icon/adidas_group.png') }}" alt="Image"width="50" style="padding-left: 70px;">
								</td>
							</tr>
							<tr>
								<td colspan="3">
									<center>
										<label style="color: white; font-size: 45px; position: absolute; padding-top: 5px; padding-left:  5px;">CONFIDENTIAL</label>
									</center>
								</td>
							</tr>
							
						</table>
						<br>
						<table>
							
							<tr>
								<td>
									<br>
									<br>
									<label class="lab">Submission NO :</label>
								</td>
								<td colspan="2">
									<br>
									<br>
									<div style="border-bottom: 1px solid black; width: 30px;"><label style="font-size: 10px;">1</label></div>
									
								</td>
							</tr>

							<tr>
								<td>
									<br>
									<label class="lab">LO :</label>
								</td>
								<td colspan="2">
									<br>
									<div style="border-bottom: 1px solid black; width: 228px;"><label style="font-size: 10px;">INDONESIA</label></div>
									
								</td>
							</tr>
						</table>
					</div>

					<br>
					<div class="body1" style="padding-left: 10px;">
						<table class="table">
							<tr>
								<td>
									<label class="lab">Supplier / Supplier Code :</label>
								</td>
								<td>
									<div style="border-bottom: 1px solid black; width: 195px;"><label style="font-size: 10px;">24P001 - APPAREL ONE INDONESIA</label></div>
								</td>
							</tr>
						</table>

						@if($value[1]->sets=='' ||$value[1]->sets=='TOP' )
							<table class="table" style="height: 40px;">
								
								<tr>
									<td width="50">
										<label style="font-family:arial; font-size:8px; font-weight: bold; position: absolute;">Fabric :</label>
									</td>
									<td>
										<div style="width: 225px;"><label style="font-size: 9px; text-decoration: underline; text-align: justify-all; line-height: 12px;">{{ $value[1]->fabric1 }}</label></div>
									</td>
								</tr>
								
								
							</table>
						@elseif($value[1]->sets=='BOT' )
							<table class="table" style="height: 40px;">
								<tr>
									<td width="50">
										<label style="font-family:arial; font-size:8px; font-weight: bold; position: absolute;">Fabric :</label>
									</td>
									<td>
										<div style="width: 225px;"><label style="font-size: 9px; text-decoration: underline; text-align: justify-all; line-height: 12px;">{{ $value[1]->fabric2 }}</label></div>
									</td>
								</tr>
							</table>
						@endif

						
						<div class="body2" style="height: 105px;">
							<table class="table">
								<tr>
									<td width="50">
										<label class="lab">Work-No :</label>
									</td>
									<td>
										@if(isset($value[1]->sets))

											<div style="border-bottom: 1px solid black; width: 225px;"><label style="font-size: 10px;">{{ $value[1]->style }}  {{ $value[1]->sets }} </label></div>
										@else
											<div style="border-bottom: 1px solid black; width: 225px;"><label style="font-size: 10px;">{{ $value[1]->style }} </label></div>
										@endif
									</td>
								</tr>

								<tr>
									<td width="50">
										<label class="lab">Art-No :</label>
									</td>
									<td>
										<div style="border-bottom: 1px solid black; width: 225px;"><label style="font-size: 10px;">{{ $value[1]->article }}</label></div>
									</td>
								</tr>

								<tr>
									<td width="50">
										
										<label class="lab">Season :</label>
									</td>
									<td>
										<div style="border-bottom: 1px solid black; width: 225px;"><label style="font-size: 10px;">{{ $value[1]->season }}</label></div>
									</td>
								</tr>

								<tr>
									<td width="50">
										
										<label class="lab">Size:</label>
									</td>
									<td>
										<div style="border-bottom: 1px solid black; width: 225px;"><label style="font-size: 10px;">{{ $value[1]->size }}</label></div>
									</td>
								</tr>

								<tr>
									<td width="50">
										<label class="lab">Style Name:</label>
									</td>
									<td>
										<div style="border-bottom: 1px solid black; width: 225px;"><label style="font-size: 10px;">{{ $value[1]->product_name }}</label></div>
									</td>
								</tr>

								<tr>
									<td width="50">
										<label class="lab">Date:</label>
									</td>
									<td>
										<div style="border-bottom: 1px solid black; width: 225px;"><label style="font-size: 10px;">{{ date_format(date_create($value[1]->reciept_date),'M d, Y')}}</label></div>
									</td>
								</tr>

								<tr>
									<td width="50">
										<label class="lab">Color:</label>
									</td>
									<td>
										<div style=" width: 225px; border-bottom: 1px solid black;"><label style="font-size: 10px; text-decoration: underline;">{{ $value[1]->color }}</label></div>
									</td>
								</tr>
							</table>
						</div>

						<br>

						<br>
						<div class="footer">
							<table class="table">
								<tr>
									<td colspan="2">
										<label class="lab">Approved By:</label>
									</td>
								</tr>

								<tr>
									<td width="50">
										<label class="lab">T1 Merch:</label>
									</td>
									<td>
										<div style="border-bottom: 1px solid black; width: 270px;"><label style="font-size: 10px;">{{ $value[1]->merch }}</label></div>
									</td>
								</tr>

								<tr>
									<td width="50">
										<label class="lab">T1 Technician:</label>
									</td>
									<td>
										<div style="border-bottom: 1px solid black; width: 270px;"><label style="font-size: 10px;">{{ $value[1]->technician }}</label></div>
									</td>
								</tr>

								<tr>
									<td width="50">
										<label class="lab">LO/OC/CCDA:</label>
									</td>
									<td>
										<div style="border-bottom: 1px solid black; width: 270px;"><label style="font-size: 10px; color:  white;">A</label></div>
									</td>
								</tr>

								<tr>
									<td width="50">
										<label class="lab">Aproval Date:</label>
									</td>
									<td>
										<div style="border-bottom: 1px solid black; width: 270px;"><label style="font-size: 10px; color:  white;">A</label></div>
									</td>
								</tr>

								<tr>
									<td colspan="2">
										<label class="lab">Comments:</label>
									</td>
								</tr>
								<tr>
									<td colspan="2">
										<div class="barcode">
				                            <div class="row">
				                                <span class="mo">{{ $value[1]->documentno }}</span >
				                            </div>
				                            <div class="img_barcode">
				                                <img src="data:image/png;base64,{{ DNS1D::getBarcodePNG($value[1]->barcode_id, 'C128',2,35) }}" alt="barcode"   />
				                            </div>
				                            <div class="row">
				                                <span class="barcode_number">{{ $value[1]->barcode_id }} {{ $value[1]->sets }}</span >
				                            </div>
				                        </div>
									</td>
								</tr>
							</table>
						</div>
					</div>
				</div>
				<label style="font-weight: bold; font-family: Arial; font-size: 10px; padding-left: 280px;">FR.01-01-MD/00</label>
				@endif

		</td>
	</tr>
	<tr>
		<td>
			@if(isset($value[2]))
				<div class="tag">
					<div class="head" style="padding-left: 10px;">
						<table>
							<tr>
								<td colspan="2">
									<label style="font-size: 9px; "><b>GARMENT WASH TEST</b></label>
								</td>
								<td>
									<img src="{{ public_path('assets/icon/adidas_group.png') }}" alt="Image"width="50" style="padding-left: 70px;">
								</td>
							</tr>
							<tr>
								<td colspan="3">
									<center>
										<label style="color: white; font-size: 45px; position: absolute; padding-top: 5px; padding-left:  5px;">CONFIDENTIAL</label>
									</center>
								</td>
							</tr>
							
						</table>
						<br>
						<table>
							
							<tr>
								<td>
									<br>
									<br>
									<label class="lab">Submission NO :</label>
								</td>
								<td colspan="2">
									<br>
									<br>
									<div style="border-bottom: 1px solid black; width: 30px;"><label style="font-size: 10px;">1</label></div>
									
								</td>
							</tr>

							<tr>
								<td>
									<br>
									<label class="lab">LO :</label>
								</td>
								<td colspan="2">
									<br>
									<div style="border-bottom: 1px solid black; width: 228px;"><label style="font-size: 10px;">INDONESIA</label></div>
									
								</td>
							</tr>
						</table>
					</div>

					<br>
					<div class="body1" style="padding-left: 10px;">
						<table class="table">
							<tr>
								<td>
									<label class="lab">Supplier / Supplier Code :</label>
								</td>
								<td>
									<div style="border-bottom: 1px solid black; width: 195px;"><label style="font-size: 10px;">24P001 - APPAREL ONE INDONESIA</label></div>
								</td>
							</tr>
						</table>

						@if($value[2]->sets=='' ||$value[2]->sets=='TOP' )
							<table class="table" style="height: 40px;">
								
								<tr>
									<td width="50">
										<label style="font-family:arial; font-size:8px; font-weight: bold; position: absolute;">Fabric :</label>
									</td>
									<td>
										<div style="width: 225px;"><label style="font-size: 9px; text-decoration: underline; text-align: justify-all; line-height: 12px;">{{ $value[2]->fabric1 }}</label></div>
									</td>
								</tr>
								
								
							</table>
						@elseif($value[2]->sets=='BOT' )
							<table class="table" style="height: 40px;">
								<tr>
									<td width="50">
										<label style="font-family:arial; font-size:8px; font-weight: bold; position: absolute;">Fabric :</label>
									</td>
									<td>
										<div style="width: 225px;"><label style="font-size: 9px; text-decoration: underline; text-align: justify-all; line-height: 12px;">{{ $value[2]->fabric2 }}</label></div>
									</td>
								</tr>
							</table>
						@endif

						
						<div class="body2" style="height: 105px;">
							<table class="table">
								<tr>
									<td width="50">
										<label class="lab">Work-No :</label>
									</td>
									<td>
										@if(isset($value[2]->sets))

											<div style="border-bottom: 1px solid black; width: 225px;"><label style="font-size: 10px;">{{ $value[2]->style }}  {{ $value[2]->sets }} </label></div>
										@else
											<div style="border-bottom: 1px solid black; width: 225px;"><label style="font-size: 10px;">{{ $value[2]->style }} </label></div>
										@endif
									</td>
								</tr>

								<tr>
									<td width="50">
										<label class="lab">Art-No :</label>
									</td>
									<td>
										<div style="border-bottom: 1px solid black; width: 225px;"><label style="font-size: 10px;">{{ $value[2]->article }}</label></div>
									</td>
								</tr>

								<tr>
									<td width="50">
										
										<label class="lab">Season :</label>
									</td>
									<td>
										<div style="border-bottom: 1px solid black; width: 225px;"><label style="font-size: 10px;">{{ $value[2]->season }}</label></div>
									</td>
								</tr>

								<tr>
									<td width="50">
										
										<label class="lab">Size:</label>
									</td>
									<td>
										<div style="border-bottom: 1px solid black; width: 225px;"><label style="font-size: 10px;">{{ $value[2]->size }}</label></div>
									</td>
								</tr>

								<tr>
									<td width="50">
										<label class="lab">Style Name:</label>
									</td>
									<td>
										<div style="border-bottom: 1px solid black; width: 225px;"><label style="font-size: 10px;">{{ $value[2]->product_name }}</label></div>
									</td>
								</tr>

								<tr>
									<td width="50">
										<label class="lab">Date:</label>
									</td>
									<td>
										<div style="border-bottom: 1px solid black; width: 225px;"><label style="font-size: 10px;">{{ date_format(date_create($value[2]->reciept_date),'M d, Y')}}</label></div>
									</td>
								</tr>

								<tr>
									<td width="50">
										<label class="lab">Color:</label>
									</td>
									<td>
										<div style=" width: 225px; border-bottom: 1px solid black;"><label style="font-size: 10px; text-decoration: underline;">{{ $value[2]->color }}</label></div>
									</td>
								</tr>
							</table>
						</div>

						<br>

						<br>
						<div class="footer">
							<table class="table">
								<tr>
									<td colspan="2">
										<label class="lab">Approved By:</label>
									</td>
								</tr>

								<tr>
									<td width="50">
										<label class="lab">T1 Merch:</label>
									</td>
									<td>
										<div style="border-bottom: 1px solid black; width: 270px;"><label style="font-size: 10px;">{{ $value[2]->merch }}</label></div>
									</td>
								</tr>

								<tr>
									<td width="50">
										<label class="lab">T1 Technician:</label>
									</td>
									<td>
										<div style="border-bottom: 1px solid black; width: 270px;"><label style="font-size: 10px;">{{ $value[2]->technician }}</label></div>
									</td>
								</tr>

								<tr>
									<td width="50">
										<label class="lab">LO/OC/CCDA:</label>
									</td>
									<td>
										<div style="border-bottom: 1px solid black; width: 270px;"><label style="font-size: 10px; color:  white;">A</label></div>
									</td>
								</tr>

								<tr>
									<td width="50">
										<label class="lab">Aproval Date:</label>
									</td>
									<td>
										<div style="border-bottom: 1px solid black; width: 270px;"><label style="font-size: 10px; color:  white;">A</label></div>
									</td>
								</tr>

								<tr>
									<td colspan="2">
										<label class="lab">Comments:</label>
									</td>
								</tr>
								<tr>
									<td colspan="2">
										<div class="barcode">
				                            <div class="row">
				                                <span class="mo">{{ $value[2]->documentno }}</span >
				                            </div>
				                            <div class="img_barcode">
				                                <img src="data:image/png;base64,{{ DNS1D::getBarcodePNG($value[2]->barcode_id, 'C128',2,35) }}" alt="barcode"   />
				                            </div>
				                            <div class="row">
				                                <span class="barcode_number">{{ $value[2]->barcode_id }} {{ $value[2]->sets }}</span >
				                            </div>
				                        </div>
									</td>
								</tr>
							</table>
						</div>
					</div>
				</div>
				<label style="font-weight: bold; font-family: Arial; font-size: 10px; padding-left: 280px;">FR.01-01-MD/00</label>
				@endif

		</td>

		<td>
			<div class="tengah" style="padding-left: 10px; padding-right: 10px;"></div>
		</td>

		<td>
			@if(isset($value[3]))
				<div class="tag">
					<div class="head" style="padding-left: 10px;">
						<table>
							<tr>
								<td colspan="2">
									<label style="font-size: 9px; "><b>GARMENT WASH TEST</b></label>
								</td>
								<td>
									<img src="{{ public_path('assets/icon/adidas_group.png') }}" alt="Image"width="50" style="padding-left: 70px;">
								</td>
							</tr>
							<tr>
								<td colspan="3">
									<center>
										<label style="color: white; font-size: 45px; position: absolute; padding-top: 5px; padding-left:  5px;">CONFIDENTIAL</label>
									</center>
								</td>
							</tr>
							
						</table>
						<br>
						<table>
							
							<tr>
								<td>
									<br>
									<br>
									<label class="lab">Submission NO :</label>
								</td>
								<td colspan="2">
									<br>
									<br>
									<div style="border-bottom: 1px solid black; width: 30px;"><label style="font-size: 10px;">1</label></div>
									
								</td>
							</tr>

							<tr>
								<td>
									<br>
									<label class="lab">LO :</label>
								</td>
								<td colspan="2">
									<br>
									<div style="border-bottom: 1px solid black; width: 228px;"><label style="font-size: 10px;">INDONESIA</label></div>
									
								</td>
							</tr>
						</table>
					</div>

					<br>
					<div class="body1" style="padding-left: 10px;">
						<table class="table">
							<tr>
								<td>
									<label class="lab">Supplier / Supplier Code :</label>
								</td>
								<td>
									<div style="border-bottom: 1px solid black; width: 195px;"><label style="font-size: 10px;">24P001 - APPAREL ONE INDONESIA</label></div>
								</td>
							</tr>
						</table>

						@if($value[3]->sets=='' ||$value[3]->sets=='TOP' )
							<table class="table" style="height: 40px;">
								
								<tr>
									<td width="50">
										<label style="font-family:arial; font-size:8px; font-weight: bold; position: absolute;">Fabric :</label>
									</td>
									<td>
										<div style="width: 225px;"><label style="font-size: 9px; text-decoration: underline; text-align: justify-all; line-height: 12px;">{{ $value[3]->fabric1 }}</label></div>
									</td>
								</tr>
								
								
							</table>
						@elseif($value[3]->sets=='BOT' )
							<table class="table" style="height: 40px;">
								<tr>
									<td width="50">
										<label style="font-family:arial; font-size:8px; font-weight: bold; position: absolute;">Fabric :</label>
									</td>
									<td>
										<div style="width: 225px;"><label style="font-size: 9px; text-decoration: underline; text-align: justify-all; line-height: 12px;">{{ $value[3]->fabric2 }}</label></div>
									</td>
								</tr>
							</table>
						@endif

						
						<div class="body2" style="height: 105px;">
							<table class="table">
								<tr>
									<td width="50">
										<label class="lab">Work-No :</label>
									</td>
									<td>
										@if(isset($value[3]->sets))

											<div style="border-bottom: 1px solid black; width: 225px;"><label style="font-size: 10px;">{{ $value[3]->style }}  {{ $value[3]->sets }} </label></div>
										@else
											<div style="border-bottom: 1px solid black; width: 225px;"><label style="font-size: 10px;">{{ $value[3]->style }} </label></div>
										@endif
									</td>
								</tr>

								<tr>
									<td width="50">
										<label class="lab">Art-No :</label>
									</td>
									<td>
										<div style="border-bottom: 1px solid black; width: 225px;"><label style="font-size: 10px;">{{ $value[3]->article }}</label></div>
									</td>
								</tr>

								<tr>
									<td width="50">
										
										<label class="lab">Season :</label>
									</td>
									<td>
										<div style="border-bottom: 1px solid black; width: 225px;"><label style="font-size: 10px;">{{ $value[3]->season }}</label></div>
									</td>
								</tr>

								<tr>
									<td width="50">
										
										<label class="lab">Size:</label>
									</td>
									<td>
										<div style="border-bottom: 1px solid black; width: 225px;"><label style="font-size: 10px;">{{ $value[3]->size }}</label></div>
									</td>
								</tr>

								<tr>
									<td width="50">
										<label class="lab">Style Name:</label>
									</td>
									<td>
										<div style="border-bottom: 1px solid black; width: 225px;"><label style="font-size: 10px;">{{ $value[3]->product_name }}</label></div>
									</td>
								</tr>

								<tr>
									<td width="50">
										<label class="lab">Date:</label>
									</td>
									<td>
										<div style="border-bottom: 1px solid black; width: 225px;"><label style="font-size: 10px;">{{ date_format(date_create($value[3]->reciept_date),'M d, Y')}}</label></div>
									</td>
								</tr>

								<tr>
									<td width="50">
										<label class="lab">Color:</label>
									</td>
									<td>
										<div style=" width: 225px; border-bottom: 1px solid black;"><label style="font-size: 10px; text-decoration: underline;">{{ $value[3]->color }}</label></div>
									</td>
								</tr>
							</table>
						</div>

						<br>

						<br>
						<div class="footer">
							<table class="table">
								<tr>
									<td colspan="2">
										<label class="lab">Approved By:</label>
									</td>
								</tr>

								<tr>
									<td width="50">
										<label class="lab">T1 Merch:</label>
									</td>
									<td>
										<div style="border-bottom: 1px solid black; width: 270px;"><label style="font-size: 10px;">{{ $value[3]->merch }}</label></div>
									</td>
								</tr>

								<tr>
									<td width="50">
										<label class="lab">T1 Technician:</label>
									</td>
									<td>
										<div style="border-bottom: 1px solid black; width: 270px;"><label style="font-size: 10px;">{{ $value[3]->technician }}</label></div>
									</td>
								</tr>

								<tr>
									<td width="50">
										<label class="lab">LO/OC/CCDA:</label>
									</td>
									<td>
										<div style="border-bottom: 1px solid black; width: 270px;"><label style="font-size: 10px; color:  white;">A</label></div>
									</td>
								</tr>

								<tr>
									<td width="50">
										<label class="lab">Aproval Date:</label>
									</td>
									<td>
										<div style="border-bottom: 1px solid black; width: 270px;"><label style="font-size: 10px; color:  white;">A</label></div>
									</td>
								</tr>

								<tr>
									<td colspan="2">
										<label class="lab">Comments:</label>
									</td>
								</tr>
								<tr>
									<td colspan="2">
										<div class="barcode">
				                            <div class="row">
				                                <span class="mo">{{ $value[3]->documentno }}</span >
				                            </div>
				                            <div class="img_barcode">
				                                <img src="data:image/png;base64,{{ DNS1D::getBarcodePNG($value[3]->barcode_id, 'C128',2,35) }}" alt="barcode"   />
				                            </div>
				                            <div class="row">
				                                <span class="barcode_number">{{ $value[3]->barcode_id }} {{ $value[3]->sets }}</span >
				                            </div>
				                        </div>
									</td>
								</tr>
							</table>
						</div>
					</div>
				</div>
				<label style="font-weight: bold; font-family: Arial; font-size: 10px; padding-left: 280px;">FR.01-01-MD/00</label>
				@endif

		</td>
	</tr>
</table>

<div class="page-break"></div>

<table>
	<tr>
		
		<td>
			@if(isset($value[0]))
			<div class="tag">
				<table style="padding-top: 30px;">
					<tr>
						<td width="50"><label style="font-size: 8px; color: white;">Material :</label></td>
						<td width="40">
							<label style="font-size: 8px;"><center>Original</center></label>
						</td>
						<td></td>
						<td width="35">
							<label style="font-size: 8px;"><center>Subtitue <br> Quality</center></label>
						</td>
						<td></td>
						<td width="35">
							<label style="font-size: 8px;"><center>Subtitue <br> Color</center></label>
						</td>
						<td></td>
						<td width="90">
							<label style="font-size: 8px;"><center>additional info / comments :</center></label>
						</td>
					</tr>

					<tr>
						<td colspan="8"><label style="font-size: 8px;">Material :</label></td>
					</tr>

					<tr>
						<td width="50"><label class="name">Shell Fabric</label></td>
						<td width="35" class="box">
							
						</td>
						<td></td>
						<td width="35" class="box">
							
						</td>
						<td></td>
						<td width="35" class="box">
							
						</td>
						<td></td>
						<td width="90">
							<div class="com"></div>
						</td>
					</tr>

					<tr>
						<td colspan="8" height="5"></td>
					</tr>

					<tr>
						<td width="50"><label class="name">Additional Fabric</label></td>
						<td width="35" class="box">
							
						</td>
						<td></td>
						<td width="35" class="box">
							
						</td>
						<td></td>
						<td width="35" class="box">
							
						</td>
						<td></td>
						<td width="90">
							<div class="com"></div>
						</td>
					</tr>
					<tr>
						<td width="50"><label class="name">Lining</label></td>
						<td width="35" class="box">
							
						</td>
						<td></td>
						<td width="35" class="box">
							
						</td>
						<td></td>
						<td width="35" class="box">
							
						</td>
						<td></td>
						<td width="90">
							<div class="com"></div>
						</td>
					</tr>

					<tr>
						<td width="50"><label class="name">Bindings</label></td>
						<td width="35" class="box">
							
						</td>
						<td></td>
						<td width="35" class="box">
							
						</td>
						<td></td>
						<td width="35" class="box">
							
						</td>
						<td></td>
						<td width="90">
							<div class="com"></div>
						</td>
					</tr>

					<tr>
						<td width="50"><label class="name">Rib</label></td>
						<td width="35" class="box">
							
						</td>
						<td></td>
						<td width="35" class="box">
							
						</td>
						<td></td>
						<td width="35" class="box">
							
						</td>
						<td></td>
						<td width="90">
							<div class="com"></div>
						</td>
					</tr>

					<tr>
						<td width="50"><label class="name">Other Additional Finishing Processes</label></td>
						<td width="35" class="box">
							
						</td>
						<td></td>
						<td width="35" class="box">
							
						</td>
						<td></td>
						<td width="35" class="box">
							
						</td>
						<td></td>
						<td width="90">
							<div class="com"></div>
						</td>
					</tr>

					<tr>
						<td colspan="8">
							<label style="font-size: 6px; font-weight: bold;"><center>FOR FINAL FABRIC APPROVAL YOU SHOULD ALWAYS REFER TO THE 1st BULK APPROVAL</center></label>
						</td>
					</tr>
					<tr>
						<td colspan="8">
							<label style="font-size: 8px;">Accessories :</label>
						</td>
					</tr>

					<tr>
						<td width="50"><label class="name">Tape</label></td>
						<td width="35" class="box">
							
						</td>
						<td></td>
						<td width="35" class="box">
							
						</td>
						<td></td>
						<td width="35" class="box">
							
						</td>
						<td></td>
						<td width="90">
							<div class="com"></div>
						</td>
					</tr>

					<tr>
						<td width="50"><label class="name">Zipper</label></td>
						<td width="35" class="box">
							
						</td>
						<td></td>
						<td width="35" class="box">
							
						</td>
						<td></td>
						<td width="35" class="box">
							
						</td>
						<td></td>
						<td width="90">
							<div class="com"></div>
						</td>
					</tr>

					<tr>
						<td width="50"><label class="name">Puller</label></td>
						<td width="35" class="box">
							
						</td>
						<td></td>
						<td width="35" class="box">
							
						</td>
						<td></td>
						<td width="35" class="box">
							
						</td>
						<td></td>
						<td width="90">
							<div class="com"></div>
						</td>
					</tr>

					<tr>
						<td width="50"><label class="name">Eyelet</label></td>
						<td width="35" class="box">
							
						</td>
						<td></td>
						<td width="35" class="box">
							
						</td>
						<td></td>
						<td width="35" class="box">
							
						</td>
						<td></td>
						<td width="90">
							<div class="com"></div>
						</td>
					</tr>

					<tr>
						<td width="50"><label class="name">Tiecord</label></td>
						<td width="35" class="box">
							
						</td>
						<td></td>
						<td width="35" class="box">
							
						</td>
						<td></td>
						<td width="35" class="box">
							
						</td>
						<td></td>
						<td width="90">
							<div class="com"></div>
						</td>
					</tr>

					<tr>
						<td width="50"><label class="name">Stopper</label></td>
						<td width="35" class="box">
							
						</td>
						<td></td>
						<td width="35" class="box">
							
						</td>
						<td></td>
						<td width="35" class="box">
							
						</td>
						<td></td>
						<td width="90">
							<div class="com"></div>
						</td>
					</tr>

					<tr>
						<td width="50"><label class="name">Elastic</label></td>
						<td width="35" class="box">
							
						</td>
						<td></td>
						<td width="35" class="box">
							
						</td>
						<td></td>
						<td width="35" class="box">
							
						</td>
						<td></td>
						<td width="90">
							<div class="com"></div>
						</td>
					</tr>

					<tr>
						<td width="50"><label class="name">Hook & Loop</label></td>
						<td width="35" class="box">
							
						</td>
						<td></td>
						<td width="35" class="box">
							
						</td>
						<td></td>
						<td width="35" class="box">
							
						</td>
						<td></td>
						<td width="90">
							<div class="com"></div>
						</td>
					</tr>

					<tr>
						<td width="50"><label class="name">Snap</label></td>
						<td width="35" class="box">
							
						</td>
						<td></td>
						<td width="35" class="box">
							
						</td>
						<td></td>
						<td width="35" class="box">
							
						</td>
						<td></td>
						<td width="90">
							<div class="com"></div>
						</td>
					</tr>

					<tr>
						<td width="50"><label class="name">Others</label></td>
						<td width="35" class="box">
							
						</td>
						<td></td>
						<td width="35" class="box">
							
						</td>
						<td></td>
						<td width="35" class="box">
							
						</td>
						<td></td>
						<td width="90">
							<div class="com"></div>
						</td>
					</tr>

					<tr>
						<td colspan="8" height="5"></td>
					</tr>
					<tr>
						<td colspan="8">
							<label style="font-size: 8px;">Artwork :</label>
						</td>
					</tr>
					
					<tr>
						<td width="50"><label class="name">Print</label></td>
						<td width="35" class="box">
							
						</td>
						<td></td>
						<td width="35" class="box">
							
						</td>
						<td></td>
						<td width="35" class="box">
							
						</td>
						<td></td>
						<td width="90">
							<div class="com"></div>
						</td>
					</tr>
					<tr>
						<td width="50"><label class="name">Embroidery</label></td>
						<td width="35" class="box">
							
						</td>
						<td></td>
						<td width="35" class="box">
							
						</td>
						<td></td>
						<td width="35" class="box">
							
						</td>
						<td></td>
						<td width="90">
							<div class="com"></div>
						</td>
					</tr>
					<tr>
						<td width="50"><label class="name">Other</label></td>
						<td width="35" class="box">
							
						</td>
						<td></td>
						<td width="35" class="box">
							
						</td>
						<td></td>
						<td width="35" class="box">
							
						</td>
						<td></td>
						<td width="90">
							<div class="com"></div>
						</td>
					</tr>

					<tr>
						<td colspan="8">
							<label style="font-size: 8px;">L & P :</label>
						</td>
					</tr>

					<tr>
						<td width="50"><label class="name">Main Label</label></td>
						<td width="35" class="box">
							
						</td>
						<td></td>
						<td width="35" class="box">
							
						</td>
						<td></td>
						<td width="35" class="box">
							
						</td>
						<td></td>
						<td width="90">
							<div class="com"></div>
						</td>
					</tr>
					<tr>
						<td width="50"><label class="name">Care Label</label></td>
						<td width="35" class="box">
							
						</td>
						<td></td>
						<td width="35" class="box">
							
						</td>
						<td></td>
						<td width="35" class="box">
							
						</td>
						<td></td>
						<td width="90">
							<div class="com"></div>
						</td>
					</tr>
					<tr>
						<td width="50"><label class="name">Other</label></td>
						<td width="35" class="box">
							
						</td>
						<td></td>
						<td width="35" class="box">
							
						</td>
						<td></td>
						<td width="35" class="box">
							
						</td>
						<td></td>
						<td width="90">
							<div class="com"></div>
						</td>
					</tr>
				</table>
			</div>
			<label style="font-weight: bold; font-family: Arial; font-size: 10px; padding-left: 280px; color: white;">FR.01-01-MD/00</label>
			@endif
		</td>
		
		<td>
			<div class="tengah" style="padding-left: 10px; padding-right: 10px;"></div>
		</td>
		
		<td>
			@if(isset($value[1]))
			<div class="tag">
				<table style="padding-top: 30px;">
					<tr>
						<td width="50"><label style="font-size: 8px; color: white;">Material :</label></td>
						<td width="40">
							<label style="font-size: 8px;"><center>Original</center></label>
						</td>
						<td></td>
						<td width="35">
							<label style="font-size: 8px;"><center>Subtitue <br> Quality</center></label>
						</td>
						<td></td>
						<td width="35">
							<label style="font-size: 8px;"><center>Subtitue <br> Color</center></label>
						</td>
						<td></td>
						<td width="90">
							<label style="font-size: 8px;"><center>additional info / comments :</center></label>
						</td>
					</tr>

					<tr>
						<td colspan="8"><label style="font-size: 8px;">Material :</label></td>
					</tr>

					<tr>
						<td width="50"><label class="name">Shell Fabric</label></td>
						<td width="35" class="box">
							
						</td>
						<td></td>
						<td width="35" class="box">
							
						</td>
						<td></td>
						<td width="35" class="box">
							
						</td>
						<td></td>
						<td width="90">
							<div class="com"></div>
						</td>
					</tr>

					<tr>
						<td colspan="8" height="5"></td>
					</tr>

					<tr>
						<td width="50"><label class="name">Additional Fabric</label></td>
						<td width="35" class="box">
							
						</td>
						<td></td>
						<td width="35" class="box">
							
						</td>
						<td></td>
						<td width="35" class="box">
							
						</td>
						<td></td>
						<td width="90">
							<div class="com"></div>
						</td>
					</tr>
					<tr>
						<td width="50"><label class="name">Lining</label></td>
						<td width="35" class="box">
							
						</td>
						<td></td>
						<td width="35" class="box">
							
						</td>
						<td></td>
						<td width="35" class="box">
							
						</td>
						<td></td>
						<td width="90">
							<div class="com"></div>
						</td>
					</tr>

					<tr>
						<td width="50"><label class="name">Bindings</label></td>
						<td width="35" class="box">
							
						</td>
						<td></td>
						<td width="35" class="box">
							
						</td>
						<td></td>
						<td width="35" class="box">
							
						</td>
						<td></td>
						<td width="90">
							<div class="com"></div>
						</td>
					</tr>

					<tr>
						<td width="50"><label class="name">Rib</label></td>
						<td width="35" class="box">
							
						</td>
						<td></td>
						<td width="35" class="box">
							
						</td>
						<td></td>
						<td width="35" class="box">
							
						</td>
						<td></td>
						<td width="90">
							<div class="com"></div>
						</td>
					</tr>

					<tr>
						<td width="50"><label class="name">Other Additional Finishing Processes</label></td>
						<td width="35" class="box">
							
						</td>
						<td></td>
						<td width="35" class="box">
							
						</td>
						<td></td>
						<td width="35" class="box">
							
						</td>
						<td></td>
						<td width="90">
							<div class="com"></div>
						</td>
					</tr>

					<tr>
						<td colspan="8">
							<label style="font-size: 6px; font-weight: bold;"><center>FOR FINAL FABRIC APPROVAL YOU SHOULD ALWAYS REFER TO THE 1st BULK APPROVAL</center></label>
						</td>
					</tr>
					<tr>
						<td colspan="8">
							<label style="font-size: 8px;">Accessories :</label>
						</td>
					</tr>

					<tr>
						<td width="50"><label class="name">Tape</label></td>
						<td width="35" class="box">
							
						</td>
						<td></td>
						<td width="35" class="box">
							
						</td>
						<td></td>
						<td width="35" class="box">
							
						</td>
						<td></td>
						<td width="90">
							<div class="com"></div>
						</td>
					</tr>

					<tr>
						<td width="50"><label class="name">Zipper</label></td>
						<td width="35" class="box">
							
						</td>
						<td></td>
						<td width="35" class="box">
							
						</td>
						<td></td>
						<td width="35" class="box">
							
						</td>
						<td></td>
						<td width="90">
							<div class="com"></div>
						</td>
					</tr>

					<tr>
						<td width="50"><label class="name">Puller</label></td>
						<td width="35" class="box">
							
						</td>
						<td></td>
						<td width="35" class="box">
							
						</td>
						<td></td>
						<td width="35" class="box">
							
						</td>
						<td></td>
						<td width="90">
							<div class="com"></div>
						</td>
					</tr>

					<tr>
						<td width="50"><label class="name">Eyelet</label></td>
						<td width="35" class="box">
							
						</td>
						<td></td>
						<td width="35" class="box">
							
						</td>
						<td></td>
						<td width="35" class="box">
							
						</td>
						<td></td>
						<td width="90">
							<div class="com"></div>
						</td>
					</tr>

					<tr>
						<td width="50"><label class="name">Tiecord</label></td>
						<td width="35" class="box">
							
						</td>
						<td></td>
						<td width="35" class="box">
							
						</td>
						<td></td>
						<td width="35" class="box">
							
						</td>
						<td></td>
						<td width="90">
							<div class="com"></div>
						</td>
					</tr>

					<tr>
						<td width="50"><label class="name">Stopper</label></td>
						<td width="35" class="box">
							
						</td>
						<td></td>
						<td width="35" class="box">
							
						</td>
						<td></td>
						<td width="35" class="box">
							
						</td>
						<td></td>
						<td width="90">
							<div class="com"></div>
						</td>
					</tr>

					<tr>
						<td width="50"><label class="name">Elastic</label></td>
						<td width="35" class="box">
							
						</td>
						<td></td>
						<td width="35" class="box">
							
						</td>
						<td></td>
						<td width="35" class="box">
							
						</td>
						<td></td>
						<td width="90">
							<div class="com"></div>
						</td>
					</tr>

					<tr>
						<td width="50"><label class="name">Hook & Loop</label></td>
						<td width="35" class="box">
							
						</td>
						<td></td>
						<td width="35" class="box">
							
						</td>
						<td></td>
						<td width="35" class="box">
							
						</td>
						<td></td>
						<td width="90">
							<div class="com"></div>
						</td>
					</tr>

					<tr>
						<td width="50"><label class="name">Snap</label></td>
						<td width="35" class="box">
							
						</td>
						<td></td>
						<td width="35" class="box">
							
						</td>
						<td></td>
						<td width="35" class="box">
							
						</td>
						<td></td>
						<td width="90">
							<div class="com"></div>
						</td>
					</tr>

					<tr>
						<td width="50"><label class="name">Others</label></td>
						<td width="35" class="box">
							
						</td>
						<td></td>
						<td width="35" class="box">
							
						</td>
						<td></td>
						<td width="35" class="box">
							
						</td>
						<td></td>
						<td width="90">
							<div class="com"></div>
						</td>
					</tr>

					<tr>
						<td colspan="8" height="5"></td>
					</tr>
					<tr>
						<td colspan="8">
							<label style="font-size: 8px;">Artwork :</label>
						</td>
					</tr>
					
					<tr>
						<td width="50"><label class="name">Print</label></td>
						<td width="35" class="box">
							
						</td>
						<td></td>
						<td width="35" class="box">
							
						</td>
						<td></td>
						<td width="35" class="box">
							
						</td>
						<td></td>
						<td width="90">
							<div class="com"></div>
						</td>
					</tr>
					<tr>
						<td width="50"><label class="name">Embroidery</label></td>
						<td width="35" class="box">
							
						</td>
						<td></td>
						<td width="35" class="box">
							
						</td>
						<td></td>
						<td width="35" class="box">
							
						</td>
						<td></td>
						<td width="90">
							<div class="com"></div>
						</td>
					</tr>
					<tr>
						<td width="50"><label class="name">Other</label></td>
						<td width="35" class="box">
							
						</td>
						<td></td>
						<td width="35" class="box">
							
						</td>
						<td></td>
						<td width="35" class="box">
							
						</td>
						<td></td>
						<td width="90">
							<div class="com"></div>
						</td>
					</tr>

					<tr>
						<td colspan="8">
							<label style="font-size: 8px;">L & P :</label>
						</td>
					</tr>

					<tr>
						<td width="50"><label class="name">Main Label</label></td>
						<td width="35" class="box">
							
						</td>
						<td></td>
						<td width="35" class="box">
							
						</td>
						<td></td>
						<td width="35" class="box">
							
						</td>
						<td></td>
						<td width="90">
							<div class="com"></div>
						</td>
					</tr>
					<tr>
						<td width="50"><label class="name">Care Label</label></td>
						<td width="35" class="box">
							
						</td>
						<td></td>
						<td width="35" class="box">
							
						</td>
						<td></td>
						<td width="35" class="box">
							
						</td>
						<td></td>
						<td width="90">
							<div class="com"></div>
						</td>
					</tr>
					<tr>
						<td width="50"><label class="name">Other</label></td>
						<td width="35" class="box">
							
						</td>
						<td></td>
						<td width="35" class="box">
							
						</td>
						<td></td>
						<td width="35" class="box">
							
						</td>
						<td></td>
						<td width="90">
							<div class="com"></div>
						</td>
					</tr>
				</table>
			</div>
			<label style="font-weight: bold; font-family: Arial; font-size: 10px; padding-left: 280px; color: white;">FR.01-01-MD/00</label>
			@endif
		</td>
		
	</tr>

	<tr>
		
		<td>
			@if(isset($value[2]))
			<div class="tag">
				<table style="padding-top: 30px;">
					<tr>
						<td width="50"><label style="font-size: 8px; color: white;">Material :</label></td>
						<td width="40">
							<label style="font-size: 8px;"><center>Original</center></label>
						</td>
						<td></td>
						<td width="35">
							<label style="font-size: 8px;"><center>Subtitue <br> Quality</center></label>
						</td>
						<td></td>
						<td width="35">
							<label style="font-size: 8px;"><center>Subtitue <br> Color</center></label>
						</td>
						<td></td>
						<td width="90">
							<label style="font-size: 8px;"><center>additional info / comments :</center></label>
						</td>
					</tr>

					<tr>
						<td colspan="8"><label style="font-size: 8px;">Material :</label></td>
					</tr>

					<tr>
						<td width="50"><label class="name">Shell Fabric</label></td>
						<td width="35" class="box">
							
						</td>
						<td></td>
						<td width="35" class="box">
							
						</td>
						<td></td>
						<td width="35" class="box">
							
						</td>
						<td></td>
						<td width="90">
							<div class="com"></div>
						</td>
					</tr>

					<tr>
						<td colspan="8" height="5"></td>
					</tr>

					<tr>
						<td width="50"><label class="name">Additional Fabric</label></td>
						<td width="35" class="box">
							
						</td>
						<td></td>
						<td width="35" class="box">
							
						</td>
						<td></td>
						<td width="35" class="box">
							
						</td>
						<td></td>
						<td width="90">
							<div class="com"></div>
						</td>
					</tr>
					<tr>
						<td width="50"><label class="name">Lining</label></td>
						<td width="35" class="box">
							
						</td>
						<td></td>
						<td width="35" class="box">
							
						</td>
						<td></td>
						<td width="35" class="box">
							
						</td>
						<td></td>
						<td width="90">
							<div class="com"></div>
						</td>
					</tr>

					<tr>
						<td width="50"><label class="name">Bindings</label></td>
						<td width="35" class="box">
							
						</td>
						<td></td>
						<td width="35" class="box">
							
						</td>
						<td></td>
						<td width="35" class="box">
							
						</td>
						<td></td>
						<td width="90">
							<div class="com"></div>
						</td>
					</tr>

					<tr>
						<td width="50"><label class="name">Rib</label></td>
						<td width="35" class="box">
							
						</td>
						<td></td>
						<td width="35" class="box">
							
						</td>
						<td></td>
						<td width="35" class="box">
							
						</td>
						<td></td>
						<td width="90">
							<div class="com"></div>
						</td>
					</tr>

					<tr>
						<td width="50"><label class="name">Other Additional Finishing Processes</label></td>
						<td width="35" class="box">
							
						</td>
						<td></td>
						<td width="35" class="box">
							
						</td>
						<td></td>
						<td width="35" class="box">
							
						</td>
						<td></td>
						<td width="90">
							<div class="com"></div>
						</td>
					</tr>

					<tr>
						<td colspan="8">
							<label style="font-size: 6px; font-weight: bold;"><center>FOR FINAL FABRIC APPROVAL YOU SHOULD ALWAYS REFER TO THE 1st BULK APPROVAL</center></label>
						</td>
					</tr>
					<tr>
						<td colspan="8">
							<label style="font-size: 8px;">Accessories :</label>
						</td>
					</tr>

					<tr>
						<td width="50"><label class="name">Tape</label></td>
						<td width="35" class="box">
							
						</td>
						<td></td>
						<td width="35" class="box">
							
						</td>
						<td></td>
						<td width="35" class="box">
							
						</td>
						<td></td>
						<td width="90">
							<div class="com"></div>
						</td>
					</tr>

					<tr>
						<td width="50"><label class="name">Zipper</label></td>
						<td width="35" class="box">
							
						</td>
						<td></td>
						<td width="35" class="box">
							
						</td>
						<td></td>
						<td width="35" class="box">
							
						</td>
						<td></td>
						<td width="90">
							<div class="com"></div>
						</td>
					</tr>

					<tr>
						<td width="50"><label class="name">Puller</label></td>
						<td width="35" class="box">
							
						</td>
						<td></td>
						<td width="35" class="box">
							
						</td>
						<td></td>
						<td width="35" class="box">
							
						</td>
						<td></td>
						<td width="90">
							<div class="com"></div>
						</td>
					</tr>

					<tr>
						<td width="50"><label class="name">Eyelet</label></td>
						<td width="35" class="box">
							
						</td>
						<td></td>
						<td width="35" class="box">
							
						</td>
						<td></td>
						<td width="35" class="box">
							
						</td>
						<td></td>
						<td width="90">
							<div class="com"></div>
						</td>
					</tr>

					<tr>
						<td width="50"><label class="name">Tiecord</label></td>
						<td width="35" class="box">
							
						</td>
						<td></td>
						<td width="35" class="box">
							
						</td>
						<td></td>
						<td width="35" class="box">
							
						</td>
						<td></td>
						<td width="90">
							<div class="com"></div>
						</td>
					</tr>

					<tr>
						<td width="50"><label class="name">Stopper</label></td>
						<td width="35" class="box">
							
						</td>
						<td></td>
						<td width="35" class="box">
							
						</td>
						<td></td>
						<td width="35" class="box">
							
						</td>
						<td></td>
						<td width="90">
							<div class="com"></div>
						</td>
					</tr>

					<tr>
						<td width="50"><label class="name">Elastic</label></td>
						<td width="35" class="box">
							
						</td>
						<td></td>
						<td width="35" class="box">
							
						</td>
						<td></td>
						<td width="35" class="box">
							
						</td>
						<td></td>
						<td width="90">
							<div class="com"></div>
						</td>
					</tr>

					<tr>
						<td width="50"><label class="name">Hook & Loop</label></td>
						<td width="35" class="box">
							
						</td>
						<td></td>
						<td width="35" class="box">
							
						</td>
						<td></td>
						<td width="35" class="box">
							
						</td>
						<td></td>
						<td width="90">
							<div class="com"></div>
						</td>
					</tr>

					<tr>
						<td width="50"><label class="name">Snap</label></td>
						<td width="35" class="box">
							
						</td>
						<td></td>
						<td width="35" class="box">
							
						</td>
						<td></td>
						<td width="35" class="box">
							
						</td>
						<td></td>
						<td width="90">
							<div class="com"></div>
						</td>
					</tr>

					<tr>
						<td width="50"><label class="name">Others</label></td>
						<td width="35" class="box">
							
						</td>
						<td></td>
						<td width="35" class="box">
							
						</td>
						<td></td>
						<td width="35" class="box">
							
						</td>
						<td></td>
						<td width="90">
							<div class="com"></div>
						</td>
					</tr>

					<tr>
						<td colspan="8" height="5"></td>
					</tr>
					<tr>
						<td colspan="8">
							<label style="font-size: 8px;">Artwork :</label>
						</td>
					</tr>
					
					<tr>
						<td width="50"><label class="name">Print</label></td>
						<td width="35" class="box">
							
						</td>
						<td></td>
						<td width="35" class="box">
							
						</td>
						<td></td>
						<td width="35" class="box">
							
						</td>
						<td></td>
						<td width="90">
							<div class="com"></div>
						</td>
					</tr>
					<tr>
						<td width="50"><label class="name">Embroidery</label></td>
						<td width="35" class="box">
							
						</td>
						<td></td>
						<td width="35" class="box">
							
						</td>
						<td></td>
						<td width="35" class="box">
							
						</td>
						<td></td>
						<td width="90">
							<div class="com"></div>
						</td>
					</tr>
					<tr>
						<td width="50"><label class="name">Other</label></td>
						<td width="35" class="box">
							
						</td>
						<td></td>
						<td width="35" class="box">
							
						</td>
						<td></td>
						<td width="35" class="box">
							
						</td>
						<td></td>
						<td width="90">
							<div class="com"></div>
						</td>
					</tr>

					<tr>
						<td colspan="8">
							<label style="font-size: 8px;">L & P :</label>
						</td>
					</tr>

					<tr>
						<td width="50"><label class="name">Main Label</label></td>
						<td width="35" class="box">
							
						</td>
						<td></td>
						<td width="35" class="box">
							
						</td>
						<td></td>
						<td width="35" class="box">
							
						</td>
						<td></td>
						<td width="90">
							<div class="com"></div>
						</td>
					</tr>
					<tr>
						<td width="50"><label class="name">Care Label</label></td>
						<td width="35" class="box">
							
						</td>
						<td></td>
						<td width="35" class="box">
							
						</td>
						<td></td>
						<td width="35" class="box">
							
						</td>
						<td></td>
						<td width="90">
							<div class="com"></div>
						</td>
					</tr>
					<tr>
						<td width="50"><label class="name">Other</label></td>
						<td width="35" class="box">
							
						</td>
						<td></td>
						<td width="35" class="box">
							
						</td>
						<td></td>
						<td width="35" class="box">
							
						</td>
						<td></td>
						<td width="90">
							<div class="com"></div>
						</td>
					</tr>
				</table>
			</div>
			<label style="font-weight: bold; font-family: Arial; font-size: 10px; padding-left: 280px; color: white;">FR.01-01-MD/00</label>
			@endif
		</td>
		
		<td>
			<div class="tengah" style="padding-left: 10px; padding-right: 10px;"></div>
		</td>
		
		<td>
			@if(isset($value[3]))
			<div class="tag">
				<table style="padding-top: 30px;">
					<tr>
						<td width="50"><label style="font-size: 8px; color: white;">Material :</label></td>
						<td width="40">
							<label style="font-size: 8px;"><center>Original</center></label>
						</td>
						<td></td>
						<td width="35">
							<label style="font-size: 8px;"><center>Subtitue <br> Quality</center></label>
						</td>
						<td></td>
						<td width="35">
							<label style="font-size: 8px;"><center>Subtitue <br> Color</center></label>
						</td>
						<td></td>
						<td width="90">
							<label style="font-size: 8px;"><center>additional info / comments :</center></label>
						</td>
					</tr>

					<tr>
						<td colspan="8"><label style="font-size: 8px;">Material :</label></td>
					</tr>

					<tr>
						<td width="50"><label class="name">Shell Fabric</label></td>
						<td width="35" class="box">
							
						</td>
						<td></td>
						<td width="35" class="box">
							
						</td>
						<td></td>
						<td width="35" class="box">
							
						</td>
						<td></td>
						<td width="90">
							<div class="com"></div>
						</td>
					</tr>

					<tr>
						<td colspan="8" height="5"></td>
					</tr>

					<tr>
						<td width="50"><label class="name">Additional Fabric</label></td>
						<td width="35" class="box">
							
						</td>
						<td></td>
						<td width="35" class="box">
							
						</td>
						<td></td>
						<td width="35" class="box">
							
						</td>
						<td></td>
						<td width="90">
							<div class="com"></div>
						</td>
					</tr>
					<tr>
						<td width="50"><label class="name">Lining</label></td>
						<td width="35" class="box">
							
						</td>
						<td></td>
						<td width="35" class="box">
							
						</td>
						<td></td>
						<td width="35" class="box">
							
						</td>
						<td></td>
						<td width="90">
							<div class="com"></div>
						</td>
					</tr>

					<tr>
						<td width="50"><label class="name">Bindings</label></td>
						<td width="35" class="box">
							
						</td>
						<td></td>
						<td width="35" class="box">
							
						</td>
						<td></td>
						<td width="35" class="box">
							
						</td>
						<td></td>
						<td width="90">
							<div class="com"></div>
						</td>
					</tr>

					<tr>
						<td width="50"><label class="name">Rib</label></td>
						<td width="35" class="box">
							
						</td>
						<td></td>
						<td width="35" class="box">
							
						</td>
						<td></td>
						<td width="35" class="box">
							
						</td>
						<td></td>
						<td width="90">
							<div class="com"></div>
						</td>
					</tr>

					<tr>
						<td width="50"><label class="name">Other Additional Finishing Processes</label></td>
						<td width="35" class="box">
							
						</td>
						<td></td>
						<td width="35" class="box">
							
						</td>
						<td></td>
						<td width="35" class="box">
							
						</td>
						<td></td>
						<td width="90">
							<div class="com"></div>
						</td>
					</tr>

					<tr>
						<td colspan="8">
							<label style="font-size: 6px; font-weight: bold;"><center>FOR FINAL FABRIC APPROVAL YOU SHOULD ALWAYS REFER TO THE 1st BULK APPROVAL</center></label>
						</td>
					</tr>
					<tr>
						<td colspan="8">
							<label style="font-size: 8px;">Accessories :</label>
						</td>
					</tr>

					<tr>
						<td width="50"><label class="name">Tape</label></td>
						<td width="35" class="box">
							
						</td>
						<td></td>
						<td width="35" class="box">
							
						</td>
						<td></td>
						<td width="35" class="box">
							
						</td>
						<td></td>
						<td width="90">
							<div class="com"></div>
						</td>
					</tr>

					<tr>
						<td width="50"><label class="name">Zipper</label></td>
						<td width="35" class="box">
							
						</td>
						<td></td>
						<td width="35" class="box">
							
						</td>
						<td></td>
						<td width="35" class="box">
							
						</td>
						<td></td>
						<td width="90">
							<div class="com"></div>
						</td>
					</tr>

					<tr>
						<td width="50"><label class="name">Puller</label></td>
						<td width="35" class="box">
							
						</td>
						<td></td>
						<td width="35" class="box">
							
						</td>
						<td></td>
						<td width="35" class="box">
							
						</td>
						<td></td>
						<td width="90">
							<div class="com"></div>
						</td>
					</tr>

					<tr>
						<td width="50"><label class="name">Eyelet</label></td>
						<td width="35" class="box">
							
						</td>
						<td></td>
						<td width="35" class="box">
							
						</td>
						<td></td>
						<td width="35" class="box">
							
						</td>
						<td></td>
						<td width="90">
							<div class="com"></div>
						</td>
					</tr>

					<tr>
						<td width="50"><label class="name">Tiecord</label></td>
						<td width="35" class="box">
							
						</td>
						<td></td>
						<td width="35" class="box">
							
						</td>
						<td></td>
						<td width="35" class="box">
							
						</td>
						<td></td>
						<td width="90">
							<div class="com"></div>
						</td>
					</tr>

					<tr>
						<td width="50"><label class="name">Stopper</label></td>
						<td width="35" class="box">
							
						</td>
						<td></td>
						<td width="35" class="box">
							
						</td>
						<td></td>
						<td width="35" class="box">
							
						</td>
						<td></td>
						<td width="90">
							<div class="com"></div>
						</td>
					</tr>

					<tr>
						<td width="50"><label class="name">Elastic</label></td>
						<td width="35" class="box">
							
						</td>
						<td></td>
						<td width="35" class="box">
							
						</td>
						<td></td>
						<td width="35" class="box">
							
						</td>
						<td></td>
						<td width="90">
							<div class="com"></div>
						</td>
					</tr>

					<tr>
						<td width="50"><label class="name">Hook & Loop</label></td>
						<td width="35" class="box">
							
						</td>
						<td></td>
						<td width="35" class="box">
							
						</td>
						<td></td>
						<td width="35" class="box">
							
						</td>
						<td></td>
						<td width="90">
							<div class="com"></div>
						</td>
					</tr>

					<tr>
						<td width="50"><label class="name">Snap</label></td>
						<td width="35" class="box">
							
						</td>
						<td></td>
						<td width="35" class="box">
							
						</td>
						<td></td>
						<td width="35" class="box">
							
						</td>
						<td></td>
						<td width="90">
							<div class="com"></div>
						</td>
					</tr>

					<tr>
						<td width="50"><label class="name">Others</label></td>
						<td width="35" class="box">
							
						</td>
						<td></td>
						<td width="35" class="box">
							
						</td>
						<td></td>
						<td width="35" class="box">
							
						</td>
						<td></td>
						<td width="90">
							<div class="com"></div>
						</td>
					</tr>

					<tr>
						<td colspan="8" height="5"></td>
					</tr>
					<tr>
						<td colspan="8">
							<label style="font-size: 8px;">Artwork :</label>
						</td>
					</tr>
					
					<tr>
						<td width="50"><label class="name">Print</label></td>
						<td width="35" class="box">
							
						</td>
						<td></td>
						<td width="35" class="box">
							
						</td>
						<td></td>
						<td width="35" class="box">
							
						</td>
						<td></td>
						<td width="90">
							<div class="com"></div>
						</td>
					</tr>
					<tr>
						<td width="50"><label class="name">Embroidery</label></td>
						<td width="35" class="box">
							
						</td>
						<td></td>
						<td width="35" class="box">
							
						</td>
						<td></td>
						<td width="35" class="box">
							
						</td>
						<td></td>
						<td width="90">
							<div class="com"></div>
						</td>
					</tr>
					<tr>
						<td width="50"><label class="name">Other</label></td>
						<td width="35" class="box">
							
						</td>
						<td></td>
						<td width="35" class="box">
							
						</td>
						<td></td>
						<td width="35" class="box">
							
						</td>
						<td></td>
						<td width="90">
							<div class="com"></div>
						</td>
					</tr>

					<tr>
						<td colspan="8">
							<label style="font-size: 8px;">L & P :</label>
						</td>
					</tr>

					<tr>
						<td width="50"><label class="name">Main Label</label></td>
						<td width="35" class="box">
							
						</td>
						<td></td>
						<td width="35" class="box">
							
						</td>
						<td></td>
						<td width="35" class="box">
							
						</td>
						<td></td>
						<td width="90">
							<div class="com"></div>
						</td>
					</tr>
					<tr>
						<td width="50"><label class="name">Care Label</label></td>
						<td width="35" class="box">
							
						</td>
						<td></td>
						<td width="35" class="box">
							
						</td>
						<td></td>
						<td width="35" class="box">
							
						</td>
						<td></td>
						<td width="90">
							<div class="com"></div>
						</td>
					</tr>
					<tr>
						<td width="50"><label class="name">Other</label></td>
						<td width="35" class="box">
							
						</td>
						<td></td>
						<td width="35" class="box">
							
						</td>
						<td></td>
						<td width="35" class="box">
							
						</td>
						<td></td>
						<td width="90">
							<div class="com"></div>
						</td>
					</tr>
				</table>
			</div>
			<label style="font-weight: bold; font-family: Arial; font-size: 10px; padding-left: 280px; color: white;">FR.01-01-MD/00</label>
			@endif
		</td>
		
	</tr>
	
</table>
@endforeach
