<style type="text/css">
@page{
    margin : 10 15 5 15;
}

/*@font-face {
    font-family: 'Arial' !important;
    src: url({{ storage_path('fonts/Arial.ttf') }}) format('truetype');
}*/

.tag{
    width: 360px;
    height: 340px;
    border-style: solid;
    padding-bottom: 5px;
   
}





.barcode {
    line-height: 16px;
    /*padding-left: 5px;*/
    padding-top: 5px;
}

.img_barcode {
    display: block;
    padding: 0px;
}

.img_barcode > img {
    width: 170px;
    height:28px;
}

.barcode_number {
    font-family: sans-serif;
    font-size: 12px !important;
}

.area_barcode {
    width: 50%;
}

.page-break {
    page-break-after: always;
}

.separator {
    margin: 25 0 0 0;
}


</style>

@php($chunk = array_chunk($data->toArray(), 4))
@foreach($chunk as $key => $value)
<table>
    <tr>
        <td>
            @if(isset($value[0]))
                <div class="tag">
                    
                    <table width="100%">
                        <tr>
                            <td colspan="2"><center><label style="font-size:18px;">{{ $value[0]->documentno }}</label></center></td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <center>
                                    <div class="barcode">
                                            <div class="img_barcode">
                                                <img src="data:image/png;base64,{{ DNS1D::getBarcodePNG($value[0]->barcode_id, 'C128',2,35) }}" alt="barcode"   />
                                            </div>
                                            <div class="row">
                                                <span class="barcode_number">{{ $value[0]->barcode_id }} {{ $value[0]->sets }}</span >
                                            </div>
                                        </div>
                                </center>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2"> <center><hr></center></td>
                        </tr>
                        <tr>
                            <td><label>Season</label></td>
                            <td><label>{{ $value[0]->season }}</label></td>
                        </tr>
                        <tr>
                            <td><label>Style</label></td>
                            <td><label>{{ $value[0]->style }}</label></td>
                        </tr>
                        <tr>
                            <td><label>Article</label></td>
                            <td><label>{{ $value[0]->article }}</label></td>
                        </tr>
                        <tr>
                            <td><label>Size</label></td>
                            <td><label>{{ $value[0]->size }}</label></td>
                        </tr>
                        <tr>
                            <td><label>Brand</label></td>
                            <td><label>{{ $value[0]->brand }}</label></td>
                        </tr>
                        <tr>
                            <td><label>Status</label></td>
                            <td><label>{{ $value[0]->status }}</label></td>
                        </tr>
                        <tr>
                            <td><label>Product Name</label></td>
                            <td><label>{{ $value[0]->product_name }}</label></td>
                        </tr>
                        <tr>
                            <td><label>Color</label></td>
                            <td><label>{{ $value[0]->color }}</label></td>
                        </tr>
                        <tr>
                            <td><label>Date Reciept</label></td>
                            <td><label>{{ $value[0]->reciept_date }}</label></td>
                        </tr>
                    </table>
                    
                </div>
                
            @endif

        </td>

        <td>
            <div class="tengah" style="padding-left: 10px; padding-right: 10px;"></div>
        </td>

        <td>
            @if(isset($value[1]))
                <div class="tag">
                    
                    <table width="100%">
                        <tr>
                            <td colspan="2"><center><label style="font-size:18px;">{{ $value[1]->documentno }}</label></center></td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <center>
                                    <div class="barcode">
                                            <div class="img_barcode">
                                                <img src="data:image/png;base64,{{ DNS1D::getBarcodePNG($value[1]->barcode_id, 'C128',2,35) }}" alt="barcode"   />
                                            </div>
                                            <div class="row">
                                                <span class="barcode_number">{{ $value[1]->barcode_id }} {{ $value[1]->sets }}</span >
                                            </div>
                                        </div>
                                </center>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2"> <center><hr></center></td>
                        </tr>
                        <tr>
                            <td><label>Season</label></td>
                            <td><label>{{ $value[1]->season }}</label></td>
                        </tr>
                        <tr>
                            <td><label>Style</label></td>
                            <td><label>{{ $value[1]->style }}</label></td>
                        </tr>
                        <tr>
                            <td><label>Article</label></td>
                            <td><label>{{ $value[1]->article }}</label></td>
                        </tr>
                        <tr>
                            <td><label>Size</label></td>
                            <td><label>{{ $value[1]->size }}</label></td>
                        </tr>
                        <tr>
                            <td><label>Brand</label></td>
                            <td><label>{{ $value[1]->brand }}</label></td>
                        </tr>
                        <tr>
                            <td><label>Status</label></td>
                            <td><label>{{ $value[1]->status }}</label></td>
                        </tr>
                        <tr>
                            <td><label>Product Name</label></td>
                            <td><label>{{ $value[1]->product_name }}</label></td>
                        </tr>
                        <tr>
                            <td><label>Color</label></td>
                            <td><label>{{ $value[1]->color }}</label></td>
                        </tr>
                        <tr>
                            <td><label>Date Reciept</label></td>
                            <td><label>{{ $value[1]->reciept_date }}</label></td>
                        </tr>
                    </table>
                    
                </div>
                
            @endif

        </td>
    </tr>

    <tr>
        <td>
            @if(isset($value[2]))
                <div class="tag">
                    
                    <table width="100%">
                        <tr>
                            <td colspan="2"><center><label style="font-size:18px;">{{ $value[2]->documentno }}</label></center></td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <center>
                                    <div class="barcode">
                                            <div class="img_barcode">
                                                <img src="data:image/png;base64,{{ DNS1D::getBarcodePNG($value[2]->barcode_id, 'C128',2,35) }}" alt="barcode"   />
                                            </div>
                                            <div class="row">
                                                <span class="barcode_number">{{ $value[2]->barcode_id }} {{ $value[2]->sets }}</span >
                                            </div>
                                        </div>
                                </center>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2"> <center><hr></center></td>
                        </tr>
                        <tr>
                            <td><label>Season</label></td>
                            <td><label>{{ $value[2]->season }}</label></td>
                        </tr>
                        <tr>
                            <td><label>Style</label></td>
                            <td><label>{{ $value[2]->style }}</label></td>
                        </tr>
                        <tr>
                            <td><label>Article</label></td>
                            <td><label>{{ $value[2]->article }}</label></td>
                        </tr>
                        <tr>
                            <td><label>Size</label></td>
                            <td><label>{{ $value[2]->size }}</label></td>
                        </tr>
                        <tr>
                            <td><label>Brand</label></td>
                            <td><label>{{ $value[2]->brand }}</label></td>
                        </tr>
                        <tr>
                            <td><label>Status</label></td>
                            <td><label>{{ $value[2]->status }}</label></td>
                        </tr>
                        <tr>
                            <td><label>Product Name</label></td>
                            <td><label>{{ $value[2]->product_name }}</label></td>
                        </tr>
                        <tr>
                            <td><label>Color</label></td>
                            <td><label>{{ $value[2]->color }}</label></td>
                        </tr>
                        <tr>
                            <td><label>Date Reciept</label></td>
                            <td><label>{{ $value[2]->reciept_date }}</label></td>
                        </tr>
                    </table>
                    
                </div>
                
            @endif

        </td>

        <td>
            <div class="tengah" style="padding-left: 10px; padding-right: 10px;"></div>
        </td>

        <td>
            @if(isset($value[3]))
                <div class="tag">
                    
                    <table width="100%">
                        <tr>
                            <td colspan="2"><center><label style="font-size:18px;">{{ $value[3]->documentno }}</label></center></td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <center>
                                    <div class="barcode">
                                            <div class="img_barcode">
                                                <img src="data:image/png;base64,{{ DNS1D::getBarcodePNG($value[3]->barcode_id, 'C128',2,35) }}" alt="barcode"   />
                                            </div>
                                            <div class="row">
                                                <span class="barcode_number">{{ $value[3]->barcode_id }} {{ $value[3]->sets }}</span >
                                            </div>
                                        </div>
                                </center>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2"> <center><hr></center></td>
                        </tr>
                        <tr>
                            <td><label>Season</label></td>
                            <td><label>{{ $value[3]->season }}</label></td>
                        </tr>
                        <tr>
                            <td><label>Style</label></td>
                            <td><label>{{ $value[3]->style }}</label></td>
                        </tr>
                        <tr>
                            <td><label>Article</label></td>
                            <td><label>{{ $value[3]->article }}</label></td>
                        </tr>
                        <tr>
                            <td><label>Size</label></td>
                            <td><label>{{ $value[3]->size }}</label></td>
                        </tr>
                        <tr>
                            <td><label>Brand</label></td>
                            <td><label>{{ $value[3]->brand }}</label></td>
                        </tr>
                        <tr>
                            <td><label>Status</label></td>
                            <td><label>{{ $value[3]->status }}</label></td>
                        </tr>
                        <tr>
                            <td><label>Product Name</label></td>
                            <td><label>{{ $value[3]->product_name }}</label></td>
                        </tr>
                        <tr>
                            <td><label>Color</label></td>
                            <td><label>{{ $value[3]->color }}</label></td>
                        </tr>
                        <tr>
                            <td><label>Date Reciept</label></td>
                            <td><label>{{ $value[3]->reciept_date }}</label></td>
                        </tr>
                    </table>
                    
                </div>
                
            @endif

        </td>
    </tr>

    <tr>
        <td>
            @if(isset($value[4]))
                <div class="tag">
                    
                    <table width="100%">
                        <tr>
                            <td colspan="2"><center><label style="font-size:18px;">{{ $value[4]->documentno }}</label></center></td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <center>
                                    <div class="barcode">
                                            <div class="img_barcode">
                                                <img src="data:image/png;base64,{{ DNS1D::getBarcodePNG($value[4]->barcode_id, 'C128',2,35) }}" alt="barcode"   />
                                            </div>
                                            <div class="row">
                                                <span class="barcode_number">{{ $value[4]->barcode_id }} {{ $value[4]->sets }}</span >
                                            </div>
                                        </div>
                                </center>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2"> <center><hr></center></td>
                        </tr>
                        <tr>
                            <td><label>Season</label></td>
                            <td><label>{{ $value[4]->season }}</label></td>
                        </tr>
                        <tr>
                            <td><label>Style</label></td>
                            <td><label>{{ $value[4]->style }}</label></td>
                        </tr>
                        <tr>
                            <td><label>Article</label></td>
                            <td><label>{{ $value[4]->article }}</label></td>
                        </tr>
                        <tr>
                            <td><label>Size</label></td>
                            <td><label>{{ $value[4]->size }}</label></td>
                        </tr>
                        <tr>
                            <td><label>Brand</label></td>
                            <td><label>{{ $value[4]->brand }}</label></td>
                        </tr>
                        <tr>
                            <td><label>Status</label></td>
                            <td><label>{{ $value[4]->status }}</label></td>
                        </tr>
                        <tr>
                            <td><label>Product Name</label></td>
                            <td><label>{{ $value[4]->product_name }}</label></td>
                        </tr>
                        <tr>
                            <td><label>Color</label></td>
                            <td><label>{{ $value[4]->color }}</label></td>
                        </tr>
                        <tr>
                            <td><label>Date Reciept</label></td>
                            <td><label>{{ $value[4]->reciept_date }}</label></td>
                        </tr>
                    </table>
                    
                </div>
                
            @endif

        </td>

        <td>
            <div class="tengah" style="padding-left: 10px; padding-right: 10px;"></div>
        </td>

        <td>
            @if(isset($value[5]))
                <div class="tag">
                    
                    <table width="100%">
                        <tr>
                            <td colspan="2"><center><label style="font-size:18px;">{{ $value[5]->documentno }}</label></center></td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <center>
                                    <div class="barcode">
                                            <div class="img_barcode">
                                                <img src="data:image/png;base64,{{ DNS1D::getBarcodePNG($value[5]->barcode_id, 'C128',2,35) }}" alt="barcode"   />
                                            </div>
                                            <div class="row">
                                                <span class="barcode_number">{{ $value[5]->barcode_id }} {{ $value[5]->sets }}</span >
                                            </div>
                                        </div>
                                </center>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2"> <center><hr></center></td>
                        </tr>
                        <tr>
                            <td><label>Season</label></td>
                            <td><label>{{ $value[5]->season }}</label></td>
                        </tr>
                        <tr>
                            <td><label>Style</label></td>
                            <td><label>{{ $value[5]->style }}</label></td>
                        </tr>
                        <tr>
                            <td><label>Article</label></td>
                            <td><label>{{ $value[5]->article }}</label></td>
                        </tr>
                        <tr>
                            <td><label>Size</label></td>
                            <td><label>{{ $value[5]->size }}</label></td>
                        </tr>
                        <tr>
                            <td><label>Brand</label></td>
                            <td><label>{{ $value[5]->brand }}</label></td>
                        </tr>
                        <tr>
                            <td><label>Status</label></td>
                            <td><label>{{ $value[5]->status }}</label></td>
                        </tr>
                        <tr>
                            <td><label>Product Name</label></td>
                            <td><label>{{ $value[5]->product_name }}</label></td>
                        </tr>
                        <tr>
                            <td><label>Color</label></td>
                            <td><label>{{ $value[5]->color }}</label></td>
                        </tr>
                        <tr>
                            <td><label>Date Reciept</label></td>
                            <td><label>{{ $value[5]->reciept_date }}</label></td>
                        </tr>
                    </table>
                    
                </div>
                
            @endif

        </td>
    </tr>

    
</table>

<div class="page-break"></div>


@endforeach