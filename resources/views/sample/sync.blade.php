@extends('layouts.app', ['active' => 'master_mo'])
@section('header')
<div class="page-header page-header-default">
	<div class="breadcrumb-line">
		<ul class="breadcrumb">
			<li><a href="#"><i class="icon-home2 position-left"></i> Master</a></li>
			<li class="active">Document No</li>
		</ul>
	</div>
</div>
@endsection

@section('content')
<div class="content">

	<div class="row">

		<!-- by search -->
		<div class="form-group ">
			<div class="panel panel-flat">
				<div class="panel-heading">
					<form action="{{ route('mastersync.ajaxGetDocSync') }}" id="form-search">
						@csrf
						<div class="row form-group" id="by_docno">
			                <label><b>Document NO</b></label>
			                <div class="input-group">
			                    <input type="text" class="form-control" name="docno" id="docno" placeholder="Document No.">
			                    <div class="input-group-btn">
			                        <button type="submit" class="btn btn-primary">Filter</button>
			                    </div>
			                </div>
			            </div>
					</form>
					<br>
				</div>
				<div class="panel-body">
					<div class="table-responsive">
						<table class ="table table-basic table-condensed" id="table-search" width="100%">
							<thead>
								<tr>
									<th>#</th>
									<th>Document No</th>
									<th>Doc. Status</th>
									<th>Season</th>
									<th>Style</th>
									<th>Article</th>
									<th>Size</th>
									<th>Brand</th>
									<th>Status</th>
									<th>Action</th>
								</tr>
							</thead>
						</table>
					</div>
				</div>
			</div>
		</div>
		<!-- by search -->
	</div>



</div>

@endsection



@section('js')
<script type="text/javascript">
$(document).ready(function(){

	//by search
	var tableS = $('#table-search').DataTable({
		processing:true,
		serverSide:true,
		deferRender:true,
		autoWidth: true,
		dom:'<"datatable-header"fBl><t><"datatable-footer"ip>',
		language: {
	        search: '<span>Filter:</span> _INPUT_',
	        searchPlaceholder: 'Type to filter...',
	        lengthMenu: '<span>Show:</span> _MENU_',
	        paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
	    },
	    ajax: {
	        type: 'GET',
	        url: $('#form-search').attr('action'),
	        data: function (d) {
	            return $.extend({},d,{
	                "docno": $('#docno').val()
	            });
	        }
	    },
	    fnCreatedRow: function (row, data, index) {
	        var info = tableS.page.info();
	        var value = index+1+info.start;
	        $('td', row).eq(0).html(value);
	    },
	    columns: [
	        {data: null, sortable: false, orderable: false, searchable: false},
	        {data: 'documentno', name: 'documentno'},
	        {data: 'docstatus', name: 'docstatus'},
	        {data: 'season', name: 'season'},
	        {data: 'style', name: 'style'},
	        {data: 'article', name: 'article'},
	        {data: 'size', name: 'size'},
	        {data: 'brand', name: 'brand'},
	        {data: 'status', name: 'status'},
	        {data: 'action', name: 'action', searchable:false, sortable:false, orderable:false}
	    ]
	});



	tableS.on('preDraw', function() {
		loading();
	    Pace.start();
	})
	.on('draw.dt', function() {
	    $.unblockUI();
	    Pace.stop();
	});

	$('#form-search').submit(function(event){
		event.preventDefault();
		
		tableS.clear();
		tableS.draw();
	});


	
});

function syncdoc(e){
	var docno = e.getAttribute('data-doc');

	$.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
        type: 'post',
        url : "{{ route('mastersync.syncDocument') }}",
        data:{docno:docno},
        beforeSend : function(){
        	loading();
        },
        success: function(response) {

        	$.unblockUI();
         	var notif = response.data;
            alert(notif.status,notif.output);
            window.location.reload(); 
        },
        error: function(response) {

        	$.unblockUI();
           var notif = response.data;
            alert(notif.status,notif.output);
            
        }
    }); 
}




</script>
@endsection