@extends('layouts.app', ['active' => 'scanship'])
@section('header')
<div class="page-header page-header-default">
	<div class="breadcrumb-line">
		<ul class="breadcrumb">
			<li><a href="#"><i class="icon-home2 position-left"></i>Shipping</a></li>
			<li class="active">Scan Shipping</li>
		</ul>
	</div>
</div>
@endsection

@section('content')
<div class="content">
	<div class="row">
		<div class=" panel panel-flat">
			<div class="panel-body">
				<form action="{{ route('scanship.ajaxScan') }}" id="form-scan">
					<div class="row">
						<div class="col-lg-7">
							<label><b>Scan Barcode</b></label>
							<input type="text" name="barcode_id" id="barcode_id" class="form-control input-new" placeholder="#scan in here" required="">
						</div>
						<div class="col-lg-3">
							<center><label><b>PIC</b></label></center>
							<input type="text" name="pic" id="pic" class="form-control" readonly style="text-align: center; font-size: 20px;">
							<input type="hidden" name="nik" id="nik">
						</div>
						<div class="col-lg-2">
							<input type="text" name="_count" id="_count" class="form-control" value="0" readonly="" style="text-align: center; font-size: 28px; margin-top: 30px;">
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
	<div class="row">
		<div class=" panel panel-flat">
			<div class="panel-body">
				<div class="table-responsive">
					<table class ="table table-basic table-condensed" id="table-list">
						<thead>
							<tr>
								<th>Barcode ID</th>
								<th>Doc. No.</th>
								<th>Season</th>
								<th>Style</th>
								<th>Article</th>
								<th>Size</th>
								<th>Status</th>
								<th></th>
							</tr>
						</thead>
						<tbody></tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>

@endsection



@section('js')
<script type="text/javascript">
$(document).ready(function(){

	$('.input-new').keypress(function(event){
		if (event.which==13) {
			event.preventDefault();
			if ($('#barcode_id').val()=="") {
				alert(422,"Barcode required ! ! ! !");
				return false;
			}


			if ($('#pic').val()=="") {
				getNik($('#barcode_id').val());
			}else if ($('#nik').val()!="") {
				ajaxGarment($('#barcode_id').val(),$('#nik').val());
			}

			$('#barcode_id').val('');
			$('#barcode_id').focus();
		}
	});
});

function getNik(barcode){
	$.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
        type: 'get',
        url : "{{ route('scanship.ajaxGetNik') }}",
        data:{barcode:barcode},
        beforeSend : function(){
        	loading();
        },
        success: function(response) {
        	$.unblockUI();
        	$('#nik').val(response.nik);
        	$('#pic').val(response.name);

        	// console.log(response);
        },
        error: function(response) {
        	$.unblockUI();
           	alert(response.status,response.responseText);
            
        }
    });
}

function ajaxGarment(barcode,nik){
	$.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
        type: 'get',
        url : "{{ route('scanship.ajaxScan') }}",
        data:{barcode:barcode,nik:nik},
        beforeSend : function(){
        	loading();
        },
        success: function(response) {
        	$('#table-list > tbody').prepend(response);
        	var counts = +$('#_count').val()+1;

        	$('#_count').val(counts);
        	$.unblockUI();
        },
        error: function(response) {
        	$.unblockUI();
           	alert(response.status,response.responseText);
            
        }
    });
}
</script>
@endsection