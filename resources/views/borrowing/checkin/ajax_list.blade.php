<tr>
	<td>{{ $data['barcode_id'] }}</td>
	<td>{{ $data['docno'] }}</td>
	<td>{{ $data['season'] }}</td>
	<td>{{ $data['style'] }}</td>
	<td>{{ $data['article'] }}</td>
	<td>{{ $data['size'] }}</td>
	<td>{{ $data['status'] }}</td>
	<td>{{ $data['expd'] }}</td>
	<td>
		@if($data['type']=="checkout")
			<span class="label label-primary">CHECK OUT TO {{ $data['loct'] }} </span>
		@elseif($data['type']=="completed")
			<span class="label label-success">COMPLETED</span>
		@endif
	</td>
</tr>