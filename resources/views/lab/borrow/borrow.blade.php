@extends('layouts.app', ['active' => 'lab_borrow'])
@section('header')
<div class="page-header page-header-default">
	<div class="breadcrumb-line">
		<ul class="breadcrumb">
			<li><a href="#"><i class="icon-home2 position-left"></i> Laboratory</a></li>
		</ul>
	</div>
</div>
@endsection

@section('content')
<div class="content">
	<div class="row">
		<div class=" panel panel-flat">
			<div class="panel-body">
				<form action="{{ route('lab.ajaxBorrow') }}" id="form-scan">
					<div class="row">
						<div class="form-group">
							<label class="radio-inline"><input type="radio" name="radio_status" checked="checked" value="borrow"><b>BORROW</b></label>
		            		<label class="radio-inline"><input type="radio" name="radio_status" value="return"><b>RETURN</b></label>
						</div>
					</div>
					<hr>
					<div class="row">
						<div class="col-lg-10">
							<label><b>Scan Barcode</b></label>
							<input type="text" name="barcode_id" id="barcode_id" class="form-control input-new" placeholder="#scan in here" required="">
						</div>
						<div class="col-lg-2">
							<input type="text" name="_count" id="_count" class="form-control" value="0" readonly="" style="text-align: center; font-size: 28px; margin-top: 30px;">
						</div>
						
					</div>
					<div class="row locar">
						<div class="col-lg-1"></div>
						<div class="col-lg-5">
							<label><b>Department</b></label>
							<select class="select-search form-control" id="dept">
								<option value="">--Choose Department--</option>
								@foreach($dept as $dt)
									<option value="{{ $dt->value_name }}">{{$dt->dept_name}}</option>
								@endforeach
							</select>
						</div>
						<div class="col-lg-5">
							<label><b>Borrower</b></label>
							<input type="text" name="txname" id="txname" class="form-control" readonly="" style="text-align: center; font-size: 20px;">
							<input type="text" name="txnik" id="txnik" class="hidden">
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
	<div class="row">
		<div class=" panel panel-flat">
			<div class="panel-body">
				<div class="table-responsive">
					<table class ="table table-basic table-condensed" id="table-list">
						<thead>
							<tr>
								<th>Barcode ID</th>
								<th>Doc. No.</th>
								<th>Season</th>
								<th>Style</th>
								<th>Article</th>
								<th>Size</th>
								<th>Status</th>
								<th></th>
							</tr>
						</thead>
						<tbody></tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>

@endsection



@section('js')
<script type="text/javascript">
$(document).ready(function(){
	$('#barcode_id').focus();

	$('input[type=radio][name=radio_status]').change(function(){
		if (this.value=="borrow") {
			if ($('.locar').hasClass('hidden')) {
				$('.locar').removeClass('hidden');
			}
			$('#barcode_id').val('');
			$('#barcode_id').focus();
		}else{
			if ($('.locar').hasClass('hidden')) {
				$('.locar').removeClass('hidden');
				$('.locar').addClass('hidden');
			}else{
				$('.locar').addClass('hidden')
			}
			$('#barcode_id').val('');
			$('#barcode_id').focus();
		}
	});


	$('.input-new').keypress(function(event){
		if (event.which==13) {
			event.preventDefault();

			var bargar = $('#barcode_id').val();
			var status = $('input[name=radio_status]:checked').val();
			var dept = $('#dept').val();
			var nik = $('#nik').val();


			if (bargar=="") {
				alert(422,"Barcode required ! ! !");
				$('#barcode_id').val('');
				$('#barcode_id').focus();
				return false;
			}

			if (status=='borrow' && dept=="") {
				alert(422,"Department not set ! ! !");
				$('#barcode_id').val('');
				$('#barcode_id').focus();
				return false;				
			}else if(status=='borrow' && dept!="" && nik==""){
				setNik(bargar);
			}else if(status=='borrow' && dept!="" && nik!=""){
				ajaxScan(bargar,status,dept,nik);
			}else if (status=='return') {
				ajaxScan(bargar,status,dept,null);
			}
			


			$('#barcode_id').val('');
			$('#barcode_id').focus();
		}
	});

	
});



function ajaxScan(bargar,status,dept,nik){
	$.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
        type: 'post',
        url : $('#form-scan').attr('action'),
        data:{bargar:bargar,status:status,dept:dept,nik:nik},
        beforeSend : function(){
        	loading();
        	$('#barcode_id').focus();
        	$('#barcode_id').val('');
        },
        success: function(response) {
        	$.unblockUI();
        	$('#table-list > tbody').prepend(response);
        	var counts = +$('#_count').val()+1;

        	$('#_count').val(counts);

        	
        },
        error: function(response) {
        	$.unblockUI();
           	alert(response.status,response.responseText);
            
        }
    });
}

function setNik(nik){
	$.ajaxSetup({
		headers: {
		    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		}
	});
	$.ajax({
		type: 'post',
		url : "{{ route('lab.getNik') }}",
		data:{nik:nik},
		beforeSend : function(){
			loading();
			$('#barcode_id').focus();
			$('#barcode_id').val('');
		},
		success: function(response) {
			$('#txname').val(response.name);
			$('#txnik').val(response.nik);
			$.unblockUI();        	
		},
		error: function(response) {
			$.unblockUI();
		   	alert(response.status,response.responseText);
		    
		}
	});
}
</script>
@endsection