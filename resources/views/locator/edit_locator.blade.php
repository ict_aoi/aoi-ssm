@extends('layouts.app', ['active' => 'locator'])
@section('header')
<div class="page-header page-header-default">
	<div class="breadcrumb-line">
		<ul class="breadcrumb">
			<li><a href="#"><i class="icon-home2 position-left"></i> Area & Locator</a></li>
			<li class="active">Edit Locator</li>
		</ul>
	</div>
</div>
@endsection

@section('content')
<div class="content">

	<div class="row">
		<div class="panel panel-flat">
			<div class="panel-body">
				<div class="col-lg-6">
					<form action="{{ route('area.editLocator') }}" id="form-edit">
						@csrf
						<div class="row">
							<label>Name</label>
							<input type="text" name="name" id="name" class="form-control" required="" value="{{$locator->name}}">
							<input type="text" name="id" id="id" value="{{$locator->id}}" hidden="">
						</div>
						<div class="row">
							<label>Description</label>
							<input type="text" name="desc" id="desc" class="form-control" required="" value="{{$locator->description}}">
						</div>
						<div class="row">
							<label>Area</label>
							<input type="text" name="areaid" id="areaid" value="{{$locator->area_id}}" hidden="">
							<select class="select form-control" id="area" required="">
								<option value="">Choosse Area</option>
							</select>
						</div>
						<div class="row">
							<button type="submit" class="btn btn-primary" id="btn-save">Save</button>
							<button type="button" class="btn btn-warning" id="btn-back">Back</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>



<a href="{{ route('area.locator') }}" id="back"></a>
@endsection

@section('js')
<script type="text/javascript">
$(document).ready(function(){

	$(window).on('load',function(){

		ajaxArea($('#areaid').val());
	});






$('#form-edit').submit(function(event){
	event.preventDefault();

	$.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
        type: 'post',
        url : $('#form-edit').attr('action'),
        data:{name:$('#name').val(),desc:$('#desc').val(),id:$('#id').val(),area:$('#area').val()},
        beforeSend : function(){
        	loading();
        },
        success: function(response) {
        	$.unblockUI();
         	var notif = response.data;
            alert(notif.status,notif.output);
        },
        error: function(response) {
        	$.unblockUI();
           var notif = response.data;
            alert(notif.status,notif.output);
            
        }
    });
});

$('#btn-back').click(function(){
	window.location.href = $('#back').attr('href');
});

});

function ajaxArea(area_id){

	$.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
        type: 'get',
        url : "{{ route('area.ajaxGetArea') }}",

        success: function(response) {
       		var carea = response.area;

       		$('#area').empty();
            $('#area').append('<option value="">Choose Role</option>');
            for (var i = 0; i < carea.length; i++) {
            	if (carea[i]['id']==area_id) {
            		var select = "Selected";
            	}else{
            		var select = "";
            	}
               $('#area').append('<option value="'+carea[i]['id']+'" '+select+'>'+carea[i]['name']+'</option>');
            }
        },
        error: function(response) {
        	console.log(response);
            
        }
    });
}
</script>
@endsection

