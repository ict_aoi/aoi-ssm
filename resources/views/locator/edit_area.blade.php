@extends('layouts.app', ['active' => 'area'])
@section('header')
<div class="page-header page-header-default">
	<div class="breadcrumb-line">
		<ul class="breadcrumb">
			<li><a href="#"><i class="icon-home2 position-left"></i> Area & Locator</a></li>
			<li class="active">Edit Area</li>
		</ul>
	</div>
</div>
@endsection

@section('content')
<div class="content">

	<div class="row">
		<div class="panel panel-flat">
			<div class="panel-body">
				<div class="col-lg-6">
					<form action="{{ route('area.editArea') }}" id="form-edit">
						@csrf
						<div class="row">
							<label>Name</label>
							<input type="text" name="name" id="name" class="form-control" required="" value="{{$area->name}}">
							<input type="text" name="id" id="id" value="{{$area->id}}" hidden="">
						</div>
						<div class="row">
							<label>Description</label>
							<input type="text" name="desc" id="desc" class="form-control" required="" value="{{$area->description}}">
						</div>
						<div class="row">
							<button type="submit" class="btn btn-primary" id="btn-save">Save</button>
							<button type="button" class="btn btn-warning" id="btn-back">Back</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>



<a href="{{ route('area.index') }}" id="back"></a>
@endsection

@section('js')
<script type="text/javascript">
$(document).ready(function(){






$('#form-edit').submit(function(event){
	event.preventDefault();

	$.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
        type: 'post',
        url : $('#form-edit').attr('action'),
        data:{name:$('#name').val(),desc:$('#desc').val(),id:$('#id').val()},
        beforeSend : function(){
        	loading();
        },
        success: function(response) {
        	$.unblockUI();
         	var notif = response.data;
            alert(notif.status,notif.output);
        },
        error: function(response) {
        	$.unblockUI();
           var notif = response.data;
            alert(notif.status,notif.output);
            
        }
    });
});

$('#btn-back').click(function(){
	window.location.href = $('#back').attr('href');
});

});
</script>
@endsection

