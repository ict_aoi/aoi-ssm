<style type="text/css">
@page{
	margin : 15 25 5 25;

}

@font-face {
    font-family: 'impact' !important;
    src: url({{ storage_path('fonts/impact.ttf') }}) format('truetype');
}

.table{
	height: 50%;
	width: 100%;
	font-family:  sans-serif;
	font-size: :  18px !important;
	line-height: 5px;

}



.barcode {
    line-height: 16px;
    padding-left: 10px;
    padding-right: 10px;
}

.img_barcode {
    display: block;
    padding: 0px;
}

.img_barcode > img {
    width: 180px;
    height:30px;
}

.barcode_number {
    font-family: sans-serif;
    font-size: 14px !important;
}

.area_barcode {
    width: 50%;
}

p{
    line-height: 1;
}
</style>

@php($chunk = array_chunk($data->toArray(), 3))
    @foreach($chunk as $key => $value)
        <table id="table">
            <tr>
                @if(isset($value[0]))
                <td style="border: 1px solid black;">
                    <center>
                        <h2 style="margin-top: 2px; margin-bottom: 5px;">{{ $value[0]->name}}</h2>
                    </center>
                    <br>
                    <div class="barcode">
                        <!-- <div class="img_barcode" style="display:block;padding:0px"> -->
                        <div class="img_barcode">
                            <img src="data:image/png;base64,{{ DNS1D::getBarcodePNG($value[0]->id, 'C128',2,35) }}" alt="barcode"   />
                        </div>
                        <div class="row">
                            <span class="barcode_number">{{ $value[0]->id }}</span >
                        </div>
                    </div>
                </td>
                @endif
                <td style="padding-right: 5px; padding-left: 5px;"></td>
                @if(isset($value[1]))
                <td style="border: 1px solid black;">
                    <center>
                        <h2 style="margin-top: 2px; margin-bottom: 5px;">{{ $value[1]->name}}</h2>
                    </center>
                    <br>
                    <div class="barcode">
                        <!-- <div class="img_barcode" style="display:block;padding:0px"> -->
                        <div class="img_barcode">
                            <img src="data:image/png;base64,{{ DNS1D::getBarcodePNG($value[1]->id, 'C128',2,35) }}" alt="barcode"   />
                        </div>
                        <div class="row">
                            <span class="barcode_number">{{ $value[1]->id }}</span >
                        </div>
                    </div>
                </td>
                @endif
                <td style="padding-right: 5px; padding-left: 5px;"></td>
                @if(isset($value[2]))
                <td style="border: 1px solid black;">
                    <center>
                        <h2 style="margin-top: 2px; margin-bottom: 5px;">{{ $value[2]->name}}</h2>
                    </center>
                    <br>
                    <div class="barcode">
                        <!-- <div class="img_barcode" style="display:block;padding:0px"> -->
                        <div class="img_barcode">
                            <img src="data:image/png;base64,{{ DNS1D::getBarcodePNG($value[2]->id, 'C128',2,35) }}" alt="barcode"   />
                        </div>
                        <div class="row">
                            <span class="barcode_number">{{ $value[2]->id }}</span >
                        </div>
                    </div>
                </td>
                @endif


            </tr>
        </table>
        <br>
    @endforeach