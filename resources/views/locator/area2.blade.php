@extends('layouts.app', ['active' => 'area'])
@section('header')
<div class="page-header page-header-default">
	<div class="breadcrumb-line">
		<ul class="breadcrumb">
			<li><a href="#"><i class="icon-home2 position-left"></i> Area & Locator</a></li>
			<li class="active">Area</li>
		</ul>
	</div>
</div>
@endsection

@section('content')
<div class="content">
	<div class="row">
		<div class="panel panel-flat">
			<div class="panel-heading">
				<button class="btn btn-default" id="addarea"><span class="icon-plus2"></span> New Area</button>
			</div>
			<div class="panel-body">
				<div class="row">
					<div class="table-responsive">
						<table class ="table table-basic table-condensed" id="table-area">
							<thead>
								<tr>
									<th>#</th>
									<th>Barcode</th>
									<th>Name</th>
									<th>Area</th>
									<th>Description</th>
									<th>Action</th>
								</tr>
								
							</thead>
						</table>
					</div>
				</div>
				
			</div>
		</div>
	</div>
</div>


@endsection



@section('js')
<script type="text/javascript">
$(document).ready(function(){

var table = $('#table-area').DataTable({
	processing:true,
	serverSide:true,
	deferRender:true,
	dom:'<"datatable-header"fBl><t><"datatable-footer"ip>',
	language: {
        search: '<span>Filter:</span> _INPUT_',
        searchPlaceholder: 'Type to filter...',
        lengthMenu: '<span>Show:</span> _MENU_',
        paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
    },
    ajax: {
        type: 'GET',
        url: "{{route('area.getDataArea')}}"
    },
    fnCreatedRow: function (row, data, index) {
        var info = table.page.info();
        var value = index+1+info.start;
        $('td', row).eq(0).css(value);
    },
    columns: [
        {data: null, sortable: false, orderable: false, searchable: false},
        {data: 'name', name: 'name'},
        {data: 'description', name: 'description'},
        {data: 'action', name: 'action', searchable:false, sortable:false, orderable:false}
        ]
});

// table.on('preDraw', function() {
// 	loading();
//     Pace.start();
// })
// .on('draw.dt', function() {
//     $.unblockUI();
//     Pace.stop();
// });




});
</script>
@endsection

