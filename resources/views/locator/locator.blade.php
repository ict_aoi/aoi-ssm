@extends('layouts.app', ['active' => 'locator'])
@section('header')
<div class="page-header page-header-default">
	<div class="breadcrumb-line">
		<ul class="breadcrumb">
			<li><a href="#"><i class="icon-home2 position-left"></i> Area & Locator</a></li>
			<li class="active">Locator</li>
		</ul>
	</div>
</div>
@endsection

@section('content')
<div class="content">
	<div class="row">
		<div class="panel panel-flat">
			<div class="panel-body">
				<div class="row">
					<div class="col-lg-3">
						<select class="select-search form-control" id="filter-area">
						</select>
					</div>
					<div class="col-lg-2">
						<button class="btn btn-default" id="btn-add"><span class="icon-plus2"></span> New Locator</button>
					</div>
					<div class="col-lg-2">
						<button class="btn btn-success" id="btn-print"><span class="icon-printer2"></span> Print Barcode</button>
					</div>
					
					
				</div>
				<div class="row">
					<div class=" table-responsive">
						<table class ="table table-basic table-condensed" id="table-list">
							<thead>
								<th>#</th>
								<th>Barcode</th>
								<th>Name</th>
								<th>Area</th>
								<th>Description</th>
								<th>Action</th>
							</thead>
						</table>
					</div>
				</div>
				
			</div>
		</div>
	</div>
</div>

<a href="{{ route('area.printLocator') }}" id="printloc"></a>
@endsection

@section('modal')
<div id="modal_add" class="modal fade">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h5 class="modal-title">Create New Locator</h5>
			</div>

			<div class="modal-body">
				<form action="{{ route('area.addLocator') }}" id="form-add">
					@csrf
					<div class="row">
						<label>Locator Name</label>
						<input type="text" name="name" id="name" class="form-control" required="">
					</div>
					<div class="row">
						<label>Description</label>
						<input type="text" name="desc" id="desc" class="form-control" required="">
					</div>
					<div class="row">
						<label>Area</label>
						<select class="select form-control" id="area" required="">
							<option value="">Choosse Area</option>
						</select>
					</div>
					<br>
					<div class="row">
						<button type="submit" class="btn btn-primary" id="btn-save">Save</button>
						<button type="reset" class="btn btn-warning" id="btn-reset">Reset</button>
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					</div>
				</form>
			</div>

			
		</div>
	</div>
</div>
@endsection

@section('js')
<script type="text/javascript">
$(document).ready(function(){

	$(window).on('load',function(){
		setFilArea();
		table.clear();
		table.draw();
	});

	$('#filter-area').change(function(event){
		event.preventDefault();
	
		table.clear();
		table.draw();
	});

var table = $('#table-list').DataTable({
	processing:true,
	serverSide:true,
	deferRender:true,
	dom:'<"datatable-header"fBl><t><"datatable-footer"ip>',
	language: {
        search: '<span>Filter:</span> _INPUT_',
        searchPlaceholder: 'Type to filter...',
        lengthMenu: '<span>Show:</span> _MENU_',
        paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
    },
    ajax: {
        type: 'GET',
        url: "{{route('area.getDataLocator')}}",
        data: function (d) {
	            return $.extend({},d,{
	                "area": $('#filter-area').val()
	            });
	        }
    },
    fnCreatedRow: function (row, data, index) {
        var info = table.page.info();
        var value = index+1+info.start;
        $('td', row).eq(1).css('max-width','50px');
    },
    columns: [
        // {data: null, sortable: false, orderable: false, searchable: false},
        {data: 'checkbox',name:'checkbox', sortable: false, orderable: false, searchable: false},
        {data: 'id', name: 'id'},
        {data: 'name', name: 'name'},
        {data: 'area_id', name: 'area_id'},
        {data: 'description', name: 'description'},
        {data: 'action', name: 'action', searchable:false, sortable:false, orderable:false}
        ]
});

table.on('preDraw', function() {
	loading();
    Pace.start();
})
.on('draw.dt', function() {
    $.unblockUI();
    Pace.stop();
});


$('#btn-add').click(function(){

	$.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
        type: 'get',
        url : "{{ route('area.ajaxGetArea') }}",

        success: function(response) {
       		var carea = response.area;

       		$('#area').empty();
            $('#area').append('<option value="">Choose Role</option>');
            for (var i = 0; i < carea.length; i++) {

               $('#area').append('<option value="'+carea[i]['id']+'">'+carea[i]['name']+'</option>');
            }
        },
        error: function(response) {
        	console.log(response);
            
        }
    });

	$('#modal_add').modal('show');
});

$('#form-add').submit(function(event){
	event.preventDefault();


	$.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
        type: 'get',
        url : $('#form-add').attr('action'),
        data:{name:$('#name').val(),desc:$('#desc').val(),area:$('#area').val()},
        beforeSend : function(){
        	loading();
        },
        success: function(response) {
        	$('#modal_add').modal('hide');
        	$.unblockUI();
         	var notif = response.data;
            alert(notif.status,notif.output);
            table.clear();
            table.draw();
        },
        error: function(response) {
        	$('#modal_add').modal('show');
        	$.unblockUI();
           var notif = response.data;
            alert(notif.status,notif.output);
            
        }
    });
});

$('#btn-print').click(function(event){
	event.preventDefault();

	
	var data = "";

	$('.barcode:checked').each(function() {
        
        var str = $(this).val()+";"
        data = data+str;
    });
	var url = $('#printloc').attr('href')+'?data='+data;
	if (data!="") {
		window.open(url);
	}else{
		alert(422,"Barcode not selected ! ! !");
	}
	
    // if (data.length>0) {
    // 	$.ajaxSetup({
	   //      headers: {
	   //          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	   //      }
	   //  });
	   //  $.ajax({
	   //      type: 'post',
	   //      url : $('#printloc').attr('href'),
	   //      data:{data:data},
	   //      success: function(response) {
	        
	   //      },
	   //      error: function(response) {
	        	
	            
	   //      }
	   //  });
    // }
});

});

function setFilArea(){
	$.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
        type: 'get',
        url : "{{ route('area.ajaxGetArea') }}",

        success: function(response) {
       		var carea = response.area;

       		$('#filter-area').empty();
       		$('#filter-area').append('<option value="">--Choose Area--</option>');
            for (var i = 0; i < carea.length; i++) {

               $('#filter-area').append('<option value="'+carea[i]['id']+'">'+carea[i]['name']+'</option>');
            }
        },
        error: function(response) {
        	console.log(response);
            
        }
    });

}
</script>
@endsection

