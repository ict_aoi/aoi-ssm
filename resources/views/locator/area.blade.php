@extends('layouts.app', ['active' => 'area'])
@section('header')
<div class="page-header page-header-default">
	<div class="breadcrumb-line">
		<ul class="breadcrumb">
			<li><a href="#"><i class="icon-home2 position-left"></i> Area & Locator</a></li>
			<li class="active">Area</li>
		</ul>
	</div>
</div>
@endsection

@section('content')
<div class="content">
	<div class="row">
		<div class="panel panel-flat">
			<div class="panel-body">
				<div class="row">
					<button class="btn btn-default" id="addarea"><span class="icon-plus2"></span> New Area</button>
				</div>
				<div class="row form-group">
					

					<div class="table-responsive">
						<table class ="table table-basic table-condensed" id="table-list" width="100%">
							<thead>
								<tr>
									<th>#</th>
									<th>Area Name</th>
									<th>Description</th>
									<th>Action</th>
								</tr>
							</thead>
						</table>
					</div>
				</div>
				
			</div>
		</div>
	</div>
</div>

@endsection

@section('modal')
<div id="modal_add" class="modal fade">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h5 class="modal-title">Create New Area</h5>
			</div>

			<div class="modal-body">
				<form action="{{ route('area.addArea') }}" id="form-add">
					@csrf
					<div class="row">
						<label>Area Name</label>
						<input type="text" name="area" id="area" class="form-control" required="">
					</div>
					<div class="row">
						<label>Description</label>
						<input type="text" name="desc" id="desc" class="form-control" required="">
					</div>
					<br>
					<div class="row">
						<button type="submit" class="btn btn-primary" id="btn-save">Save</button>
						<button type="reset" class="btn btn-warning" id="btn-reset">Reset</button>
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					</div>
				</form>
			</div>

			
		</div>
	</div>
</div>
@endsection

@section('js')
<script type="text/javascript">
$(document).ready(function(){



	var table = $('#table-list').DataTable({
		processing:true,
		serverSide:true,
		deferRender:true,
		autowidth:true,
		dom:'<"datatable-header"fBl><t><"datatable-footer"ip>',
		language: {
	        search: '<span>Filter:</span> _INPUT_',
	        searchPlaceholder: 'Type to filter...',
	        lengthMenu: '<span>Show:</span> _MENU_',
	        paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
	    },
	    ajax: {
	        type: 'GET',
	        url: "{{route('area.getDataArea')}}"
	    },
	    fnCreatedRow: function (row, data, index) {
	        var info = table.page.info();
	        var value = index+1+info.start;
	        $('td', row).eq(0).html(value);
	    },
	    columns: [
	        {data: null, sortable: false, orderable: false, searchable: false},
	        {data: 'name', name: 'name'},
	        {data: 'description', name: 'description'},
	        {data: 'action', name: 'action', searchable:false, sortable:false, orderable:false}
	    ]
	});

$('#addarea').click(function(){
	$('#modal_add').modal('show');
});

$('#form-add').submit(function(event){
	event.preventDefault();

	$.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
        type: 'post',
        url : $('#form-add').attr('action'),
        data:{name:$('#area').val(),desc:$('#desc').val(),},
        beforeSend : function(){
        	loading();
        },
        success: function(response) {
        	$('#modal_add').modal('hide');
        	$.unblockUI();
         	var notif = response.data;
            alert(notif.status,notif.output);
            table.clear();
            table.draw();
        },
        error: function(response) {
        	$('#modal_add').modal('show');
        	$.unblockUI();
           var notif = response.data;
            alert(notif.status,notif.output);
            
        }
    });
});

});

function modalDispose(){
	$('')
}
</script>
@endsection

