<tr>
	<td>{{ $data['carton_id'] }}</td>
	<td>{{ $data['invoice'] }}</td>
	<td>{{ $data['custno'] }}</td>
	<td>{{ $data['country'] }}</td>
	<td>{{ $data['doctype'] }}</td>
	<td>
		<span class="label label-success">{{ $data['location'] }}</span>
		<span class="label label-primary">{{ $data['status'] }}</span>
	</td>
</tr>