@extends('layouts.app', ['active' => 'scanstuffing'])
@section('header')
<div class="page-header page-header-default">
	<div class="breadcrumb-line">
		<ul class="breadcrumb">
			<li><a href="#"><i class="icon-home2 position-left"></i>Stuffing</a></li>
		</ul>
	</div>
</div>
@endsection

@section('content')
<div class="content">
	<div class="row">
		<div class=" panel panel-flat">
			<div class="panel-body">
				<div class="row">
					<div class="col-lg-8">
						<label><b>Scan Barcode</b></label>
						<input type="text" name="barcode_id" id="barcode_id" class="form-control input-new" placeholder="#scan in here" required="">
					</div>
					<div class="col-lg-4">
						<label><b>Qty Scan</b></label>
						<input type="text" name="_count" id="_count" class="form-control" value="0" readonly="" style="text-align: center; font-size: 28px;">
					</div>

				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class=" panel panel-flat">
			<div class="panel-body">
				<div class="table-responsive">
					<table class ="table table-basic table-condensed" id="table-list">
						<thead>
							<tr>
								<th>Carton ID</th>
								<th>Invoice</th>
								<th>Cust. No.</th>
								<th>Country</th>
								<th>Document Type</th>
								<th></th>
							</tr>
						</thead>
						<tbody></tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>

@endsection



@section('js')
<script type="text/javascript">
$(document).ready(function(){
	$('#barcode_id').focus();

	
	$('.input-new').keypress(function(event){
		if (event.which==13) {
			event.preventDefault();

			var ctn = $('#barcode_id').val();

			$.ajaxSetup({
		        headers: {
		            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		        }
		    });
		    $.ajax({
		        type: 'post',
		        url : "{{ route('whs.stuff.ajaxScanStuffing') }}",
		        data:{ctn:ctn},
		        beforeSend : function(){
		        	loading();
		        },
		        success: function(response) {
		        	$.unblockUI();
		           	

		        	$('#table-list > tbody').prepend(response);
        	
		        	var count = +$('#_count').val()+1;
		        	$('#_count').val(count);
		        },
		        error: function(response) {
		        	$.unblockUI();
		           	alert(response.status,response.responseText);
		            
		        }
		    });

			
        	$('#barcode_id').val('');
        	$('#barcode_id').focus();
		}
		
	});

	
	
});
</script>
@endsection