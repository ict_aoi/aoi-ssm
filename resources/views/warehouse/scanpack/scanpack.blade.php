@extends('layouts.app', ['active' => 'scanpack'])
@section('header')
<div class="page-header page-header-default">
	<div class="breadcrumb-line">
		<ul class="breadcrumb">
			<li><a href="#"><i class="icon-home2 position-left"></i>Scan Pack</a></li>
		</ul>
	</div>
</div>
@endsection

@section('content')
<div class="content">
	<div class="row">
		<div class=" panel panel-flat">
			<div class="panel-body">
				<div class="row">
					<div class="col-lg-8">
						<label><b>Carton ID</b></label>
						<input type="text" name="ctn_id" id="ctn_id" readonly="" class="form-control" style=" font-size: 28px;">
					</div>
					<div class="col-lg-4">
						<label><b>Qty Save Garment</b></label>
						<input type="text" name="ctn_scan" id="ctn_scan" readonly="" class="form-control" style="text-align: center; font-size: 28px;">
					</div>
				</div>
				<div class="row">
					<div class="col-lg-8">
						<label><b>Scan Barcode</b></label>
						<input type="text" name="barcode_id" id="barcode_id" class="form-control input-new" placeholder="#scan in here" required="">
					</div>
					<div class="col-lg-4">
						<label><b>Qty Scan</b></label>
						<input type="text" name="_count" id="_count" class="form-control" value="0" readonly="" style="text-align: center; font-size: 28px;">
					</div>

				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class=" panel panel-flat">
			<div class="panel-body">
				<div class="table-responsive">
					<table class ="table table-basic table-condensed" id="table-list">
						<thead>
							<tr>
								<th>Barcode ID</th>
								<th>Doc. No.</th>
								<th>Season</th>
								<th>Style</th>
								<th>Article</th>
								<th>Size</th>
								<th>Status</th>
								<th>Carton ID</th>
							</tr>
						</thead>
						<tbody></tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>

@endsection



@section('js')
<script type="text/javascript">
$(document).ready(function(){
	$('#barcode_id').focus();

	
	$('.input-new').keypress(function(event){
		if (event.which==13) {
			event.preventDefault();

			var barcode = $('#barcode_id').val();

			var cek = barcode.substr(0,3);

			if (cek=='CTN') {
				ajaxCtn(barcode);
			}else if(cek!='CTN' && barcode!="") {
				var ctn = $('#ctn_id').val();
				ajaxGarm(barcode,ctn);
			}else{
				alert(422,"Barcode required ! ! !");
			}

			
        	$('#barcode_id').val('');
        	$('#barcode_id').focus();
		}
		
	});

	
	
});


function ajaxCtn(barcode){
	$.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
        type: 'get',
        url : "{{ route('whs.scan.ajaxGetCarton') }}",
        data:{barcode:barcode},
        beforeSend : function(){
        	loading();
        },
        success: function(response) {
        	$.unblockUI();
           	$('#ctn_id').val(response.ctn_id);
           	$('#ctn_scan').val(response.grm);

        	
        },
        error: function(response) {
        	$.unblockUI();
           	alert(response.status,response.responseText);
            
        }
    });
}

function ajaxGarm(bargar,ctn){
	
    $.ajax({
        type: 'post',
        url : "{{ route('whs.scan.ajaxScan') }}",
        data:{bargar:bargar,ctn:ctn},
        success: function(response) {
        	$('#table-list > tbody').prepend(response);
        	
        	var gcount = +$('#_count').val()+1;
        	$('#_count').val(gcount);

        	var ccount = +$('#ctn_scan').val()+1;
        	$('#ctn_scan').val(ccount);

        	
        },
        error: function(response) {
        	$.unblockUI();
           	alert(response.status,response.responseText);
            
        }
    });
}
</script>
@endsection