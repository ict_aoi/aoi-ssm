@extends('layouts.app', ['active' => 'report_mutation'])
@section('header')
<div class="page-header page-header-default">
	<div class="breadcrumb-line">
		<ul class="breadcrumb">
			<li><a href="#"><i class="icon-home2 position-left"></i> Report</a></li>
			<li class="active"> Mutation</li>
		</ul>
	</div>
</div>
@endsection

@section('content')
<div class="content">
	<div class="panel panel-flat">
		<div class="panel-body">
			<div class="row">
				<form action="{{ route('report.mutation.getData') }}" id="form-search">
					@csrf
                    <div class="row form-group">
                        <label class="radio-inline"><input type="radio" name="radio_status" checked="checked" value="checkin"><b> Checkin</b></label>
	        			<label class="radio-inline"><input type="radio" name="radio_status" value="checkout"><b> Checkout</b></label>
						<label class="radio-inline"><input type="radio" name="radio_status" value="barcodeid"><b> Barcode ID</b></label>
                    </div>
					<div class="row form-group" >
		                <div class="input-group">
		                    <input type="text" class="form-control daterange-basic" name="date_range" id="date_range" required="">
							<input type="text" class="form-control hidden" name="barcodeID" id="barcodeID" placeholder="Input Barcode In Here">
		                    <div class="input-group-btn">
		                        <button type="submit" class="btn btn-primary">Filter</button>
		                    </div>
		                </div>
		            </div>
				</form>
			</div>
			<div class="row {{Auth::user()->admin_role === false ? 'hidden' : '' }} ">
				<div class="col-md-2"> Factory :
	                <select class="select form-control" name="factory_id" id="factory_id">
	                	@foreach($factory as $fc)
	                		<option value="{{$fc->id}}" {{ Auth::user()->factory_id == $fc->id ? 'selected' : '' }}>{{$fc->factory_name}}</option>
	                	@endforeach
	                </select>
	            </div>
			</div>
			<div class="row table-responsive">
				<table class ="table table-basic table-condensed" id="table-list" width="100%">
					<thead>
						<tr>
							<th>#</th>
							<th>Barcode ID</th>
							<th>Document No.</th>
							<th>Item</th>
							<th>Checkin PIC</th>
                            <th>Checkin Date</th>
                            <th>Store</th>
							<th>Checkout</th>
                            <th>Current Loc.</th>
						</tr>
					</thead>
				</table>
			</div>
		</div>
	</div>
</div>
<a href="{{ route('report.mutation.exportMutation') }}" id="exportMutation"></a>
@endsection

@section('js')
<script type="text/javascript" src="{{url('assets/js/plugins/tables/datatables/extensions/buttons.min.js')}}"></script>
<script type="text/javascript">
$(document).ready(function(){
    $.extend( $.fn.dataTable.defaults, {
        stateSave: true,
        autoWidth: false,
        autoLength: false,
        processing: true,
        serverSide: true,
        dom: '<"datatable-header"fBl><t><"datatable-footer"ip>',
        language: {
            search: '<span>Filter:</span> _INPUT_',
            searchPlaceholder: 'Type to filter...',
            lengthMenu: '<span>Show:</span> _MENU_',
            paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
        }
    });

    var _token = $("input[name='_token']").val();
    var table = $('#table-list').DataTable({
        buttons: [
            {
                text: 'Export to Excel',
                className: 'btn btn-sm bg-success exportExcel',
				action: function (e, dt, node, config)
                {
                    var columns = table.settings().init().columns;
                    var current_order = table.order();

                    var column_name = columns[current_order[0][0]].name;
                    var direction = current_order[0][1];
					var filter = table.search();

                    window.location.href = $('#exportMutation').attr('href')
                                            + '?factory_id=' + $('#factory_id').val()
                                            + '&date_range=' + $('#date_range').val()
											+ '&barcode_id=' + $('#barcodeID').val()
											+ '&radio=' 	+ $('input[name=radio_status]:checked').val()
											+ '&filter=' +filter;
                }
            }
       ],
        ajax: {
	        type: 'GET',
	        url: $('#form-search').attr('action'),
	        data: function (d) {
	            return $.extend({},d,{
	            	"date_range":$('#date_range').val(),
	                "factory_id": $('#factory_id').val(),
					"barcode_id":$('#barcodeID').val(),
                    "radio"     : $('input[name=radio_status]:checked').val()
	            });
	        }
	    },
        fnCreatedRow: function (row, data, index) {
            var info = table.page.info();
            var value = index+1+info.start;
            $('td', row).eq(0).html(value);
        },
		initComplete:function(){
            $('.dataTables_filter input').unbind();
            $('.dataTables_filter input').bind('keyup',function(e){
                var code = e.keyCode || e.which;
				if (code == 13) {
					table.search(this.value).draw();
					
				}
            });
        },
        columnDefs: [
            {
                className: 'dt-center'
            }
        ],
        columns: [
	        {data: null, sortable: false, orderable: false, searchable: false},
	        {data: 'barcode_id', name: 'barcode_id', sortable: false, orderable: false, searchable: false},
	        {data: 'documentno', name: 'documentno', sortable: false, orderable: false, searchable: false},
	        {data: 'item', name: 'item', sortable: false, orderable: false, searchable: false},
	       	{data: 'checkin_pic', name: 'checkin_pic', sortable: false, orderable: false, searchable: false},
	        {data: 'checkin_date', name: 'checkin_date', sortable: false, orderable: false, searchable: false},
            {data: 'store', name: 'store', sortable: false, orderable: false, searchable: false},
	        {data: 'checkout', name: 'checkout', sortable: false, orderable: false, searchable: false},
            {data: 'current_location', name: 'current_location', sortable: false, orderable: false, searchable: false}
	    ]
    });

    table.on('preDraw', function() {
		loading();
	    Pace.start();
	})
	.on('draw.dt', function() {
	    $.unblockUI();
	    Pace.stop();
	});

	$('#form-search').submit(function(event){
		event.preventDefault();
		
		table.clear();
		table.draw();
	});

	$('#factory_id').change(function(event){
		event.preventDefault();
		
		table.clear();
		table.draw();
	});

	$('input[type=radio][name=radio_status]').change(function(event){
		var radiostat = this.value;

		if (radiostat=="checkin" || radiostat=="checkout") {
			$('#date_range').removeClass('hidden');
			document.getElementById('date_range').required = true;
			$('#barcodeID').addClass('hidden');
			document.getElementById('barcodeID').required = false;
		}else{
			$('#date_range').addClass('hidden');
			document.getElementById('date_range').required = false;
			$('#barcodeID').removeClass('hidden');
			document.getElementById('barcodeID').required = true;
		}

		table.clear();
		table.draw();
	});
});
</script>
@endsection