@extends('layouts.app', ['active' => 'report_borrowing'])
@section('header')
<div class="page-header page-header-default">
	<div class="breadcrumb-line">
		<ul class="breadcrumb">
			<li><a href="#"><i class="icon-home2 position-left"></i> Report</a></li>
			<li class="active">Borrowing</li>
		</ul>
	</div>
</div>
@endsection

@section('content')
<div class="content">
	<div class="panel panel-flat">
		<div class="panel-heading">
			<div class="row">
				<label class="radio-inline"><input type="radio" name="radio_type" checked="checked" value="onborrow"><b>Borrowing</b></label>
				<label class="radio-inline"><input type="radio" name="radio_type" value="scanout"><b>Scan Out Borrowing</b></label>
			</div>
			<div class="row input-group hidden" id="dateborrow">
				<input type="text" class="form-control daterange-basic" name="date_range" id="date_range" required="">
				<div class="input-group-btn">
					<button id="submit" type="button" class="btn btn-primary">Filter</button>
				</div>
			</div>
		</div>
		<div class="panel-body">
			<div class="row {{Auth::user()->admin_role === false ? 'hidden' : '' }} ">
				<div class="col-md-2"> Factory :
	                <select class="select form-control" name="factory_id" id="factory_id">
	                	@foreach($factory as $fc)
	                		<option value="{{$fc->id}}" {{ Auth::user()->factory_id == $fc->id ? 'selected' : '' }}>{{$fc->factory_name}}</option>
	                	@endforeach
	                </select>
	            </div>
			</div>
			<div class="row table-responsive">
				<table class ="table table-basic table-condensed" id="table-list" width="100%">
					<thead>
						<tr>
							<th>#</th>
							<th>Barcode ID</th>
							<th>Document No.</th>
							<th>Item</th>
							<th>Borrowing Date</th>
							<th>Loc. Borrowing</th>
							<th>Borrower</th>
						</tr>
					</thead>
				</table>
			</div>
		</div>
	</div>
</div>

<a href="{{ route('report.borrow.exportBorrowing') }}" id="export_borrowing"></a>
@endsection



@section('js')
<script type="text/javascript" src="{{url('assets/js/plugins/tables/datatables/extensions/buttons.min.js')}}"></script>
<script type="text/javascript">
$(document).ready(function(){
	
	$(window).on('load',function(){
		loading();
		table.clear();
		table.draw();
		$.unblockUI();
	});

	$.extend( $.fn.dataTable.defaults, {
        stateSave: true,
        autoWidth: false,
        autoLength: false,
        processing: true,
        serverSide: true,
        dom: '<"datatable-header"fBl><t><"datatable-footer"ip>',
        language: {
            search: '<span>Filter:</span> _INPUT_',
            searchPlaceholder: 'Type to filter...',
            lengthMenu: '<span>Show:</span> _MENU_',
            paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
        }
    });

    var _token = $("input[name='_token']").val();
    var table = $('#table-list').DataTable({
        buttons: [
            {
                text: 'Export to Excel',
                className: 'btn btn-sm bg-success exportExcel',
                action: function (e, dt, node, config)
                {
                    var columns = table.settings().init().columns;
                    var current_order = table.order();

                    var column_name = columns[current_order[0][0]].name;
                    var direction = current_order[0][1];
                    var filter = table.search();
                    window.location.href = $('#export_borrowing').attr('href')
                                            + '?factory_id=' + $('#factory_id').val()
											+ '&radio_type=' + $('input[name=radio_type]:checked').val()
											+ '&date_range=' + $('#date_range').val()
                                            + '&filterby=' + filter;
					
                }
            }
       ],
        ajax: {
	        type: 'GET',
	        url: "{{ route('report.borrow.ajaxGetBorrowing') }}",
	        data: function (d) {
	            return $.extend({},d,{
					"radio_type":$('input[name=radio_type]:checked').val(),
					"date_range":$('#date_range').val(),
	            	"factory_id":$('#factory_id').val()
	            });
	        }
	    },
        fnCreatedRow: function (row, data, index) {
            var info = table.page.info();
            var value = index+1+info.start;
            $('td', row).eq(0).html(value);
        },
        columnDefs: [
            {
                className: 'dt-center'
            }
        ],
		initComplete:function(){
		
			$('.dataTables_filter input').unbind();
			$('.dataTables_filter input').bind('keyup', function(e){
				var code = e.keyCode || e.which;
				if (code == 13) {
					table.search(this.value).draw();
					
				}
			});
		},
        columns: [
	        {data: null, sortable: false, orderable: false, searchable: false},
	        {data: 'barcode_id', name: 'barcode_garment.barcode_id'},
	        {data: 'documentno', name: 'master_mo.documentno'},
	        {data: 'season', name: 'master_mo.season'},
	       	{data: 'created_at', name: 'garment_movements.created_at'},
	        {data: 'locator_to', name: 'garment_movements.locator_to'},
	        {data: 'borrower', name: 'garment_movements.borrower'}
	    ]
    });

	



	table.on('preDraw', function() {
		loading();
	    Pace.start();
	})
	.on('draw.dt', function() {
	    $.unblockUI();
	    Pace.stop();
	});

	$('#form-search').submit(function(event){
		event.preventDefault();
		
		table.clear();
		table.draw();
	});

	$('input[type=radio][name=radio_type]').change(function(event){
		event.preventDefault();

		// $('#dateborrow')
		if (this.value=="scanout") {
			if($('#dateborrow').hasClass('hidden')){
				$('#dateborrow').removeClass('hidden');
			}
		}else{
			$('#dateborrow').addClass('hidden');
		}

		table.clear();
		table.draw();
	});

	$('#submit').click(function(event){
		table.clear();
		table.draw();
	});
});






</script>
@endsection