@extends('layouts.app', ['active' => 'report_receiving'])
@section('header')
<div class="page-header page-header-default">
	<div class="breadcrumb-line">
		<ul class="breadcrumb">
			<li><a href="#"><i class="icon-home2 position-left"></i> Report</a></li>
			<li class="active">Report Receiving</li>
		</ul>
	</div>
</div>
@endsection

@section('content')
<div class="content">
	<div class="panel panel-flat">
		<div class="panel-heading">
			<form action="{{ route('report.stock.ajaxGetReceiving') }}" id="form-search">
				@csrf
				<div class="row">
					<div class="col-lg-11">
						<div class="form-group hidden" id="bydate">
			                <label><b>Date</b></label>
			                <input type="text" class="form-control daterange-basic" name="date_range" id="date_range">
			            </div>

			            <div class="form-group" id="bystyle">
			                <label><b>Style</b></label>
			                <input type="text" class="form-control" name="style" id="style" placeholder="Style">
			            </div>

			            <div class="form-group hidden" id="bymo">
			                <label><b>Document No.</b></label>
			                 <input type="text" class="form-control" name="nomo" id="nomo"  placeholder="Document No.">
			            </div>
					</div>
					<div class="col-lg-1">
						<button type="submit" class="btn btn-primary" style="margin-top: 30px;">Filter</button>
					</div>
				</div>
				
				

	            <div class="row form-group">
	                <label class="radio-inline"><input type="radio" name="radio_search" checked="checked" value="sstyle"><b>By Style</b></label>
		            <label class="radio-inline"><input type="radio" name="radio_search" value="sdate"><b>By Date</b></label>
		            <label class="radio-inline"><input type="radio" name="radio_search" value="sdocno"><b>By Document No.</b></label>
	            </div>

			</form>
		</div>
		<div class="panel-body">
			<div class="row {{Auth::user()->admin_role === false ? 'hidden' : '' }} ">
				<div class="col-md-2"> Factory :
	                <select class="select form-control" name="factory_id" id="factory_id">
	                	@foreach($factory as $fc)
	                		<option value="{{$fc->id}}" {{ Auth::user()->factory_id == $fc->id ? 'selected' : '' }}>{{$fc->factory_name}}</option>
	                	@endforeach
	                </select>
	            </div>
			</div>
			<div class="table-responsive">
				<table class ="table table-basic table-condensed" id="table-list" width="100%">
					<thead>
						<tr>
							<th>#</th>
							<th>Barcode ID</th>
							<th>Document No.</th>
							<th>Item</th>
							<th>Brand</th>
							<th>Date Checkin</th>
							<th>Current Loc.</th>
							<th></th>
						</tr>
					</thead>
				</table>
			</div>
		</div>
	</div>
</div>
<a href="{{ route('report.stock.exportReceiving') }}" id="export_reciept" ></a>

@endsection



@section('js')
<script type="text/javascript" src="{{url('assets/js/plugins/tables/datatables/extensions/buttons.min.js')}}"></script>
<script type="text/javascript">
$(document).ready(function(){
	
	$('input[type=radio][name=radio_search]').change(function(){
		if (this.value=='sstyle') {
			if ($('#bystyle').hasClass('hidden')) {
				$('#bystyle').removeClass('hidden');
			}
			
			$('#bymo').addClass('hidden');
			$('#bydate').addClass('hidden');
		}else if (this.value=='sdate') {
			if ($('#bydate').hasClass('hidden')) {
				$('#bydate').removeClass('hidden');
			}

			$('#bymo').addClass('hidden');
			$('#bystyle').addClass('hidden');
		}else if (this.value=='sdocno') {
			if ($('#bymo').hasClass('hidden')) {
				$('#bymo').removeClass('hidden');
			}

			$('#bydate').addClass('hidden');
			$('#bystyle').addClass('hidden');
		}
	});

	$.extend( $.fn.dataTable.defaults, {
        stateSave: true,
        autoWidth: false,
        autoLength: false,
        processing: true,
        serverSide: true,
        dom: '<"datatable-header"fBl><t><"datatable-footer"ip>',
        language: {
            search: '<span>Filter:</span> _INPUT_',
            searchPlaceholder: 'Type to filter...',
            lengthMenu: '<span>Show:</span> _MENU_',
            paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
        }
    });

    var _token = $("input[name='_token']").val();
    var table = $('#table-list').DataTable({
        buttons: [
            {
                text: 'Export to Excel',
                className: 'btn btn-sm bg-success exportExcel',
                action: function (e, dt, node, config)
                {
                    var columns = table.settings().init().columns;
                    var current_order = table.order();

                    var column_name = columns[current_order[0][0]].name;
                    var direction = current_order[0][1];
                    var filter = table.search();
                    window.location.href = $('#export_reciept').attr('href')
                                            + '?radio_search=' + $('input[name=radio_search]:checked').val()
                                            + '&factory_id=' + $('#factory_id').val()
                                            + '&date_range=' + $('#date_range').val()
                                            + '&style=' + $('#style').val()
                                            + '&docno=' + $('#nomo').val()
                                            + '&filterby=' + filter;
                }
            }
       ],
        ajax: {
            url: $('#form-search').attr('action'),
            type: 'GET',
            data: function (d) {
                return $.extend({},d,{
                    "radio_search": $('input[name=radio_search]:checked').val(),
                    "date_range": $('#date_range').val(),
                    "style": $('#style').val(),
                    "docno": $('#nomo').val(),
	                "factory_id": $('#factory_id').val(),
                    "_token": _token
                });
            }
        },
        fnCreatedRow: function (row, data, index) {
            var info = table.page.info();
            var value = index+1+info.start;
            $('td', row).eq(0).html(value);
        },
        columnDefs: [
            {
                className: 'dt-center'
            }
        ],
        columns: [
	        {data: null, sortable: false, orderable: false, searchable: false},
	        {data: 'barcode_id', name: 'barcode_id'},
	        {data: 'documentno', name: 'documentno'},
	        {data: 'item', name: 'item'},
	        {data: 'brand', name: 'brand'},
	        {data: 'created_at', name: 'created_at'},
	        {data: 'current_location', name: 'current_location'},
	        {data: 'rack', name: 'rack'}
	    ]
    });
	


	table.on('preDraw', function() {
		loading();
	    Pace.start();
	})
	.on('draw.dt', function() {
	    $.unblockUI();
	    Pace.stop();
	});

	$('#form-search').submit(function(event){
		event.preventDefault();
		
		table.clear();
		table.draw();
	});

	$('#factory_id').change(function(event){
		event.preventDefault();
		
		table.clear();
		table.draw();
	})
	
});






</script>
@endsection