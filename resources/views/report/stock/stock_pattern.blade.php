@extends('layouts.app', ['active' => 'report_stockpattern'])
@section('header')
<div class="page-header page-header-default">
	<div class="breadcrumb-line">
		<ul class="breadcrumb">
			<li><a href="#"><i class="icon-home2 position-left"></i> Report</a></li>
			<li class="active">Stock Pattern</li>
		</ul>
	</div>
</div>
@endsection

@section('content')
<div class="content">
	<div class="panel panel-flat">
		<div class="panel-body">
			<div class="row {{Auth::user()->admin_role === false ? 'hidden' : '' }} ">
				<div class="col-md-2"> Factory :
	                <select class="select form-control" name="factory_id" id="factory_id">
	                	@foreach($factory as $fc)
	                		<option value="{{$fc->id}}" {{ Auth::user()->factory_id == $fc->id ? 'selected' : '' }}>{{$fc->factory_name}}</option>
	                	@endforeach
	                </select>
	            </div>
			</div>
			<div class="table-responsive">
				<table class ="table table-basic table-condensed" id="table-list" width="100%">
					<thead>
						<tr>
							<th>#</th>
							<th>Barcode ID</th>
							<th>Date Reciept</th>
							<th>Document No.</th>
							<th>Item</th>
							<th>Brand</th>
							<th>Current Loc.</th>
							<th>Scan Date</th>
							<th>Rack / PIC</th>
							<th>Marker Remark</th>
						</tr>
					</thead>
				</table>
			</div>
		</div>
	</div>
</div>

<a href="{{ route('report.stock.exportStockPattern') }}" id="export_stockpattern"></a>
@endsection



@section('js')
<script type="text/javascript" src="{{url('assets/js/plugins/tables/datatables/extensions/buttons.min.js')}}"></script>
<script type="text/javascript">
$(document).ready(function(){
	
	$(window).on('load',function(){
		loading();
		table.clear();
		table.draw();
		$.unblockUI();
	});

	$.extend( $.fn.dataTable.defaults, {
        stateSave: true,
        autoWidth: false,
        autoLength: false,
        processing: true,
        serverSide: true,
        dom: '<"datatable-header"fBl><t><"datatable-footer"ip>',
        language: {
            search: '<span>Filter:</span> _INPUT_',
            searchPlaceholder: 'Type to filter...',
            lengthMenu: '<span>Show:</span> _MENU_',
            paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
        }
    });

    var _token = $("input[name='_token']").val();
    var table = $('#table-list').DataTable({
        buttons: [
            {
                text: 'Export to Excel',
                className: 'btn btn-sm bg-success exportExcel',
                action: function (e, dt, node, config)
                {
                    var columns = table.settings().init().columns;
                    var current_order = table.order();

                    var column_name = columns[current_order[0][0]].name;
                    var direction = current_order[0][1];
                    var filter = table.search();
                    window.location.href = $('#export_stockpattern').attr('href')
                                            + '?factory_id=' + $('#factory_id').val()
                                            + '&filterby=' +filter;
                }
            }
       ],
        ajax: {
	        type: 'GET',
	        url: "{{ route('report.stock.ajaxGetStockPattern') }}",
	        data: function (d) {
	            return $.extend({},d,{
	                "factory_id": $('#factory_id').val()
	            });
	        }
	    },
        fnCreatedRow: function (row, data, index) {
            var info = table.page.info();
            var value = index+1+info.start;
            $('td', row).eq(0).html(value);
        },
        columnDefs: [
            {
                className: 'dt-center'
            }
        ],
        columns: [
	        {data: null, sortable: false, orderable: false, searchable: false},
	        {data: 'barcode_id', name: 'barcode_id'},
	       	{data: 'date_reciept', name: 'date_reciept'},
	        {data: 'documentno', name: 'documentno'},
	        {data: 'item', name: 'item'},
	        {data: 'brand', name: 'brand'},
	        {data: 'current_location', name: 'current_location'},
	        {data: 'scan_date', name: 'scan_date', sortable: false, orderable: false, searchable: false},
	        {data: 'rack', name: 'rack', sortable: false, orderable: false, searchable: false},
	        {data: 'remark_marker', name: 'remark_marker', sortable: false, orderable: false, searchable: false},
	    ]
    });

	



	table.on('preDraw', function() {
		loading();
	    Pace.start();
	})
	.on('draw.dt', function() {
	    $.unblockUI();
	    Pace.stop();
	});

	$('#factory_id').change(function(event){
		event.preventDefault();
		
		table.clear();
		table.draw();
	})
	
});






</script>
@endsection