@extends('layouts.app', ['active' => 'trf'])
@section('header')
<div class="page-header page-header-default">
	<div class="breadcrumb-line">
		<ul class="breadcrumb">
			<li><a href="#"><i class="icon-home2 position-left"></i> TRF</a></li>
			<li class="active">Create TRF</li>
		</ul>
	</div>
</div>
@endsection

@section('content')
<div class="content">
	<div class="row">
		<div class=" panel panel-flat">
			<div class="page-header-content">
				<div class="page-title">
					<h4></i> <span class="text-semibold">&nbsp &nbsp  Create TRF</span> </h4>
				</div>
            </div>
			<div class="panel-body">
                <div class="row form-group">
                    <label style="color:red;"><b>* Required Value</b></label>
                </div>
				<div class="row form-group">
                    
                      
                        <div class="col-lg-6">
                            
                            <div class="row">
                                <label><b>Buyer</b> <b style="color:red;">*</b></label>
                                <select class="select" id="buyer" onchange="getCtg(this);" required>
                                    <option value="null">--Chosse Buyer--</option>
                                    @foreach($buyer as $by)
                                        <option value="{{$by->buyer}}">{{$by->buyer}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="row">
                                <label><b>Category</b> <b style="color:red;">*</b></label>
                                <select class="select" id="category" onchange="getCtgSpm(this);" required >
                       
                                </select>
                            </div>
                            <div class="row">
                                <label><b>Category Specimen</b> <b style="color:red;">*</b></label>
                                <select class="select" id="category_specimen" onchange="getType(this);" required>
                                  
                                </select>
                            </div>

                            <div class="row">
                                <label><b>Type Specimen</b> <b style="color:red;">*</b></label>
                                <select class="select" id="type_specimen" onchange="getMeth(this);" required>
                                  
                                </select>
                            </div>
                            <div class="row">
                                <label><b>Testing Methods</b> <b style="color:red;">*</b></label>
                                <div class="multi-select-full">
                                    <select multiple="multiple" data-placeholder="Choosse Methods" class="select" id="test_methods">
                                     
                                    </select>
                                </div>
                            </div>
                            <div class="row">
                                <label><b>Return Test</b> </label>
                                <select class="select" id="returntest">
                                    <option value="false">NO</option>
                                    <option value="true">YES</option>
                                </select>
                            </div>
                            
                        </div>
 
                        <div class="col-lg-6">
                            <div class="row">
                                <label><b>Lab Location</b></label>
                                <select class="select" id="labloction">
                                    @foreach($fact as $fc)
                                        <option value="{{$fc->factory_name}}" {{Auth::user()->factory_id==$fc->id ? 'selected' : ''}}>{{$fc->factory_name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="row">
                                
                                <div class="radio">
                                    <div class="col-md-4">
                                        <label class="display-block"><b> 1 Bulk Testing</b></label>
                                        <label class="radio"><input type="radio" name="test_req" value="bulktesting_m">
                                            M (Model Level)
                                        </label>
                                        <label class="radio"><input type="radio" name="test_req" value="bulktesting_a">
                                            A (Article Level)
                                        </label>
                                        <label class="radio"><input type="radio" name="test_req" value="bulktesting_selective">
                                            Selective
                                        </label>
                                    </div>

                                    <div class="col-md-4">
                                        <label class="display-block"><b> Re-Order Testing</b></label>
                                        <label class="radio"><input type="radio" name="test_req" value="reordertesting_m">
                                            M (Model Level)
                                        </label>
                                        <label class="radio"><input type="radio" name="test_req" value="reordertesting_a">
                                            A (Article Level)
                                        </label>
                                        <label class="radio"><input type="radio" name="test_req" value="reordertesting_selective">
                                            Selective
                                        </label>
                                    </div>

                                    <div class="col-md-4">
                                        <label class="display-block"><b> Re-Test</b></label>
                                        <label class="radio"><input type="radio" name="test_req" value="retest">
                                            Re-Test
                                        </label>
                                        <input type="text" name="no_trf" id="no_trf" class="form-control">
                                    </div>
                                    
                                </div>
                            </div>
                            <div class="row">
                                <div class="row">
                                    <label><b>Date Infromation</b> <b style="color:red;">*</b></label>
                                    <div class="form-group">
                                        <label class="radio-inline"><input type="radio" name="radio_date_info" checked="checked" value="buy_ready">Buy ready (special for development testing)</label>
                                        <label class="radio-inline"><input type="radio" name="radio_date_info" value="podd">PODD</label>
                                        <label class="radio-inline"><input type="radio" name="radio_date_info" value="output_sewing">1st output sewing</label>
                                    </div>
                                    <div class="form-group">
                                        <input type="text" name="date_info" class="form-control date_info" id="anytime-weekday" required="">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <label><b>Part yang akan ditest</b> <b style="color:red;">*</b></label>
                                
                                <textarea name="part_test" id="part_test" class="form-control" placeholder="part yang akan ditest" required=""></textarea>
                            </div>
                        </div>
               
                    
				</div>

                
			</div>
		</div>

        <div class="panel panel-flat">
            <div class="panel-body">
                <div class="row form-group">
                    <div class="row">
                        <button class="btn btn-primary" id="btn-add"><span class="icon-plus2"></span> Add</button>    
                    </div>
                    <div class="row">
                        <div class="table-responsive">
                            <table class ="table table-basic table-condensed" id="table-list">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Doc Type</th>
                                        <th>Doc. Number</th>
                                        <th>Season</th>
                                        <th>Style</th>
                                        <th>Article</th>
                                        <th>Size</th>
                                        <th>Color</th>
                                        <th>Item</th>
                                        <th>Fibre Composition</th>
                                        <th>Fabric Finish</th>
                                        <th>Gauge</th>
                                        <th>Fabric Weight</th>
                                        <th>PLM No.</th>
                                        <th>Care Instruction</th>
                                        <th>Manufacture Name</th>
                                        <th>Exported To</th>
                                        <th>No. Roll</th>
                                        <th>Batch No.</th>
                                        <th>PO. Supplier</th>
                                        <th>Yds Roll</th>
                                        <th>Additional Info</th>
                                        <th>Description</th>
                                    </tr>
                                </thead>
                                <tbody></tbody>
                            </table>
                        </div>
                    </div>
                    <div class="row">
                        <button class="btn btn-success" id="btn-save"><span class="icon-add"></span> Save</button>    
                    </div>

                </div>
            </div>
        </div>
	</div>
</div>

@endsection

@section('modal')
	@include('trf.md_doc');
	@include('trf.md_erp');
@endsection

@section('js')
<script type="text/javascript">
const arrs = [];
$(document).ready(function(){
    
    $('#btn-add').click(function(){
        $('#modal_add').modal('show');
    });

    $('#modal_add .btn-md-save').click(function(){
        
        if ($('#modal_add .mddoctype').val()=="") {
            alert(422,"Document Type Required ! ! !");

            return false;
        }

        if ($('#modal_add .mddocno').val()=='' || $('#modal_add .mdstyle').val()=='' || $('#modal_add .mdarticle').val()=='' || $('#modal_add .mdsize').val()=='' || $('#modal_add .mditem').val()=='' || $('#modal_add .mdseason').val()=='' ) {


            alert(422,"Document No. / Season / Style / Article / Size / Item is Required ! ! !");

            return false;
        }

      

        if ($('#modal_add .mdidx').val()=="") {
            arrs.push({
                'mddoctype' : $('#modal_add .mddoctype').val(),
                'mddocno' : $('#modal_add .mddocno').val(),
                'mdstyle' : $('#modal_add .mdstyle').val(),
                'mdarticle' : $('#modal_add .mdarticle').val(),
                'mdsize' : $('#modal_add .mdsize').val(),
                'mdcolor' : $('#modal_add .mdcolor').val(),
                'mditem' : $('#modal_add .mditem').val(),
                'mdfibre' : $('#modal_add .mdfibre').val(),
                'mdfabfin' : $('#modal_add .mdfabfin').val(),
                'mdgauge' : $('#modal_add .mdgauge').val(),
                'mdweight' : $('#modal_add .mdweight').val(),
                'mdplmno' : $('#modal_add .mdplmno').val(),
                'mdcare' : $('#modal_add .mdcare').val(),
                'mdmanuf' : $('#modal_add .mdmanuf').val(),
                'mdexport' : $('#modal_add .mdexport').val(),
                'mdroll' : $('#modal_add .mdroll').val(),
                'mdbatch' : $('#modal_add .mdbatch').val(),
                'mdposupp' : $('#modal_add .mdposupp').val(),
                'mdydsroll' : $('#modal_add .mdydsroll').val(),
                'mdaddinfo' : $('#modal_add .addInfo').val(),
                'mddescript' : $('#modal_add .mddescrpt').val(),
                'mdseason' : $('#modal_add .mdseason').val(),
            });

            
        }else{
            var id = $('#modal_add .mdidx').val();
            arrs[id]['mddoctype']=$('#modal_add .mddoctype').val();
            arrs[id]['mddoctype']=$('#modal_add .mddoctype').val();
            arrs[id]['mddocno']=$('#modal_add .mddocno').val();
            arrs[id]['mdstyle']=$('#modal_add .mdstyle').val();
            arrs[id]['mdarticle']=$('#modal_add .mdarticle').val();
            arrs[id]['mdsize']=$('#modal_add .mdsize').val();
            arrs[id]['mdcolor']=$('#modal_add .mdcolor').val();
            arrs[id]['mditem']=$('#modal_add .mditem').val();
            arrs[id]['mdfibre']=$('#modal_add .mdfibre').val();
            arrs[id]['mdfabfin']=$('#modal_add .mdfabfin').val();
            arrs[id]['mdgauge']=$('#modal_add .mdgauge').val();
            arrs[id]['mdweight']=$('#modal_add .mdweight').val();
            arrs[id]['mdplmno']=$('#modal_add .mdplmno').val();
            arrs[id]['mdcare']=$('#modal_add .mdcare').val();
            arrs[id]['mdmanuf']=$('#modal_add .mdmanuf').val();
            arrs[id]['mdexport']=$('#modal_add .mdexport').val();
            arrs[id]['mdroll']=$('#modal_add .mdroll').val();
            arrs[id]['mdbatch']=$('#modal_add .mdbatch').val();
            arrs[id]['mdposupp']=$('#modal_add .mdposupp').val();
            arrs[id]['mdydsroll']=$('#modal_add .mdydsroll').val();
            arrs[id]['mdaddinfo']=$('#modal_add .addInfo').val();
            arrs[id]['mddescrpt']=$('#modal_add .mddescrpt').val();
            arrs[id]['mdseason']=$('#modal_add .mdseason').val();

           
        }
        
        addList(arrs);



        $('#modal_add').modal('hide');
    });


    $('#modal_add').on('hidden.bs.modal', function (e) {
        $(this)
            .find("input,textarea")
                .val('')
                .end()
            .find("select")
            .val('')
            .trigger('change')
            .end();
    });

    $('#btn-save').click(function(event){
        event.preventDefault();

        var origin = "SSM";
        var buyer = $('#buyer').val();
        var category = $('#category').val();
        var category_specimen = $('#category_specimen').val();
        var type_specimen = $('#type_specimen').val();
        var test_methods = $('#test_methods').val();
        var returntest = $('#returntest').val();
        // var addInfo = $('#addInfo').val();
        var part_test = $('#part_test').val();
        var no_trf = $('#no_trf').val();
        var date_info = $('.date_info').val();
        var radio_date_info =$('input[name=radio_date_info]:checked').val();
        var test_req =$('input[name=test_req]:checked').val();
        var asal = "DEVELOPMENT";
        var labloction = $('#labloction').val();

       

        if (origin=='' || buyer=='' || category=='' || category_specimen=='' || returntest=='' || part_test=='' || radio_date_info==''  || date_info=='' || test_methods=='' || asal=='' || labloction=='') {

            alert(422,"Origin / Buyer / Category / Category Specimen / Return Test / Part Test / Date Info / Test Requirement Required ! ! !");

            return false;
        }

        if (test_req=="retest" && no_trf=="") {
            alert(422,"Previous number TRF required ! ! !");
        }

        if (arrs.length==0) {
            alert(422,"Document Speciment Required ! ! !");

            return false;
        }

        Swal.fire({
            title:'Do you want to save TRF?',
            showCancelButton: true,
            confirmButtonText: 'Save',
            width: '75rem',
            height: '50rem'
        }).then((result)=>{
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type: 'post',
                url : "{{ route('trf.addTrf') }}",
                data:{
                    origin:origin,
                    asal:asal,
                    buyer:buyer,
                    category_specimen:category_specimen,
                    category:category,
                    type_specimen:type_specimen,
                    test_methods:test_methods,
                    test_req:test_req,
                    radio_date_info:radio_date_info,
                    date_info:date_info,
                    part_test:part_test,
                    returntest:returntest,
                    no_trf:no_trf,
                    datas:arrs,
                    labloction:labloction 
                },
                beforeSend: function() {
                    loading();
                },
                success: function(response) {
                    $.unblockUI();
                    
                   var notif = response.data;
                    alert(notif.status,notif.output);
                    window.location.href="{{ route('trf.index') }}";
                },
                error: function(response) {
                    $.unblockUI();
                    var notif = response.data;
                    alert(notif.status,notif.output);

                }
            });
        });

    });

    $('#modal_add .mddocno').keypress(function(event){
        if(event.which==13){
           var type = $('#modal_add .mddoctype').val();
           var docno = $('#modal_add .mddocno').val();

           
           if (type=="") {
            alert(422,"Document type required ! ! !");

            return false;
           }

           if (docno=="") {
            alert(422,"Document No required ! ! !");

            return false;
           }

            $.ajax({
                type: 'get',
                url :"{{ route('trf.getDocument') }}",
                data :{type:type,docno:docno} ,
                success: function(response) {
                    var dtx= response.data;
              
                    if (dtx.length==0) {
                        alert(422,"Document Not Found ! ! !");
                    }else if (dtx.length>1) {
                        
                         dataTAbErp(dtx);
                    
                        $('#modal_erp').modal('show');
                    
                    }else{
                        
                        $('#modal_add .mddocno').val(dtx[0]['documentno']);
                        $('#modal_add .mdseason').val(dtx[0]['season']);
                        $('#modal_add .mdstyle').val(dtx[0]['style']);
                        $('#modal_add .mdarticle').val(dtx[0]['article']);
                        $('#modal_add .mdsize').val(dtx[0]['size']);
                        $('#modal_add .mdcolor').val(dtx[0]['color']);
                        $('#modal_add .mditem').val(dtx[0]['item']);
                        $('#modal_add .mdexport').val(dtx[0]['export_to']);
                        $('#modal_add .mdposupp').val(dtx[0]['supplier']);
                    }
                     
                },
                error: function(response) {
                    console.log(response);
                }
                    
            });
        }
    });


    $('#modal_erp .table-erp > tbody').on('click','tr',function(){
        var dataar = $('#modal_erp .table-erp').DataTable().row(this).data();

        

        $('#modal_add .mddocno').val(dataar['documentno']);
        $('#modal_add .mdseason').val(dataar['season']);
        $('#modal_add .mdstyle').val(dataar['style']);
        $('#modal_add .mdarticle').val(dataar['article']);
        $('#modal_add .mdsize').val(dataar['size']);
        $('#modal_add .mdcolor').val(dataar['color']);
        $('#modal_add .mditem').val(dataar['item']);
        $('#modal_add .mdexport').val(dataar['export_to']);
        $('#modal_add .mdposupp').val(dataar['supplier']);

       
        
        $('#modal_add .mddocno').trigger('focus');
        
        $('#modal_erp').modal('hide');
    });
    


    $(document).on('show.bs.modal', '.modal', function () {
            var zIndex = 1040 + (10 * $('.modal:visible').length);
            $(this).css('z-index', zIndex);
            setTimeout(function() {
                $('.modal-backdrop').not('.modal-stack').css('z-index', zIndex - 1).addClass('modal-stack');
            }, 0);
    });

    $(document).on('hidden.bs.modal', '.modal', function () {
            $('.modal:visible').length && $(document.body).addClass('modal-open');
    });
});

function getCtg(e){
    var buyer = e.value;

    if (buyer=="") {
        $('#category').empty();
        $('#category').append('<option value="null">--Filter Category--</option>');
    }else{
        $.ajax({
            type: 'get',
            url :"{{ route('trf.ajaxGetCategory') }}",
            data :{buyer:buyer} ,
            success: function(response) {
               var data = response.data;
               $('#category').empty();
               $('#category').append('<option value="null">--Filter Category--</option>');
               for (var i = 0; i < data.length; i++) {
                    $('#category').append('<option value="'+data[i]['category']+'">'+data[i]['category']+'</option>');
               }
                 
            },
            error: function(response) {
                console.log(response);
            }
        });
    }


    
}

function getCtgSpm(e){
   var category = e.value;
   var buyer = $('#buyer').val();

    if (buyer=="" && category=="") {
        $('#category_specimen').empty();
        $('#category_specimen').append('<option value="null">--Filter Category Specimen--</option>');
    }else{
        $.ajax({
            type: 'get',
            url :"{{ route('trf.ajaxGetCtgSpes') }}",
            data :{buyer:buyer,category:category} ,
            success: function(response) {
               var data = response.data;
               $('#category_specimen').empty();
               $('#category_specimen').append('<option value="null">--Filter Category Specimen--</option>');
               for (var i = 0; i < data.length; i++) {
                    $('#category_specimen').append('<option value="'+data[i]['category_specimen']+'">'+data[i]['category_specimen']+'</option>');
               }
                 
            },
            error: function(response) {
                console.log(response);
            }
        });
    }
}

function getType(e){
   var category_specimen = e.value;
   var buyer = $('#buyer').val();
   var category = $('#category').val();

    if (buyer=="" && category=="" && category_specimen=="") {
        $('#type_specimen').empty();
        $('#type_specimen').append('<option value="null">--Filter Type Specimen--</option>');
    }else{
        $.ajax({
            type: 'get',
            url :"{{ route('trf.ajaxGetTypeSpc') }}",
            data :{buyer:buyer,category:category,category_specimen:category_specimen} ,
            success: function(response) {
               var data = response.data;
               $('#type_specimen').empty();
               $('#type_specimen').append('<option value="null">--Filter Type Specimen--</option>');
               for (var i = 0; i < data.length; i++) {
                    $('#type_specimen').append('<option value="'+data[i]['type_specimen']+'">'+data[i]['type_specimen']+'</option>');
               }
                 
            },
            error: function(response) {
                console.log(response);
            }
        });
    }
}

function getMeth(e){
   var category_specimen = $('#category_specimen').val();
   var buyer = $('#buyer').val();
   var category = $('#category').val();
   var type = e.value;

    if (buyer=="" && category=="" && category_specimen) {
        $('#test_methods').empty();

    }else{
        $.ajax({
            type: 'get',
            url :"{{ route('trf.ajaxGetTestMethod') }}",
            data :{buyer:buyer,category:category,category_specimen:category_specimen,type_specimen:type} ,
            success: function(response) {
               var data = response.data;
               $('#test_methods').empty();

               for (var i = 0; i < data.length; i++) {
                    $('#test_methods').append('<option value="'+data[i]['master_method_id']+'">'+data[i]['method_code']+' '+data[i]['method_name']+'</option>');
               }
                 
            },
            error: function(response) {
                console.log(response);
            }
        });
    }
}


function addList(arr){
    $('#table-list > tbody').empty();
    for (var i = 0; i < arr.length; i++) {
       $('#table-list > tbody').append("<tr><td><button class='btn btn-danger btn-xs' data-id="+i+" onclick='del(this);'><i class='icon-trash'></i></button><button class='btn btn-info btn-xs' data-id="+i+" onclick='edt(this);'><i class='icon-pencil'></i></button></td><td>"+arr[i]['mddoctype']+"</td><td>"+arr[i]['mddocno']+"</td><td>"+arr[i]['mdseason']+"</td><td>"+arr[i]['mdstyle']+"</td><td>"+arr[i]['mdarticle']+"</td><td>"+arr[i]['mdsize']+"</td><td>"+arr[i]['mdcolor']+"</td><td>"+arr[i]['mditem']+"</td><td>"+arr[i]['mdfibre']+"</td><td>"+arr[i]['mdfabfin']+"</td><td>"+arr[i]['mdgauge']+"</td><td>"+arr[i]['mdweight']+"</td><td>"+arr[i]['mdplmno']+"</td><td>"+arr[i]['mdcare']+"</td><td>"+arr[i]['mdmanuf']+"</td><td>"+arr[i]['mdexport']+"</td><td>"+arr[i]['mdroll']+"</td><td>"+arr[i]['mdbatch']+"</td><td>"+arr[i]['mdposupp']+"</td><td>"+arr[i]['mdydsroll']+"</td><td>"+arr[i]['mdaddinfo']+"</td><td>"+arr[i]['mddescript']+"</td></tr>"
        );
    }
    
}

function del(e){
    var idx = e.getAttribute('data-id');

   arrs.splice(idx,1);

   addList(arrs);
}


function edt(e){
      var idx = e.getAttribute('data-id');

      var data = arrs[idx];
        $('#modal_add .mddoctype').val(data['mddoctype']).trigger('change');
        $('#modal_add .mddocno').val(data['mddocno']);
        $('#modal_add .mdstyle').val(data['mdstyle']);
        $('#modal_add .mdarticle').val(data['mdarticle']);
        $('#modal_add .mdsize').val(data['mdsize']);
        $('#modal_add .mdcolor').val(data['mdcolor']);
        $('#modal_add .mditem').val(data['mditem']);
        $('#modal_add .mdfibre').val(data['mdfibre']);
        $('#modal_add .mdfabfin').val(data['mdfabfin']);
        $('#modal_add .mdgauge').val(data['mdgauge']);
        $('#modal_add .mdweight').val(data['mdweight']);
        $('#modal_add .mdplmno').val(data['mdplmno']);
        $('#modal_add .mdcare').val(data['mdcare']);
        $('#modal_add .mdmanuf').val(data['mdmanuf']);
        $('#modal_add .mdexport').val(data['mdexport']);
        $('#modal_add .mdroll').val(data['mdroll']);
        $('#modal_add .mdbatch').val(data['mdbatch']);
        $('#modal_add .mdposupp').val(data['mdposupp']);
        $('#modal_add .mdydsroll').val(data['mdydsroll']);
        $('#modal_add .addInfo').val(data['addInfo']).trigger('change');
        $('#modal_add .mddescrpt').text(data['mddescrpt']);
        $('#modal_add .mdseason').val(data['mdseason']);
        $('#modal_add .mdidx').val(idx);

        $('#modal_add').modal('show');
       



}

function dataTAbErp(dts){
    $('#modal_erp .table-erp').DataTable().destroy();
    $('#modal_erp .table-erp').DataTable({
        "searching":false,
        "paging":false,
        data:dts,
        columns: [
            {data: 'no',searchable:false, orderable:true},
            {data: 'documentno',searchable:false, orderable:true},
            {data: 'style',searchable:false, orderable:true},
            {data: 'article',searchable:false, orderable:true},
            {data: 'size',searchable:false, orderable:true},
            {data: 'item',searchable:false, orderable:true},
            {data: 'season',searchable:false, orderable:true},
            {data: 'supplier',searchable:false, orderable:true},
            {data: 'export_to',searchable:false, orderable:true}
        ]
    });
}
</script>
@endsection
