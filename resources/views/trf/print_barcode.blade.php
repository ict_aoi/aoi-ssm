<style type="text/css">
@page{
    margin : 10 10 10 10;
}

.img_barcode {
    display: block;
    padding: 0px;
    margin-right: 10px;
    margin-left: 10px;
    margin-top: 10px;
    margin-bottom: 10px;
}

.img_barcode > img {
    width: 375px;
    height:85px;
}

.tag{
    border-style: solid;
    padding: 5px;
   
}

</style>

<table class="tag">
    <tr>
        <td><center><label style="font-size: 15px; font-weight: bold;">SAMPLE STORAGE MANAGEMENT</label></center></td>
    </tr>
    <tr>
        <td><center><label style="font-size: 15px; font-weight: bold;">PT. APPAREL ONE INDONESIA</label></center></td>
    </tr>
    <tr>
        <td><center>
            <div class="barcode">
                <div class="img_barcode">
                    <img src="data:image/png;base64,{{ DNS1D::getBarcodePNG($data->id, 'C128',2,35) }}" alt="barcode"   />
                </div>
            </div>
        </center></td>
    </tr>
    <tr>
        <td><center><label style="font-size: 25px; font-weight: bold;">{{ $data->trf_id }}</label></center></td>
    </tr>
    <tr>
        <td><center><label style="font-size: 12px; font-weight:bold;">{{ $data->id }}</label></center></td>
    </tr>
</table>


