<style type="text/css">
@page{
    margin : 10 10 10 10;
}

.tab{
    /*font-family: sans-serif;*/
    font-size: 18px;
}
.tab tr  td{
    vertical-align: top;
    margin-bottom: 30px;
    word-wrap: break-word;
}

.img_barcode {
    display: block;
    padding: 0px;
    margin-right: 10px;
    margin-left: 10px;
    margin-top: 10px;
    margin-bottom: 10px;
}

.img_barcode > img {
    width: 200px;
    height:40px;
}

.tabdthead  tr  td{
    border: 1px solid black;
    vertical-align: top;
}

.tabdthead{
    width: 100%;
    border-collapse: collapse;
    word-wrap: break-word;
    page-break-inside: avoid;
    page-break-after: auto;
}

.tabdsampl  tr  td{
    border: 1px solid black;
    vertical-align: top;
    
}

.tabdsampl{
    width: 100%;
    border-collapse: collapse;
    word-wrap: break-word;
    page-break-inside: avoid;
    page-break-after: auto;
}

</style>

<table width="100%">
    <tr>
        <td>
            <center><img src="{{ public_path('assets/icon/aoi.png') }}" alt="Image"width="125"></center>
        </td>
        <td>
            <center><label style="font-size: 20px; font-weight: bold;">TEST REQUISITION FORM</label> </center>
        </td>
        <td>
            <center><img src="{{ public_path('assets/icon/bbi_logo.png') }}" alt="Image"width="135"></center>
        </td>
    </tr>
    <tr>
        <td></td>
        <td colspan="2">
            <center>#FORM NO.: {{ $data->trf_id}}</center>
        </td>
    </tr>
  
</table>
<table  class="tabdthead">
    <tr>
       
        <td colspan="4">
            @if($data->buyer=="ADIDAS")<img src="{{ public_path('assets/icon/check.png') }}" alt="Image"width="15" style="margin-left:50px;"> @else<img src="{{ public_path('assets/icon/boxs.png') }}" alt="Image"width="15" style="margin-left:50px;">@endif <label style="margin-left: 10px;">Adidas</label><br>

            @if($data->buyer!="ADIDAS")<img src="{{ public_path('assets/icon/check.png') }}" alt="Image"width="15" style="margin-left:50px;"> @else<img src="{{ public_path('assets/icon/boxs.png') }}" alt="Image"width="15" style="margin-left:50px;">@endif<label style="margin-left: 10px;">Other</label>
        </td>
    </tr>
    <tr>
        <td width="150"><label style="font-size:14px; font-weight:bold; margin-left:10px">Laboratory Location</label></td>
        <td colspan="3">
            @if($data->lab_location=="AOI1")<img src="{{ public_path('assets/icon/check.png') }}" alt="Image"width="15"> @else<img src="{{ public_path('assets/icon/boxs.png') }}" alt="Image"width="15">@endif <label style="font-size:14px; margin-left:10px">AOI 1</label><br>

            @if($data->lab_location=="AOI2")<img src="{{ public_path('assets/icon/check.png') }}" alt="Image"width="15"> @else<img src="{{ public_path('assets/icon/boxs.png') }}" alt="Image"width="15">@endif <label style="font-size:14px; margin-left:10px">AOI 2</label><br>
    </tr>

    <tr>
        <td width="150"><label style="font-size:14px; font-weight:bold; margin-left:10px">Company Name</label></td>
        <td colspan="3"><label style="font-size:14px; margin-left:10px">PT. Apparel One Indonesia</label></td>
    </tr>

    <tr>
        <td width="150"><label style="font-size:14px; font-weight:bold; margin-left:10px">Adress</label></td>
        <td colspan="3"><label style="font-size:14px; margin-left:10px">KAWASAN BERIKAT PT PUTRA WIJAYAKUSUMA SAKTI, KIW,
JL. RAYA SEMARANG KENDAL KM 12 B5 RANDUGARUT TUGU KOTA SEMARANG JAWA TENGAH</label></td>
    </tr>

    <tr>
        <td width="150"><label style="font-size:14px; font-weight:bold; margin-left:10px">Contact Person</label></td>
        <td colspan="3"><label style="font-size:14px; margin-left:10px">{{ $submit_by}}</label></td>
    </tr>

    <tr>
        <td width="150"><label style="font-size:14px; font-weight:bold; margin-left:10px">Telephone</label></td>
        <td><label style="font-size:14px; margin-left:10px">024 866 448</label></td>
        <td width="150"><label style="font-size:14px; font-weight:bold; margin-left:10px">Fax</label></td>
        <td><label style="font-size:14px; margin-left:10px">024 866 4483</label></td>
    </tr>

    <tr>
        <td width="150"><label style="font-size:14px; font-weight:bold; margin-left:10px">Category Specimen</label></td>
        <td colspan="3">

           @if($data->category_specimen=="FABRIC")<img src="{{ public_path('assets/icon/check.png') }}" alt="Image"width="15"> @else<img src="{{ public_path('assets/icon/boxs.png') }}" alt="Image"width="15">@endif <label style="font-size:14px; margin-left:10px">Fabric</label><br>

            @if($data->category_specimen=="ACCESORIES/TRIM")<img src="{{ public_path('assets/icon/check.png') }}" alt="Image"width="15"> @else<img src="{{ public_path('assets/icon/boxs.png') }}" alt="Image"width="15">@endif <label style="font-size:14px; margin-left:10px">Accessories/Trim</label><br>

            @if($data->category_specimen=="STRIKE OFF")<img src="{{ public_path('assets/icon/check.png') }}" alt="Image"width="15"> @else<img src="{{ public_path('assets/icon/boxs.png') }}" alt="Image"width="15">@endif <label style="font-size:14px; margin-left:10px">Strikeoff</label><br>

            @if($data->category_specimen=="GARMENT")<img src="{{ public_path('assets/icon/check.png') }}" alt="Image"width="15"> @else<img src="{{ public_path('assets/icon/boxs.png') }}" alt="Image"width="15">@endif <label style="font-size:14px; margin-left:10px">Garment</label><br>

            @if($data->category_specimen=="MOCKUP")<img src="{{ public_path('assets/icon/check.png') }}" alt="Image"width="15"> @else<img src="{{ public_path('assets/icon/boxs.png') }}" alt="Image"width="15">@endif <label style="font-size:14px; margin-left:10px">Mockup</label><br>
        </td>
    </tr>

    <tr>
        <td width="150"><label style="font-size:14px; font-weight:bold; margin-left:10px">Additional Information</label></td>
        <td colspan="3">
           @if($data->additional_information=="TOP")<img src="{{ public_path('assets/icon/check.png') }}" alt="Image"width="15"> @else<img src="{{ public_path('assets/icon/boxs.png') }}" alt="Image"width="15">@endif <label style="font-size:14px; margin-left:10px">Top</label><br>
           @if($data->additional_information=="BOTTOM")<img src="{{ public_path('assets/icon/check.png') }}" alt="Image"width="15"> @else<img src="{{ public_path('assets/icon/boxs.png') }}" alt="Image"width="15">@endif <label style="font-size:14px; margin-left:10px">Bottom</label><br>
           @if($data->additional_information=="SET")<img src="{{ public_path('assets/icon/check.png') }}" alt="Image"width="15"> @else<img src="{{ public_path('assets/icon/boxs.png') }}" alt="Image"width="15">@endif <label style="font-size:14px; margin-left:10px">Set (Top - Bottom)</label><br>
        </td>
    </tr>

</table>
<br>
<table class="tabdthead">
    <tr>
        <td width="25%">
            <label style="font-size:14px; margin-bottom:50px; margin-left:15px;">Develop testing</label><br>
            <img src="{{ public_path('assets/icon/boxs.png') }}" alt="Image"width="15"> <label style="font-size:14px; margin-bottom:50px; margin-left:30px;">T1</label><br>
            <img src="{{ public_path('assets/icon/boxs.png') }}" alt="Image"width="15"> <label style="font-size:14px; margin-bottom:50px; margin-left:30px;">T2</label><br>
            <img src="{{ public_path('assets/icon/boxs.png') }}" alt="Image"width="15"> <label style="font-size:14px; margin-bottom:50px; margin-left:30px;">Selective</label><br>

        </td>
        <td width="25%">
            <label style="font-size:14px; margin-bottom:50px; margin-left:15px;">1st Bulk testing</label><br>

            @if($data->test_required=="bulktesting_m")<img src="{{ public_path('assets/icon/check.png') }}" alt="Image"width="15"> @else<img src="{{ public_path('assets/icon/boxs.png') }}" alt="Image"width="15">@endif <label style="font-size:14px; margin-bottom:50px; margin-left:30px;">M (Model Level)</label><br>

            @if($data->test_required=="bulktesting_a")<img src="{{ public_path('assets/icon/check.png') }}" alt="Image"width="15"> @else<img src="{{ public_path('assets/icon/boxs.png') }}" alt="Image"width="15">@endif <label style="font-size:14px; margin-bottom:50px; margin-left:30px;">A (Article Level)</label><br>

            @if($data->test_required=="bulktesting_selective")<img src="{{ public_path('assets/icon/check.png') }}" alt="Image"width="15"> @else<img src="{{ public_path('assets/icon/boxs.png') }}" alt="Image"width="15">@endif <label style="font-size:14px; margin-bottom:50px; margin-left:30px;">Selective</label><br>
        </td>
        <td width="25%">
            <label style="font-size:14px; margin-bottom:50px; margin-left:15px;">Re-order testing</label><br>

            @if($data->test_required=="reordertesting_m")<img src="{{ public_path('assets/icon/check.png') }}" alt="Image"width="15"> @else<img src="{{ public_path('assets/icon/boxs.png') }}" alt="Image"width="15">@endif<label style="font-size:14px; margin-bottom:50px; margin-left:30px;">M (Model Level)</label><br>

            @if($data->test_required=="reordertesting_a")<img src="{{ public_path('assets/icon/check.png') }}" alt="Image"width="15"> @else<img src="{{ public_path('assets/icon/boxs.png') }}" alt="Image"width="15">@endif<label style="font-size:14px; margin-bottom:50px; margin-left:30px;">A (Article Level)</label><br>

            @if($data->test_required=="reordertesting_selective")<img src="{{ public_path('assets/icon/check.png') }}" alt="Image"width="15"> @else<img src="{{ public_path('assets/icon/boxs.png') }}" alt="Image"width="15">@endif <label style="font-size:14px; margin-bottom:50px; margin-left:30px;">Selective</label><br>
        </td>
        <td width="25%">
            <label style="font-size:14px; margin-bottom:50px; margin-left:15px;">Retest:</label><br>
            {{$data->test_required=="retest" ? $data->previous_trf_id : ''}}
        </td>
    </tr>
    <tr>
        <td colspan="4">
            <label style="font-size:14px; margin-bottom:50px; margin-left:15px;">Date information</label><br>

            @if($data->date_information_remark=="buy_ready")<img src="{{ public_path('assets/icon/check.png') }}" alt="Image"width="15"> @else<img src="{{ public_path('assets/icon/boxs.png') }}" alt="Image"width="15">@endif<label style="font-size:14px; margin-bottom:50px; margin-left:30px;">Buy ready (special for development testing)</label> {{ $data->date_information_remark=="buy_ready" ? date_format(date_create($data->date_information),'d-F-Y') : '' }}<br>

            @if($data->date_information_remark=="output_sewing")<img src="{{ public_path('assets/icon/check.png') }}" alt="Image"width="15"> @else<img src="{{ public_path('assets/icon/boxs.png') }}" alt="Image"width="15">@endif<label style="font-size:14px; margin-bottom:50px; margin-left:30px;">1st output sewing</label> {{ $data->date_information_remark=="output_sewing" ? date_format(date_create($data->date_information),'d-F-Y') : '' }}<br>

            @if($data->date_information_remark=="podd")<img src="{{ public_path('assets/icon/check.png') }}" alt="Image"width="15"> @else<img src="{{ public_path('assets/icon/boxs.png') }}" alt="Image"width="15">@endif<label style="font-size:14px; margin-bottom:50px; margin-left:30px;">PODD</label> {{ $data->date_information_remark=="podd" ? date_format(date_create($data->date_information),'d-F-Y') : '' }}<br>

        </td>
    </tr> 
    <tr>
        <td colspan="4">
            <label style="font-size:14px; margin-bottom:50px; margin-left:15px;">Category</label><br>

            @if($data->category=="FASHION/CASUAL")<img src="{{ public_path('assets/icon/check.png') }}" alt="Image"width="15"> @else<img src="{{ public_path('assets/icon/boxs.png') }}" alt="Image"width="15">@endif<label style="font-size:14px; margin-bottom:50px; margin-left:30px;">Fashion / Casual</label><br>

            @if($data->category=="HYBRID/PERFORMANCE")<img src="{{ public_path('assets/icon/check.png') }}" alt="Image"width="15"> @else<img src="{{ public_path('assets/icon/boxs.png') }}" alt="Image"width="15">@endif<label style="font-size:14px; margin-bottom:50px; margin-left:30px;">Hybrid / Performance</label><br>
        </td>
    </tr>
</table>
<br>

@php($i=1)
@foreach($docm as $key => $value)
<table class="tabdsampl">
        <tr>
            <td colspan="4">
                
                <label style="font-size:20px; margin-left:10px; font-weight: bold;">Sample {{$i++}}</label><br>
            </td>
        </tr>
        <tr >
            <td width="25%" height="50px">
                <label style="font-size:14px; margin-bottom:50px; margin-left:15px; font-weight: bold;">{{$value->document_type}}</label><br>
            </td>
            <td width="25%">
                {{$value->document_no}}
            </td>
            <td width="25%">
                 <label style="font-size:14px; margin-bottom:50px; margin-left:15px; font-weight: bold;">Fiber composition</label><br>
            </td>
            <td width="25%" rowspan="5">
                <label style="font-size:14px; margin-bottom:50px; margin-left:15px; font-weight: bold;">Care instruction</label><br>
            </td>
        </tr>

        <tr>
            <td width="25%" height="50px">
                <label style="font-size:14px; margin-bottom:50px; margin-left:15px; font-weight: bold;">Size</label><br>
            </td>
            <td width="25%">
                {{$value->size}}
            </td>
            <td width="25%">
                 <label style="font-size:14px; margin-bottom:50px; margin-left:15px; font-weight: bold;">PLM no.</label><br>
            </td>
           
           
        </tr>

        <tr>
            <td width="25%" height="50px">
                <label style="font-size:14px; margin-bottom:50px; margin-left:15px; font-weight: bold;">Style</label><br>
            </td>
            <td width="25%">
                {{$value->style}}
            </td>
            <td width="25%">
                 <label style="font-size:14px; margin-bottom:50px; margin-left:15px; font-weight: bold;">Manufacturer’s name</label><br>
            </td>
           
           
        </tr>

        <tr>
            <td width="25%" height="50px">
                <label style="font-size:14px; margin-bottom:50px; margin-left:15px; font-weight: bold;">Article</label><br>
            </td>
            <td width="25%">
                {{$value->article_no}}
            </td>
            <td width="25%">
                 <label style="font-size:14px; margin-bottom:50px; margin-left:15px; font-weight: bold;">Fabric weight</label><br>
            </td>
            
           
        </tr>

        <tr>
            <td width="25%" height="50px">
                <label style="font-size:14px; margin-bottom:50px; margin-left:15px; font-weight: bold;">Colour</label><br>
            </td>
            <td width="25%">
                {{$value->color}}
            </td>
            <td width="25%">
             
            </td>
            
           
        </tr>
</table>

@endforeach
<br>
<table class="tabdthead">
    @php($mtd = array_column($method,'master_method_id','master_method_id'))
    <tr>
        <td>
           <label style="font-size:14px; margin-bottom:50px; margin-left:15px; font-weight: bold;"> Testing methode</label>
        </td>
    </tr>
    <tr>
        <td>
            <label style="font-size:14px; margin-bottom:50px; margin-left:15px; font-weight: bold;"> PHYSICAL TESTS</label>
        </td>
    </tr>
    <tr>
        <td>
            

            @if(isset($mtd['16a80f10-67c5-11ec-b6e4-d97c2a73fcef']))<img src="{{ public_path('assets/icon/check.png') }}" alt="Image"width="15"> @else<img src="{{ public_path('assets/icon/boxs.png') }}" alt="Image"width="15">@endif<label style="font-size:12px; margin-left:5px;">PHX-AP0701 Dimensional Change/Spirality/Appearance Change After Laundering (Follow Care Label By Line Dry / Tumble Dry)</label><br>

            @if(isset($mtd['16ad7c20-67c5-11ec-bd79-f12a0676a040']))<img src="{{ public_path('assets/icon/check.png') }}" alt="Image"width="15"> @else<img src="{{ public_path('assets/icon/boxs.png') }}" alt="Image"width="15">@endif<label style="font-size:12px; margin-left:5px;">PHM-AP0405 Heat Shrinkage</label><br>

            @if(isset($mtd['16bf6750-67c5-11ec-ae0c-f70022b07158']))<img src="{{ public_path('assets/icon/check.png') }}" alt="Image"width="15"> @else<img src="{{ public_path('assets/icon/boxs.png') }}" alt="Image"width="15">@endif<label style="font-size:12px; margin-left:5px;">PHM-AP0407 Random Tumble Pilling </label><br>

            @if(isset($mtd['PHX-AP0413']))<img src="{{ public_path('assets/icon/check.png') }}" alt="Image"width="15"> @else<img src="{{ public_path('assets/icon/boxs.png') }}" alt="Image"width="15">@endif<label style="font-size:12px; margin-left:5px;">#PHX-AP0413 Seam Slippage# </label><br>

            @if(isset($mtd['PHX-AP0305']))<img src="{{ public_path('assets/icon/check.png') }}" alt="Image"width="15"> @else<img src="{{ public_path('assets/icon/boxs.png') }}" alt="Image"width="15">@endif<label style="font-size:12px; margin-left:5px;">#PHX-AP0305 T-Strength Test# </label><br>

            @if(isset($mtd['16d5f1d0-67c5-11ec-9fc0-c566b83a9cdb']))<img src="{{ public_path('assets/icon/check.png') }}" alt="Image"width="15"> @else<img src="{{ public_path('assets/icon/boxs.png') }}" alt="Image"width="15">@endif<label style="font-size:12px; margin-left:5px;">PHM-AP0441 Snagging Test (Snag Pad)</label><br>

            @if(isset($mtd['PHP-AP0450 ']))<img src="{{ public_path('assets/icon/check.png') }}" alt="Image"width="15"> @else<img src="{{ public_path('assets/icon/boxs.png') }}" alt="Image"width="15">@endif<label style="font-size:12px; margin-left:5px;">#PHP-AP0450 Seam Breakage#</label><br>

            @if(isset($mtd['16b2e350-67c5-11ec-825d-21e9243a1386']))<img src="{{ public_path('assets/icon/check.png') }}" alt="Image"width="15"> @else<img src="{{ public_path('assets/icon/boxs.png') }}" alt="Image"width="15">@endif<label style="font-size:12px; margin-left:5px;">PHX-AP0451 Odour</label><br>

            @if(isset($mtd['16b90980-67c5-11ec-8615-4918f4d814b8']))<img src="{{ public_path('assets/icon/check.png') }}" alt="Image"width="15"> @else<img src="{{ public_path('assets/icon/boxs.png') }}" alt="Image"width="15">@endif<label style="font-size:12px; margin-left:5px;">PHM-AP0453 pH Value</label><br>

            @if(isset($mtd['16c093a0-67c5-11ec-a000-43aa4932ea18']))<img src="{{ public_path('assets/icon/check.png') }}" alt="Image"width="15"> @else<img src="{{ public_path('assets/icon/boxs.png') }}" alt="Image"width="15">@endif<label style="font-size:12px; margin-left:5px;">PHX-AP0309 Bonding Material (Seam/Tape) Washing Durability Test</label><br>

            @if(isset($mtd['16b2e350-67c5-11ec-825d-21e9243a1386']))<img src="{{ public_path('assets/icon/check.png') }}" alt="Image"width="15"> @else<img src="{{ public_path('assets/icon/boxs.png') }}" alt="Image"width="15">@endif<label style="font-size:12px; margin-left:5px;">PHX-AP0437 Pull Test for Buttons/Teicord ends</label>
        </td>
    </tr>
    <tr>
        <td>
            <label style="font-size:14px; margin-bottom:50px; margin-left:15px; font-weight: bold;"> COLOR FASTNESS TESTS</label>
        </td>
    </tr>
    <tr>
        <td>
            @if(isset($mtd['16b1c150-67c5-11ec-a820-9d92d44c754d']))<img src="{{ public_path('assets/icon/check.png') }}" alt="Image"width="15"> @else<img src="{{ public_path('assets/icon/boxs.png') }}" alt="Image"width="15">@endif<label style="font-size:12px; margin-left:5px;">PHM-AP0501 Washing Fastness</label><br>
            @if(isset($mtd['16b81c60-67c5-11ec-85ec-9797ead5676a']))<img src="{{ public_path('assets/icon/check.png') }}" alt="Image"width="15"> @else<img src="{{ public_path('assets/icon/boxs.png') }}" alt="Image"width="15">@endif<label style="font-size:12px; margin-left:5px;">PHM-AP0502 Perspiration Fastness</label><br>
            @if(isset($mtd['16b090d0-67c5-11ec-a3b7-6d09d363fea1']))<img src="{{ public_path('assets/icon/check.png') }}" alt="Image"width="15"> @else<img src="{{ public_path('assets/icon/boxs.png') }}" alt="Image"width="15">@endif<label style="font-size:12px; margin-left:5px;">PHM-AP0503 Water Fastness</label><br>
            @if(isset($mtd['16ae79e0-67c5-11ec-a94c-33a089145e1a']))<img src="{{ public_path('assets/icon/check.png') }}" alt="Image"width="15"> @else<img src="{{ public_path('assets/icon/boxs.png') }}" alt="Image"width="15">@endif<label style="font-size:12px; margin-left:5px;">PHM-AP0504 Crocking Fastness</label><br>
            @if(isset($mtd['16b41090-67c5-11ec-9006-273656cd670c']))<img src="{{ public_path('assets/icon/check.png') }}" alt="Image"width="15"> @else<img src="{{ public_path('assets/icon/boxs.png') }}" alt="Image"width="15">@endif<label style="font-size:12px; margin-left:5px;">PHM-AP0510 Phenolic Yellowing</label><br>
            @if(isset($mtd['16ed60c0-67c5-11ec-9703-8da03bad15b3']))<img src="{{ public_path('assets/icon/check.png') }}" alt="Image"width="15"> @else<img src="{{ public_path('assets/icon/boxs.png') }}" alt="Image"width="15">@endif<label style="font-size:12px; margin-left:5px;">PHM-AP0512 Dry Cleaning Fastness</label><br>
            @if(isset($mtd['16ab3d80-67c5-11ec-a33b-77926e5b9753']))<img src="{{ public_path('assets/icon/check.png') }}" alt="Image"width="15"> @else<img src="{{ public_path('assets/icon/boxs.png') }}" alt="Image"width="15">@endif<label style="font-size:12px; margin-left:5px;">PHX-AP0514 Colour Migration-Oven Tests</label><br>
            @if(isset($mtd['16af7610-67c5-11ec-9f6e-53d6ae729a3b']))<img src="{{ public_path('assets/icon/check.png') }}" alt="Image"width="15"> @else<img src="{{ public_path('assets/icon/boxs.png') }}" alt="Image"width="15">@endif<label style="font-size:12px; margin-left:5px;">PHX-AP0515 Color Migration Fastness</label><br>
            @if(isset($mtd['16b9efe0-67c5-11ec-a371-a11a0082cca1']))<img src="{{ public_path('assets/icon/check.png') }}" alt="Image"width="15"> @else<img src="{{ public_path('assets/icon/boxs.png') }}" alt="Image"width="15">@endif<label style="font-size:12px; margin-left:5px;">PHX-AP0519 Saliva Fastness</label>
        </td>
    </tr>
    <tr>
        <td>
            <label style="font-size:14px; margin-bottom:50px; margin-left:15px; font-weight: bold;"> FUNCTIONAL TESTS</label>
        </td>
    </tr>
    <tr>
        <td>
            @if(isset($mtd['16b629f0-67c5-11ec-adb8-f7a047ff0ccc']))<img src="{{ public_path('assets/icon/check.png') }}" alt="Image"width="15"> @else<img src="{{ public_path('assets/icon/boxs.png') }}" alt="Image"width="15">@endif<label style="font-size:12px; margin-left:5px;">PHM-AP0604 Water Absorbency (Drop Test)</label>
        </td>
    </tr>
    <tr>
        <td>
            <label style="font-size:14px; margin-bottom:50px; margin-left:15px; font-weight: bold;"> Other Test / Special Request : </label>
        </td>
    </tr>
    <tr>
        <td>
            <label style="font-size:14px; margin-bottom:50px; margin-left:15px; font-weight: bold;"> DOWN TESTS </label>
        </td>
    </tr>
    <tr>
        <td>
            @if(isset($mtd['16b72a80-67c5-11ec-ae96-93c2086bf028']))<img src="{{ public_path('assets/icon/check.png') }}" alt="Image"width="15"> @else<img src="{{ public_path('assets/icon/boxs.png') }}" alt="Image"width="15">@endif<label style="font-size:12px; margin-left:5px;">PHP-AP0703 Moisture Content Test</label>
        </td>
    </tr>
    <tr>
        <td height="75px">
            <label style="font-size:14px; margin-bottom:50px; margin-left:15px; font-weight: bold;">Other Test / Special Request : </label>
        </td>
    </tr>
    <tr>
        <td>
            <label style="font-size:14px; margin-bottom:50px; margin-left:15px; font-weight: bold;">Other Test / Special Request : </label><br>

            @if($data->return_test_sample==true)<img src="{{ public_path('assets/icon/check.png') }}" alt="Image"width="15"> @else<img src="{{ public_path('assets/icon/boxs.png') }}" alt="Image"width="15">@endif<label style="font-size:12px; margin-left:5px;">Yes</label><br>
            @if($data->return_test_sample==false)<img src="{{ public_path('assets/icon/check.png') }}" alt="Image"width="15"> @else<img src="{{ public_path('assets/icon/boxs.png') }}" alt="Image"width="15">@endif<label style="font-size:12px; margin-left:5px;">No</label>
        </td>
    </tr>
    <tr>
        <td height="75px">
            <label style="font-size:14px;  margin-left:15px;">Remarks: Reporting maximum of 4 days</label><br>

            <label style="font-size:14px; margin-left:15px;">{{$remark}}</label>
        </td>
    </tr>
    <tr>
        <td>
            <label style="font-size:14px; margin-left:15px;">Date: (date of validation by lab technician)</label>
        </td>
    </tr>
</table>
<br>
