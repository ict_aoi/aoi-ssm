<style type="text/css">
@page{
    margin : 10 10 10 10;
}

.tab{
    /*font-family: sans-serif;*/
    font-size: 18px;
}
.tab tr  td{
    vertical-align: top;
    margin-bottom: 30px;
}

.img_barcode {
    display: block;
    padding: 0px;
    margin-right: 10px;
    margin-left: 10px;
    margin-top: 10px;
    margin-bottom: 10px;
}

.img_barcode > img {
    width: 200px;
    height:40px;
}

.spc{
    border-collapse: collapse;
    width: 100%;
}
.spc tr td{
    border: 1px solid black;
    padding-left: 10px;
}
.spc td{
   height: 10px;
}
</style>


<table width="100%">
    <tr>
        <td rowspan="2">
            <center><img src="{{ public_path('assets/icon/aoi.png') }}" alt="Image"width="125"></center>
        </td>
        <td>
            <center><label style="font-size: 20px; font-weight: bold;">APPAREL MATERIAL / GARMENT</label> </center>
        </td>
        <td rowspan="2">
            <center><img src="{{ public_path('assets/icon/bbi_logo.png') }}" alt="Image"width="135"></center>
        </td>
    </tr>
    <tr>
        <td>
            <center><label style="font-size: 20px; font-weight: bold;">TEST REQUISITION FORM</label></center>
        </td>
    </tr>
</table>
<br>
<table width="100%" class="tab">
    <!-- <tr>
        <td colspan="3">
            <div class="img_barcode">
                <img src="data:image/png;base64,{{ DNS1D::getBarcodePNG($data->trf_id, 'C128',2,35) }}" alt="barcode"   />
            </div>
        </td>
    </tr> -->

    <tr>
        <td width="130">No. TRF</td>
        <td width="20">:</td>
        <td>{{$data->trf_id}}</td>
        <td rowspan="4">
            <img style="float: right; padding-right: 20px;" src="data:image/png;base64, {!! $qrcode !!}">
        </td>
    </tr>
    
    <tr>
        <td width="130">Submitted by</td>
        <td width="20">:</td>
        <td colspan="2">{{$submit_by}}</td>
    </tr>

    <tr>
        <td width="130">Date of Submitted</td>
        <td width="20">:</td>
        <td colspan="2">{{date_format(date_create($data->created_at),'d-M-Y H:i:s')}}</td>
    </tr>
    <tr>
        <td width="130">Factory</td>
        <td width="20">:</td>
        <td colspan="2">{{$data->factory_name}}</td>
    </tr>

    <tr>
        <td colspan="4" style="height: 20px;"></td>
    </tr>

    <tr>
        <td width="130">Buyer</td>
        <td width="20">:</td>
        <td colspan="2">{{ $data->buyer}}</td>
    </tr>
    <tr>
        <td width="130">Lab Location</td>
        <td width="20">:</td>
        <td colspan="2">{{ $data->lab_location}}</td>
    </tr>
    <tr>
        <td width="130">Origin of speciment</td>
        <td width="20">:</td>
        <td colspan="2">{{ $data->asal_specimen}}</td>
    </tr>
    <tr>
        <td width="130">Category</td>
        <td width="20">:</td>
        <td>{{ $data->category}}</td>
    </tr>
    <tr>
        <td width="130">Category Speciment</td>
        <td width="20">:</td>
        <td colspan="2">{{ $data->category_specimen}}</td>
    </tr>
    <tr>
        <td width="130">Type of Speciment</td>
        <td width="20">:</td>
        <td colspan="2">{{ $data->type_specimen}}</td>
    </tr>

    

    <tr>
        <td width="130">Test Required</td>
        <td width="20">:</td>
        <td>{{ $test_required}}</td>
    </tr>

    <tr>
        <td width="130">Date Information</td>
        <td width="20">:</td>
        <td colspan="2">{{ explode("<br>",$dateinfo)[0] }} <br>{{ date_format(date_create(explode("<br>",$dateinfo)[1]),'d-M-Y') }}</td>
    </tr>

    <tr>
        <td colspan="4" style="height: 20px;"></td>
    </tr>

    

     <tr>
        <td colspan="4" style="height: 20px;"></td>
    </tr>

    <tr>
        <td width="130">Return Test</td>
        <td width="20">:</td>
        <td colspan="2">{{ isset($data->return_test_sample)==true ? "YES" : "NO"}}</td>
    </tr>
    <tr>
        <td width="130">Part of speciment tested</td>
        <td width="20">:</td>
        <td colspan="2">{{ $data->part_of_specimen}}</td>
    </tr>
    <tr>
        <td width="130">Testing methode</td>
        <td width="20">:</td>
        <td colspan="2">
            @foreach($method as $mtd)
                <label>+ {{$mtd->method_code}} / {{$mtd->method_name}}</label><br>
            @endforeach
        </td>
    </tr>

    <tr>
        <td colspan="4" style="height: 20px;"></td>
    </tr>
    <tr>
        <td colspan="4">
            Remarks: Reporting maximum of 4 days
        </td>
    </tr>
    <tr>
        <td width="130">Status</td>
        <td width="20">:</td>
        <td colspan="2">{{ $data->status}}</td>
    </tr>
    <tr>
        <td width="130">Date of validate</td>
        <td width="20">:</td>
        <td colspan="2">{{$labdate}}</td>
    </tr>
    <tr>
        <td width="130">Name of validate</td>
        <td width="20">:</td>
        <td colspan="2">{{ $labpic }}</td>
    </tr>


</table>
<br>

<table class="spc">
@php( $i = 1)
@foreach($docm as $dc)
    <tr>
        <td colspan="4">
            <h4>SPECIMEN {{$i}}</h4>
        </td>
    </tr>

    <tr >
        <td width="100">
            <b>{{$dc->document_type}}</b>
        </td>
        <td>
            {{$dc->document_no}}
        </td>

        <td width="100">
            <b>Season</b>
        </td>
        <td>
            {{$dc->season}}
        </td>

    </tr>

    <tr >
        <td>
            <b>Style</b>
        </td>
        <td>
            {{$dc->style}}
        </td>

        <td>
            <b>Article</b>
        </td>
        <td>
            {{$dc->article_no}} /  {{$dc->color}}
        </td>

    </tr>
    <tr >
        <td>
            <b>Size</b>
        </td>
        <td>
            {{$dc->size}} 
        </td>

        <td>
            <b>Fibre Composition</b>
        </td>
        <td>
            {{$dc->fibre_composition}}
        </td>
    </tr>

    <tr >
        <td>
            <b>Fabric Finish</b>
        </td>
        <td>
            {{$dc->fabric_finish}} 
        </td>

        <td>
            <b>Gauge</b>
        </td>
        <td>
            {{$dc->gauge}}
        </td>
    </tr>

    <tr >
        <td>
            <b>Fabric Weight</b>
        </td>
        <td>
            {{$dc->fabric_weight}} 
        </td>

        <td>
            <b>PLM No.</b>
        </td>
        <td>
            {{$dc->plm_no}}
        </td>
    </tr>

    <tr >
        <td>
            <b>Care Instruction</b>
        </td>
        <td>
            {{$dc->care_instruction}} 
        </td>

        <td>
            <b>Manufacture Name</b>
        </td>
        <td>
            {{$dc->manufacture_name}}
        </td>
    </tr>

    <tr >
        <td>
            <b>Export To</b>
        </td>
        <td>
            {{$dc->export_to}} 
        </td>

        <td>
            <b>Nomor Roll</b>
        </td>
        <td>
            {{$dc->nomor_roll}}
        </td>
    </tr>

    <tr >
        <td>
            <b>Batch Number</b>
        </td>
        <td>
            {{$dc->batch_number}} 
        </td>

        <td>
            <b>Item</b>
        </td>
        <td>
            {{$dc->item}}
        </td>
    </tr>

    <tr >
        <td>
            <b>PO Supplier</b>
        </td>
        <td>
            {{$dc->barcode_supplier}} 
        </td>

        <td>
            <b>Yds Roll</b>
        </td>
        <td>
            {{$dc->yds_roll}} 
        </td>

    </tr>

    <tr >
        <td>
            <b>Additional Information</b>
        </td>
        <td>
            {{$dc->additional_information}} 
        </td>

        <td>
            <b>Description</b>
        </td>
        <td>
            {{$dc->description}} 
        </td>

    </tr>
    {{$i++}}
@endforeach
</table>


