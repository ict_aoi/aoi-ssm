<style type="text/css">
@page{
    margin : 10 10 10 10;
}

#head{
    border-collapse: collapse;
    width: 100%;
    font-size: 12px;
}
#head  tr td{
    border: 1px solid black;
    padding-left: 5px;
   
}

footer {
    position: fixed; 
    bottom: 0px; 
    left: 0px; 
    right: 0px;
    height: 10px;

    /** Extra personal styles **/
    /*background-color: #03a9f4;
    color: white;
    text-align: center;
    line-height: 1.5cm;*/
}

.page-break {
    page-break-after: always;
}
</style>

@foreach($data as $dt)

<table id="head">
    <tr>
        <td rowspan="4">
            <center><img src="{{ public_path('assets/icon/bbi_logo.png') }}" alt="Image"width="135"></center>
        </td>
        <td colspan="4">
            <center><h3>REPORT TESTING SPECIMEN</h3></center>
        </td>
        <td rowspan="4">
             <center><img src="{{ public_path('assets/icon/aoi.png') }}" alt="Image"width="125"></center>
        </td>
    </tr>
    <tr>
        <td>
          
            <label style="font-size:12px; font-weight: bold;">TRF NO</label>
        </td>
        <td>
            <label style="font-size:12px; ">{{$dt['notrf']}}</label>
        </td>
        <td>
            
            <label style="font-size:12px; font-weight: bold;">ORIGIN</label>
        </td>
        <td>
            
            <label style="font-size:12px; ">{{$dt['asal_specimen']}} {{$dt['fact']}}</label>
        </td>
    </tr>

    <tr>
        <td>
          
            <label style="font-size:12px; font-weight: bold;">Buyer</label>
        </td>
        <td>
            <label style="font-size:12px; ">{{$dt['buyer']}}</label>
        </td>
        <td>
            
            <label style="font-size:12px; font-weight: bold;">{{$dt['document_type']}}</label>
        </td>
        <td>
            
            <label style="font-size:12px; ">{{$dt['document_no']}}</label>
        </td>
    </tr>

    <tr>
        <td>
          
            <label style="font-size:12px; font-weight: bold;">Lab Location</label>
        </td>
        <td>
            <label style="font-size:12px; ">{{$dt['lab_location']}}</label>
        </td>
        <td colspan="2">
            
        </td>
        
    </tr>

</table>
<br>
<table id="head">
    <tr>
        <td colspan="8">
            <label style="font-size: 14px; font-weight: bold;">Specimen Identity</label>
        </td>
    </tr>
    <tr>
        <td>
            <label>Category</label>
        </td>
        <td>
            <label>{{$dt['category']}}</label>
        </td>
        <td>
            <label>Category Specimen</label>
        </td>
        <td>
            <label>{{$dt['category_specimen']}}</label>
        </td>
        <td>
            <label>Type Specimen</label>
        </td>
        <td>
            <label>{{$dt['type_specimen']}}</label>
        </td>
        <td>
            <label>Additional Info</label>
        </td>
        <td>
            <label>{{$dt['additional_information']}}</label>
        </td>
    </tr>

    <tr>
        <td>
            <label>Season</label>
        </td>
        <td>
            <label>{{$dt['season']}}</label>
        </td>
        <td>
            <label>Style</label>
        </td>
        <td>
            <label>{{$dt['style']}}</label>
        </td>
        <td>
            <label>Article / Color</label>
        </td>
        <td>
            <label>{{$dt['article_no']}} {{$dt['color']}}</label>
        </td>
        <td>
            <label>Size</label>
        </td>
        <td>
            <label>{{$dt['size']}}</label>
        </td>
        
    </tr>

    <tr>
        <td>
            <label>Item</label>
        </td>
        <td>
            <label>{{$dt['item']}}</label>
        </td>
        <td>
            <label>Date Information</label>
        </td>
        <td>
            <label>{{ str_replace('<br>',' ',$dt['set_date_info'])}}</label>
        </td>
        <td>
            <label>Test Required</label>
        </td>
        <td>
            <label>{{$dt['set_testreq']}}</label>
        </td>
        <td>
            <label>Part Tested</label>
        </td>
        <td>
            <label>{{$dt['part_of_specimen']}}</label>
        </td>
        
    </tr>

    <tr>
        <td>
            <label>Return Test</label>
        </td>
        <td>
            @if($dt['return_test_sample']==true)
                <label>YES</label>
            @else
                <label>NO</label>
            @endif
        </td>
        <td>
            <label>Fibre Composition</label>
        </td>
        <td>
            <label>{{$dt['fibre_composition']}}</label>
        </td>
        <td>
            <label>Fabric Finish</label>
        </td>
        <td>
            <label>{{$dt['fabric_finish']}}</label>
        </td>
        <td>
            <label>Fabric Width</label>
        </td>
        <td>
            <label>{{$dt['fabric_weight']}}</label>
        </td>
        
    </tr>

    <tr>
        <td>
            <label>Gauge</label>
        </td>
        <td>
            <label>{{$dt['gauge']}}</label>
        </td>
        <td>
            <label>PLM No.</label>
        </td>
        <td>
            <label>{{$dt['plm_no']}}</label>
        </td>
        <td>
            <label>Care Instruction</label>
        </td>
        <td>
            <label>{{$dt['care_instruction']}}</label>
        </td>
        <td>
            <label>Manufacture Name</label>
        </td>
        <td>
            <label>{{$dt['manufacture_name']}}</label>
        </td>
        
    </tr>

    <tr>
        <td>
            <label>Export To</label>
        </td>
        <td>
            <label>{{$dt['export_to']}}</label>
        </td>
        <td>
            <label>Batch Number</label>
        </td>
        <td>
            <label>{{$dt['batch_number']}}</label>
        </td>
        <td>
            <label>Nomor Roll</label>
        </td>
        <td>
            <label>{{$dt['nomor_roll']}}</label>
        </td>
        <td>
            <label>PO Supplier</label>
        </td>
        <td>
            <label>{{$dt['barcode_supplier']}}</label>
        </td>
        
    </tr>
    <tr>
        <td>
            <label>Yard Roll</label>
        </td>
        <td>
            <label>{{$dt['yds_roll']}}</label>
        </td>
        <td>
            <label>Description</label>
        </td>
        <td colspan="5">
            <label>{{$dt['description']}}</label>
        </td>
    </tr>
</table>
<br>
@if(isset($dt['result_test']))
<table id="head">
    <tr>
        <td colspan="8">
             <label style="font-size: 14px; font-weight: bold;">Specimen Result</label>
        </td>
    </tr>
    <tr>
        <td>No.</td>
        <td>Methode Code</td>
        <th>Methode</th>
        <td>Requirement</td>
        <td>Supplier Result</td>
        <td>Before Test</td>
        <td>Result Value</td>
        <td>Result Test</td>
    </tr>
  
    @foreach($dt['result_test'] as $rt)

        <tr>
            <td>
                <label>{{$rt['no']}}</label>
            </td>
            <td>
                <label>{{$rt['method_code']}}</label>
            </td>
            <td>
                <label>{{$rt['method_name']}}</label>
            </td>
            <td>
                 <label>{{$rt['requirment']}}</label>
            </td>
            <td>
                 <label>{{$rt['supplier_result']}}</label>
            </td>
            <td>
                 <label>{{$rt['before_test']}}</label>
            </td>
            <td>
                 <label>{{$rt['result']}}</label>
            </td>
            <td>
                 <label>{{$rt['set_result']}}</label>
            </td>
        </tr>
    @endforeach
</table>
<br>
<table id="head">
    <tr>
        <td style="text-align:right;" colspan="2">
            <label style="font-weight: bold; padding-right: 10px;">Semarang, {{date_format(date_create($dt['updated_at']),'d F Y')}}</label>
        </td>
    </tr>
    <tr>
        <td>
            <center><label style="font-size:16px; font-weight:bold;" >Technician</label></center>
        </td>
        <td>
            <center><label style="font-size:16px; font-weight:bold;" >Lab Sub. Dept. Head</label></center>
        </td>
    </tr>
    <tr>
        <td height="50px">
            
        </td>
        <td height="50px">
           
        </td>
    </tr>
    <tr>
        <td>
            <center><label style="font-size:16px; font-weight:bold;" >{{isset($dt['test_pic']) ? $dt['test_pic'] : ""}}</label></center>
        </td>
        <td>
            <center><label style="font-size:16px; font-weight:bold;" >DEVI ANASTASYA MANTOUW</label></center>
        </td>
    </tr>
</table>
<br>

@endif
<img src="data:image/png;base64, {!! $dt['qrcode'] !!}" style="margin-top:10px">

<footer>
    <table width="100%">
        <tr>
            <td style="text-align:left;">
               <label style="font-size:8px;">{{$dt['trfid']}}</label> 
            </td>
            <td style="text-align:right;">
                <label style="font-size:8px;">Print Date : </label>
                <label style="font-size:8px;"> {{date('d/M/Y')}}</label>
            </td>
        </tr>
    </table>
    
    
</footer>
<div class="page-break"></div>
@if(isset($dt['result_test']))
<table id="head">
    @foreach($dt['result_test'] as $im)
        @if(file_exists($im['imgdir']))
        <tr>
            <td>
                <h5>{{$im['method_code']}} {{$im['method_name']}}</h5>
            </td>
        </tr>
        <tr>
            <td>
                <center>
                    <!-- <img src="{{ public_path($im['imgdir']) }}" border="0" width="500" class="img-rounded" align="center"> -->
                    <img src="{{ public_path($im['imgdir']) }}" style="margin : 5px; width: 500px;" class="img-rounded" align="center">
                </center>
            </td>
        </tr>
        @endif
    @endforeach
</table>
@endif
@endforeach


