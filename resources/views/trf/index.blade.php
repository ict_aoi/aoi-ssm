@extends('layouts.app', ['active' => 'trf'])
@section('header')
<div class="page-header page-header-default">
	<div class="breadcrumb-line">
		<ul class="breadcrumb">
			<li><a href="#"><i class="icon-home2 position-left"></i> TRF</a></li>
			<li class="active">List Trf</li>
		</ul>
	</div>
</div>
@endsection

@section('content')
<div class="content">
	<div class="panel panel-flat">
		<div class="panel-body">
			<!-- 
			<div class="row">
				<a href="{{ route('trf.createTrf') }}" class="btn btn-default" id="btn-add"><span class="icon-plus2"></span> Create Trf</a>
			
			</div> -->
			<div class="row">
				<form action="{{ route('trf.ajaxGetData') }}" id="form-search">
					@csrf
					<label><b>Filter TRF</b></label>
					<div class="input-group">
						<input type="text" class="form-control" name="key" id="key" placeholder="TRF Number / Buyer / Lab Location / Status / User">
						<div class="input-group-btn">
							<button type="submit" class="btn btn-primary">Filter</button>
						</div>
					</div>
				</form>
			</div>
			<div class="row table-responsive">
				<table class ="table table-basic table-condensed" id="table-list" width="100%">
					<thead>
						<tr>
							<th>#</th>
							<th>Trf. No.</th>
							<th>Date Submit</th>
							<th>Testing Required</th>
							<th>Date Infromation</th>
							<th>Buyer</th>
							<th>Lab Loc.</th>
							<th>User</th>
							<th>Catgory</th>
							<th>Ctg. Spesimen</th>
							<th>Testing Methods</th>
							<th>Status</th>
							<th>Action</th>
						</tr>
					</thead>
				</table>
			</div>
		</div>
	</div>
</div>


@endsection



@section('js')
<script type="text/javascript" src="{{url('assets/js/plugins/tables/datatables/extensions/buttons.min.js')}}"></script>
<script type="text/javascript">
$(document).ready(function(){
	


	$.extend( $.fn.dataTable.defaults, {
        stateSave: true,
        autoWidth: false,
        autoLength: false,
        processing: true,
        serverSide: true,
        dom: '<"datatable-header"fBl><t><"datatable-footer"ip>',
        language: {
            search: '<span>Filter:</span> _INPUT_',
            searchPlaceholder: 'Type to filter...',
            lengthMenu: '<span>Show:</span> _MENU_',
            paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
        }
    }); 

  
    var table = $('#table-list').DataTable({
    	buttons: [
	        {
	            text: 'Create TRF',
	            className: 'btn btn-sm bg-info',
	            action: function (e, dt, node, config)
	            {
	                window.location.href = "{{ route('trf.createTrf')}}";
	            }
	        }
	   	],
        ajax: {
	        type: 'GET',
	        url: $('#form-search').attr('action'),
	        data: function (d) {
	            return $.extend({},d,{
	                "key":$('#key').val()
	            });
	        }
	    },
        fnCreatedRow: function (row, data, index) {
            var info = table.page.info();
            var value = index+1+info.start;
            $('td', row).eq(0).html(value);
            $('td', row).eq(1).css('min-width','250px');
            $('td', row).eq(2).css('min-width','150px');
            $('td', row).eq(10).css('min-width','200px');
        },
        columnDefs: [
            {
                className: 'dt-center'
            }
        ],
        columns: [
	        {data: null, sortable: false, orderable: false, searchable: false},
	        {data: 'trf_id', name: 'trf_id'},
	        {data: 'created_at', name: 'created_at'},
	        {data: 'test_required', name: 'test_required'},
	       	{data: 'date_information', name: 'date_information'},
	        {data: 'buyer', name: 'buyer'},
	        {data: 'lab_location', name: 'lab_location'},
	        {data: 'nik', name: 'nik'},
	        {data: 'category', name: 'category'},
	        {data: 'category_specimen', name: 'category_specimen'},
	        {data: 'testing_method_id', name: 'testing_method_id'},
	        {data: 'status', name: 'status'},
	        {data: 'action', name: 'action'}
	    ]
    });

	
    $('#form-search').submit(function(event){
    	event.preventDefault();

    	table.clear();
    	table.draw();
    });


	table.on('preDraw', function() {
		loading();
	    Pace.start();
	})
	.on('draw.dt', function() {
	    $.unblockUI();
	    Pace.stop();
	});

	
 
	
});






</script>
@endsection